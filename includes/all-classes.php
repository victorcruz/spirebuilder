<?php
$plugin_dir = SpireBuilder::$plugin_dir;

include($plugin_dir . '/classes/central.class.php');
include($plugin_dir . '/classes/handlebars_helpers.class.php');
include($plugin_dir . '/classes/miscellaneous.class.php');
include($plugin_dir . '/classes/settings.class.php');
include($plugin_dir . '/classes/validator.class.php');
include($plugin_dir . '/classes/wp_posts.class.php');

//Handlebars
include($plugin_dir . '/includes/Handlebars/Autoloader.php');

