<?php
/**
 * Include to show the Requirements page assets.
 *
 * @package admin-panel
 * @since   0.1
 *
 */
/*
 * Load needed styles.
 */
wp_enqueue_style('spirebuilder-codefield-font', self::$theme_url . 'admin/fonts/CodeField/style.css');
wp_enqueue_style('spirebuilder-fontawesome', self::$theme_url . 'admin/fonts/FontAwesome/font-awesome.min.css');
wp_enqueue_style('spirebuilder-fontawesomemore-corp', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-corp.css');
wp_enqueue_style('spirebuilder-fontawesomemore-ext', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-ext.css');
wp_enqueue_style('spirebuilder-fontawesomemore-social', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-social.css');

wp_enqueue_style('spirebuilder-global', self::$theme_url . 'admin/css/global.min.css');

wp_enqueue_style('spirebuilder-settings', self::$theme_url . 'admin/css/settings.min.css');
wp_enqueue_style('spirebuilder-requirements', self::$theme_url . 'admin/css/requirements.min.css');