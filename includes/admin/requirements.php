<?php
/**
 * Include to show the template for the Requirements menu item.
 *
 * @package admin-panel
 * @since   0.1
 *
 */

// WordPress version
global $wp_version;

$options_requirements = SpireBuilder_Settings::get_requirements();

$requirements = array();

if (version_compare($wp_version, $options_requirements['wp_version'], "<")) {
	$requirements[] = array('message' => __('This plugin require WordPress ' . $options_requirements['wp_version'] . ' or newer', SpireBuilder::$i18n_prefix) . '. <a target="_blank" href="http://codex.wordpress.org/Upgrading_WordPress">' . __('Please update', SpireBuilder::$i18n_prefix) . '</a>', 'type' => 'error');
} else {
	$requirements[] = array('message' => __('You already have a valid WordPress version:', SpireBuilder::$i18n_prefix) . ' ' . $wp_version, 'type' => 'success');
}

// PHP version
$phpversion = phpversion();
if (version_compare($phpversion, $options_requirements['php_version_min'], "<")) {
	$requirements[] = array('message' => __('This plugin require PHP ' . $options_requirements['php_version_min'] . ' or newer. We suggest you request this upgrade from your hosting provider. Your current version is:', SpireBuilder::$i18n_prefix) . ' ' . $phpversion, 'type' => 'error');
} else {
	$requirements[] = array('message' => __('You already have a valid PHP version:', SpireBuilder::$i18n_prefix) . ' ' . $phpversion, 'type' => 'success');
}

// Include HTML template
include(SpireBuilder::$theme_dir . 'admin/templates/requirements.php');