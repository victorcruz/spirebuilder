<?php
/**
 * Include to show the Builder widget.
 *
 * @package admin-panel
 * @since   0.1
 *
 */

// Set Settings
$global_settings = SpireBuilder_Settings::get_settings();
$settings        = SpireBuilder_WPPosts::get_spire_builder_settings_post($post_id);

$global_settings['edit_modal_exclusions'] = implode(",", $global_settings['edit_modal_exclusions']);

if (!isset($settings['allow_spire_builder_editor'])) {
	$settings['allow_spire_builder_editor'] = $global_settings['allow_spire_builder_editor'];
}
if (!isset($settings['content_placement'])) {
	$settings['content_placement'] = $global_settings['content_placement'];
}

$toolbar_layout = $global_settings['toolbar_layout'];

// Get all categories active.
$all_categories        = SpireBuilder_Central::get_all_category_with_widgets('active');
if (isset($global_settings['available_widgets_all']) && $global_settings['available_widgets_all'] == 1) {
	$all_categories = SpireBuilder_Central::get_all_category_with_widgets('active_and_disable');
}

// Sort by categories title
sort($all_categories);
$all_categories_sort = array();
foreach ($all_categories as $category) {
	sort($category['widgets']);
	$all_categories_sort[] = $category;
}

$all_categories = $all_categories_sort;

$available_widgets_array = array();
foreach ($all_categories as $category) {
	foreach ($category['widgets'] as $widget) {
		$available_widgets_array[] = $widget['id'];
	}
}
$available_widgets = implode(",", $available_widgets_array);

// Get LOM from WP_Post
$settings['LOM'] = SpireBuilder_WPPosts::get_spirebuilder_LOM($post_id);

// Get template saved
$options   = SpireBuilder_Settings::get_options();
$templates = (isset($options['templates'])) ? $options['templates'] : __return_empty_array();
sort($templates);

$toolbar_layout = '';
switch ($global_settings['toolbar_layout']):
	case 'text_below_icons' :
		$toolbar_layout = 'spirebuilder-toolbar-text-below-icons';
		break;
	case 'text_after_icons' :
		$toolbar_layout = 'spirebuilder-toolbar-text-after-icons';
		break;
	case 'icons_only'       :
		$toolbar_layout = 'spirebuilder-toolbar-icons-only';
		break;
	case 'text_only'        :
		$toolbar_layout = 'spirebuilder-toolbar-text-only';
		break;
endswitch;

$toolbar_icon_size = '';
switch ($global_settings['toolbar_icon_size']):
	case 'mini'     :
		$toolbar_icon_size = 'icon-1x';
		break;
	case 'normal'   :
		$toolbar_icon_size = 'icon-large';
		break;
	case 'large'    :
		$toolbar_icon_size = 'icon-2x';
		break;
endswitch;

$widgets = array();
foreach ($all_categories as $category) {
	foreach ($category['widgets'] as $widget) {
		$widgets[] = $widget;
	}
}
sort($widgets);

// Include HTML template
include(SpireBuilder::$theme_dir . '/admin/templates/builder.php');