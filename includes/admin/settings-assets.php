<?php
/**
 * Include to show the Settings page assets.
 *
 * @package admin-panel
 * @since   0.1
 *
 */
/*
 * Load needed styles.
 */
wp_enqueue_style('jquery-ui-core', self::$theme_url . 'admin/scripts/jslib/ui/css/jquery.ui.core.css');
wp_enqueue_style('jquery-ui-tabs', self::$theme_url . 'admin/scripts/jslib/ui/css/jquery.ui.tabs.css');

wp_enqueue_style('jquery-iCheckbox', self::$theme_url . 'admin/scripts/jslib/iCheckbox/css/iCheckbox.css');
wp_enqueue_style('jquery-growl', self::$theme_url . 'admin/scripts/jslib/growl/jquery.growl.css');
wp_enqueue_style('jquery-chosen', self::$theme_url . 'admin/scripts/jslib/chosen/chosen.css');

wp_enqueue_style('spirebuilder-bootstrap', self::$theme_url . 'admin/scripts/jslib/bootstrap/css/bootstrap.css');

wp_enqueue_style('spirebuilder-codefield-font', self::$theme_url . 'admin/fonts/CodeField/style.css');
wp_enqueue_style('spirebuilder-fontawesome', self::$theme_url . 'admin/fonts/FontAwesome/font-awesome.min.css');
wp_enqueue_style('spirebuilder-fontawesomemore-corp', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-corp.css');
wp_enqueue_style('spirebuilder-fontawesomemore-ext', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-ext.css');
wp_enqueue_style('spirebuilder-fontawesomemore-social', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-social.css');

wp_enqueue_style('spirebuilder-global', self::$theme_url . 'admin/css/global.min.css');
wp_enqueue_style('spirebuilder-settings', self::$theme_url . 'admin/css/settings.min.css');

/*
 * Load needed script libraries.
 */
wp_enqueue_script('jquery');

wp_enqueue_script('jquery-effects-blind');
wp_enqueue_script('jquery-effects-highlight');

wp_enqueue_script('jquery-ui-core');
wp_enqueue_script('jquery-ui-widget');
wp_enqueue_script('jquery-ui-tabs');
wp_enqueue_script('jquery-ui-button');

wp_enqueue_script('jquery-cookie', self::$theme_url . 'admin/scripts/jslib/jquery.cookie.js');
wp_enqueue_script('jquery-form');
wp_enqueue_script('jquery-iCheckbox', self::$theme_url . 'admin/scripts/jslib/iCheckbox/jquery.iCheckbox.js');
wp_enqueue_script('jquery-growl', self::$theme_url . 'admin/scripts/jslib/growl/jquery.growl.js');
wp_enqueue_script('jquery-chosen', self::$theme_url . 'admin/scripts/jslib/chosen/chosen.jquery.min.js');
wp_enqueue_script('jquery-bootstrap', self::$theme_url . 'admin/scripts/jslib/bootstrap/js/bootstrap.js');

wp_enqueue_script('jquery-handlebars', self::$theme_url . 'admin/scripts/jslib/handlebars.js');

wp_enqueue_script('spirebuilder-codefield-utilities', self::$plugin_url . 'theme/admin/scripts/CodeField/Utils.min.js');

wp_enqueue_script('spirebuilder-global', self::$theme_url . 'admin/scripts/global.min.js');

wp_enqueue_script('spirebuilder-settings', self::$plugin_url . 'theme/admin/scripts/settings.min.js');