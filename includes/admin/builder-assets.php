<?php
/**
 * Include to show the Builder assets.
 *
 * @package admin-panel
 * @since   0.1
 *
 */
/*
 * Load needed styles.
 */
wp_enqueue_style('jquery-ui-core', self::$theme_url . 'admin/scripts/jslib/ui/css/jquery.ui.core.css');
wp_enqueue_style('jquery-ui-tabs', self::$theme_url . 'admin/scripts/jslib/ui/css/jquery.ui.tabs.css');

wp_enqueue_style('jquery-iCheckbox', self::$theme_url . 'admin/scripts/jslib/iCheckbox/css/iCheckbox.css');
wp_enqueue_style('jquery-growl', self::$theme_url . 'admin/scripts/jslib/growl/jquery.growl.css');
wp_enqueue_style('farbtastic');

wp_enqueue_style('spirebuilder-bootstrap', self::$theme_url . 'admin/scripts/jslib/bootstrap/css/bootstrap.css');

wp_enqueue_style('spirebuilder-codefield-font', self::$theme_url . 'admin/fonts/CodeField/style.css');
wp_enqueue_style('spirebuilder-fontawesome', self::$theme_url . 'admin/fonts/FontAwesome/font-awesome.min.css');
wp_enqueue_style('spirebuilder-fontawesomemore-corp', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-corp.css');
wp_enqueue_style('spirebuilder-fontawesomemore-ext', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-ext.css');
wp_enqueue_style('spirebuilder-fontawesomemore-social', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-social.css');

wp_enqueue_style('spirebuilder-global', self::$theme_url . 'admin/css/global.min.css');
wp_enqueue_style('spirebuilder-builder', self::$theme_url . 'admin/css/builder.min.css');

/*
 * Load needed script libraries.
 */
wp_enqueue_script('jquery');
wp_enqueue_script('farbtastic');

wp_enqueue_script('jquery-effects-blind');
wp_enqueue_script('jquery-effects-highlight');

wp_enqueue_script('jquery-ui-core');
wp_enqueue_script('jquery-ui-mouse');
wp_enqueue_script('jquery-ui-widget');
wp_enqueue_script('jquery-ui-draggable');
wp_enqueue_script('jquery-ui-droppable');
wp_enqueue_script('jquery-ui-sortable');
wp_enqueue_script('jquery-ui-tabs');
wp_enqueue_script('jquery-ui-button');
wp_enqueue_script('jquery-ui-accordion');

wp_enqueue_script('jquery-cookie', self::$theme_url . 'admin/scripts/jslib/jquery.cookie.js');
wp_enqueue_script('jquery-form');
wp_enqueue_script('jquery-iCheckbox', self::$theme_url . 'admin/scripts/jslib/iCheckbox/jquery.iCheckbox.js');
wp_enqueue_script('jquery-growl', self::$theme_url . 'admin/scripts/jslib/growl/jquery.growl.js');
wp_enqueue_script('jquery-bootstrap', self::$theme_url . 'admin/scripts/jslib/bootstrap/js/bootstrap.js');
wp_enqueue_script('jquery-expander', self::$theme_url . 'admin/scripts/jslib/jquery.expander.min.js');
wp_enqueue_script('jquery-slimscroll', self::$theme_url . 'admin/scripts/jslib/jquery.slimscroll.min.js');

wp_enqueue_script('jquery-handlebars', self::$theme_url . 'admin/scripts/jslib/handlebars.js');

wp_enqueue_script('spirebuilder-codefield-utils', self::$plugin_url . 'theme/admin/scripts/CodeField/Utils.min.js');

foreach (self::$options['spirebuilder_widgets'] as $widget) {
	// Load every CSS of the modules in order to enable its functionality.
	if (isset($widget['id']) && is_file(self::$widgets_dir . $widget['id'] . '/css/builder.min.css')) {
		wp_enqueue_style('style-spirebuilder-builder-widget-' . $widget['id'], self::$widgets_url . $widget['id'] . '/css/builder.min.css');
	}

	// Load every js of the modules in order to enable its functionality.
	if (is_file(self::$widgets_dir . $widget['id'] . '/scripts/builder.min.js')) {
		wp_enqueue_script('script-spirebuilder-builder-widget-' . $widget['id'], self::$widgets_url . $widget['id'] . '/scripts/builder.min.js');
	}

	// Includes needed to enhance builder functionality.
	if (is_file(self::$widgets_dir . $widget['id'] . '/templates/builder/includes.php')) {
		include(self::$widgets_dir . $widget['id'] . '/templates/builder/includes.php');
	}
}

wp_enqueue_script('spirebuilder-global', self::$theme_url . 'admin/scripts/global.min.js');

wp_enqueue_script('spirebuilder-builder', self::$plugin_url . 'theme/admin/scripts/builder.min.js');