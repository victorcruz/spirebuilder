<?php
/**
 * Include to show the template for the Settings menu item.
 *
 * @package admin-panel
 * @since   0.1
 *
 */

// Get all Global Settings
$settings = SpireBuilder_Settings::get_settings();

// Get all widgets active or disable.
$all_categories = SpireBuilder_Central::get_all_category_with_widgets('active_and_disable');
// Sort Categories and Widgets
sort($all_categories);

// Include HTML template
include(SpireBuilder::$theme_dir . 'admin/templates/settings.php');