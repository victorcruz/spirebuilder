<?php
/*
Plugin Name: Spire Builder
Plugin URI: http://spirebuilder.com
Description: Create stunning landing pages, squeeze pages and sales pages in your WordPress blog through an innovative drag and drop interface!
Version: 0.1
Author: Spire Builder
Author URI: http://spirebuilder.com
*/

// FIXME: J: Remove or Comment the next line before send.
error_reporting( E_ALL );

// Avoid name collisions.
if ( ! class_exists( 'SpireBuilder' ) ) {
	/**
	 * Class SpireBuilder
	 */
	class SpireBuilder {
		/**
		 * The Plugin version
		 *
		 * @const string
		 */
		const VERSION = '0.1';

		/**
		 * The url to the plugin
		 *
		 * @static
		 * @var string
		 */
		static $plugin_url;

		/**
		 * The path to the plugin
		 *
		 * @static
		 * @var string
		 */
		static $plugin_dir;

		/**
		 * The prefix to the Database tables
		 *
		 * @static
		 * @var string
		 */
		static $db_prefix;

		/**
		 * The prefix for the plugin internationalization
		 *
		 * @static
		 * @var string
		 */
		static $i18n_prefix;

		/**
		 * The path to the plugin templates files
		 *
		 * @static
		 * @var string
		 */
		static $theme_dir;

		/**
		 * The path to the plugin templates files
		 *
		 * @static
		 * @var string
		 */
		static $theme_url;

		/**
		 * The path to the plugin templates files
		 *
		 * @static
		 * @var string
		 */
		static $widgets_dir;

		/**
		 * The path to the plugin templates files
		 *
		 * @static
		 * @var string
		 */
		static $widgets_url;

		/**
		 * The Plugin settings
		 *
		 * @static
		 * @var string
		 */
		static $options;

		/**
		 * Categories
		 *
		 * @static
		 * @var string
		 */
		static $categories;

		/**
		 * Timeout for requests (in seconds)
		 *
		 * @static
		 * @var int
		 */
		static $timeout;

		/**
		 * Executes all initialization code for the plugin.
		 *
		 * @return void
		 * @access public
		 */
		function SpireBuilder() {
			// Define static values
			$dir_name          = dirname( plugin_basename( __FILE__ ) );

			self::$plugin_url  = trailingslashit( WP_PLUGIN_URL . '/' . $dir_name );
			self::$plugin_dir  = trailingslashit( WP_PLUGIN_DIR . '/' . $dir_name );

			self::$theme_dir   = self::$plugin_dir . 'theme/';
			self::$theme_url   = self::$plugin_url . 'theme/';

			self::$widgets_dir = self::$theme_dir . 'widgets/';
			self::$widgets_url = self::$theme_url . 'widgets/';

			self::$db_prefix   = 'spirebuilder_';
			self::$timeout     = 10; // By default is 5
			self::$i18n_prefix = 'spirebuilder';

			// Include all classes.
			include( self::$plugin_dir . '/includes/all-classes.php' );

			// Add AJAX nonces support through native WP admin-ajax action.
			add_action( 'wp_ajax_spirebuilder_update_builder_settings', array( &$this, 'ajax_update_builder_settings' ) );
			add_action( 'wp_ajax_spirebuilder_update_general_settings', array( &$this, 'ajax_update_general_settings' ) );
			add_action( 'wp_ajax_spirebuilder_update_settings_builder_settings', array( &$this, 'ajax_spirebuilder_update_settings_builder_settings' ) );
			add_action( 'wp_ajax_spirebuilder_update_builder_lom', array( &$this, 'ajax_update_builder_lom' ) );
			add_action( 'wp_ajax_spirebuilder_update_builder_templates', array( &$this, 'ajax_update_builder_templates' ) );
			add_action( 'wp_ajax_spirebuilder_update_builder_templates_delete', array( &$this, 'ajax_update_builder_templates_delete' ) );

			// Add Menu Box in Admin Panel
			add_action( 'admin_menu', array( &$this, 'admin_menu' ) );

			// Add the front end needed resources.
			add_action( 'wp_enqueue_scripts', array( &$this, 'load_front_end_resources' ) );

			// Init actions
			add_filter( 'init', array( &$this, 'wp_init' ) );

			// Translations for plugin
			self::handle_load_domain();
		}

		/**
		 * Handles the AJAX action for update Builder settings.
		 *
		 */
		function ajax_update_builder_settings() {
			// This is how you get access to the database.
			global $wpdb;

			include( self::$plugin_dir . '/pages/ajax/update-data.php' );
		}

		/**
		 * Handles the AJAX action for update Builder Templates.
		 *
		 */
		function ajax_update_builder_templates() {
			// This is how you get access to the database.
			global $wpdb;

			include( self::$plugin_dir . '/pages/ajax/update-data.php' );
		}

		/**
		 * Handles the AJAX action for update Builder Templates.
		 *
		 */
		function ajax_update_builder_templates_delete() {
			// This is how you get access to the database.
			global $wpdb;

			include( self::$plugin_dir . '/pages/ajax/update-data.php' );
		}

		/**
		 * Handles the AJAX action for update Builder settings.
		 *
		 */
		function ajax_update_general_settings() {
			// This is how you get access to the database.
			global $wpdb;

			include( self::$plugin_dir . '/pages/ajax/update-data.php' );
		}

		/**
		 * Handles the AJAX action for update Builder settings.
		 *
		 */
		function ajax_spirebuilder_update_settings_builder_settings() {
			// This is how you get access to the database.
			global $wpdb;

			include( self::$plugin_dir . '/pages/ajax/update-data.php' );
		}

		/**
		 * Handles the AJAX action for update content layout.
		 *
		 */
		function ajax_update_builder_lom() {
			// This is how you get access to the database.
			global $wpdb;

			include( self::$plugin_dir . '/pages/ajax/update-data.php' );
		}

		static function save_post( $post_id ) {
			global $_POST;

			if ( isset( $post_id ) && SpireBuilder_Validator::parse_int( $post_id ) != '' && isset( $_POST['content_placement'] ) ) {

				// Update/Save Settings
				$settings['allow_spire_builder_editor'] = 0;
				if ( isset ( $_POST['allow_spire_builder_editor'] ) && SpireBuilder_Validator::parse_int( $_POST['allow_spire_builder_editor'] ) ) {
					$settings['allow_spire_builder_editor'] = 1;
				}

				$settings['content_placement'] = $_POST['content_placement'];

				SpireBuilder_WPPosts::update_spire_builder_settings_post( $post_id, $settings );
			}
		}

		/**
		 * Init actions
		 *
		 */
		function wp_init() {

			// Saved/Update Post
			add_action( 'save_post', array( &$this, 'save_post' ) );

			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( &$this, 'add_settings_link' ), 10, 2 );

			add_action( 'the_content', array( &$this, 'show_builder_content' ) );
		}

		/**
		 * Load needed styles and scripts at front end.
		 */
		function load_front_end_resources() {
			global $post;
			$post_id         = $post->ID;
			$settings_global = SpireBuilder_Settings::get_settings();

			if ( isset( $settings_global['allow_spire_builder_editor'] ) ) {
				$allow_spire_builder_editor = $settings_global['allow_spire_builder_editor'];
			}

			if ( isset( $settings_post['allow_spire_builder_editor'] ) ) {
				$allow_spire_builder_editor = $settings_post['allow_spire_builder_editor'];
			}

			// Validate if chown or not
			if ( $allow_spire_builder_editor == 1 ) {
				// Load needed global styles.
				wp_enqueue_style('codefield-helpers', self::$theme_url . 'admin/css/global.min.css');
				wp_enqueue_style('spirebuilder-front-end-styles', self::$theme_url . 'admin/css/front-end.min.css');

				// Get all types widgets from LOM.
				$widgets_types_inside_LOM = SpireBuilder_WPPosts::get_widgets_types_inside_LOM( $post_id );
				foreach ( $widgets_types_inside_LOM as $widget_type ) {
					if ( is_file( self::$widgets_dir . $widget_type . '/templates/front-end/includes.php' ) ) {
						include( self::$widgets_dir . $widget_type . '/templates/front-end/includes.php' );
					}
				}
			}
		}

		/**
		 * Build HTML to front end
		 *
		 * @param $the_content
		 *
		 * @return string $the_content
		 */
		function show_builder_content( $the_content ) {
			global $post;
			$post_id = $post->ID;

			$settings_global   = SpireBuilder_Settings::get_settings();
			$content_placement = $settings_global['content_placement'];

			$settings_post = SpireBuilder_WPPosts::get_spire_builder_settings_post( $post_id );

			if ( isset( $settings_global['allow_spire_builder_editor'] ) ) {
				$allow_spire_builder_editor = $settings_global['allow_spire_builder_editor'];
			}

			if ( isset( $settings_post['allow_spire_builder_editor'] ) ) {
				$allow_spire_builder_editor = $settings_post['allow_spire_builder_editor'];
			}

			// Validate if chown or not
			if ( $allow_spire_builder_editor == 1 ) {
				// Content HTML generated
				$get_LOM_content = SpireBuilder_WPPosts::get_spirebuilder_LOM_content( $post_id );
				$get_LOM_content = str_replace( '$the_content', $the_content, $get_LOM_content );

				// Validate that $settings_post['content_placement'] have any think
				if ( isset( $settings_post['content_placement'] ) && $settings_post['content_placement'] != '' ) {
					$content_placement = $settings_post['content_placement'];
				}

				switch ( $content_placement ) {
					case 'replace':
						$the_content = $get_LOM_content;
						break;
					case 'before':
						$the_content = $the_content . $get_LOM_content;
						break;
					case 'after':
						$the_content = $get_LOM_content . $the_content;
						break;
					default :
						break;
				}
			}

			return $the_content;
		}

		/**
		 * Handles the translation of plugin
		 *
		 * @return void
		 * @access public
		 */
		function handle_load_domain() {
			// Get language in use from settings
			$locale = SpireBuilder_Settings::get_locale();

			if ( $locale != '' ) {
				// FIXME: J:
				// locate translation file
				//$mofile = self::$plugin_dir. '/lang/' . $plugin_domain . '-' . $locale . '.mo';
				$mofile = self::$plugin_dir . 'lang/' . $locale . '.mo';

				if ( file_exists( $mofile ) && is_readable( $mofile ) ) {
					// load translation
					load_textdomain( self::$i18n_prefix, $mofile );
				} else {
					// Store in log that the translation fail load fail
				}
			}
		}

		/**
		 * Display the plugin main menu.
		 *
		 * @return void
		 * @access public
		 * @since  0.1
		 */
		function admin_menu() {
			add_menu_page( __( 'Spire Builder', self::$i18n_prefix ), __( 'Spire Builder', self::$i18n_prefix ), 'manage_options', basename( __FILE__ ), array( &$this, 'handle_settings' ) /* TODO: D: Icon. Pending. ,self::$plugin_url . '/templates/images/icon.png'*/ );
			$page_settings     = add_submenu_page( basename( __FILE__ ), __( 'Settings', self::$i18n_prefix ), __( 'Settings', self::$i18n_prefix ), 'manage_options', basename( __FILE__ ), array( &$this, 'handle_settings' ) );
			$page_requirements = add_submenu_page( basename( __FILE__ ), __( 'Requirements', self::$i18n_prefix ), __( 'Requirements', self::$i18n_prefix ), 'manage_options', 'spirebuilder-requirements', array( &$this, 'handle_requirements' ) );

			/* 
			 * Add custom panel for Edit Post and Pages
			 * Add the boxes for the Post, Page and new custom types added
			 * only new added to avoid: attachment, revision, nav_menu_item that are built in WP
			 */
			// Get content type and show if your status == active
			$types_to_have_boxes = SpireBuilder_Settings::get_builder_content_types();
			foreach ( $types_to_have_boxes as $types_to_have_boxes_name ) {
				if ( $types_to_have_boxes_name['status'] == 'active' ) {
					$builder_box = add_meta_box( 'spirebuilder_builder', 'Spire Builder Editor', array( &$this, 'show_builder_box' ), $types_to_have_boxes_name['id'], 'advanced', 'core' );

					add_action( "admin_print_scripts-$builder_box", array( &$this, 'handle_admin_scripts' ) );
				}
			}

			add_action( "admin_print_scripts-$page_settings", array( &$this, 'handle_admin_scripts' ) );
			add_action( "admin_print_scripts-$page_requirements", array( &$this, 'handle_admin_scripts' ) );

			// Used "-post.php" to add those scripts associated to the Post edit page and the "-post-new.php" for the Add new Post
			add_action( "admin_print_scripts-post.php", array( &$this, 'handle_admin_scripts' ) );
			add_action( "admin_print_scripts-post-new.php", array( &$this, 'handle_admin_scripts' ) );
		}

		/**
		 * handle_admin_scripts
		 *
		 * @since    v0.1
		 */
		function handle_admin_scripts() {
			wp_localize_script( 'jquery' , 'SpireBuilder_i18n' , array(
				'plugin_url' => self::$plugin_url ) );

			wp_localize_script( 'jquery' , 'SpireBuilderNonce' , array(
				'spirebuilder_update_builder_settings'          => wp_create_nonce('spirebuilder_update_builder_settings'),
				'spirebuilder_update_builder_lom'               => wp_create_nonce('spirebuilder_update_builder_lom'),
				'spirebuilder_update_builder_templates'         => wp_create_nonce('spirebuilder_update_builder_templates'),
				'spirebuilder_update_builder_templates_delete'  => wp_create_nonce('spirebuilder_update_builder_templates_delete')
			));
		}
		/**
		 * Load pages to show.
		 *
		 * @global    $post    POST Object
		 *
		 * @param $page_name string page name to load.
		 *
		 * @return    void
		 * @access    public
		 * @since     0.1
		 */
		static function show_page($page_name) {
			global $post;
			isset($post)? $post_id = $post->ID : $post_id = '-1';

			// Load needed assets.
			include( self::$plugin_dir . 'includes/admin/' . $page_name . '-assets.php' );

			// Show include for template
			include( self::$plugin_dir . '/includes/admin/' . $page_name . '.php' );
		}

		/**
		 * Display Builder Editor box in Add/Edit Post/Page sections.
		 *
		 * @return    void
		 * @access    public
		 * @since     0.1
		 */
		function show_builder_box() {
			SpireBuilder::show_page('builder');
		}

		/**
		 * Handles the Settings main menu page.
		 *
		 * @since  0.1
		 */
		function handle_settings() {
			SpireBuilder::show_page('settings');
		}

		/**
		 * Handles the Requirements main menu page.
		 *
		 * @since  0.1
		 */
		function handle_requirements() {
			SpireBuilder::show_page('requirements');
		}

		static function update_main_options() {
			// Get all data from db.
			$data = SpireBuilder_Settings::get_options();
			// Check directories widgets and compare.
			$data['spirebuilder_widgets'] = SpireBuilder_Central::get_diff_widgets_db_and_disk();
			// Get new content types and compare with db.
			$data['settings']['builder_content_types'] = SpireBuilder_Central::get_content_types();

			// Update db.
			SpireBuilder_Settings::update_options( $data );

			self::$categories = SpireBuilder_Central::get_categories();
		}

		function add_settings_link( $links ) {
			$settings = '<a href="' . esc_url( admin_url( 'admin.php?page=spirebuilder.php' ) ) . '">' . __( 'Settings', self::$i18n_prefix ) . '</a>';
			array_push( $links, $settings );
			return $links;
		}

		/**
		 *
		 * Execute code in activation
		 *
		 * @return    void
		 * @access    public
		 */
		function install() {
			// Update Settings.
			self::update_main_options();
		}

		/**
		 *
		 * Execute code in deactivation
		 *
		 * @return    void
		 * @access    public
		 */
		function deactivation() {
			// Remove Settings in case you need.
			// TODO: J: Eliminar - solo para etapa de prueba.
			delete_option( SpireBuilder_Settings::$db_option );
		}

		/**
		 *
		 * Execute code in uninstall
		 *
		 * @return    void
		 * @access    public
		 */
		function uninstall() {
			// Remove Settings.
			delete_option( SpireBuilder_Settings::$db_option );
		}
	}
}

try {
	// Create new instance of the class.
	$SpireBuilder = new SpireBuilder();
	if ( isset( $SpireBuilder ) ) {
		// Register the activation function by passing the reference to our instance.
		register_activation_hook( __FILE__, array( &$SpireBuilder, 'install' ) );
		register_deactivation_hook( __FILE__, array( &$SpireBuilder, 'deactivation' ) );
		// register_uninstall_hook( __FILE__, array( &$SpireBuilder, 'uninstall' ) );
	}
} catch ( Exception $e ) {
	$exit_msg = 'Problem activating: ' . $e->getMessage();
	exit ( $exit_msg );
}