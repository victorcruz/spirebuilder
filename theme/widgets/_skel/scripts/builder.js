var SpireBuilderWidgets;
(function (SpireBuilderWidgets) {
    (function (Skel) {
        var default_context_options = {
            additional_classes: '',
            widget_label: 'Skel'
        };
        function getBuilderContext(LOM_section) {
            return {
            };
        }
        Skel.getBuilderContext = getBuilderContext;
        function getShowOptionsContext(options) {
        }
        Skel.getShowOptionsContext = getShowOptionsContext;
        function getSaveOptionsContext(options) {
        }
        Skel.getSaveOptionsContext = getSaveOptionsContext;
        function setUpdatedContent(context) {
        }
        Skel.setUpdatedContent = setUpdatedContent;
        function setBuilderBehaviour($inserted_widget) {
        }
        Skel.setBuilderBehaviour = setBuilderBehaviour;
        function setShowOptionsBehaviour($filled_template) {
        }
        Skel.setShowOptionsBehaviour = setShowOptionsBehaviour;
        function regenerateOptions(options) {
        }
        Skel.regenerateOptions = regenerateOptions;
        function setUpdatedClone($cloned_HTML_section) {
        }
        Skel.setUpdatedClone = setUpdatedClone;
        function refreshBehaviour($widget) {
        }
        Skel.refreshBehaviour = refreshBehaviour;
    })(SpireBuilderWidgets.Skel || (SpireBuilderWidgets.Skel = {}));
    var Skel = SpireBuilderWidgets.Skel;

})(SpireBuilderWidgets || (SpireBuilderWidgets = {}));

//@ sourceMappingURL=builder.js.map
