/**
 * Specific widget behaviour on front end.
 *
 * @fileOverview
 */
/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;

/*
 * Launch App code on DOM load.
 * Prevent name collisions wrapping the code in an anonymous function.
 */
jQuery(function ($) {
	/*
	 * Put specific front-end behaviour code here.
	 */
});