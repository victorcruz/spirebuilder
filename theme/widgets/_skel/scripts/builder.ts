/**
 * Specific widget behavior in the builder area.
 * For real code for the hooks refer to all default widgets code.
 *
 * @fileOverview
 */
/// <reference path="../../../admin/scripts/CodeField/Utils.d.ts" />
/// <reference path="../../../admin/scripts/builder.d.ts" />

/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;
declare var SpireBuilder;

/**
 * SpireBuilderWidgets. Builder modules container.
 *
 * @namespace
 * @since 0.1
 * @version 0.1
 */
module SpireBuilderWidgets {
	/**
	 * Skel. Definition for a widget template to be used when creating other widgets.
	 *
	 * @namespace
	 * @since 0.1
	 * @version 0.1
	 */
	export module Skel {
		/*
		 * Declare all used internal variables.
		 */
		var default_context_options = {
			additional_classes: '',
			widget_label      : 'Skel'

			// Put other default options here.
		};

		/**
		 * Retrieve the widget builder context to be able to fill the template with the default options set.
		 *
		 * @param    {LOM} LOM_section
		 * @returns  {LOM}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getBuilderContext(LOM_section:LOM):LOM {
			return <LOM>{};
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be shown to user.
		 *
		 * @param   {*} options
		 * @returns  {*} {Object}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getShowOptionsContext(options:any):any {
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be saved to server.
		 *
		 * @param   {*} options
		 * @returns {*}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getSaveOptionsContext(options:any):any {
		}

		/**
		 * Updates any widget builder HTML with its specific variables.
		 *
		 * @param {*} context
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setUpdatedContent(context:any):void {
		}

		/**
		 * Set the specific widget behaviour in the builder.
		 *
		 * @param {*} $inserted_widget
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setBuilderBehaviour($inserted_widget):void {
		}

		/**
		 * Set the widget Edit dialog specific components behaviour.
		 *
		 * @param {*} $filled_template
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setShowOptionsBehaviour($filled_template):void {
		}

		/**
		 * Regenerate options from the inner object data dropzone ids.
		 *
		 * @param {*}   options
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function regenerateOptions(options:any):void {
		}

		/**
		 * Updates cloned widget with needed fresh identifiers.
		 *
		 * @param {*} $cloned_HTML_section
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setUpdatedClone($cloned_HTML_section:any):void {
		}

		/**
		 * Refresh widget behaviour.
		 *
		 * @param {*} $widget
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function refreshBehaviour($widget:any):void {
		}
	}
}