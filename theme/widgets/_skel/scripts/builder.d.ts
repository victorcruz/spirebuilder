var jQuery;
var SpireBuilder;
module SpireBuilderWidgets.Skel {
    export function getBuilderContext(LOM_section: LOM): LOM;
    export function getShowOptionsContext(options: any): any;
    export function getSaveOptionsContext(options: any): any;
    export function setUpdatedContent(context: any): void;
    export function setBuilderBehaviour($inserted_widget): void;
    export function setShowOptionsBehaviour($filled_template): void;
    export function regenerateOptions(options: any): void;
    export function setUpdatedClone($cloned_HTML_section: any): void;
    export function refreshBehaviour($widget: any): void;
}
