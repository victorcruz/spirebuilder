<?php
	/**
	 * Template to show a single accordion section.
	 *
	 * @uses    array   $widget[]                       A collection of widgets along with its data.
	 * @uses    string  $widget['id']                   The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string  $widget['title']                The widget title to be displayed to users.
	 * @uses    string  $widget['description']          The widget description to be displayed to users.
	 *
	 * @uses    array   $widget['icons']                A collection of icons classes or paths to be used in the plugin.
	 * @uses    string  $widget['icons']['builder']     A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-widget-template="builder-<?php echo $widget['id']; ?>-section" type="text/x-handlebars-template">
	<div class="codefield-accordion-group">
		<h3>
			<span class="codefield-accordion-title">{{ title }}</span>

			<span class="codefield-display-none spirebuilder-widget-accordion-actions">
				<i class="spirebuilder-widget-accordion-control-edit icon icon-pencil tip" title="<?php _e('Edit Options', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
				<i class="spirebuilder-widget-accordion-control-clone icon icon-paste tip" title="<?php _e('Clone', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
				<i class="spirebuilder-widget-accordion-control-remove icon icon-remove tip" title="<?php _e('Remove', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
			</span>
		</h3>

		<div>
			{{ dropzone-little }}

			<div id="spirebuilder-dropzone-{{ dropzone }}" class="spirebuilder-dropzone tip" title="<?php _e('Click to Add widget', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip">
				<div class="spirebuilder-dropzone-info">
					<i class="icon icon-plus icon-2x"></i>
				</div>
			</div>
		</div>
	</div>
</script>