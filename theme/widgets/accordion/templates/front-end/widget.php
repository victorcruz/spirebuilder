{{!
	/**
	 * Template to show the Tabs widget on the front end.
	 *
	 * @param    {Array}   $options                                  A collection of widget options.
	 * @param    {string}  $options['additional_classes']            A list of additional space-separated CSS classes that will be applied to the section.
	 * @param    {string}  $options['collapsible']
	 * @param    {string}  $options['opening_method']
	 *
	 * @param    {Array}   $options['icons']
	 * @param    {string}  $options['icons']['closed']
	 * @param    {string}  $options['icons']['opened']
	 *
	 * @param    {Array}   $options['objects']
	 * @param    {string}  $options['objects']['title']
	 *
	 * @param    {Array}   $dropzones          A collection of widget dropzones.
	 *
	 */
}}
<div class="row-fluid {{ options.additional_classes }}">
	<div class="span12">
		<div class="codefield-accordion"
		     data-codefield-accordion-collapsible="{{ options.collapsible }}"
		     data-codefield-accordion-opening-method="{{ options.opening_method }}"
		     data-codefield-accordion-icons-closed="{{ options.icons.closed }}"
		     data-codefield-accordion-icons-opened="{{ options.icons.opened }}">

			{{#each options.objects}}
				<div class="codefield-accordion-group">
					<h3>
						<span class="codefield-accordion-title">{{ this.title }}</span>
					</h3>

					<div>
						{{#content ../dropzones @index}}{{/content}}
					</div>
				</div>
			{{/each}}

		</div>
	</div>
</div>