<?php
	/**
	 * Includes needed for the needed front end functionality.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
	// Load needed styles.
	wp_enqueue_style( 'twitter-bootstrap', self::$theme_url . 'admin/scripts/jslib/bootstrap/css/bootstrap.min.css' );

	wp_enqueue_style('jquery-ui-core', self::$theme_url . 'admin/scripts/jslib/ui/css/jquery.ui.core.css');

	wp_enqueue_style('spirebuilder-codefield-font', self::$theme_url . 'admin/fonts/CodeField/style.css');
	wp_enqueue_style('spirebuilder-fontawesome', self::$theme_url . 'admin/fonts/FontAwesome/font-awesome.min.css');
	wp_enqueue_style('spirebuilder-fontawesomemore-corp', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-corp.css');
	wp_enqueue_style('spirebuilder-fontawesomemore-ext', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-ext.css');
	wp_enqueue_style('spirebuilder-fontawesomemore-social', self::$theme_url . 'admin/fonts/Fontstrap/font-awesome-social.css');

	wp_enqueue_style('jquery-ui-accordion', self::$theme_url . 'admin/scripts/jslib/ui/css/jquery.ui.accordion.css');

	wp_enqueue_style( 'spirebuilder-widget-' . $widget_type, self::$widgets_url . $widget_type . '/css/front-end.min.css' );

	// Load needed scripts.
	wp_enqueue_script('jquery-ui-accordion');
	wp_enqueue_script( 'spirebuilder-widget-' . $widget_type, self::$widgets_url . $widget_type . '/scripts/front-end.min.js' );
?>