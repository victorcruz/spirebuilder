var SpireBuilderWidgets;
(function (SpireBuilderWidgets) {
    (function (Accordion) {
        var default_context_options = {
            widget_label: 'Accordion',
            additional_classes: '',
            opening_method: 'click',
            collapsible: 1,
            icons: {
                opened: 'icon-minus',
                closed: 'icon-plus'
            },
            objects: [
                {
                    title: 'Panel #1',
                    dropzone: ''
                }, 
                {
                    title: 'Panel #2',
                    dropzone: ''
                }
            ]
        };
        var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
        var Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
        var flags = {
            action_bar_behaviour: false
        };
        function getBuilderContext(LOM_section) {
            var merge_buffer = {
            };
            jQuery.extend(true, merge_buffer, default_context_options);
            if(LOM_section.hasOwnProperty('options') && LOM_section.options.hasOwnProperty('objects')) {
                merge_buffer.objects = LOM_section.options.objects;
            } else {
                for(var i = 0; i < merge_buffer.objects.length; i++) {
                    merge_buffer.objects[i].dropzone = jQuery.now() + '-' + alphabet[i];
                }
            }
            var full_merge_buffer = {
            };
            jQuery.extend(true, full_merge_buffer, {
                id: jQuery.now(),
                dropzones: regenerateDropzones(merge_buffer.objects),
                options: merge_buffer
            }, LOM_section);
            return full_merge_buffer;
        }
        Accordion.getBuilderContext = getBuilderContext;
        function getShowOptionsContext(options) {
            var context = {
            };
            jQuery.extend(true, context, {
                options: default_context_options
            }, options);
            context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');
            return context;
        }
        Accordion.getShowOptionsContext = getShowOptionsContext;
        function getSaveOptionsContext(options) {
            options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');
            if(options.hasOwnProperty('collapsible')) {
                options.collapsible = 1;
            } else {
                options['collapsible'] = 0;
            }
        }
        Accordion.getSaveOptionsContext = getSaveOptionsContext;
        function setUpdatedContent(context) {
            var $widget = jQuery('#spirebuilder-widget-' + context.id);
            $widget.find('.spirebuilder-widget-field-widget_label').first().text(context.options.widget_label);
            $widget.find('.codefield-accordion').first().accordion('option', {
                collapsible: (context.options.collapsible) ? true : false,
                event: context.options.opening_method,
                icons: {
                    header: context.options.icons.closed,
                    activeHeader: context.options.icons.opened
                }
            }).accordion('refresh');
        }
        Accordion.setUpdatedContent = setUpdatedContent;
        function setBuilderBehaviour($inserted_widget) {
            var $body = jQuery('body');
            var options = JSON.parse($inserted_widget.attr('data-widget-options'));
            $inserted_widget.find('.codefield-accordion').accordion({
                collapsible: (options.collapsible) ? true : false,
                event: options.opening_method,
                header: '> div > h3',
                heightStyle: 'content',
                icons: {
                    header: options.icons.closed,
                    activeHeader: options.icons.opened
                }
            });
            $body.on('mouseover', '#spirebuilder-editzone .codefield-accordion-group', function (event) {
                jQuery(this).find('.spirebuilder-widget-accordion-actions').show();
                event.stopPropagation();
            });
            $body.on('mouseleave', '#spirebuilder-editzone .codefield-accordion-group', function (event) {
                jQuery(this).find('.spirebuilder-widget-accordion-actions').hide();
                event.stopPropagation();
            });
            if(!flags.action_bar_behaviour) {
                flags.action_bar_behaviour = !flags.action_bar_behaviour;
                var $filled_modal_remove_template;
                var object_index;
                var $accordion;
                var $item;
                var $widget;
                var widget_options;
                $body.on('click', '.spirebuilder-widget-accordion-control-remove', function () {
                    var $this_button = jQuery(this);
                    $accordion = $this_button.closest('.codefield-accordion');
                    $widget = $this_button.closest('.spirebuilder-widget');
                    widget_options = JSON.parse($widget.attr('data-widget-options'));
                    $item = $this_button.closest('.codefield-accordion-group');
                    object_index = $item.index();
                    $filled_modal_remove_template = jQuery(SpireBuilder.templates_container['modal-remove']({
                        object: 'section'
                    }));
                    $filled_modal_remove_template.modal().on('hidden', function () {
                        jQuery(this).remove();
                    });
                });
                $body.on('click', '.codefield-modal-button-remove-section', function () {
                    widget_options.objects.splice(object_index, 1);
                    $widget.attr('data-widget-options', JSON.stringify(widget_options));
                    $item.remove();
                    $accordion.accordion('refresh');
                    var dropzones = regenerateDropzones(widget_options.objects);
                    $widget.attr('data-widget-dropzones', JSON.stringify(dropzones));
                    jQuery('.spire-builder-upload-lom').first().trigger('click');
                });
                $body.on('click', '.spirebuilder-widget-accordion-control-add', function () {
                    var $this_button = jQuery(this);
                    $accordion = $this_button.parent().children('.codefield-accordion');
                    $widget = $this_button.closest('.spirebuilder-widget');
                    widget_options = JSON.parse($widget.attr('data-widget-options'));
                    $filled_modal_remove_template = jQuery(SpireBuilder.templates_container['modal-add']({
                        object: 'section'
                    }));
                    $filled_modal_remove_template.modal().on('hidden', function () {
                        jQuery(this).remove();
                    }).on('shown', function () {
                        jQuery(this).find('input[type="text"]').focus();
                    });
                });
                $body.on('click', '.codefield-modal-button-add-section', function () {
                    var $this_button = jQuery(this);
                    var object_title = $this_button.closest('.modal').find('input[name="section_title"]').val();
                    var object_count = widget_options.objects.length;
                    var now = jQuery.now();
                    var object_data = {
                        title: (object_title) ? object_title : 'Section #' + (object_count + 1),
                        dropzone: now + '-' + alphabet[0]
                    };
                    var object_template = SpireBuilder.templates_container['builder-accordion-section'](object_data);
                    widget_options.objects.push(object_data);
                    $widget.attr('data-widget-options', JSON.stringify(widget_options));
                    $accordion.append(object_template);
                    $accordion.accordion('refresh');
                    var dropzones = regenerateDropzones(widget_options.objects);
                    $widget.attr('data-widget-dropzones', JSON.stringify(dropzones));
                    SpireBuilder['builders'][0].updateRelatedDropzoneOrder();
                    SpireBuilder['builders'][0].hookDropPostWidgetAction();
                });
                var $filled_modal_edit_template;
                var $object_title;
                var object_index;
                $body.on('click', '.spirebuilder-widget-accordion-control-edit', function () {
                    var $this_button = jQuery(this);
                    $accordion = $this_button.closest('.codefield-accordion');
                    $widget = $this_button.closest('.spirebuilder-widget');
                    widget_options = JSON.parse($widget.attr('data-widget-options'));
                    $item = $this_button.closest('.codefield-accordion-group');
                    object_index = $item.index();
                    $filled_modal_edit_template = jQuery(SpireBuilder.templates_container['builder-accordion-modal-edit']());
                    $filled_modal_edit_template.modal().on('hidden', function () {
                        jQuery(this).remove();
                    }).on('shown', function () {
                        $object_title = $this_button.closest('h3').find('.codefield-accordion-title');
                        jQuery(this).find('input[type="text"]').val($object_title.text()).focus();
                    });
                });
                $body.on('click', '.codefield-modal-button-edit-section', function () {
                    var $this_button = jQuery(this);
                    var new_object_title = $this_button.closest('.modal').find('input[name="section_title"]').val();
                    $object_title.text(new_object_title);
                    widget_options.objects[object_index].title = new_object_title;
                    $widget.attr('data-widget-options', JSON.stringify(widget_options));
                    SpireBuilder['builders'][0].hookDropPostWidgetAction();
                });
                $body.on('click', '.spirebuilder-widget-accordion-control-clone', function () {
                    var $this_button = jQuery(this);
                    $item = $this_button.closest('.codefield-accordion-group');
                    $accordion = $this_button.closest('.codefield-accordion');
                    $widget = $this_button.closest('.spirebuilder-widget');
                    widget_options = JSON.parse($widget.attr('data-widget-options'));
                    var object_title = $item.find('.codefield-accordion-title').text();
                    var now = jQuery.now();
                    var object_data = {
                        title: object_title,
                        dropzone: now + '-' + alphabet[0]
                    };
                    var object_template = SpireBuilder.templates_container['builder-accordion-section'](object_data);
                    widget_options.objects.push(object_data);
                    $widget.attr('data-widget-options', JSON.stringify(widget_options));
                    $accordion.append(object_template);
                    $accordion.accordion('refresh');
                    var dropzones = regenerateDropzones(widget_options.objects);
                    $widget.attr('data-widget-dropzones', JSON.stringify(dropzones));
                    if($item.children('.ui-accordion-content').children('.spirebuilder-widget').length > 0) {
                        SpireBuilder['builders'][0].cloneWidgetSection($item.children('.ui-accordion-content'), $accordion.children('.codefield-accordion-group').last().children('.ui-accordion-content'), true);
                    } else {
                        SpireBuilder['builders'][0].hookDropPostWidgetAction();
                    }
                });
            }
            var $all_objects = jQuery('.codefield-accordion', '#spirebuilder-editzone');
            $all_objects.each(function () {
                var $this_accordion = jQuery(this);
                var old_object_index;
                var $widget;
                var widget_options;
                $this_accordion.sortable({
                    axis: 'y',
                    handle: 'h3',
                    start: function (event, ui) {
                        $widget = $this_accordion.closest('.spirebuilder-widget');
                        widget_options = JSON.parse($widget.attr('data-widget-options'));
                        old_object_index = jQuery(ui.item).index();
                    },
                    stop: function (event, ui) {
                        ui.item.children('h3').triggerHandler('focusout');
                        $this_accordion.accordion('refresh');
                    },
                    update: function (event, ui) {
                        var new_object_index = jQuery(ui.item).index();
                        var object_data = widget_options.objects.splice(old_object_index, 1);
                        widget_options.objects.splice(new_object_index, 0, object_data[0]);
                        $widget.attr('data-widget-options', JSON.stringify(widget_options));
                        SpireBuilder['builders'][0].hookDropPostWidgetAction();
                    }
                });
            });
        }
        Accordion.setBuilderBehaviour = setBuilderBehaviour;
        function regenerateDropzones(objects_data) {
            var dropzones = {
            };
            for(var i = 0; i < objects_data.length; i++) {
                dropzones[Alphabet[i]] = objects_data[i].dropzone;
            }
            return dropzones;
        }
        function regenerateOptions(options) {
            for(var i = 0; i < options.objects.length; i++) {
                options.objects[i].dropzone = options.objects[i].dropzone + '-TEMPLATE';
            }
        }
        Accordion.regenerateOptions = regenerateOptions;
        function setUpdatedClone($cloned_HTML_section) {
            var new_dropzones_id = jQuery.parseJSON($cloned_HTML_section.attr('data-widget-dropzones'));
            var options = jQuery.parseJSON($cloned_HTML_section.attr('data-widget-options'));
            var objects_count = options.objects.length;
            var now = jQuery.now();
            for(var i = 0; i < objects_count; i++) {
                var new_id = now + alphabet[i];
                options.objects[i].dropzone = new_dropzones_id[Alphabet[i]];
                options.objects[i].id = new_id;
            }
            $cloned_HTML_section.attr('data-widget-options', JSON.stringify(options));
            $cloned_HTML_section.find('.ui-accordion-header-icon').remove();
            $cloned_HTML_section.find('.ui-state-active').removeClass('ui-state-active');
            $cloned_HTML_section.find('.codefield-accordion').first().accordion({
                collapsible: (options.collapsible) ? true : false,
                event: options.opening_method,
                header: '> div > h3',
                heightStyle: 'content',
                icons: {
                    header: options.icons.closed,
                    activeHeader: options.icons.opened
                }
            });
        }
        Accordion.setUpdatedClone = setUpdatedClone;
        function refreshBehaviour($widget) {
            var options = jQuery.parseJSON($widget.attr('data-widget-options'));
            $widget.find('.ui-accordion-header-icon').first().remove();
            $widget.find('.ui-accordion-header-icon').remove();
            $widget.find('.ui-state-active').removeClass('ui-state-active');
            $widget.find('.codefield-accordion').first().accordion({
                collapsible: (options.collapsible) ? true : false,
                event: options.opening_method,
                header: '> div > h3',
                heightStyle: 'content',
                icons: {
                    header: options.icons.closed,
                    activeHeader: options.icons.opened
                }
            });
        }
        Accordion.refreshBehaviour = refreshBehaviour;
        function setShowOptionsBehaviour($filled_template) {
            if(jQuery.fn.hasOwnProperty('iCheckbox')) {
                $filled_template.find('[type="checkbox"]').iCheckbox({
                    switch_container_src: SpireBuilder_i18n.plugin_url + 'theme/admin/scripts/jslib/iCheckbox/images/switch-frame.png',
                    class_container: 'codefield-checkbox-switcher-container',
                    class_switch: 'codefield-checkbox-switch',
                    class_checkbox: 'codefield-checkbox-checkbox',
                    switch_speed: 100,
                    switch_swing: -13
                });
            }
        }
        Accordion.setShowOptionsBehaviour = setShowOptionsBehaviour;
    })(SpireBuilderWidgets.Accordion || (SpireBuilderWidgets.Accordion = {}));
    var Accordion = SpireBuilderWidgets.Accordion;

})(SpireBuilderWidgets || (SpireBuilderWidgets = {}));

//@ sourceMappingURL=builder.js.map
