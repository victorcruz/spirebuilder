/**
 * Specific widget behaviour on builder actions.
 *
 * @fileOverview
 */
/// <reference path="../../../admin/scripts/CodeField/Utils.d.ts" />
/// <reference path="../../../admin/scripts/builder.d.ts" />

/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;
declare var SpireBuilder;

// Declared and initialized through WordPress.
declare var SpireBuilder_i18n;

/**
 * SpireBuilderWidgets. Builder modules container.
 *
 * @namespace
 * @since 0.1
 * @version 0.1
 */
module SpireBuilderWidgets {
	/**
	 * Accordion. Definition for a widget specialized creating collapsible sections of content.
	 *
	 * @namespace
	 * @since 0.1
	 * @version 0.1
	 */
	export module Accordion {
		/*
		 * Declare all used internal variables.
		 */
		var default_context_options = {
			widget_label      : 'Accordion',
			additional_classes: '',

			opening_method: 'click',
			collapsible   : 1,
			icons         : {
				opened: 'icon-minus',
				closed: 'icon-plus'
			},

			objects: [
				{
					title   : 'Panel #1',
					dropzone: ''
				},
				{
					title   : 'Panel #2',
					dropzone: ''
				}
			]
		};

		var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
		var Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

		var flags = {
			action_bar_behaviour: false
		};

		/**
		 * Retrieve the widget builder context to be able to fill the template with the default options set.
		 *
		 * @param   {LOM}     LOM_section
		 * @returns {LOM}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getBuilderContext(LOM_section:LOM):LOM {
			// Create intermediate merge buffer to hold modified needed values to not directly modify the pristine default options nor LOM_section.
			var merge_buffer:any = {};
			jQuery.extend(
				true,
				merge_buffer,
				default_context_options);

			if (LOM_section.hasOwnProperty('options') && LOM_section.options.hasOwnProperty('objects')) {
				merge_buffer.objects = LOM_section.options.objects;
			}
			else {
				// Regenerate specific data for the default context.
				for (var i = 0; i < merge_buffer.objects.length; i++) {
					merge_buffer.objects[i].dropzone = jQuery.now() + '-' + alphabet[i];
				}
			}

			var full_merge_buffer = <LOM>{};

			// Deep copy.
			jQuery.extend(
				true,
				full_merge_buffer,
				{
					id       : jQuery.now(),
					dropzones: regenerateDropzones(merge_buffer.objects),
					options  : merge_buffer
				},
				LOM_section);

			return full_merge_buffer;
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be shown to user.
		 *
		 * @param   {*}    options
		 * @returns {*}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getShowOptionsContext(options:any):any {
			var context:any = {};

			// Deep merge.
			jQuery.extend(true, context, {
				options: default_context_options
			}, options);

			/*
			 * Transforms any option that need to.
			 */
			context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');

			// And last but not least, return the already processed context.
			return context;
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be saved to server.
		 *
		 * @param {*}    options
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getSaveOptionsContext(options:any):void {
			/*
			 * Transforms any option that need to.
			 */
			options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');

			if (options.hasOwnProperty('collapsible')) {
				options.collapsible = 1;
			}
			else {
				options['collapsible'] = 0;
			}
		}

		/**
		 * Updates widget builder HTML with its specific presentation to resemble its variables.
		 *
		 * @param {LOM}    context
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setUpdatedContent(context:LOM):void {
			// Cache references for performance reasons to be used later on.
			var $widget = jQuery('#spirebuilder-widget-' + context.id);

			// Widget label.
			$widget.find('.spirebuilder-widget-field-widget_label').first().text(context.options.widget_label);

			// Opening Method.
			$widget.find('.codefield-accordion').first()
				.accordion('option', {
					collapsible: (context.options.collapsible) ? true : false,
					event      : context.options.opening_method,

					icons: {
						header      : context.options.icons.closed,
						activeHeader: context.options.icons.opened
					}
				})
				.accordion('refresh');
		}

		/**
		 * Set the specific widget behaviour in the builder.
		 *
		 * @param {*}       $inserted_widget
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setBuilderBehaviour($inserted_widget:any):void {
			// Cache references for performance reasons to be used later on.
			var $body = jQuery('body');

			// Get widget options.
			var options = JSON.parse($inserted_widget.attr('data-widget-options'));

			// Refresh the tabbed widget.
			$inserted_widget.find('.codefield-accordion').accordion({
				collapsible: (options.collapsible) ? true : false,
				event      : options.opening_method,
				header     : '> div > h3',
				heightStyle: 'content',

				icons: {
					header      : options.icons.closed,
					activeHeader: options.icons.opened
				}
			});

			/*
			 * Action bar visibility behaviour.
			 */
			$body.on('mouseover', '#spirebuilder-editzone .codefield-accordion-group', function (event) {
				// Only show this widget actions bar.
				jQuery(this).find('.spirebuilder-widget-accordion-actions').show();

				// Only allow event triggering on this element, not on the ancestors.
				event.stopPropagation();
			});

			$body.on('mouseleave', '#spirebuilder-editzone .codefield-accordion-group', function (event) {
				jQuery(this).find('.spirebuilder-widget-accordion-actions').hide();

				// Only allow event triggering on this element, not on the ancestors.
				event.stopPropagation();
			});

			/*
			 * Action bar buttons behaviour.
			 */
			/*
			 * Action: remove object.
			 * This is a global definition, because that its the flag to not run that twice.
			 */
			if (!flags.action_bar_behaviour) {
				flags.action_bar_behaviour = !flags.action_bar_behaviour;

				var $filled_modal_remove_template;
				var object_index;
				var $accordion;
				var $item;
				var $widget;
				var widget_options;

				/*
				 * Remove object.
				 */
				$body.on('click', '.spirebuilder-widget-accordion-control-remove', function () {
					// Cache references for performance reasons to be used later on.
					var $this_button = jQuery(this);

					$accordion = $this_button.closest('.codefield-accordion');
					$widget = $this_button.closest('.spirebuilder-widget');
					widget_options = JSON.parse($widget.attr('data-widget-options'));

					$item = $this_button.closest('.codefield-accordion-group');
					object_index = $item.index();

					// Fill the widget template by passing the data to the knowledgeable specific function.
					$filled_modal_remove_template = jQuery(SpireBuilder.templates_container['modal-remove']({
						object: 'section'
					}));

					// Finally, launch the modal with the filled template.
					$filled_modal_remove_template.modal().on('hidden', function () {
						jQuery(this).remove();
					});
				});

				$body.on('click', '.codefield-modal-button-remove-section', function () {
					// Splice modifies the array directly.
					widget_options.objects.splice(object_index, 1);
					$widget.attr('data-widget-options', JSON.stringify(widget_options));

					// Remove tabs from UI (related panel included).
					$item.remove();

					$accordion.accordion('refresh');

					// Regenerate dropzones id.
					var dropzones = regenerateDropzones(widget_options.objects);
					$widget.attr('data-widget-dropzones', JSON.stringify(dropzones));

					// Autosave the changes.
					jQuery('.spire-builder-upload-lom').first().trigger('click');
				});

				/*
				 * Add object.
				 */
				$body.on('click', '.spirebuilder-widget-accordion-control-add', function () {
					// Cache references for performance reasons to be used later on.
					var $this_button = jQuery(this);

					$accordion = $this_button.parent().children('.codefield-accordion');

					$widget = $this_button.closest('.spirebuilder-widget');
					widget_options = JSON.parse($widget.attr('data-widget-options'));

					// Fill the widget template by passing the data to the knowledgeable specific function.
					$filled_modal_remove_template = jQuery(SpireBuilder.templates_container['modal-add']({
						object: 'section'
					}));

					// Finally, launch the modal with the filled template.
					$filled_modal_remove_template.modal()
						.on('hidden', function () {
							jQuery(this).remove();
						})
						.on('shown', function () {
							jQuery(this).find('input[type="text"]').focus();
						});
				});

				$body.on('click', '.codefield-modal-button-add-section', function () {
					var $this_button = jQuery(this);

					var object_title = $this_button.closest('.modal').find('input[name="section_title"]').val();
					var object_count = widget_options.objects.length;
					var now = jQuery.now();

					var object_data = {
						title   : (object_title) ? object_title : 'Section #' + (object_count + 1),
						dropzone: now + '-' + alphabet[0]
					};

					var object_template = SpireBuilder.templates_container['builder-accordion-section'](object_data);

					// Update specific tab data.
					widget_options.objects.push(object_data);
					$widget.attr('data-widget-options', JSON.stringify(widget_options));

					// Add new section to UI.
					$accordion.append(object_template);

					$accordion.accordion('refresh');

					// Regenerate dropzones id.
					var dropzones = regenerateDropzones(widget_options.objects);
					$widget.attr('data-widget-dropzones', JSON.stringify(dropzones));

					// Update the widgets dropzone order.
					SpireBuilder['builders'][0].updateRelatedDropzoneOrder();

					// Call the global hook for post widget drop action.
					SpireBuilder['builders'][0].hookDropPostWidgetAction();
				});

				/*
				 * Edit object.
				 */
				var $filled_modal_edit_template;
				var $object_title;
				var object_index;
				$body.on('click', '.spirebuilder-widget-accordion-control-edit', function () {
					// Cache references for performance reasons to be used later on.
					var $this_button = jQuery(this);

					$accordion = $this_button.closest('.codefield-accordion');
					$widget = $this_button.closest('.spirebuilder-widget');
					widget_options = JSON.parse($widget.attr('data-widget-options'));

					$item = $this_button.closest('.codefield-accordion-group');
					object_index = $item.index();

					// Fill the widget template by passing the data to the knowledgeable specific function.
					$filled_modal_edit_template = jQuery(SpireBuilder.templates_container['builder-accordion-modal-edit']());

					// Finally, launch the modal with the filled template.
					$filled_modal_edit_template.modal()
						.on('hidden', function () {
							jQuery(this).remove();
						})
						.on('shown', function () {
							$object_title = $this_button.closest('h3').find('.codefield-accordion-title');
							jQuery(this).find('input[type="text"]')
								.val($object_title.text())
								.focus();
						});
				});

				$body.on('click', '.codefield-modal-button-edit-section', function () {
					var $this_button = jQuery(this);

					var new_object_title = $this_button.closest('.modal').find('input[name="section_title"]').val();

					$object_title.text(new_object_title);

					// Update specific tab data.
					widget_options.objects[object_index].title = new_object_title;
					$widget.attr('data-widget-options', JSON.stringify(widget_options));

					// Call the global hook for post widget drop action.
					SpireBuilder['builders'][0].hookDropPostWidgetAction();
				});

				/*
				 * Clone object.
				 */
				$body.on('click', '.spirebuilder-widget-accordion-control-clone', function () {
					// Cache references for performance reasons to be used later on.
					var $this_button = jQuery(this);
					$item = $this_button.closest('.codefield-accordion-group');
					$accordion = $this_button.closest('.codefield-accordion');
					$widget = $this_button.closest('.spirebuilder-widget');
					widget_options = JSON.parse($widget.attr('data-widget-options'));

					/*
					 * First add a new section to hold the inner cloned widgets.
					 */
					var object_title = $item.find('.codefield-accordion-title').text();
					var now = jQuery.now();

					var object_data = {
						title   : object_title,
						dropzone: now + '-' + alphabet[0]
					};

					var object_template = SpireBuilder.templates_container['builder-accordion-section'](object_data);

					// Update specific tab data.
					widget_options.objects.push(object_data);
					$widget.attr('data-widget-options', JSON.stringify(widget_options));

					// Add new section to UI.
					$accordion.append(object_template);

					$accordion.accordion('refresh');

					// Regenerate dropzones id.
					var dropzones = regenerateDropzones(widget_options.objects);
					$widget.attr('data-widget-dropzones', JSON.stringify(dropzones));

					/*
					 * TODO: D: Clone inner widgets to newly created object.
					 */
					if ($item.children('.ui-accordion-content').children('.spirebuilder-widget').length > 0) {
						SpireBuilder['builders'][0].cloneWidgetSection(
							$item.children('.ui-accordion-content') /* $source */,
							$accordion.children('.codefield-accordion-group').last().children('.ui-accordion-content') /* $destination*/,
							true /* is_section */);
					}
					else {
						// Call the global hook for post widget drop action.
						SpireBuilder['builders'][0].hookDropPostWidgetAction();
					}
				});
			}

			/*
			 * Sortable objects.
			 */
			var $all_objects = jQuery('.codefield-accordion', '#spirebuilder-editzone');
			$all_objects.each(function () {
				var $this_accordion = jQuery(this);

				var old_object_index;
				var $widget;
				var widget_options;

				$this_accordion.sortable({
					axis  : 'y',
					handle: 'h3',

					start: function (event, ui) {
						$widget = $this_accordion.closest('.spirebuilder-widget');
						widget_options = JSON.parse($widget.attr('data-widget-options'));

						old_object_index = jQuery(ui.item).index()
					},

					stop: function (event, ui) {
						// IE doesn't register the blur when sorting so trigger focusout handlers to remove .ui-state-focus
						ui.item.children('h3').triggerHandler('focusout');

						$this_accordion.accordion('refresh');
					},

					update: function (event, ui) {
						// Update the tab order in the widget data.
						var new_object_index = jQuery(ui.item).index();

						// Remove tab from older position.
						var object_data = widget_options.objects.splice(old_object_index, 1);

						// Insert tab data to new position.
						widget_options.objects.splice(new_object_index, 0, object_data[0]);

						$widget.attr('data-widget-options', JSON.stringify(widget_options));

						// Call the global hook for post widget drop action.
						SpireBuilder['builders'][0].hookDropPostWidgetAction();
					}
				});
			});
		}

		/**
		 * Regenerate dropzones from the inner tab data dropzone ids.
		 *
		 * @param   {*}   objects_data
		 * @returns {LOMDropzoneMap}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		function regenerateDropzones(objects_data:any):LOMDropzoneMap {
			var dropzones = <LOMDropzoneMap>{};

			for (var i = 0; i < objects_data.length; i++) {
				dropzones[Alphabet[i]] = objects_data[i].dropzone;
			}

			return dropzones;
		}

		/**
		 * Regenerate options from the inner object data dropzone ids.
		 *
		 * @param {*}   options
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function regenerateOptions(options:any):void {
			for (var i = 0; i < options.objects.length; i++) {
				options.objects[i].dropzone = options.objects[i].dropzone + '-TEMPLATE';
			}
		}

		/**
		 * Updates cloned widget with needed fresh identifiers.
		 *
		 * @param {*} $cloned_HTML_section
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setUpdatedClone($cloned_HTML_section:any):void {
			var new_dropzones_id = jQuery.parseJSON($cloned_HTML_section.attr('data-widget-dropzones'));
			var options:any = jQuery.parseJSON($cloned_HTML_section.attr('data-widget-options'));

			var objects_count = options.objects.length;
			var now = jQuery.now();
			for (var i = 0; i < objects_count; i++) {
				// Regenerate tabs and panels identifiers with new ones.
				var new_id = now + alphabet[i];

				options.objects[i].dropzone = new_dropzones_id[Alphabet[i]];
				options.objects[i].id = new_id;
			}

			// Update the widget attributes with the regenerated identifiers.
			$cloned_HTML_section.attr('data-widget-options', JSON.stringify(options));

			// Remove duplicated icons.
			$cloned_HTML_section.find('.ui-accordion-header-icon').remove();

			// HTML & CSS fixes.
			$cloned_HTML_section.find('.ui-state-active').removeClass('ui-state-active');

			$cloned_HTML_section.find('.codefield-accordion').first().accordion({
				collapsible: (options.collapsible) ? true : false,
				event      : options.opening_method,
				header     : '> div > h3',
				heightStyle: 'content',

				icons: {
					header      : options.icons.closed,
					activeHeader: options.icons.opened
				}
			});
		}

		/**
		 * Refresh widget behaviour.
		 *
		 * @param {*} $widget
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function refreshBehaviour($widget:any):void {
			var options:any = jQuery.parseJSON($widget.attr('data-widget-options'));

			// Remove duplicated icons.
			$widget.find('.ui-accordion-header-icon').first().remove();

			// Remove duplicated icons.
			$widget.find('.ui-accordion-header-icon').remove();

			// HTML & CSS fixes.
			$widget.find('.ui-state-active').removeClass('ui-state-active');

			$widget.find('.codefield-accordion').first().accordion({
				collapsible: (options.collapsible) ? true : false,
				event      : options.opening_method,
				header     : '> div > h3',
				heightStyle: 'content',

				icons: {
					header      : options.icons.closed,
					activeHeader: options.icons.opened
				}
			});
		}

		/**
		 * Set the widget Edit dialog specific components behaviour.
		 *
		 * @param {*} $filled_template
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setShowOptionsBehaviour($filled_template):void {
			// Checkboxes.
			if (jQuery.fn.hasOwnProperty('iCheckbox')) {
				$filled_template.find('[type="checkbox"]')
					.iCheckbox({
						switch_container_src: SpireBuilder_i18n.plugin_url + 'theme/admin/scripts/jslib/iCheckbox/images/switch-frame.png',
						class_container     : 'codefield-checkbox-switcher-container',
						class_switch        : 'codefield-checkbox-switch',
						class_checkbox      : 'codefield-checkbox-checkbox',
						switch_speed        : 100,
						switch_swing        : -13
					});
			}
		}
	}
}