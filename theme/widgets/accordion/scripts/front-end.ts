/**
 * Specific widget behaviour on front end.
 *
 * @fileOverview
 */
/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;

/*
 * Launch App code on DOM load.
 * Prevent name collisions wrapping the code in an anonymous function.
 */
jQuery(function ($) {
	/*
	 * Accordion.
	 */
	var $accordion = jQuery('.codefield-accordion');
	$accordion.each(function () {
		var $this_accordion = jQuery(this);

		$this_accordion.accordion({
			active     : ($this_accordion.attr('data-codefield-accordion-collapsible')) ? false : true,
			collapsible: ($this_accordion.attr('data-codefield-accordion-collapsible')) ? true : false,
			event      : $this_accordion.attr('data-codefield-accordion-opening-method'),
			header     : '> div > h3',
			heightStyle: 'content',

			icons: {
				header      : $this_accordion.attr('data-codefield-accordion-icons-closed'),
				activeHeader: $this_accordion.attr('data-codefield-accordion-icons-opened')
			}
		});
	});
});