var jQuery;
var SpireBuilder;
var SpireBuilder_i18n;
module SpireBuilderWidgets.Accordion {
    export function getBuilderContext(LOM_section: LOM): LOM;
    export function getShowOptionsContext(options: any): any;
    export function getSaveOptionsContext(options: any): void;
    export function setUpdatedContent(context: LOM): void;
    export function setBuilderBehaviour($inserted_widget: any): void;
    export function regenerateOptions(options: any): void;
    export function setUpdatedClone($cloned_HTML_section: any): void;
    export function refreshBehaviour($widget: any): void;
    export function setShowOptionsBehaviour($filled_template): void;
}
