<?php
	/**
	 * Includes needed for the needed front end functionality.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
	// Load needed styles.
	wp_enqueue_style( 'twitter-bootstrap', self::$theme_url . 'admin/scripts/jslib/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'codefield-helpers', self::$theme_url . 'admin/css/helpers.min.css' );
?>