{{!
	/**
	 * Template to show the Plain Text widget on the front end.
	 *
	 * @uses    array   $options                                  A collection of widget options.
	 * @uses    string  $options['additional_classes']            A list of additional space-separated CSS classes that will be applied to the section.
	 * @uses    string  $options['text_content']                  The widget plain text content.
	 * @uses    string  $options['alignment']                     The widget plain text alignment. Can be one of 'left', 'center', 'right'.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
}}
<div class="row-fluid {{ options.additional_classes }}">
	<div class="span12">
		<div class="codefield-text-align-{{ options.alignment }}">
			{{{ options.text_content }}}
		</div>
	</div>
</div>