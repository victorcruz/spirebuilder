/**
 * Specific widget behaviour on builder actions.
 *
 * @fileOverview
 */
/// <reference path="../../../admin/scripts/builder.d.ts" />

/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;

/**
 * SpireBuilderWidgets. Builder modules container.
 *
 * @namespace
 * @since 0.1
 * @version 0.1
 */
module SpireBuilderWidgets {
	/**
	 * Text. Definition for a widget specialized creating a simple and un-styled plain text block.
	 *
	 * @namespace
	 * @since 0.1
	 * @version 0.1
	 */
	export module Text {
		/*
		 * Declare all used internal variables.
		 */
		var default_context_options = {
			additional_classes: '',
			widget_label      : 'Text',

			text_content      : 'Empty content.',
			alignment         : 'left'
		};

		/**
		 * Retrieve the widget builder context to be able to fill the template with the default options set.
		 *
		 * @param   {LOM} LOM_section
		 * @returns {LOM}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getBuilderContext(LOM_section:LOM):LOM {
			// Deep copy.
			return jQuery.extend(
				true,
				{},
				{
					id     : jQuery.now(),
					options: default_context_options
				},
				LOM_section);
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be shown to user.
		 *
		 * @param   {*}  options
		 * @returns {*}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getShowOptionsContext(options:any):any {
			var context:any = {};

			// Deep merge.
			jQuery.extend(true, context, {
				options: default_context_options
			}, options);

			/*
			 * Transforms any option that need to.
			 */
			context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');
			context.options.text_content = context.options.text_content.replace(/<br class="spirebuilder-widget-text-break">/g, '\r\n');

			// And last but not least, return the already processed context.
			return context;
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be saved to server.
		 *
		 * @param {*} options
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getSaveOptionsContext(options:any):void {
			/*
			 * Transforms any option that need to.
			 */
			options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');
			options.text_content = options.text_content.replace(/\r\n/g, '<br class="spirebuilder-widget-text-break">');
		}

		/**
		 * Updates widget builder HTML with its specific presentation to resemble its variables.
		 *
		 * @param {*} context
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setUpdatedContent(context:any):void {
			// Cache references for performance reasons to be used later on.
			var $widget = jQuery('#spirebuilder-widget-' + context.id);

			// Widget label.
			$widget.find('.spirebuilder-widget-field-widget_label').text(context.options.widget_label);

			// Text content.
			$widget.find('.spirebuilder-widget-field-text_content')
				.html(context.options.text_content)
				.removeClass('codefield-text-align-left codefield-text-align-center codefield-text-align-right')
				.addClass('codefield-text-align-' + context.options.alignment);
		}
	}
}