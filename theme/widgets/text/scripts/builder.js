var SpireBuilderWidgets;
(function (SpireBuilderWidgets) {
    (function (Text) {
        var default_context_options = {
            additional_classes: '',
            widget_label: 'Text',
            text_content: 'Empty content.',
            alignment: 'left'
        };
        function getBuilderContext(LOM_section) {
            return jQuery.extend(true, {
            }, {
                id: jQuery.now(),
                options: default_context_options
            }, LOM_section);
        }
        Text.getBuilderContext = getBuilderContext;
        function getShowOptionsContext(options) {
            var context = {
            };
            jQuery.extend(true, context, {
                options: default_context_options
            }, options);
            context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');
            context.options.text_content = context.options.text_content.replace(/<br class="spirebuilder-widget-text-break">/g, '\r\n');
            return context;
        }
        Text.getShowOptionsContext = getShowOptionsContext;
        function getSaveOptionsContext(options) {
            options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');
            options.text_content = options.text_content.replace(/\r\n/g, '<br class="spirebuilder-widget-text-break">');
        }
        Text.getSaveOptionsContext = getSaveOptionsContext;
        function setUpdatedContent(context) {
            var $widget = jQuery('#spirebuilder-widget-' + context.id);
            $widget.find('.spirebuilder-widget-field-widget_label').text(context.options.widget_label);
            $widget.find('.spirebuilder-widget-field-text_content').html(context.options.text_content).removeClass('codefield-text-align-left codefield-text-align-center codefield-text-align-right').addClass('codefield-text-align-' + context.options.alignment);
        }
        Text.setUpdatedContent = setUpdatedContent;
    })(SpireBuilderWidgets.Text || (SpireBuilderWidgets.Text = {}));
    var Text = SpireBuilderWidgets.Text;

})(SpireBuilderWidgets || (SpireBuilderWidgets = {}));

//@ sourceMappingURL=builder.js.map
