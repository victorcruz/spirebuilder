/**
 * Specific widget behaviour on front end.
 *
 * @fileOverview
 */
/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;

/*
 * Launch App code on DOM load.
 * Prevent name collisions wrapping the code in an anonymous function.
 */
jQuery(function ($) {
	/*
	 * Tabs.
	 */
	var $tabs = jQuery('.codefield-tabs');
	$tabs.each(function () {
		var $this_tab = jQuery(this);

		$this_tab.tabs({
			event: $this_tab.attr('data-codefield-tabs-opening-method'),

			fx  : {
				duration: 150,
				opacity : 'toggle'
			},
			hide: {
				// jQuery UI 1.10 compatibility.
				duration: 150
			},
			show: {
				// jQuery UI 1.10 compatibility.
				duration: 150
			}
		});
	});
});