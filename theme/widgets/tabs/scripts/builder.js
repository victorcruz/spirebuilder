var SpireBuilderWidgets;
(function (SpireBuilderWidgets) {
    (function (Tabs) {
        var default_context_options = {
            widget_label: 'Tabs',
            additional_classes: '',
            opening_method: 'click',
            tab_alignment: 'horizontal',
            horizontal_alignment: 'top',
            vertical_alignment: 'left',
            objects: [
                {
                    title: 'Panel #1',
                    dropzone: '',
                    id: ''
                }, 
                {
                    title: 'Panel #2',
                    dropzone: '',
                    id: ''
                }
            ]
        };
        var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
        var Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
        var flags = {
            action_bar_behaviour: false
        };
        function getBuilderContext(LOM_section) {
            var merge_buffer = {
            };
            jQuery.extend(true, merge_buffer, default_context_options);
            if(LOM_section.hasOwnProperty('options') && LOM_section.options.hasOwnProperty('objects')) {
                merge_buffer.objects = LOM_section.options.objects;
            } else {
                for(var i = 0; i < merge_buffer.objects.length; i++) {
                    merge_buffer.objects[i].id = 'tab-' + jQuery.now() + '-' + alphabet[i];
                    merge_buffer.objects[i].dropzone = jQuery.now() + '-' + alphabet[i];
                }
            }
            var full_merge_buffer = {
            };
            jQuery.extend(true, full_merge_buffer, {
                id: jQuery.now(),
                dropzones: regenerateDropzones(merge_buffer.objects),
                options: merge_buffer
            }, LOM_section);
            return full_merge_buffer;
        }
        Tabs.getBuilderContext = getBuilderContext;
        function getShowOptionsContext(options) {
            var context = {
            };
            jQuery.extend(true, context, {
                options: default_context_options
            }, options);
            context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');
            return context;
        }
        Tabs.getShowOptionsContext = getShowOptionsContext;
        function getSaveOptionsContext(options) {
            options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');
            if(options.hasOwnProperty('tab_alignment') && options.tab_alignment == 'horizontal') {
                options.tab_alignment = 'horizontal';
            } else {
                options['tab_alignment'] = 'vertical';
            }
        }
        Tabs.getSaveOptionsContext = getSaveOptionsContext;
        function setUpdatedContent(context) {
            var $widget = jQuery('#spirebuilder-widget-' + context.id);
            $widget.find('.spirebuilder-widget-field-widget_label').first().text(context.options.widget_label);
            $widget.find('.codefield-tabs').first().tabs('option', 'event', context.options.opening_method);
            $widget.find('.codefield-tabs').first().removeClass('codefield-tabs-alignment-type-horizontal codefield-tabs-alignment-type-vertical codefield-tabs-horizontal-alignment-top codefield-tabs-horizontal-alignment-bottom codefield-tabs-vertical-alignment-left codefield-tabs-vertical-alignment-right').addClass('codefield-tabs-alignment-type-' + context.options.tab_alignment).addClass('codefield-tabs-horizontal-alignment-' + context.options.horizontal_alignment).addClass('codefield-tabs-vertical-alignment-' + context.options.vertical_alignment);
        }
        Tabs.setUpdatedContent = setUpdatedContent;
        function setBuilderBehaviour($inserted_widget) {
            var $body = jQuery('body');
            var options = JSON.parse($inserted_widget.attr('data-widget-options'));
            $inserted_widget.find('.codefield-tabs').tabs({
                event: options.opening_method,
                fx: {
                    duration: 150,
                    opacity: 'toggle'
                },
                hide: {
                    duration: 150
                },
                show: {
                    duration: 150
                }
            });
            $body.on('mouseover', '#spirebuilder-editzone .spirebuilder-widget-tabs .ui-tabs-nav > li', function (event) {
                jQuery(this).find('.spirebuilder-widget-tabs-actions').css('display', 'inline');
                event.stopPropagation();
            });
            $body.on('mouseleave', '#spirebuilder-editzone .spirebuilder-widget-tabs .ui-tabs-nav > li', function (event) {
                jQuery(this).find('.spirebuilder-widget-tabs-actions').css('display', 'none');
                event.stopPropagation();
            });
            if(!flags.action_bar_behaviour) {
                flags.action_bar_behaviour = !flags.action_bar_behaviour;
                var $filled_modal_remove_template;
                var object_index;
                var $tabs;
                var tab_id;
                var $widget;
                var widget_options;
                $body.on('click', '.spirebuilder-widget-tabs-control-remove', function () {
                    var $this_button = jQuery(this);
                    $tabs = $this_button.closest('.codefield-tabs');
                    $widget = $this_button.closest('.spirebuilder-widget');
                    widget_options = JSON.parse($widget.attr('data-widget-options'));
                    tab_id = $this_button.closest('li').attr('aria-controls');
                    $filled_modal_remove_template = jQuery(SpireBuilder.templates_container['modal-remove']({
                        object: 'tab'
                    }));
                    $filled_modal_remove_template.modal().on('hidden', function () {
                        jQuery(this).remove();
                    });
                });
                $body.on('click', '.codefield-modal-button-remove-tab', function () {
                    widget_options.objects.splice(object_index, 1);
                    $widget.attr('data-widget-options', JSON.stringify(widget_options));
                    jQuery('#' + tab_id + ', [aria-controls=' + tab_id + ']').remove();
                    $tabs.tabs('refresh');
                    var dropzones = regenerateDropzones(widget_options.objects);
                    $widget.attr('data-widget-dropzones', JSON.stringify(dropzones));
                    jQuery('.spire-builder-upload-lom').first().trigger('click');
                });
                $body.on('click', '.spirebuilder-widget-tabs-control-add', function () {
                    var $this_button = jQuery(this);
                    $tabs = $this_button.closest('.codefield-tabs');
                    $widget = $this_button.closest('.spirebuilder-widget');
                    widget_options = JSON.parse($widget.attr('data-widget-options'));
                    tab_id = $this_button.closest('li').attr('aria-controls');
                    $filled_modal_remove_template = jQuery(SpireBuilder.templates_container['modal-add']({
                        object: 'tab'
                    }));
                    $filled_modal_remove_template.modal().on('hidden', function () {
                        jQuery(this).remove();
                    }).on('shown', function () {
                        jQuery(this).find('input[type="text"]').focus();
                    });
                });
                $body.on('click', '.codefield-modal-button-add-tab', function () {
                    var $this_button = jQuery(this);
                    var tab_title = $this_button.closest('.modal').find('input[name="tab_title"]').val();
                    var tabs_count = widget_options.objects.length;
                    var now = jQuery.now();
                    var tab_data = {
                        id: 'tab-' + now + '-' + alphabet[tabs_count],
                        title: (tab_title) ? tab_title : 'Tab #' + (tabs_count + 1),
                        dropzone: now + '-' + alphabet[0]
                    };
                    var tabs_item = SpireBuilder.templates_container['builder-tabs-item'](tab_data);
                    var tabs_panel = SpireBuilder.templates_container['builder-tabs-panel']({
                        id: 'tab-' + now + '-' + alphabet[tabs_count],
                        dropzones: {
                            A: now
                        }
                    });
                    widget_options.objects.push(tab_data);
                    $widget.attr('data-widget-options', JSON.stringify(widget_options));
                    $tabs.find('.ui-tabs-nav li').eq(-2).after(tabs_item);
                    $tabs.append(tabs_panel);
                    $tabs.tabs('refresh');
                    $tabs.tabs('option', 'active', tabs_count);
                    var dropzones = regenerateDropzones(widget_options.objects);
                    $widget.attr('data-widget-dropzones', JSON.stringify(dropzones));
                    SpireBuilder['builders'][0].updateRelatedDropzoneOrder();
                    SpireBuilder['builders'][0].hookDropPostWidgetAction();
                });
                var $filled_modal_edit_template;
                var $tab_title;
                var tab_index;
                $body.on('click', '.spirebuilder-widget-tabs-control-edit', function () {
                    var $this = jQuery(this);
                    $tabs = $this.closest('.codefield-tabs');
                    $widget = $this.closest('.spirebuilder-widget');
                    widget_options = JSON.parse($widget.attr('data-widget-options'));
                    tab_index = $this.closest('li').prevAll('li').length;
                    tab_id = $this.closest('li').attr('aria-controls');
                    $filled_modal_edit_template = jQuery(SpireBuilder.templates_container['builder-tabs-modal-edit']());
                    $filled_modal_edit_template.modal().on('hidden', function () {
                        jQuery(this).remove();
                    }).on('shown', function () {
                        $tab_title = $this.closest('a').find('.spirebuilder-widget-tabs-title');
                        jQuery(this).find('input[type="text"]').val($tab_title.text()).focus();
                    });
                });
                $body.on('click', '.codefield-modal-button-edit-tab', function () {
                    var $this_button = jQuery(this);
                    var new_tab_title = $this_button.closest('.modal').find('input[name="tab_title"]').val();
                    $tab_title.text(new_tab_title);
                    widget_options.objects[tab_index].title = new_tab_title;
                    $widget.attr('data-widget-options', JSON.stringify(widget_options));
                    SpireBuilder['builders'][0].hookDropPostWidgetAction();
                });
            }
            var $all_tabs = jQuery('.codefield-tabs', '#spirebuilder-editzone');
            $all_tabs.each(function () {
                var $this_tab = jQuery(this);
                var old_tab_index;
                var $widget;
                var widget_options;
                $this_tab.find('.ui-tabs-nav').sortable({
                    containment: 'parent',
                    items: '> li:not(.codefield-sortable-not)',
                    start: function (event, ui) {
                        $widget = $this_tab.closest('.spirebuilder-widget');
                        widget_options = JSON.parse($widget.attr('data-widget-options'));
                        old_tab_index = jQuery(ui.item).index();
                    },
                    stop: function () {
                        $this_tab.tabs('refresh');
                        $this_tab.tabs('refresh');
                    },
                    update: function (event, ui) {
                        var new_tab_index = jQuery(ui.item).index();
                        var tab_data = widget_options.objects.splice(old_tab_index, 1);
                        widget_options.objects.splice(new_tab_index, 0, tab_data[0]);
                        $widget.attr('data-widget-options', JSON.stringify(widget_options));
                        SpireBuilder['builders'][0].hookDropPostWidgetAction();
                    }
                });
            });
        }
        Tabs.setBuilderBehaviour = setBuilderBehaviour;
        function regenerateDropzones(objects_data) {
            var dropzones = {
            };
            for(var i = 0; i < objects_data.length; i++) {
                dropzones[Alphabet[i]] = objects_data[i].dropzone;
            }
            return dropzones;
        }
        function regenerateOptions(options) {
            for(var i = 0; i < options.objects.length; i++) {
                options.objects[i].dropzone = options.objects[i].dropzone + '-TEMPLATE';
                options.objects[i].id = options.objects[i].id + '-TEMPLATE';
            }
        }
        Tabs.regenerateOptions = regenerateOptions;
        function setUpdatedClone($cloned_HTML_section) {
            var new_dropzones_id = jQuery.parseJSON($cloned_HTML_section.attr('data-widget-dropzones'));
            var options = jQuery.parseJSON($cloned_HTML_section.attr('data-widget-options'));
            var tabs_count = options.objects.length;
            var now = jQuery.now();
            for(var i = 0; i < tabs_count; i++) {
                var new_id = now + alphabet[i];
                $cloned_HTML_section.find('a[href="#' + new_id + '"]').attr('href', '#' + new_id);
                $cloned_HTML_section.find('#' + new_id).attr('id', new_id);
                options.objects[i].dropzone = new_dropzones_id[Alphabet[i]];
                options.objects[i].id = new_id;
            }
            $cloned_HTML_section.attr('data-widget-options', JSON.stringify(options));
            $cloned_HTML_section.find('.codefield-tabs').first().tabs({
                event: options.opening_method,
                fx: {
                    duration: 150,
                    opacity: 'toggle'
                },
                hide: {
                    duration: 150
                },
                show: {
                    duration: 150
                }
            });
        }
        Tabs.setUpdatedClone = setUpdatedClone;
        function refreshBehaviour($widget) {
            var options = jQuery.parseJSON($widget.attr('data-widget-options'));
            $widget.find('.codefield-tabs').first().tabs({
                event: options.opening_method,
                fx: {
                    duration: 150,
                    opacity: 'toggle'
                },
                hide: {
                    duration: 150
                },
                show: {
                    duration: 150
                }
            });
        }
        Tabs.refreshBehaviour = refreshBehaviour;
    })(SpireBuilderWidgets.Tabs || (SpireBuilderWidgets.Tabs = {}));
    var Tabs = SpireBuilderWidgets.Tabs;

})(SpireBuilderWidgets || (SpireBuilderWidgets = {}));

//@ sourceMappingURL=builder.js.map
