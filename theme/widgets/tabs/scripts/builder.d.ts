var jQuery;
var SpireBuilder;
module SpireBuilderWidgets.Tabs {
    export function getBuilderContext(LOM_section: LOM): LOM;
    export function getShowOptionsContext(options: any): any;
    export function getSaveOptionsContext(options: any): void;
    export function setUpdatedContent(context: LOM): void;
    export function setBuilderBehaviour($inserted_widget: any): void;
    export function regenerateOptions(options: any): void;
    export function setUpdatedClone($cloned_HTML_section: any): void;
    export function refreshBehaviour($widget: any): void;
}
