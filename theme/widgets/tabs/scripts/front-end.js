jQuery(function ($) {
    var $tabs = jQuery('.codefield-tabs');
    $tabs.each(function () {
        var $this_tab = jQuery(this);
        $this_tab.tabs({
            event: $this_tab.attr('data-codefield-tabs-opening-method'),
            fx: {
                duration: 150,
                opacity: 'toggle'
            },
            hide: {
                duration: 150
            },
            show: {
                duration: 150
            }
        });
    });
});
//@ sourceMappingURL=front-end.js.map
