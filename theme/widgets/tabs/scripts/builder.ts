/**
 * Specific widget behaviour on builder actions.
 *
 * @fileOverview
 */
/// <reference path="../../../admin/scripts/CodeField/Utils.d.ts" />
/// <reference path="../../../admin/scripts/builder.d.ts" />

/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;
declare var SpireBuilder;

/**
 * SpireBuilderWidgets. Builder modules container.
 *
 * @namespace
 * @since 0.1
 * @version 0.1
 */
module SpireBuilderWidgets {
	/**
	 * Tabs. Definition for a widget specialized creating tabbed sections of content.
	 *
	 * @namespace
	 * @since 0.1
	 * @version 0.1
	 */
	export module Tabs {
		/*
		 * Declare all used internal variables.
		 */
		var default_context_options = {
			widget_label      : 'Tabs',
			additional_classes: '',

			opening_method      : 'click',
			tab_alignment       : 'horizontal',
			horizontal_alignment: 'top',
			vertical_alignment  : 'left',

			objects: [
				{
					title   : 'Panel #1',
					dropzone: '',
					id      : ''
				},
				{
					title   : 'Panel #2',
					dropzone: '',
					id      : ''
				}
			]
		};

		var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
		var Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

		var flags = {
			action_bar_behaviour: false
		};

		/**
		 * Retrieve the widget builder context to be able to fill the template with the default options set.
		 *
		 * @param   {LOM} LOM_section
		 * @returns {LOM}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getBuilderContext(LOM_section:LOM):LOM {
			// Create intermediate merge buffer to hold modified needed values to not directly modify the pristine default options nor LOM_section.
			var merge_buffer:any = {};
			jQuery.extend(
				true,
				merge_buffer,
				default_context_options);

			if (LOM_section.hasOwnProperty('options') && LOM_section.options.hasOwnProperty('objects')) {
				merge_buffer.objects = LOM_section.options.objects;
			}
			else {
				// Regenerate specific data for the default context.
				for (var i = 0; i < merge_buffer.objects.length; i++) {
					merge_buffer.objects[i].id = 'tab-' + jQuery.now() + '-' + alphabet[i];
					merge_buffer.objects[i].dropzone = jQuery.now() + '-' + alphabet[i];
				}
			}

			var full_merge_buffer = <LOM>{};

			// Deep copy.
			jQuery.extend(
				true,
				full_merge_buffer,
				{
					id       : jQuery.now(),
					dropzones: regenerateDropzones(merge_buffer.objects),
					options  : merge_buffer
				},
				LOM_section);

			return full_merge_buffer;
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be shown to user.
		 *
		 * @param   {*}  options
		 * @returns {*}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getShowOptionsContext(options:any):any {
			var context:any = {};

			// Deep merge.
			jQuery.extend(true, context, {
				options: default_context_options
			}, options);

			/*
			 * Transforms any option that need to.
			 */
			context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');

			// And last but not least, return the already processed context.
			return context;
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be saved to server.
		 *
		 * @param {*}    options
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getSaveOptionsContext(options:any):void {
			/*
			 * Transforms any option that need to.
			 */
			options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');

			if (options.hasOwnProperty('tab_alignment') && options.tab_alignment == 'horizontal') {
				options.tab_alignment = 'horizontal';
			}
			else {
				options['tab_alignment'] = 'vertical';
			}
		}

		/**
		 * Updates widget builder HTML with its specific presentation to resemble its variables.
		 *
		 * @param {LOM}    context
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setUpdatedContent(context:LOM):void {
			// Cache references for performance reasons to be used later on.
			var $widget = jQuery('#spirebuilder-widget-' + context.id);

			// Widget label.
			$widget.find('.spirebuilder-widget-field-widget_label').first().text(context.options.widget_label);

			// Opening Method.
			$widget.find('.codefield-tabs').first().tabs('option', 'event', context.options.opening_method);

			$widget.find('.codefield-tabs').first()
				.removeClass('codefield-tabs-alignment-type-horizontal codefield-tabs-alignment-type-vertical codefield-tabs-horizontal-alignment-top codefield-tabs-horizontal-alignment-bottom codefield-tabs-vertical-alignment-left codefield-tabs-vertical-alignment-right')
				.addClass('codefield-tabs-alignment-type-' + context.options.tab_alignment)
				.addClass('codefield-tabs-horizontal-alignment-' + context.options.horizontal_alignment)
				.addClass('codefield-tabs-vertical-alignment-' + context.options.vertical_alignment);
		}

		/**
		 * Set the specific widget behaviour in the builder.
		 *
		 * @param {*}     $inserted_widget
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setBuilderBehaviour($inserted_widget:any):void {
			// Cache references for performance reasons to be used later on.
			var $body = jQuery('body');

			// Get widget options.
			var options = JSON.parse($inserted_widget.attr('data-widget-options'));

			// Refresh the tabbed widget.
			$inserted_widget.find('.codefield-tabs').tabs({
				event: options.opening_method,

				fx  : {
					duration: 150,
					opacity : 'toggle'
				},
				hide: {
					// jQuery UI 1.10 compatibility.
					duration: 150
				},
				show: {
					// jQuery UI 1.10 compatibility.
					duration: 150
				}
			});

			/*
			 * Action bar visibility behaviour.
			 */
			$body.on('mouseover', '#spirebuilder-editzone .spirebuilder-widget-tabs .ui-tabs-nav > li', function (event) {
				// Only show this widget actions bar.
				jQuery(this).find('.spirebuilder-widget-tabs-actions').css('display', 'inline');

				// Only allow event triggering on this element, not on the ancestors.
				event.stopPropagation();
			});

			$body.on('mouseleave', '#spirebuilder-editzone .spirebuilder-widget-tabs .ui-tabs-nav > li', function (event) {
				jQuery(this).find('.spirebuilder-widget-tabs-actions').css('display', 'none');

				// Only allow event triggering on this element, not on the ancestors.
				event.stopPropagation();
			});

			/*
			 * Action bar buttons behaviour.
			 */
			/*
			 * Action: remove object.
			 * This is a global definition, because that its the flag to not run that twice.
			 */
			if (!flags.action_bar_behaviour) {
				flags.action_bar_behaviour = !flags.action_bar_behaviour;

				var $filled_modal_remove_template;
				var object_index;
				var $tabs;
				var tab_id;
				var $widget;
				var widget_options;

				/*
				 * Remove object.
				 */
				$body.on('click', '.spirebuilder-widget-tabs-control-remove', function () {
					// Cache references for performance reasons to be used later on.
					var $this_button = jQuery(this);

					$tabs = $this_button.closest('.codefield-tabs');
					$widget = $this_button.closest('.spirebuilder-widget');
					widget_options = JSON.parse($widget.attr('data-widget-options'));

					tab_id = $this_button.closest('li').attr('aria-controls');

					// Fill the widget template by passing the data to the knowledgeable specific function.
					$filled_modal_remove_template = jQuery(SpireBuilder.templates_container['modal-remove']({
						object: 'tab'
					}));

					// Finally, launch the modal with the filled template.
					$filled_modal_remove_template.modal().on('hidden', function () {
						jQuery(this).remove();
					});
				});

				$body.on('click', '.codefield-modal-button-remove-tab', function () {
					// Splice modifies the array directly.
					widget_options.objects.splice(object_index, 1);
					$widget.attr('data-widget-options', JSON.stringify(widget_options));

					// Remove tabs from UI (related panel included).
					jQuery('#' + tab_id + ', [aria-controls=' + tab_id + ']').remove();

					$tabs.tabs('refresh');

					// Regenerate dropzones id.
					var dropzones = regenerateDropzones(widget_options.objects);
					$widget.attr('data-widget-dropzones', JSON.stringify(dropzones));

					// Autosave the changes.
					jQuery('.spire-builder-upload-lom').first().trigger('click');
				});

				/*
				 * Add object.
				 */
				$body.on('click', '.spirebuilder-widget-tabs-control-add', function () {
					// Cache references for performance reasons to be used later on.
					var $this_button = jQuery(this);

					$tabs = $this_button.closest('.codefield-tabs');
					$widget = $this_button.closest('.spirebuilder-widget');
					widget_options = JSON.parse($widget.attr('data-widget-options'));

					tab_id = $this_button.closest('li').attr('aria-controls');

					// Fill the widget template by passing the data to the knowledgeable specific function.
					$filled_modal_remove_template = jQuery(SpireBuilder.templates_container['modal-add']({
						object: 'tab'
					}));

					// Finally, launch the modal with the filled template.
					$filled_modal_remove_template.modal()
						.on('hidden', function () {
							jQuery(this).remove();
						})
						.on('shown', function () {
							jQuery(this).find('input[type="text"]').focus();
						});
				});

				$body.on('click', '.codefield-modal-button-add-tab', function () {
					var $this_button = jQuery(this);

					var tab_title = $this_button.closest('.modal').find('input[name="tab_title"]').val();
					var tabs_count = widget_options.objects.length;
					var now = jQuery.now();

					var tab_data = {
						id      : 'tab-' + now + '-' + alphabet[tabs_count],
						title   : (tab_title) ? tab_title : 'Tab #' + (tabs_count + 1),
						dropzone: now + '-' + alphabet[0]
					};

					var tabs_item = SpireBuilder.templates_container['builder-tabs-item'](tab_data);

					var tabs_panel = SpireBuilder.templates_container['builder-tabs-panel']({
						id: 'tab-' + now + '-' + alphabet[tabs_count],

						dropzones: {
							A: now
						}
					});

					// Update specific tab data.
					widget_options.objects.push(tab_data);
					$widget.attr('data-widget-options', JSON.stringify(widget_options));

					// Add new tab to UI.
					$tabs.find('.ui-tabs-nav li').eq(-2).after(tabs_item);
					$tabs.append(tabs_panel);

					$tabs.tabs('refresh');

					// Select the new added tab.
					$tabs.tabs('option', 'active', tabs_count);

					// Regenerate dropzones id.
					var dropzones = regenerateDropzones(widget_options.objects);
					$widget.attr('data-widget-dropzones', JSON.stringify(dropzones));

					// Update the widgets dropzone order.
					SpireBuilder['builders'][0].updateRelatedDropzoneOrder();

					// Call the global hook for post widget drop action.
					SpireBuilder['builders'][0].hookDropPostWidgetAction();
				});

				/*
				 * Edit object.
				 */
				var $filled_modal_edit_template;
				var $tab_title;
				var tab_index;
				$body.on('click', '.spirebuilder-widget-tabs-control-edit', function () {
					// Cache references for performance reasons to be used later on.
					var $this = jQuery(this);

					$tabs = $this.closest('.codefield-tabs');
					$widget = $this.closest('.spirebuilder-widget');
					widget_options = JSON.parse($widget.attr('data-widget-options'));

					tab_index = $this.closest('li').prevAll('li').length;
					tab_id = $this.closest('li').attr('aria-controls');

					// Fill the widget template by passing the data to the knowledgeable specific function.
					$filled_modal_edit_template = jQuery(SpireBuilder.templates_container['builder-tabs-modal-edit']());

					// Finally, launch the modal with the filled template.
					$filled_modal_edit_template.modal()
						.on('hidden', function () {
							jQuery(this).remove();
						})
						.on('shown', function () {
							$tab_title = $this.closest('a').find('.spirebuilder-widget-tabs-title');
							jQuery(this).find('input[type="text"]')
								.val($tab_title.text())
								.focus();
						});
				});

				$body.on('click', '.codefield-modal-button-edit-tab', function () {
					var $this_button = jQuery(this);

					var new_tab_title = $this_button.closest('.modal').find('input[name="tab_title"]').val();

					$tab_title.text(new_tab_title);

					// Update specific tab data.
					widget_options.objects[tab_index].title = new_tab_title;
					$widget.attr('data-widget-options', JSON.stringify(widget_options));

					// Call the global hook for post widget drop action.
					SpireBuilder['builders'][0].hookDropPostWidgetAction();
				});
			}

			/*
			 * Sortable objects.
			 */
			var $all_tabs = jQuery('.codefield-tabs', '#spirebuilder-editzone');
			$all_tabs.each(function () {
				var $this_tab = jQuery(this);

				var old_tab_index;
				var $widget;
				var widget_options;

				$this_tab.find('.ui-tabs-nav')
					.sortable({
						containment: 'parent',
						items      : '> li:not(.codefield-sortable-not)',

						start: function (event, ui) {
							$widget = $this_tab.closest('.spirebuilder-widget');
							widget_options = JSON.parse($widget.attr('data-widget-options'));

							old_tab_index = jQuery(ui.item).index()
						},

						stop: function () {
							$this_tab.tabs('refresh');
							$this_tab.tabs('refresh');
						},

						update: function (event, ui) {
							// Update the tab order in the widget data.
							var new_tab_index = jQuery(ui.item).index();

							// Remove tab from older position.
							var tab_data = widget_options.objects.splice(old_tab_index, 1);

							// Insert tab data to new position.
							widget_options.objects.splice(new_tab_index, 0, tab_data[0]);

							$widget.attr('data-widget-options', JSON.stringify(widget_options));

							// Call the global hook for post widget drop action.
							SpireBuilder['builders'][0].hookDropPostWidgetAction();
						}
					});
			});
		}

		/**
		 * Regenerate dropzones from the inner tab data dropzone ids.
		 *
		 * @param   {*}   objects_data
		 * @returns {LOMDropzoneMap}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		function regenerateDropzones(objects_data:any):LOMDropzoneMap {
			var dropzones = <LOMDropzoneMap>{};

			for (var i = 0; i < objects_data.length; i++) {
				dropzones[Alphabet[i]] = objects_data[i].dropzone;
			}

			return dropzones;
		}

		/**
		 * Regenerate options from the inner object data dropzone ids.
		 *
		 * @param {*}   options
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function regenerateOptions(options:any):void {
			for (var i = 0; i < options.objects.length; i++) {
				options.objects[i].dropzone = options.objects[i].dropzone + '-TEMPLATE';
				options.objects[i].id = options.objects[i].id + '-TEMPLATE';
			}
		}

		/**
		 * Updates cloned widget with needed fresh identifiers.
		 *
		 * @param {*} $cloned_HTML_section
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setUpdatedClone($cloned_HTML_section:any):void {
			var new_dropzones_id = jQuery.parseJSON($cloned_HTML_section.attr('data-widget-dropzones'));
			var options = jQuery.parseJSON($cloned_HTML_section.attr('data-widget-options'));

			var tabs_count = options.objects.length;
			var now = jQuery.now();
			for (var i = 0; i < tabs_count; i++) {
				// Regenerate tabs and panels identifiers with new ones.
				var new_id = now + alphabet[i];

				$cloned_HTML_section.find('a[href="#' + new_id + '"]').attr('href', '#' + new_id);
				$cloned_HTML_section.find('#' + new_id).attr('id', new_id);

				options.objects[i].dropzone = new_dropzones_id[Alphabet[i]];
				options.objects[i].id = new_id;
			}

			// Update the widget attributes with the regenerated identifiers.
			$cloned_HTML_section.attr('data-widget-options', JSON.stringify(options));

			$cloned_HTML_section.find('.codefield-tabs').first().tabs({
				event: options.opening_method,

				fx  : {
					duration: 150,
					opacity : 'toggle'
				},
				hide: {
					// jQuery UI 1.10 compatibility.
					duration: 150
				},
				show: {
					// jQuery UI 1.10 compatibility.
					duration: 150
				}
			});
		}

		/**
		 * Refresh widget behaviour.
		 *
		 * @param {*} $widget
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function refreshBehaviour($widget:any):void {
			var options = jQuery.parseJSON($widget.attr('data-widget-options'));

			$widget.find('.codefield-tabs').first().tabs({
				event: options.opening_method,

				fx  : {
					duration: 150,
					opacity : 'toggle'
				},
				hide: {
					// jQuery UI 1.10 compatibility.
					duration: 150
				},
				show: {
					// jQuery UI 1.10 compatibility.
					duration: 150
				}
			});
		}
	}
}