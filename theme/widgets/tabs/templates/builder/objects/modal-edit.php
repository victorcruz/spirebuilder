<?php
	/**
	 * Template to show a modal Edit dialog.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-codefield-template="builder-<?php echo $widget['id']; ?>-modal-edit" type="text/x-handlebars-template">
	<div class="codefield-modal modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4>
				<?php _e('Edit Tab Options', SpireBuilder::$i18n_prefix); ?>
			</h4>
		</div>
		<div class="modal-body">
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row">
							<label for="spirebuilder-field-widget-tab_title-<?php echo $widget['id']; ?>">
								<?php _e('Tab title ', SpireBuilder::$i18n_prefix); ?>
							</label>
						</th>
						<td>
							<input id="spirebuilder-field-widget-tab_title-<?php echo $widget['id']; ?>" type="text" name="tab_title" value="" />
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button class="codefield-modal-button-edit-tab button button-large button-primary" data-dismiss="modal" onclick="return false;"><?php _e('Ok', SpireBuilder::$i18n_prefix); ?></button>
			<button class="button button-large" data-dismiss="modal" onclick="return false;"><?php _e('Cancel', SpireBuilder::$i18n_prefix); ?></button>
		</div>
	</div>
</script>