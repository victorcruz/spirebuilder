<?php
	/**
	 * Template to contain the wrapper around the tabs.
	 *
	 * @uses    array   $widget[]                       A collection of widgets along with its data.
	 * @uses    string  $widget['id']                   The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string  $widget['title']                The widget title to be displayed to users.
	 * @uses    string  $widget['description']          The widget description to be displayed to users.
	 *
	 * @uses    array   $widget['icons']                A collection of icons classes or paths to be used in the plugin.
	 * @uses    string  $widget['icons']['builder']     A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-widget-template="builder-<?php echo $widget['id']; ?>-item" type="text/x-handlebars-template">
	<li>
		<a href="#{{ id }}">
			<span class="spirebuilder-widget-tabs-title">{{ title }}</span>

			<span class="spirebuilder-widget-tabs-actions">
				<i class="spirebuilder-widget-tabs-control-edit icon icon-pencil tip" title="<?php _e('Edit Options', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
				<i class="spirebuilder-widget-tabs-control-clone icon icon-paste tip" title="<?php _e('Clone', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
				<i class="spirebuilder-widget-tabs-control-remove icon icon-remove tip" title="<?php _e('Remove', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
			</span>
		</a>
	</li>
</script>