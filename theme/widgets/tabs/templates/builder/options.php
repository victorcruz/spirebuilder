<?php
	/**
	 * Template to show the Tabs builder options.
	 *
	 * @uses    array   $widget[]                       A collection of widgets along with its data.
	 * @uses    string  $widget['id']                   The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string  $widget['title']                The widget title to be displayed to users.
	 * @uses    string  $widget['description']          The widget description to be displayed to users.
	 *
	 * @uses    array   $widget['icons']                A collection of icons classes or paths to be used in the plugin.
	 * @uses    string  $widget['icons']['builder']     A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-widget-template="options-<?php echo $widget['id']; ?>" type="text/x-handlebars-template">
	<div class="codefield-modal spirebuilder-modal-edit-options modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4><?php _e('Edit ', SpireBuilder::$i18n_prefix); ?>
				<span class="codefield-border-bottom-dashed"></span><?php _e($widget['title'] . ' options', SpireBuilder::$i18n_prefix); ?>
				<small><?php _e($widget['description'], SpireBuilder::$i18n_prefix); ?></small>
			</h4>
		</div>
		<div class="modal-body">
			<div class="codefield-tabs-second-lvl">
				<ul>
					<li>
						<a href="#spirebuilder-widget-options-<?php echo $widget['id']; ?>">
							<i class="icon icon-1x <?php echo $widget['icons']['builder']; ?>"></i>
							<?php _e($widget['title'] . ' Options', SpireBuilder::$i18n_prefix); ?>
						</a>
					</li>
					<li>
						<a href="#spirebuilder-widget-options-general">
							<i class="icon icon-1x icon-cogs"></i>
							<?php _e('General Options', SpireBuilder::$i18n_prefix); ?>
						</a>
					</li>
				</ul>

				<form>
					<div id="spirebuilder-widget-options-<?php echo $widget['id']; ?>" class="ui-corner-top">
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row">
										<label for="spirebuilder-field-widget-label-{{ id }}">
											<?php _e('Widget Label', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input type="text" id="spirebuilder-field-widget-label-{{ id }}" class="codefield-wide" name="widget_label" value="{{ options.widget_label }}" />
											<span class="description">
												<?php _e('Widget label to be shown in the builder widget representation.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>
									</td>
								</tr>
								<table class="form-table">
									<tbody>
										<tr>
											<th scope="row">
												<label for="spirebuilder-opening_method-{{ id }}">
													<?php _e('Opening Method', SpireBuilder::$i18n_prefix); ?>
												</label>
											</th>
											<td>
												<select class="codefield-half-wide" id="spirebuilder-opening_method-{{ id }}" name="opening_method">
													<option value="click" {{select options.opening_method 'click'}}><?php _e('Click', SpireBuilder::$i18n_prefix); ?></option>
													<option value="mouseover" {{select options.opening_method 'mouseover'}}><?php _e('Mouse Over', SpireBuilder::$i18n_prefix); ?></option>
												</select>
											</td>
										</tr>

										<tr>
											<th colspan="2">
												<h2><?php _e('Tabs Placement', SpireBuilder::$i18n_prefix); ?></h2>
											</th>
										</tr>
										<tr>
											<th scope="row">
												<label for="spirebuilder-tab_alignment-{{ id }}">
													<?php _e('Placement', SpireBuilder::$i18n_prefix); ?>
												</label>
											</th>
											<td>
												<label class="codefield-quarter-wide codefield-display-inline-block">
													<input id="spirebuilder-tab_alignment-{{ id }}" type="radio" name="tab_alignment" value="horizontal" {{radio options.tab_alignment 'horizontal'}} />
													<?php _e('Horizontal', SpireBuilder::$i18n_prefix); ?>
												</label>

												<!--
												&nbsp;&nbsp;
												<select class="codefield-half-wide" name="horizontal_alignment">
													<option value="top" {{select options.horizontal_alignment 'top'}}><?php _e('Top', SpireBuilder::$i18n_prefix); ?></option>
													<option value="bottom" {{select options.horizontal_alignment 'bottom'}}><?php _e('Bottom', SpireBuilder::$i18n_prefix); ?></option>
												</select>
												-->
												<br />

												<label class="codefield-quarter-wide codefield-display-inline-block">
													<input type="radio" name="tab_alignment" value="vertical" {{radio options.tab_alignment 'vertical'}} />
													<?php _e('Vertical', SpireBuilder::$i18n_prefix); ?>
												</label>

												<!--
												&nbsp;&nbsp;
												<select class="codefield-half-wide" name="vertical_alignment">
													<option value="left" {{select options.vertical_alignment 'left'}}><?php _e('Left', SpireBuilder::$i18n_prefix); ?></option>
													<option value="right" {{select options.vertical_alignment 'right'}}><?php _e('Right', SpireBuilder::$i18n_prefix); ?></option>
												</select>
												-->
											</td>
										</tr>
									</tbody>
								</table>
							</tbody>
						</table>
					</div>

					<div id="spirebuilder-widget-options-general" class="ui-corner-top">
						<input type="hidden" name="id" value="{{ id }}" />
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row">
										<label for="spirebuilder-field-additional-classes-{{ id }}">
											<?php _e('Additional class names', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<textarea id="spirebuilder-field-additional-classes-{{ id }}" class="codefield-wide" name="additional_classes">{{ options.additional_classes }}</textarea>
											<span class="description">
												<?php _e('One class name per line.', SpireBuilder::$i18n_prefix); ?>
												<br />
												<?php _e('Only the class name, no "', SpireBuilder::$i18n_prefix); ?><strong><?php _e('.', SpireBuilder::$i18n_prefix); ?></strong><?php _e('" prefix is needed.', SpireBuilder::$i18n_prefix); ?>
												<br />
												<?php _e('Add custom ', SpireBuilder::$i18n_prefix); ?>
												<strong><?php _e('CSS', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' classes to style this widget in a different way.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>
									</td>
								</tr>
							</tbody>
						</table>
				</form>
			</div>
		</div>
		<div class="modal-footer">
			<button class="spirebuilder-modal-confirmation-button button button-primary button-large" data-dismiss="modal" onclick="return false;"><?php _e('Ok', SpireBuilder::$i18n_prefix); ?></button>
			&nbsp;
			<button class="button button-large" data-dismiss="modal" onclick="return false;"><?php _e('Close', SpireBuilder::$i18n_prefix); ?></button>
		</div>
	</div>
</script>