{{!
	/**
	 * Template to show the Tabs widget on the front end.
	 *
	 * @param    {Array}   $options                                  A collection of widget options.
	 * @param    {string}  $options['additional_classes']            A list of additional space-separated CSS classes that will be applied to the section.
	 * @param    {string}  $options['tab_alignment']
	 * @param    {string}  $options['horizontal_alignment']
	 * @param    {string}  $options['vertical_alignment']
	 * @param    {string}  $options['opening_method']
	 *
	 * @param    {Array}   $options['objects']
	 * @param    {string}  $options['objects']['id']
	 * @param    {string}  $options['objects']['title']
	 *
	 * @param    {Array}   $dropzones          A collection of widget dropzones.
	 *
	 */
}}
<div class="row-fluid {{ options.additional_classes }}">
	<div class="span12">
		<div class="codefield-tabs codefield-tabs-alignment-type-{{ options.tab_alignment }} codefield-tabs-horizontal-alignment-{{ options.horizontal_alignment }} codefield-tabs-vertical-alignment-{{ options.vertical_alignment }}" data-codefield-tabs-opening-method="{{ options.opening_method }}">
			<ul>
				{{#each options.objects}}
					<li>
						<a href="#{{ this.id }}">
							{{ this.title }}
						</a>
					</li>
				{{/each}}
			</ul>

			{{#each options.objects}}
				<div id="{{ this.id }}">
					{{#content ../dropzones @index}}{{/content}}
				</div>
			{{/each}}
		</div>
	</div>
</div>