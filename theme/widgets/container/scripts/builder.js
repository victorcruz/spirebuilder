var SpireBuilderWidgets;
(function (SpireBuilderWidgets) {
    (function (Container) {
        var default_context_options = {
            additional_classes: '',
            widget_label: 'Container'
        };
        function getBuilderContext(LOM_section) {
            return jQuery.extend(true, {
            }, {
                id: jQuery.now(),
                dropzones: {
                    A: jQuery.now()
                },
                options: default_context_options
            }, LOM_section);
        }
        Container.getBuilderContext = getBuilderContext;
        function getShowOptionsContext(options) {
            var context = {
            };
            jQuery.extend(true, context, {
                options: default_context_options
            }, options);
            context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');
            return context;
        }
        Container.getShowOptionsContext = getShowOptionsContext;
        function getSaveOptionsContext(options) {
            options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');
        }
        Container.getSaveOptionsContext = getSaveOptionsContext;
        function setUpdatedContent(context) {
            var $widget = jQuery('#spirebuilder-widget-' + context.id);
            $widget.find('.spirebuilder-widget-field-widget_label').text(context.options.widget_label);
        }
        Container.setUpdatedContent = setUpdatedContent;
    })(SpireBuilderWidgets.Container || (SpireBuilderWidgets.Container = {}));
    var Container = SpireBuilderWidgets.Container;

})(SpireBuilderWidgets || (SpireBuilderWidgets = {}));

//@ sourceMappingURL=builder.js.map
