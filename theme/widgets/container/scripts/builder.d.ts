var jQuery;
module SpireBuilderWidgets.Container {
    export function getBuilderContext(LOM_section: any): any;
    export function getShowOptionsContext(options: any): any;
    export function getSaveOptionsContext(options: any): any;
    export function setUpdatedContent(context: any): void;
}
