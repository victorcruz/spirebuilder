var SpireBuilderWidgets;
(function (SpireBuilderWidgets) {
    (function (Columns) {
        var default_context_options = {
            additional_classes: '',
            widget_label: 'Columns',
            type: '1-2+1-2'
        };
        var available_columns = [
            [
                {
                    layout: '1-2+1-2',
                    icon: '1-2_1-2',
                    title: '1/2 + 1/2'
                }, 
                {
                    layout: 'divider'
                }, 
                {
                    layout: '2-3+1-3',
                    icon: '2-3_1-3',
                    title: '2/3 + 1/3'
                }, 
                {
                    layout: '1-3+2-3',
                    icon: '2-3_1-3 icon-flip-horizontal',
                    title: '1/3 + 2/3'
                }, 
                {
                    layout: 'divider'
                }, 
                {
                    layout: '3-4+1-4',
                    icon: '3-4_1-4',
                    title: '3/4 + 1/4'
                }, 
                {
                    layout: '1-4+3-4',
                    icon: '3-4_1-4 icon-flip-horizontal',
                    title: '1/4 + 3/4'
                }, 
                {
                    layout: 'divider'
                }, 
                {
                    layout: '5-6+1-6',
                    icon: '5-6_1-6',
                    title: '5/6 + 1/6'
                }, 
                {
                    layout: '1-6+5-6',
                    icon: '5-6_1-6 icon-flip-horizontal',
                    title: '1/6 + 5/6'
                }
            ], 
            [
                {
                    layout: '1-3+1-3+1-3',
                    icon: '1-3_1-3_1-3',
                    title: '1/3 + 1/3 + 1/3'
                }, 
                {
                    layout: 'divider'
                }, 
                {
                    layout: '1-4+1-2+1-4',
                    icon: '1-4_1-2_1-4',
                    title: '1/4 + 1/2 + 1/4'
                }, 
                {
                    layout: '1-2+1-4+1-4',
                    icon: '1-2_1-4_1-4',
                    title: '1/2 + 1/4 + 1/4'
                }, 
                {
                    layout: '1-4+1-4+1-2',
                    icon: '1-2_1-4_1-4 icon-flip-horizontal',
                    title: '1/4 + 1/4 + 1/2'
                }
            ], 
            [
                {
                    layout: '1-4+1-4+1-4+1-4',
                    icon: '1-4_1-4_1-4_1-4',
                    title: '1/4 + 1/4 + 1/4 + 1/4'
                }
            ], 
            [
                {
                    layout: '1-6+1-6+1-6+1-6+1-6',
                    icon: '1-5_1-5_1-5_1-5_1-5',
                    title: '1/5 + 1/5 + 1/5 + 1/5 + 1/5'
                }
            ], 
            [
                {
                    layout: '1-6+1-6+1-6+1-6+1-6+1-6',
                    icon: '1-6_1-6_1-6_1-6_1-6_1-6',
                    title: '1/6 + 1/6 + 1/6 + 1/6 + 1/6 + 1/6'
                }
            ]
        ];
        var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
        var Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
        var flags = {
            is_initialized: false
        };
        function init() {
            Handlebars.registerHelper('column-each', function (context, options) {
                var data;
                var columns = context.split('+');
                var ret = '';
                var column_width;
                for(var i = 0, j = columns.length; i < j; i++) {
                    if(options.data) {
                        data = Handlebars.createFrame(options.data || {
                        });
                        data.index = i;
                    }
                    var fraction = columns[i].split('-');
                    column_width = fraction[0] * 12 / fraction[1];
                    ret = ret + options.fn(column_width, {
                        data: data
                    });
                }
                return ret;
            });
        }
        Columns.init = init;
        function getBuilderContext(LOM_section) {
            if(!flags.is_initialized) {
                flags.is_initialized = true;
                this.init();
            }
            var new_columns_qty;
            if(LOM_section.hasOwnProperty('options') && LOM_section.options.hasOwnProperty('type')) {
                new_columns_qty = LOM_section.options.type.split('+').length;
            } else {
                new_columns_qty = default_context_options.type.split('+').length;
            }
            var dropzones = {
            };
            for(var col = 0; col < new_columns_qty; col++) {
                dropzones[Alphabet[col]] = jQuery.now() + '-' + alphabet[col];
            }
            return jQuery.extend(true, {
            }, {
                id: jQuery.now(),
                dropzones: dropzones,
                options: default_context_options,
                generic: {
                    available_columns: available_columns
                }
            }, LOM_section);
        }
        Columns.getBuilderContext = getBuilderContext;
        function getShowOptionsContext(options) {
            var context = {
            };
            jQuery.extend(true, context, {
                options: default_context_options
            }, options);
            context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');
            return context;
        }
        Columns.getShowOptionsContext = getShowOptionsContext;
        function getSaveOptionsContext(options) {
            options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');
        }
        Columns.getSaveOptionsContext = getSaveOptionsContext;
        function setUpdatedContent(context) {
            var $widget = jQuery('#spirebuilder-widget-' + context.id);
            $widget.find('.spirebuilder-widget-field-widget_label').text(context.options.widget_label);
        }
        Columns.setUpdatedContent = setUpdatedContent;
        function setBuilderBehaviour() {
            var $body = jQuery('body');
            $body.on('click', '.spirebuilder-widget-columns-controls a', function () {
                var $this_button = jQuery(this);
                var current_column_type = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-options')).type;
                var new_column_type = $this_button.attr('data-widget-columns-type');
                var $columns_wrapper = $this_button.closest('.spirebuilder-widget').children().children('.spirebuilder-box-content').find('> .row-fluid > .span12 > .spirebuilder-widget-columns-wrapper');
                var $columns = $columns_wrapper.find('> div');
                var $columns_controls_wrapper = $this_button.closest('.spirebuilder-widget').children().children('.spirebuilder-box-header').find('> .spirebuilder-widget-columns-controls');
                $columns_controls_wrapper.find('.dropdown-toggle').removeClass('btn-success');
                $this_button.closest('.btn-group').find('.btn').first().addClass('btn-success');
                $columns_controls_wrapper.find('.dropdown-menu li.active').removeClass('active');
                $this_button.closest('li').addClass('active');
                if(new_column_type != current_column_type) {
                    var current_columns_qty = current_column_type.split('+').length;
                    var new_columns_array = new_column_type.split('+');
                    var new_columns_qty = new_columns_array.length;
                    if(new_columns_qty == current_columns_qty) {
                        for(var col = 0; col < new_columns_qty; col++) {
                            var new_span = 'span' + 12 * new_columns_array[col].split('-')[0] / new_columns_array[col].split('-')[1];
                            $columns.eq(col).removeClass().addClass(new_span);
                        }
                        var old_options = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-options'));
                        old_options.type = new_column_type;
                        $this_button.closest('.spirebuilder-widget').attr('data-widget-options', JSON.stringify(old_options));
                    } else {
                        if(new_columns_qty > current_columns_qty) {
                            var delta_cols = new_columns_qty - current_columns_qty;
                            var new_dropzones = [];
                            for(var col = 0; col < delta_cols; col++) {
                                var new_dropzone_id = jQuery.now() + '-' + alphabet[current_columns_qty + col];
                                new_dropzones.push(new_dropzone_id);
                                var context = {
                                    column_width: 'span' + 12 * new_columns_array[col].split('-')[0] / new_columns_array[col].split('-')[1],
                                    dropzones: {
                                        A: new_dropzone_id
                                    }
                                };
                                var $new_col = jQuery(SpireBuilder.templates_container['builder-columns-column'](context));
                                $columns_wrapper.append($new_col.hide());
                                setTimeout(function () {
                                    $columns_wrapper.find('> div:hidden').show();
                                }, 300);
                            }
                            for(var col = 0; col < new_columns_qty; col++) {
                                var new_span = 'span' + 12 * new_columns_array[col].split('-')[0] / new_columns_array[col].split('-')[1];
                                $columns.eq(col).removeClass().addClass(new_span);
                            }
                            var old_options = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-options'));
                            old_options.type = new_column_type;
                            $this_button.closest('.spirebuilder-widget').attr('data-widget-options', JSON.stringify(old_options));
                            var old_dropzones = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-dropzones'));
                            for(var index = 0; index < new_dropzones.length; index++) {
                                old_dropzones[Alphabet[current_columns_qty + index]] = new_dropzones[index];
                            }
                            $this_button.closest('.spirebuilder-widget').attr('data-widget-dropzones', JSON.stringify(old_dropzones));
                        } else {
                            var delta_cols = current_columns_qty - new_columns_qty;
                            var related_dropzone_id = $columns.eq(new_columns_qty - 1).children('.spirebuilder-dropzone').attr('id').split('spirebuilder-dropzone-')[1];
                            for(var col = 0; col < delta_cols; col++) {
                                var $top_level_widgets = $columns.eq(new_columns_qty + col).children('.spirebuilder-widget');
                                for(var widget = 0; widget < $top_level_widgets.length; widget++) {
                                    $top_level_widgets.eq(widget).attr('data-widget-related-dropzone-id', related_dropzone_id).hide();
                                    SpireBuilder.moveWidgetSection(related_dropzone_id, false, $top_level_widgets.eq(widget));
                                }
                                $columns.eq(new_columns_qty + col).remove();
                            }
                            for(var col = 0; col < new_columns_qty; col++) {
                                var new_span = 'span' + 12 * new_columns_array[col].split('-')[0] / new_columns_array[col].split('-')[1];
                                $columns.eq(col).removeClass().addClass(new_span);
                            }
                            var old_options = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-options'));
                            old_options.type = new_column_type;
                            $this_button.closest('.spirebuilder-widget').attr('data-widget-options', JSON.stringify(old_options));
                            var old_dropzones = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-dropzones'));
                            var new_dropzones_option = {
                            };
                            for(var index = 0; index < new_columns_qty; index++) {
                                new_dropzones_option[Alphabet[index]] = old_dropzones[Alphabet[index]];
                            }
                            $this_button.closest('.spirebuilder-widget').attr('data-widget-dropzones', JSON.stringify(new_dropzones_option));
                        }
                    }
                    if(new_columns_qty == 5) {
                        $columns.first().addClass('offset1');
                    } else {
                        $columns.first().removeClass('offset1');
                    }
                    SpireBuilder['builders'][0].updateRelatedDropzoneOrder();
                    SpireBuilder['builders'][0].hookDropPostWidgetAction();
                }
            });
        }
        Columns.setBuilderBehaviour = setBuilderBehaviour;
    })(SpireBuilderWidgets.Columns || (SpireBuilderWidgets.Columns = {}));
    var Columns = SpireBuilderWidgets.Columns;

})(SpireBuilderWidgets || (SpireBuilderWidgets = {}));

//@ sourceMappingURL=builder.js.map
