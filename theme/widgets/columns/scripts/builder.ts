/**
 * Specific widget behaviour on builder actions.
 *
 * @fileOverview
 */
/// <reference path="../../../admin/scripts/tslib/handlebars.d.ts" />
/// <reference path="../../../admin/scripts/CodeField/Utils.d.ts" />
/// <reference path="../../../admin/scripts/builder.d.ts" />

/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;
declare var SpireBuilder;

/**
 * SpireBuilderWidgets. Builder modules container.
 *
 * @namespace
 * @since 0.1
 * @version 0.1
 */
module SpireBuilderWidgets {
	/**
	 * Columns. Definition for a widget specialized creating grouping sections or rows of content.
	 *
	 * @namespace
	 * @since 0.1
	 * @version 0.1
	 */
	export module Columns {
		/*
		 * Declare all used internal variables.
		 */
		var default_context_options = {
			additional_classes: '',
			widget_label      : 'Columns',

			type: '1-2+1-2'
		};

		var available_columns:any = [
			[
				/*
				 * 2 Columns.
				 */
				{
					layout: '1-2+1-2',
					icon  : '1-2_1-2',
					title : '1/2 + 1/2'
				},
				{
					layout: 'divider'
				},
				{
					layout: '2-3+1-3',
					icon  : '2-3_1-3',
					title : '2/3 + 1/3'
				},
				{
					layout: '1-3+2-3',
					icon  : '2-3_1-3 icon-flip-horizontal',
					title : '1/3 + 2/3'
				},
				{
					layout: 'divider'
				},
				{
					layout: '3-4+1-4',
					icon  : '3-4_1-4',
					title : '3/4 + 1/4'
				},
				{
					layout: '1-4+3-4',
					icon  : '3-4_1-4 icon-flip-horizontal',
					title : '1/4 + 3/4'
				},
				{
					layout: 'divider'
				},
				{
					layout: '5-6+1-6',
					icon  : '5-6_1-6',
					title : '5/6 + 1/6'
				},
				{
					layout: '1-6+5-6',
					icon  : '5-6_1-6 icon-flip-horizontal',
					title : '1/6 + 5/6'
				}
			],
			[
				/*
				 * 3 Columns.
				 */
				{
					layout: '1-3+1-3+1-3',
					icon  : '1-3_1-3_1-3',
					title : '1/3 + 1/3 + 1/3'
				},
				{
					layout: 'divider'
				},
				{
					layout: '1-4+1-2+1-4',
					icon  : '1-4_1-2_1-4',
					title : '1/4 + 1/2 + 1/4'
				},
				{
					layout: '1-2+1-4+1-4',
					icon  : '1-2_1-4_1-4',
					title : '1/2 + 1/4 + 1/4'
				},
				{
					layout: '1-4+1-4+1-2',
					icon  : '1-2_1-4_1-4 icon-flip-horizontal',
					title : '1/4 + 1/4 + 1/2'
				}
			],
			[
				/*
				 * 4 Columns.
				 */
				{
					layout: '1-4+1-4+1-4+1-4',
					icon  : '1-4_1-4_1-4_1-4',
					title : '1/4 + 1/4 + 1/4 + 1/4'
				}
			],
			[
				/*
				 * 5 Columns.
				 */
				{
					layout: '1-6+1-6+1-6+1-6+1-6',
					icon  : '1-5_1-5_1-5_1-5_1-5',
					title : '1/5 + 1/5 + 1/5 + 1/5 + 1/5'
				}
			],
			[
				/*
				 * 6 Columns.
				 */
				{
					layout: '1-6+1-6+1-6+1-6+1-6+1-6',
					icon  : '1-6_1-6_1-6_1-6_1-6_1-6',
					title : '1/6 + 1/6 + 1/6 + 1/6 + 1/6 + 1/6'
				}
			]
		];

		var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
		var Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

		var flags = {
			is_initialized: false
		};

		/**
		 * Init functions for the widget to behave correctly.
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function init():void {
			/*
			 * Register needed custom Handlebars helpers.
			 */
			/*
			 * Customized each to cycle n-column times depending on layout string.
			 * Usage: {{#column-each column-type}}column content{{/column-each}}
			 */
			Handlebars.registerHelper('column-each', function (context, options) {
				var data;
				var columns:any = context.split('+');

				var ret:string = '';
				var column_width:number;

				for (var i = 0, j = columns.length; i < j; i++) {
					if (options.data) {
						data = Handlebars.createFrame(options.data || {});
						data.index = i;
					}

					var fraction = columns[i].split('-');
					column_width = fraction[0] * 12 / fraction[1];

					ret = ret + options.fn(column_width, {
						data: data
					});
				}

				return ret;
			});
		}

		/**
		 * Retrieve the widget builder context to be able to fill the template with the default options set.
		 *
		 * @param   {LOM} LOM_section
		 * @returns {LOM}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getBuilderContext(LOM_section:LOM):LOM {
			// Initialize widget behaviour if not already done.
			if (!flags.is_initialized) {
				flags.is_initialized = true;

				this.init();
			}

			var new_columns_qty;
			if (LOM_section.hasOwnProperty('options') && LOM_section.options.hasOwnProperty('type')) {
				new_columns_qty = LOM_section.options.type.split('+').length;
			}
			else {
				new_columns_qty = default_context_options.type.split('+').length;
			}

			var dropzones = <LOMDropzoneMap>{};

			for (var col = 0; col < new_columns_qty; col++) {
				dropzones[Alphabet[col]] = jQuery.now() + '-' + alphabet[col];
			}

			// Deep copy.
			return jQuery.extend(
				true,
				{},
				{
					id       : jQuery.now(),
					dropzones: dropzones,
					options  : default_context_options,

					/*
					 * Generic: Static data global to all widgets this kind.
					 * Not saved to LOM.
					 */
					generic  : {
						available_columns: available_columns
					}
				},
				LOM_section);
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be shown to user.
		 *
		 * @param   {*}  options
		 * @returns {*}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getShowOptionsContext(options:any):any {
			var context:any = {};

			// Deep merge.
			jQuery.extend(true, context, {
				options: default_context_options
			}, options);

			/*
			 * Transforms any option that need to.
			 */
			context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');

			// And last but not least, return the already processed context.
			return context;
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be saved to server.
		 *
		 * @param {*}    options
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getSaveOptionsContext(options:any):void {
			/*
			 * Transforms any option that need to.
			 */
			options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');
		}

		/**
		 * Updates any widget builder HTML with its specific variables.
		 *
		 * @param {*}    context
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setUpdatedContent(context:any):void {
			// Cache references for performance reasons to be used later on.
			var $widget = jQuery('#spirebuilder-widget-' + context.id);

			// Widget label.
			$widget.find('.spirebuilder-widget-field-widget_label').text(context.options.widget_label);
		}

		/**
		 * Set the specific widget behaviour in the builder.
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setBuilderBehaviour():void {
			// Cache references for performance reasons to be used later on.
			var $body = jQuery('body');

			$body.on('click', '.spirebuilder-widget-columns-controls a', function () {
				// Cache references for performance reasons to be used later on.
				var $this_button = jQuery(this);

				// Get the current type for later use.
				var current_column_type = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-options')).type;
				var new_column_type = $this_button.attr('data-widget-columns-type');
				var $columns_wrapper = $this_button.closest('.spirebuilder-widget').children().children('.spirebuilder-box-content').find('> .row-fluid > .span12 > .spirebuilder-widget-columns-wrapper');
				var $columns = $columns_wrapper.find('> div');

				/*
				 * Check the correct selected column type.
				 */
				// Meta column type indicator.
				var $columns_controls_wrapper = $this_button.closest('.spirebuilder-widget').children().children('.spirebuilder-box-header').find('> .spirebuilder-widget-columns-controls');
				$columns_controls_wrapper.find('.dropdown-toggle').removeClass('btn-success');
				$this_button.closest('.btn-group').find('.btn').first().addClass('btn-success');

				// Column item indicator.
				$columns_controls_wrapper.find('.dropdown-menu li.active').removeClass('active');
				$this_button.closest('li').addClass('active');

				// If there is no change in type, do nothing.
				if (new_column_type != current_column_type) {
					var current_columns_qty = current_column_type.split('+').length;
					var new_columns_array = new_column_type.split('+');
					var new_columns_qty = new_columns_array.length;

					if (new_columns_qty == current_columns_qty) {
						/*
						 * Is equal.
						 */
						// Update column classes.
						for (var col = 0; col < new_columns_qty; col++) {
							/*
							 * Col format is in the form: 2-3, this means 2/3.
							 * To determine span number, do 12 * 2 / 3 and the result will be the span number.
							 */
							var new_span = 'span' + 12 * new_columns_array[col].split('-')[0] / new_columns_array[col].split('-')[1];

							// Update column width.
							$columns.eq(col).removeClass().addClass(new_span);
						}

						// Update column type.
						var old_options = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-options'));
						old_options.type = new_column_type;

						$this_button.closest('.spirebuilder-widget').attr('data-widget-options', JSON.stringify(old_options));
					}
					else if (new_columns_qty > current_columns_qty) {
						/*
						 * Is enlarging.
						 */
						// Add the new columns.
						var delta_cols = new_columns_qty - current_columns_qty;

						var new_dropzones = [];
						for (var col = 0; col < delta_cols; col++) {
							var new_dropzone_id = jQuery.now() + '-' + alphabet[current_columns_qty + col];
							new_dropzones.push(new_dropzone_id);

							var context = {
								column_width: 'span' + 12 * new_columns_array[col].split('-')[0] / new_columns_array[col].split('-')[1],
								dropzones   : {
									A: new_dropzone_id
								}
							};

							// Fill the widget template by passing the data to the knowledgeable specific function.
							var $new_col = jQuery(SpireBuilder.templates_container['builder-columns-column'](context));

							$columns_wrapper.append($new_col.hide());

							setTimeout(function () {
								$columns_wrapper.find('> div:hidden').show();
							}, 300);
						}

						// Update column classes.
						for (var col = 0; col < new_columns_qty; col++) {
							/*
							 * Col format is in the form: 2-3, this means 2/3.
							 * To determine span number, do 12 * 2 / 3 and the result will be the span number.
							 */
							var new_span = 'span' + 12 * new_columns_array[col].split('-')[0] / new_columns_array[col].split('-')[1];

							// Update column width.
							$columns.eq(col).removeClass().addClass(new_span);
						}

						// Update column type.
						var old_options = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-options'));
						old_options.type = new_column_type;
						$this_button.closest('.spirebuilder-widget').attr('data-widget-options', JSON.stringify(old_options));

						// Update column dropzones. Add new dropzones.
						var old_dropzones = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-dropzones'));
						for (var index = 0; index < new_dropzones.length; index++) {
							old_dropzones[Alphabet[current_columns_qty + index]] = new_dropzones[index];
						}
						$this_button.closest('.spirebuilder-widget').attr('data-widget-dropzones', JSON.stringify(old_dropzones));
					}
					else {
						/*
						 * Is shrinking.
						 */
						// Move the delta content to the last column.
						var delta_cols = current_columns_qty - new_columns_qty;
						var related_dropzone_id:number = <number>$columns.eq(new_columns_qty - 1).children('.spirebuilder-dropzone').attr('id').split('spirebuilder-dropzone-')[1];

						for (var col = 0; col < delta_cols; col++) {
							var $top_level_widgets = $columns.eq(new_columns_qty + col).children('.spirebuilder-widget');

							for (var widget = 0; widget < $top_level_widgets.length; widget++) {
								// Update column data.
								$top_level_widgets.eq(widget).attr('data-widget-related-dropzone-id', related_dropzone_id).hide();

								// Move the HTML section to its new place.
								SpireBuilder.moveWidgetSection(
									related_dropzone_id,
									false,
									$top_level_widgets.eq(widget));
							}

							// Remove the already moved column.
							$columns.eq(new_columns_qty + col).remove();
						}

						// Update column classes.
						for (var col = 0; col < new_columns_qty; col++) {
							/*
							 * Col format is in the form: 2-3, this means 2/3.
							 * To determine span number, do 12 * 2 / 3 and the result will be the span number.
							 */
							var new_span = 'span' + 12 * new_columns_array[col].split('-')[0] / new_columns_array[col].split('-')[1];

							// Update column width.
							$columns.eq(col).removeClass().addClass(new_span);
						}

						// Update column type.
						var old_options = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-options'));
						old_options.type = new_column_type;
						$this_button.closest('.spirebuilder-widget').attr('data-widget-options', JSON.stringify(old_options));

						// Update column dropzones. Remove exceeded dropzones.
						var old_dropzones = jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-dropzones'));
						var new_dropzones_option = {};
						for (var index = 0; index < new_columns_qty; index++) {
							new_dropzones_option[Alphabet[index]] = old_dropzones[Alphabet[index]];
						}
						$this_button.closest('.spirebuilder-widget').attr('data-widget-dropzones', JSON.stringify(new_dropzones_option));
					}

					/*
					 * Update the specific columns settings.
					 */
					if (new_columns_qty == 5) {
						$columns.first().addClass('offset1');
					}
					else {
						$columns.first().removeClass('offset1');
					}

					// Update the widgets dropzone order.
					SpireBuilder['builders'][0].updateRelatedDropzoneOrder();

					// Call the global hook for post widget drop action.
					SpireBuilder['builders'][0].hookDropPostWidgetAction();
				}
			});
		}
	}
}