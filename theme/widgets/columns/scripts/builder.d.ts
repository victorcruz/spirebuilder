var jQuery;
var SpireBuilder;
module SpireBuilderWidgets.Columns {
    export function init(): void;
    export function getBuilderContext(LOM_section: LOM): LOM;
    export function getShowOptionsContext(options: any): any;
    export function getSaveOptionsContext(options: any): void;
    export function setUpdatedContent(context: any): void;
    export function setBuilderBehaviour(): void;
}
