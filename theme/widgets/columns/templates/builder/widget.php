<?php
	/**
	 * Template to show the Columns widget representation in the builder.
	 *
	 * @uses    array   $widget[]                       A collection of widgets along with its data.
	 * @uses    string  $widget['id']                   The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string  $widget['title']                The widget title to be displayed to users.
	 * @uses    string  $widget['description']          The widget description to be displayed to users.
	 *
	 * @uses    array   $widget['icons']                A collection of icons classes or paths to be used in the plugin.
	 * @uses    string  $widget['icons']['builder']     A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-widget-template="builder-<?php echo $widget['id']; ?>" type="text/x-handlebars-template">
	<div id="spirebuilder-widget-{{ id }}" class="row-fluid spirebuilder-widget spirebuilder-widget-<?php echo $widget['id']; ?>" data-widget-type="<?php echo $widget['id']; ?>" data-widget-related-dropzone-id="{{ related_dropzone_id }}" data-widget-related-dropzone-order="{{ related_dropzone_order }}" data-widget-dropzones="{{ JSON2string dropzones }}" data-widget-options="{{ JSON2string options }}">
		<div class="span12 spirebuilder-box">
			<div class="spirebuilder-box-header">

				<h4>
					<i class="icon <?php echo $widget['icons']['builder']; ?> icon-1x"></i>
					<span class="spirebuilder-widget-field-widget_label">{{ options.widget_label }}</span>
				</h4>

				<div class="spirebuilder-actions">
					<i class="spirebuilder-control-drop icon icon-chevron-up tip" title="<?php _e('Minimize/Maximize', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
					<i class="spirebuilder-control-edit icon icon-pencil tip" title="<?php _e('Edit Options', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
					<i class="spirebuilder-control-save icon icon-save tip" title="<?php _e('Save As Template', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
					<i class="spirebuilder-control-clone icon icon-paste tip" title="<?php _e('Clone', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
					<i class="spirebuilder-control-remove icon icon-remove tip" title="<?php _e('Remove', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
				</div>

				<div class="spirebuilder-widget-columns-controls">
					{{#each generic.available_columns}}
						<div class="btn-group">
							<button class='btn btn-mini
								{{#each this}}
									{{#equal ../../options.type this.layout}}btn-success{{/equal}}
								{{/each}}
								dropdown-toggle' data-toggle="dropdown">{{sum @index 2}}</button>

							<ul class="dropdown-menu">
								{{#each this}}
									{{#equal this.layout "divider"}}
										<li class="divider"></li>
									{{else}}
										<li {{#equal ../../options.type this.layout}}class="active"{{/equal}}>
											<a onclick="return false;" href="#" data-widget-columns-type="{{ this.layout }}">
												<i class="icon icon-large icon-columns-{{ this.icon }}"></i>
												&nbsp;&nbsp;

												{{ this.title }}
											</a>
										</li>
									{{/equal}}
								{{/each}}
							</ul>
						</div>
					{{/each}}
				</div>

				<span class="clearfix"></span>
			</div>

			<div class="spirebuilder-box-content">
				<div class="row-fluid">
					<div class="span12">
						<div class="row-fluid spirebuilder-widget-columns-wrapper">

							{{#column-each options.type}}
								<div class="span{{ this }}">
									{{ dropzone-little }}
									<div id="spirebuilder-dropzone-{{#content ../dropzones @index}}{{/content}}" class="spirebuilder-dropzone tip" title="<?php _e('Click to Add widget', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip">
										<div class="spirebuilder-dropzone-info">
											<i class="icon icon-plus icon-2x"></i>
										</div>
									</div>
								</div>
							{{/column-each}}

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</script>

<?php include "objects/column.php"; ?>