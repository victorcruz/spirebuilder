<?php
	/**
	 * Template to show a single column.
	 *
	 * @uses    array   $widget[]                       A collection of widgets along with its data.
	 * @uses    string  $widget['id']                   The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string  $widget['title']                The widget title to be displayed to users.
	 * @uses    string  $widget['description']          The widget description to be displayed to users.
	 *
	 * @uses    array   $widget['icons']                A collection of icons classes or paths to be used in the plugin.
	 * @uses    string  $widget['icons']['builder']     A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-widget-template="builder-<?php echo $widget['id']; ?>-column" type="text/x-handlebars-template">
	<div class="{{ column_width }}">
		{{ dropzone-little }}
		<div id="spirebuilder-dropzone-{{ dropzones.A }}" class="spirebuilder-dropzone tip" title="<?php _e('Click to Add widget', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip">
			<div class="spirebuilder-dropzone-info">
				<i class="icon icon-plus icon-2x"></i>
			</div>
		</div>
	</div>
</script>