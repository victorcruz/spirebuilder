<?php
	/**
	 * Template to show the Columns builder options.
	 *
	 * @uses    array   $widget[]                       A collection of widgets along with its data.
	 * @uses    string  $widget['id']                   The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string  $widget['title']                The widget title to be displayed to users.
	 * @uses    string  $widget['description']          The widget description to be displayed to users.
	 *
	 * @uses    array   $widget['icons']                A collection of icons classes or paths to be used in the plugin.
	 * @uses    string  $widget['icons']['builder']     A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-widget-template="options-<?php echo $widget['id']; ?>" type="text/x-handlebars-template">
	<div class="codefield-modal spirebuilder-modal-edit-options modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4><?php _e('Edit ', SpireBuilder::$i18n_prefix); ?>
				<span class="codefield-border-bottom-dashed"></span><?php _e($widget['title'] . ' options', SpireBuilder::$i18n_prefix); ?>
				<small><?php _e($widget['description'], SpireBuilder::$i18n_prefix); ?></small>
			</h4>
		</div>
		<div class="modal-body">
			<div class="codefield-tabs-second-lvl">
				<ul>
					<li>
						<a href="#spirebuilder-widget-options-general">
							<i class="icon icon-1x icon-cogs"></i>
							<?php _e('General Options', SpireBuilder::$i18n_prefix); ?>
						</a>
					</li>
				</ul>

				<div id="spirebuilder-widget-options-general" class="ui-corner-top">
					<input type="hidden" name="id" value="{{ id }}" />
					<form>
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row">
										<label for="spirebuilder-field-widget-label-{{ id }}">
											<?php _e('Widget Label', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input type="text" id="spirebuilder-field-widget-label-{{ id }}" class="codefield-wide" name="widget_label" value="{{ options.widget_label }}" />
											<span class="description">
												<?php _e('Widget label to be shown in the builder instead of the default widget name.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="spirebuilder-field-additional-classes-{{ id }}">
											<?php _e('Additional Class Names', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<textarea id="spirebuilder-field-additional-classes-{{ id }}" class="codefield-wide" name="additional_classes">{{ options.additional_classes }}</textarea>
											<span class="description">
												<?php _e('One class name per line.', SpireBuilder::$i18n_prefix); ?>
												<br />
												<?php _e('Only the class name, no "', SpireBuilder::$i18n_prefix); ?><strong><?php _e('.', SpireBuilder::$i18n_prefix); ?></strong><?php _e('" prefix is needed.', SpireBuilder::$i18n_prefix); ?>
												<br />
												<?php _e('Add custom ', SpireBuilder::$i18n_prefix); ?>
												<strong><?php _e('CSS', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' classes to style this widget in a different way.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button class="spirebuilder-modal-confirmation-button button button-primary button-large" data-dismiss="modal" onclick="return false;"><?php _e('Ok', SpireBuilder::$i18n_prefix); ?></button>
			&nbsp;
			<button class="button button-large" data-dismiss="modal" onclick="return false;"><?php _e('Close', SpireBuilder::$i18n_prefix); ?></button>
		</div>
	</div>
</script>