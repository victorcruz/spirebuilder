{{!
	/**
	 * Template to show the Columns widget on the front end.
	 *
	 * @uses    array   $options                                  A collection of widget options.
	 * @uses    string  $options['additional_classes']            A list of additional space-separated CSS classes that will be applied to the section.
	 *
	 * @uses    array   $dropzones          A collection of widget dropzones.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
}}
<div class="row-fluid {{ options.additional_classes }}">
	<div class="span12">
		<div class="row-fluid">

			{{#column-each options.type}}
				<div class="span{{ this }}">
					{{#content ../dropzones @index}}{{/content}}
				</div>
			{{/column-each}}

		</div>
	</div>
</div>