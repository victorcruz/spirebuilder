var jQuery;
var tinymce;
var tinyMCEPreInit;
module SpireBuilderWidgets.RichText {
    export function getBuilderContext(LOM_section: any): any;
    export function getShowOptionsContext(options: any): any;
    export function getSaveOptionsContext(options: any): any;
    export function setUpdatedContent(context: any): void;
    export function setShowOptionsBehaviour($filled_template): void;
}
