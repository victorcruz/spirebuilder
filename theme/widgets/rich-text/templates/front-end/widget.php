{{!
	/**
	 * Template to show the Rich text widget on the front end.
	 *
	 * @uses    array   $options                                  A collection of widget options.
	 * @uses    string  $options['additional_classes']            A list of additional space-separated CSS classes that will be applied to the section.
	 * @uses    string  $options['text_content']                  The widget plain text content.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
}}
<div class="spirebuilder-widget-rich-text row-fluid {{ options.additional_classes }}">
	<div class="span12">
		{{{ options.text_content }}}
	</div>
</div>