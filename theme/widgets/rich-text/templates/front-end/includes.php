<?php
	/**
	 * Includes needed for the needed front end functionality.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
	// Load needed styles.
	wp_enqueue_style( 'twitter-bootstrap', self::$theme_url . 'admin/scripts/jslib/bootstrap/css/bootstrap.min.css' );

	wp_enqueue_style( 'spirebuilder-widget-' . $widget_type, self::$widgets_url . $widget_type . '/css/front-end.min.css' );
?>