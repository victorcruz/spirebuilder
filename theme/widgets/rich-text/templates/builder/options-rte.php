<?php
	/**
	 * Template to show the Rich Text editor in the options.
	 * This is due WordPress RTE doesn't support DOM cloning nor rearrangements.
	 *
	 * @uses    string  $_GET['text_content']     The RTE content to be displayed.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
	require_once(dirname(__FILE__) . '/../../../../../../../../wp-load.php');

	wp_enqueue_script('post');
	wp_enqueue_media( array( 'post' => $_GET['post_id']) );

	show_admin_bar(false);
?>

<form>
	<?php wp_editor( stripslashes( urldecode( $_GET['text_content']) ) , 'text_content', $settings = array(
		'media_buttons' => true,
		'textarea_rows' => 12,
		'force_p_newlines' => false
	));
	?>
</form>

<?php
	wp_footer();
?>

<style type="text/css">
	/* Here due iframe scope. */
	#text_content_ifr {
		height : 18em;
	}
</style>

<script type="text/javascript">
	jQuery(function ($) {
		// Was needed a timeout since RTE is not initialized when this code run.
		setTimeout(function () {
			for (var i = 0; i < tinymce.editors.length; i++) {
				tinymce.editors[i].onChange.add(function (ed, e) {
					// Update HTML view textarea (that is the one used to send the data to server).
					ed.save();
				});
			}
		}, 1000);
	});
</script>