{{!
	/**
	 * Template to show the WP Content widget on the front end.
	 *
	 * @uses    array   $options                                  A collection of widget options.
	 * @uses    string  $options['additional_classes']            A list of additional space-separated CSS classes that will be applied to the section.
	 *
	 * @uses    string  $the_content           String to search for and replace for the real content. Cannot be a Handlebars expression.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
}}
<div class="row-fluid {{ options.additional_classes }}">
	<div class="span12">
		$the_content
	</div>
</div>