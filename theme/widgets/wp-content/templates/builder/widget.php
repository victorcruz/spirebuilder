<?php
	/**
	 * Template to show the WP Content widget representation in the builder.
	 *
	 * @uses    array   $widget[]                       A collection of widgets along with its data.
	 * @uses    string  $widget['id']                   The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string  $widget['title']                The widget title to be displayed to users.
	 * @uses    string  $widget['description']          The widget description to be displayed to users.
	 *
	 * @uses    array   $widget['icons']                A collection of icons classes or paths to be used in the plugin.
	 * @uses    string  $widget['icons']['builder']     A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-widget-template="builder-<?php echo $widget['id']; ?>" type="text/x-handlebars-template">
	<div id="spirebuilder-widget-{{ id }}" class="row-fluid spirebuilder-widget spirebuilder-widget-<?php echo $widget['id']; ?>" data-widget-type="<?php echo $widget['id']; ?>" data-widget-related-dropzone-id="{{ related_dropzone_id }}" data-widget-related-dropzone-order="{{ related_dropzone_order }}" data-widget-dropzones="{}" data-widget-options="{{ JSON2string options }}">
		<div class="span12 spirebuilder-box spirebuilder-box-folded">
			<div class="spirebuilder-box-header codefield-position-relative">

				<div class="spirebuilder-actions">
					<i class="spirebuilder-control-edit icon icon-pencil tip" title="<?php _e('Edit Options', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
					<i class="spirebuilder-control-save icon icon-save tip" title="<?php _e('Save As Template', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
					<i class="spirebuilder-control-clone icon icon-paste tip" title="<?php _e('Clone', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
					<i class="spirebuilder-control-remove icon icon-remove tip" title="<?php _e('Remove', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip"></i>
				</div>

				<h4>
					<i class="icon <?php echo $widget['icons']['builder']; ?> icon-1x"></i>
					<span class="spirebuilder-widget-field-widget_label">{{ options.widget_label }}</span>
				</h4>
			</div>
		</div>
	</div>
</script>