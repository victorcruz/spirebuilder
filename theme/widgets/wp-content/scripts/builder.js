var SpireBuilderWidgets;
(function (SpireBuilderWidgets) {
    (function (WpContent) {
        var default_context_options = {
            additional_classes: '',
            widget_label: 'Content'
        };
        function getBuilderContext(LOM_section) {
            return jQuery.extend(true, {
            }, {
                id: jQuery.now(),
                options: default_context_options
            }, LOM_section);
        }
        WpContent.getBuilderContext = getBuilderContext;
        function getShowOptionsContext(options) {
            var context = {
            };
            jQuery.extend(true, context, {
                options: default_context_options
            }, options);
            context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');
            return context;
        }
        WpContent.getShowOptionsContext = getShowOptionsContext;
        function getSaveOptionsContext(options) {
            options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');
        }
        WpContent.getSaveOptionsContext = getSaveOptionsContext;
        function setUpdatedContent(context) {
            var $widget = jQuery('#spirebuilder-widget-' + context.id);
            $widget.find('.spirebuilder-widget-field-widget_label').text(context.options.widget_label);
        }
        WpContent.setUpdatedContent = setUpdatedContent;
    })(SpireBuilderWidgets.WpContent || (SpireBuilderWidgets.WpContent = {}));
    var WpContent = SpireBuilderWidgets.WpContent;

})(SpireBuilderWidgets || (SpireBuilderWidgets = {}));

//@ sourceMappingURL=builder.js.map
