<?php
	/**
	 * Template to show the Divider builder options.
	 *
	 * @uses    array   $widget[]                       A collection of widgets along with its data.
	 * @uses    string  $widget['id']                   The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string  $widget['title']                The widget title to be displayed to users.
	 * @uses    string  $widget['description']          The widget description to be displayed to users.
	 *
	 * @uses    array   $widget['icons']                A collection of icons classes or paths to be used in the plugin.
	 * @uses    string  $widget['icons']['builder']     A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-widget-template="options-<?php echo $widget['id']; ?>" type="text/x-handlebars-template">
	<div class="codefield-modal spirebuilder-modal-edit-options modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4><?php _e('Edit ', SpireBuilder::$i18n_prefix); ?>
				<span class="codefield-border-bottom-dashed"></span><?php _e($widget['title'] . ' options', SpireBuilder::$i18n_prefix); ?>
				<small><?php _e($widget['description'], SpireBuilder::$i18n_prefix); ?></small>
			</h4>
		</div>
		<div class="modal-body">
			<div class="codefield-tabs-second-lvl">
				<ul>
					<li>
						<a href="#spirebuilder-widget-options-divider">
							<i class="icon icon-1x <?php echo $widget['icons']['builder']; ?>"></i>
							<?php _e($widget['title'] . ' Options', SpireBuilder::$i18n_prefix); ?>
						</a>
					</li>
					<li>
						<a href="#spirebuilder-widget-options-general">
							<i class="icon icon-1x icon-cogs"></i>
							<?php _e('General Options', SpireBuilder::$i18n_prefix); ?>
						</a>
					</li>
				</ul>

				<form>
					<div id="spirebuilder-widget-options-divider" class="ui-corner-top">
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row">
										<label for="spirebuilder-field-widget-label-{{ id }}">
											<?php _e('Widget Label', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input type="text" id="spirebuilder-field-widget-label-{{ id }}" class="codefield-wide" name="widget_label" value="{{ options.widget_label }}" />
											<span class="description">
												<?php _e('Widget label to be shown in the builder widget representation.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>
									</td>
								</tr>
								<tr>
									<th colspan="2">
										<h2><?php _e('Line Styles', SpireBuilder::$i18n_prefix); ?></h2>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<label for="spirebuilder-style-{{ id }}">
											<?php _e('Type', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<select id="spirebuilder-style-{{ id }}" class="codefield-half-wide" name="style">
												<option value="solid" {{select options.style 'solid'}}><?php _e('Solid', SpireBuilder::$i18n_prefix); ?></option>
												<option value="dashed" {{select options.style 'dashed'}}><?php _e('Dashed', SpireBuilder::$i18n_prefix); ?></option>
												<option value="dotted" {{select options.style 'dotted'}}><?php _e('Dotted', SpireBuilder::$i18n_prefix); ?></option>
											</select>
										</label>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="spirebuilder-color-{{ id }}">
											<?php _e('Color', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input id="spirebuilder-color-{{ id }}" type="text" class="codefield-half-wide codefield-colorpicker-field" value="{{ options.color }}" name="color">
											<div class="spirebuilder-color-selector codefield-position-absolute"></div>
										</label>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="spirebuilder-width-{{ id }}">
											<?php _e('Width', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input id="spirebuilder-width-{{ id }}" type="text" class="codefield-half-wide codefield-text-align-right" value="{{ options.width }}" name="width">
										</label>
									</td>
								</tr>
								<tr>
									<th colspan="2">
										<h2><?php _e('Title Separator', SpireBuilder::$i18n_prefix); ?></h2>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<label for="spirebuilder-use_title_separator-{{ id }}">
											<?php _e('Add title separator', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input id="spirebuilder-use_title_separator-{{ id }}" type="checkbox" name="use_title_separator" value="1" {{checkbox options.use_title_separator}} />
											<span class="description">
												<?php _e('Check to use a title separator over the divider.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="spirebuilder-field-label-{{ id }}">
											<?php _e('Label', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input id="spirebuilder-field-label-{{ id }}" type="text" class="codefield-wide" name="text_label" value="{{ options.text_label }}" />
											<span class="description">
												<?php _e('Text to be displayed in the divider.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="spirebuilder-text-alignment-{{ id }}">
											<?php _e('Alignment', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<select id="spirebuilder-text-alignment-{{ id }}" class="codefield-half-wide" name="alignment">
												<option value="left" {{select options.alignment 'left'}}><?php _e('Left', SpireBuilder::$i18n_prefix); ?></option>
												<option value="center" {{select options.alignment 'center'}}><?php _e('Center', SpireBuilder::$i18n_prefix); ?></option>
												<option value="right" {{select options.alignment 'right'}}><?php _e('Right', SpireBuilder::$i18n_prefix); ?></option>
											</select>
										</label>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="spirebuilder-back-to-top-{{ id }}">
											<?php _e('Back to top link', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input id="spirebuilder-back-to-top-{{ id }}" type="checkbox" name="back_to_top" value="1" {{checkbox options.back_to_top}} />
											<span class="description">
												<?php _e('Add an anchor (link) to the top.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div id="spirebuilder-widget-options-general" class="ui-corner-top">
						<input type="hidden" name="id" value="{{ id }}" />
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row">
										<label for="spirebuilder-field-additional-classes-{{ id }}">
											<?php _e('Additional class names', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<textarea id="spirebuilder-field-additional-classes-{{ id }}" class="codefield-wide" name="additional_classes">{{ options.additional_classes }}</textarea>
											<span class="description">
												<?php _e('One class name per line.', SpireBuilder::$i18n_prefix); ?>
												<br />
												<?php _e('Only the class name, no "', SpireBuilder::$i18n_prefix); ?><strong><?php _e('.', SpireBuilder::$i18n_prefix); ?></strong><?php _e('" prefix is needed.', SpireBuilder::$i18n_prefix); ?>
												<br />
												<?php _e('Add custom ', SpireBuilder::$i18n_prefix); ?>
												<strong><?php _e('CSS', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' classes to style this widget in a different way.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button class="spirebuilder-modal-confirmation-button button button-primary button-large" data-dismiss="modal" onclick="return false;"><?php _e('Ok', SpireBuilder::$i18n_prefix); ?></button>
			&nbsp;
			<button class="button button-large" data-dismiss="modal" onclick="return false;"><?php _e('Close', SpireBuilder::$i18n_prefix); ?></button>
		</div>
	</div>
</script>