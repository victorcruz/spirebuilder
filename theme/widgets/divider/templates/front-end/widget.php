{{!
	/**
	 * Template to show the Divider widget on the front end.
	 *
	 * @uses    string  $type                                     The widget type.
	 *
	 * @uses    array   $options                                  A collection of widget options.
	 * @uses    string  $options['additional_classes']            A list of additional space-separated CSS classes that will be applied to the section.
	 *
	 * @uses    string  $options['style']                         The divider line border style. Can be one of 'solid', 'dotted', 'dashed'.
	 * @uses    string  $options['color']                         The divider line color.
	 * @uses    string  $options['use_title_separator']           If a title separator will be used.
	 * @uses    string  $options['text_label']                    The title separator text.
	 * @uses    string  $options['alignment']                     The title separator alignment. Can be one of 'left', 'center', 'right'.
	 * @uses    string  $options['back_to_top']                   If a 'Back to top' link is added to the title separator.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
}}
<div class="spirebuilder-widget-divider row-fluid {{ options.additional_classes }}">
	<div class="span12">
		<div class="codefield-text-align-{{ options.alignment }} codefield-border-bottom-{{ options.style }}" style="border-width: {{ options.width }}px; border-color: {{ options.color }}">
			{{#if options.use_title_separator}}
				<div>
					{{#if options.back_to_top}}
						<a href="" onclick="return false;">{{ options.text_label }}</a>
					{{else}}
						{{ options.text_label }}
					{{/if}}
				</div>
			{{/if}}
		</div>
	</div>
</div>