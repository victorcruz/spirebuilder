/**
 * Specific widget behaviour on front end.
 *
 * @fileOverview
 */
/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;

/*
 * Launch App code on DOM load.
 * Prevent name collisions wrapping the code in an anonymous function.
 */
jQuery(function ($) {
	// Divider 'Back to top' link behaviour.
	$('body').on('click', '.spirebuilder-widget-divider a', function () {
		$.scrollTo(0, 800, {
			easing: 'swing'
		});
	});
});