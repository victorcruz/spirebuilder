/**
 * Specific widget behaviour on builder actions.
 *
 * @fileOverview
 */
/// <reference path="../../../admin/scripts/CodeField/Utils.d.ts" />
/// <reference path="../../../admin/scripts/builder.d.ts" />

/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;

// Declared and initialized through WordPress.
declare var SpireBuilder_i18n;

/**
 * SpireBuilderWidgets. Builder modules container.
 *
 * @namespace
 * @since 0.1
 * @version 0.1
 */
module SpireBuilderWidgets {
	/**
	 * Divider. Definition for a widget specialized creating horizontal bars to divide content sections.
	 *
	 * @namespace
	 * @since 0.1
	 * @version 0.1
	 */
	export module Divider {
		/*
		 * Declare all used internal variables.
		 */
		var default_context_options = {
			additional_classes : '',
			widget_label       : 'Divider',

			style              : 'solid',
			color              : '#21759B',
			width              : '1',
			use_title_separator: '0',
			text_label         : '↑ Back to Top',
			alignment          : 'center',
			back_to_top        : '0'
		};

		/**
		 * Retrieve the widget builder context to be able to fill the template with the default options set.
		 *
		 * @param   {LOM} LOM_section
		 * @returns {LOM}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getBuilderContext(LOM_section:LOM):LOM {
			// Deep copy.
			return jQuery.extend(
				true,
				{},
				{
					id     : jQuery.now(),
					options: default_context_options
				},
				LOM_section);
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be shown to user.
		 *
		 * @param   {*}   options
		 * @returns {*}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getShowOptionsContext(options:any):any {
			var context:any = {};

			// Deep merge.
			jQuery.extend(true, context, {
				options: default_context_options
			}, options);

			/*
			 * Transforms any option that need to.
			 */
			context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');

			// And last but not least, return the already processed context.
			return context;
		}

		/**
		 * Retrieve the widget options context to be able to fill the template when it will be saved to server.
		 *
		 * @param {*}   options
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getSaveOptionsContext(options:any):void {
			/*
			 * Transforms any option that need to.
			 */
			options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');

			if (options.hasOwnProperty('use_title_separator')) {
				options.use_title_separator = 1;
			}
			else {
				options['use_title_separator'] = 0;
			}

			if (options.hasOwnProperty('back_to_top')) {
				options.back_to_top = 1;
			}
			else {
				options['back_to_top'] = 0;
			}
		}

		/**
		 * Updates widget builder HTML with its specific presentation to resemble its variables.
		 *
		 * @param {*}    context
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setUpdatedContent(context:any):void {
			// Cache references for performance reasons to be used later on.
			var $widget = jQuery('#spirebuilder-widget-' + context.id);

			// Widget label.
			$widget.find('.spirebuilder-widget-field-widget_label').text(context.options.widget_label);

			// Text content.
			$widget.find('.spirebuilder-widget-field-text_content')
				.html(context.options.text_content)
				.removeClass('codefield-text-align-left codefield-text-align-center codefield-text-align-right')
				.addClass('codefield-text-align-' + context.options.alignment);
		}

		/**
		 * Set the widget Edit dialog specific components behaviour.
		 *
		 * @param {*} $filled_template
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setShowOptionsBehaviour($filled_template:any):void {
			// Colorpickers.
			$filled_template.find('.spirebuilder-color-selector').each(function () {
				var $this_colorpicker = jQuery(this);

				$this_colorpicker.hide()
					.prev()
					.css({
						'backgroundColor': $this_colorpicker.prev().val(),
						'color'          : (CodeField.Utils.convertRGB2HSL(CodeField.Utils.convertHex2RGB($this_colorpicker.prev().val()))[2] > 125) ? '#000' : '#FFF'
					});

				jQuery.farbtastic($filled_template.find('[name=color]').next('.spirebuilder-color-selector'))
					.setColor($filled_template.find('[name="color"]').val())
					.linkTo(function (color) {
						$filled_template.find('[name="color"]').css({
							'backgroundColor': color,
							'color'          : (CodeField.Utils.convertRGB2HSL(CodeField.Utils.convertHex2RGB(color))[2] > 125) ? '#000' : '#FFF'
						}).val(color);
					});
			});

			// Checkboxes.
			if (jQuery.fn.hasOwnProperty('iCheckbox')) {
				$filled_template.find('[type="checkbox"]')
					.iCheckbox({
						switch_container_src: SpireBuilder_i18n.plugin_url + 'theme/admin/scripts/jslib/iCheckbox/images/switch-frame.png',
						class_container     : 'codefield-checkbox-switcher-container',
						class_switch        : 'codefield-checkbox-switch',
						class_checkbox      : 'codefield-checkbox-checkbox',
						switch_speed        : 100,
						switch_swing        : -13
					});
			}
		}
	}
}