var jQuery;
var SpireBuilder_i18n;
module SpireBuilderWidgets.Divider {
    export function getBuilderContext(LOM_section: LOM): LOM;
    export function getShowOptionsContext(options: any): any;
    export function getSaveOptionsContext(options: any): void;
    export function setUpdatedContent(context: any): void;
    export function setShowOptionsBehaviour($filled_template: any): void;
}
