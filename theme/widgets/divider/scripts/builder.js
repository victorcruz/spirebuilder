var SpireBuilderWidgets;
(function (SpireBuilderWidgets) {
    (function (Divider) {
        var default_context_options = {
            additional_classes: '',
            widget_label: 'Divider',
            style: 'solid',
            color: '#21759B',
            width: '1',
            use_title_separator: '0',
            text_label: '↑ Back to Top',
            alignment: 'center',
            back_to_top: '0'
        };
        function getBuilderContext(LOM_section) {
            return jQuery.extend(true, {
            }, {
                id: jQuery.now(),
                options: default_context_options
            }, LOM_section);
        }
        Divider.getBuilderContext = getBuilderContext;
        function getShowOptionsContext(options) {
            var context = {
            };
            jQuery.extend(true, context, {
                options: default_context_options
            }, options);
            context.options.additional_classes = context.options.additional_classes.replace(/\s/g, '\r\n');
            return context;
        }
        Divider.getShowOptionsContext = getShowOptionsContext;
        function getSaveOptionsContext(options) {
            options.additional_classes = options.additional_classes.replace(/\r\n/g, ' ');
            if(options.hasOwnProperty('use_title_separator')) {
                options.use_title_separator = 1;
            } else {
                options['use_title_separator'] = 0;
            }
            if(options.hasOwnProperty('back_to_top')) {
                options.back_to_top = 1;
            } else {
                options['back_to_top'] = 0;
            }
        }
        Divider.getSaveOptionsContext = getSaveOptionsContext;
        function setUpdatedContent(context) {
            var $widget = jQuery('#spirebuilder-widget-' + context.id);
            $widget.find('.spirebuilder-widget-field-widget_label').text(context.options.widget_label);
            $widget.find('.spirebuilder-widget-field-text_content').html(context.options.text_content).removeClass('codefield-text-align-left codefield-text-align-center codefield-text-align-right').addClass('codefield-text-align-' + context.options.alignment);
        }
        Divider.setUpdatedContent = setUpdatedContent;
        function setShowOptionsBehaviour($filled_template) {
            $filled_template.find('.spirebuilder-color-selector').each(function () {
                var $this_colorpicker = jQuery(this);
                $this_colorpicker.hide().prev().css({
                    'backgroundColor': $this_colorpicker.prev().val(),
                    'color': (CodeField.Utils.convertRGB2HSL(CodeField.Utils.convertHex2RGB($this_colorpicker.prev().val()))[2] > 125) ? '#000' : '#FFF'
                });
                jQuery.farbtastic($filled_template.find('[name=color]').next('.spirebuilder-color-selector')).setColor($filled_template.find('[name="color"]').val()).linkTo(function (color) {
                    $filled_template.find('[name="color"]').css({
                        'backgroundColor': color,
                        'color': (CodeField.Utils.convertRGB2HSL(CodeField.Utils.convertHex2RGB(color))[2] > 125) ? '#000' : '#FFF'
                    }).val(color);
                });
            });
            if(jQuery.fn.hasOwnProperty('iCheckbox')) {
                $filled_template.find('[type="checkbox"]').iCheckbox({
                    switch_container_src: SpireBuilder_i18n.plugin_url + 'theme/admin/scripts/jslib/iCheckbox/images/switch-frame.png',
                    class_container: 'codefield-checkbox-switcher-container',
                    class_switch: 'codefield-checkbox-switch',
                    class_checkbox: 'codefield-checkbox-checkbox',
                    switch_speed: 100,
                    switch_swing: -13
                });
            }
        }
        Divider.setShowOptionsBehaviour = setShowOptionsBehaviour;
    })(SpireBuilderWidgets.Divider || (SpireBuilderWidgets.Divider = {}));
    var Divider = SpireBuilderWidgets.Divider;

})(SpireBuilderWidgets || (SpireBuilderWidgets = {}));

//@ sourceMappingURL=builder.js.map
