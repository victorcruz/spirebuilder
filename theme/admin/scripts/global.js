/**
 * Global behaviour.
 *
 * @fileOverview
 */
// Prevent name collisions wrapping the code in an anonymous function.
jQuery(function ($) {
	// Solve conflicts between jQueryUI and Twitter Bootstrap libraries.
	CodeField.Utils.noConflict();

	// jQuery Utility function to reverse a collection.
	if (!jQuery.fn.hasOwnProperty('reverse')) {
		jQuery.fn.reverse = [].reverse;
	}

	// Buttons.
	if ($.fn.hasOwnProperty('button')) {
		$('.codefield-button,.codefield-submit-button').button();
	}

	// Tabs.
	if ($.fn.hasOwnProperty('tabs')) {
		$('.codefield-tabs,.codefield-tabs-second-lvl').tabs({
			fx  : {
				duration: 150,
				opacity : 'toggle'
			},
			hide: {
				// jQuery UI 1.10 compatibility.
				duration: 150
			},
			show: {
				// jQuery UI 1.10 compatibility.
				duration: 150
			}
		});
	}

	// Checkboxes.
	if ($.fn.hasOwnProperty('iCheckbox')) {
		$('.codefield-page [type="checkbox"]').iCheckbox({
			switch_container_src: SpireBuilder_i18n.plugin_url + 'theme/admin/scripts/jslib/iCheckbox/images/switch-frame.png',
			class_container     : 'codefield-checkbox-switcher-container',
			class_switch        : 'codefield-checkbox-switch',
			class_checkbox      : 'codefield-checkbox-checkbox',
			switch_speed        : 100,
			switch_swing        : -13
		});
	}

	// Farbtastic.
	$('.spirebuilder-color-selector').hide();

	$('body').on('click', '.codefield-colorpicker-field', function () {
		$(this).next().fadeIn();
	});

	$(document).on('mousedown', function () {
		$('.spirebuilder-color-selector').each(function () {
			var $the_colorpicker = $(this);

			var display = $the_colorpicker.css('display');
			if (display == 'block')
				$the_colorpicker.fadeOut();
		});
	});

	// Dropdowns.
	if ($.fn.hasOwnProperty('chosen')) {
		$('.chzn-select').chosen({ width: '100%' });
	}
});