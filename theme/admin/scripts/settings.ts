/**
 * Behaviour for the Settings page.
 *
 * @fileOverview
 */
/// <reference path="CodeField/Utils.d.ts" />

/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;

/**
 * SpireBuilderSettings.
 *
 * @class
 * @since 0.1
 * @version 0.1
 */
class SpireBuilderSettings {
	/*
	 * Declare all needed global variables.
	 */
	// Contains all the Handlebars compiled templates so it will ease the templates management and utilization.
	private templates_container:TemplatesContainer;

	// Cache a reference to this button to use it in the inner functions. For performance reasons.
	private $message_dashboard:any;

	/**
	 * Initialize all needed variables.
	 *
	 * @constructor
	 */
		constructor() {
		this.templates_container = {};

		// Cache a reference to this button to use it in the inner functions. For performance reasons.
		this.$message_dashboard = jQuery('.codefield-message-dashboard');
	}

	/**
	 * Initialize builder behaviour.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	public init():void {
		// Compile all needed templates.
		this.templates_container = CodeField.Utils.compileTemplates(jQuery('#codefield-templates'));

		/*
		 * Register all needed Handlebars helpers.
		 * Pass it the templates container to register needed helpers from already compiled templates.
		 */
		CodeField.Utils.registerHandlebarsHelpers(this.templates_container);

		// Set the AJAX forms behaviour.
		CodeField.Utils.setWordPressAJAXForms({
			$message_dashboard : this.$message_dashboard,
			templates_container: this.templates_container
		});

		this.setWidgetsFieldsBehavior();
	}

	/**
	 * Initialize widgets fields behaviour.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setWidgetsFieldsBehavior():void {

	}
}

/*
 * Launch App code on DOM load.
 * Prevent name collisions wrapping the code in an anonymous function.
 */
jQuery(function () {
	var spire_builder_settings = new SpireBuilderSettings();
	spire_builder_settings.init();
});