var SpireBuilderSettings = (function () {
    function SpireBuilderSettings() {
        this.templates_container = {
        };
        this.$message_dashboard = jQuery('.codefield-message-dashboard');
    }
    SpireBuilderSettings.prototype.init = function () {
        this.templates_container = CodeField.Utils.compileTemplates(jQuery('#codefield-templates'));
        CodeField.Utils.registerHandlebarsHelpers(this.templates_container);
        CodeField.Utils.setWordPressAJAXForms({
            $message_dashboard: this.$message_dashboard,
            templates_container: this.templates_container
        });
        this.setWidgetsFieldsBehavior();
    };
    SpireBuilderSettings.prototype.setWidgetsFieldsBehavior = function () {
    };
    return SpireBuilderSettings;
})();
jQuery(function () {
    var spire_builder_settings = new SpireBuilderSettings();
    spire_builder_settings.init();
});
//@ sourceMappingURL=settings.js.map
