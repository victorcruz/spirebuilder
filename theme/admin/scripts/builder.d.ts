var jQuery;
var SpireBuilderNonce;
var ajaxurl: string;
var SpireBuilderWidgets;
interface LOMChildrenMap {
    [identifier: number]: LOM;
}
interface LOMDropzoneMap {
    [identifier: number]: LOM;
}
interface LOM {
    id?: number;
    type: string;
    related_dropzone_id: number;
    related_dropzone_order: number;
    dropzones?: LOMDropzoneMap;
    options?: any;
    children?: LOMChildrenMap;
}
class SpireBuilder {
    private LOM: LOM;
    private post_id: number;
    static templates_container: TemplatesContainer;
    private $widget_related_dropzones_little: any;
    private flags: any;
    constructor ();
    public init(): void;
    private setInitialBehaviour(): void;
    private setDefaultLOM(): LOM;
    private setDragAndDrop(): void;
    private setDraggables(): void;
    private setDroppables(): void;
    public hookDropPostWidgetAction(): void;
    private setWidgetActions(): void;
    private setActionsToggleVisibility(): void;
    private setActionWidgetToggle(): void;
    private setActionWidgetRemove(): void;
    private setActionWidgetEdit(): void;
    private setActionWidgetEditSave(): void;
    private setActionWidgetClone(): void;
    private cloneWidgetSection($source: any, $destination: any, is_section: any): void;
    private setActionWidgetAdd(): void;
    private setActionWidgetSaveTemplate(): void;
    private setAjaxForms(): void;
    private setUploadLOM(): void;
    private updateHTML2LOM($LOM_section, is_template?: any): LOM;
    private updateLOM2HTML(): void;
    private createHTMLFromLOM(callee: Function, LOM_section: LOM, self: SpireBuilder, dropzone_little: any): void;
    private createWidgetFragment(LOM_section: any, self?: SpireBuilder, $dropzone_little?: any): any;
    static moveWidgetSection(related_dropzone_id: number, $dropzone_little: any, $cloned_HTML_section): void;
    public updateRelatedDropzoneOrder(): void;
    private setToolbarBehaviour(): void;
    private setFixedToolbar(): void;
    private updateToolbarPosition($toolbar: any): void;
    private setRemoveTemplateBehaviour(): void;
}
