/**
 * Builder definition.
 *
 * @fileOverview
 */
/// <reference path="CodeField/Utils.d.ts" />

/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;

// Declared and initialized through WordPress.
declare var SpireBuilderNonce;

// URL to http://site.com/wp-admin/admin-ajax.php. Declared and initialized through WordPress.
declare var ajaxurl:string;

// Declared and initialized by the builder widgets.
declare var SpireBuilderWidgets;

/*
 * Declare all needed interfaces.
 */
/**
 * LOM children sections.
 *
 * @since 0.1
 * @version 0.1
 */
interface LOMChildrenMap {
	[identifier: number]: LOM;
}

/**
 * Dropzones.
 *
 * @since 0.1
 * @version 0.1
 */
interface LOMDropzoneMap {
	[identifier: number]: LOM;
}

/**
 * LOM.
 *
 * @since 0.1
 * @version 0.1
 */
interface LOM {
	id?: number;
	type: string;

	// Dropzone to which the widget is related with.
	related_dropzone_id : number;

	// Widget order inside the related dropzone.
	related_dropzone_order: number;

	// The identifiers for the widget dropzones.
	dropzones?: LOMDropzoneMap;

	// Widget specific options. Only understandable by the specific widget.
	options ?: any;

	// Further children for this LOM section.
	children?: LOMChildrenMap;
}

/**
 * SpireBuilder.
 *
 * @class
 * @since 0.1
 * @version 0.1
 */
class SpireBuilder {
	/*
	 * Declare all needed global variables.
	 */
	private LOM:LOM;

	private post_id:number;

	// Contains all the Handlebars compiled templates so it will ease the templates management and utilization.
	static templates_container:TemplatesContainer;

	// Due jQuery UI bug that not allow us to select correctly the related widget dropzones at dragstop event by using closures.
	private $widget_related_dropzones_little:any;

	private flags:any;

	/**
	 * Initialize all needed variables.
	 *
	 * @constructor
	 */
		constructor() {
		this.LOM = this.setDefaultLOM();

		// Get post id from WordPress fields.
		this.post_id = jQuery('#post_ID').val();

		SpireBuilder.templates_container = {};

		this.flags = {
			first_load: true
		};
	}

	/**
	 * Initialize builder behaviour.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	public init():void {
		// Compile all needed templates.
		SpireBuilder.templates_container = CodeField.Utils.compileTemplates(jQuery('#codefield-templates'));

		/*
		 * Register all needed Handlebars helpers.
		 * Pass it the templates container to register needed helpers from already compiled templates.
		 */
		CodeField.Utils.registerHandlebarsHelpers(SpireBuilder.templates_container);

		// Set all needed behaviour for AJAX forms.
		this.setAjaxForms();

		// Set all needed behaviour for widgets actions.
		this.setUploadLOM();

		// Recreate the saved LOM into the builder editor.
		this.updateLOM2HTML();

		// Set all needed behaviour for widgets actions.
		this.setWidgetActions();

		// Set toolbar behaviour.
		this.setToolbarBehaviour();

		// Set the initial behaviour.
		this.setInitialBehaviour();
	}

	/**
	 * Sets all the initial behaviour.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setInitialBehaviour():void {
		// Selects the All tab.
		jQuery('#spirebuilder-builder-toolbar', '.codefield-page').tabs('option', 'active', 2);
	}

	/**
	 * Resets to default LOM.
	 *
	 * @returns {LOM}
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setDefaultLOM():LOM {
		return {
			id  : 0,
			type: 'root',

			related_dropzone_id   : 0,
			related_dropzone_order: 0,

			options : {},
			children: <LOMChildrenMap> {}
		};
	}

	/**
	 * Wrap all Drag-n'-Drop functions.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setDragAndDrop():void {
		this.setDraggables();
		this.setDroppables();
	}

	/**
	 * Set all draggables behaviour here.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setDraggables():void {
		// Cache a reference to this class to use it in the inner functions.
		var self:SpireBuilder = this;

		// Widgets in the toolbar.
		jQuery('.codefield-draggable', '.spirebuilder-widgets-list').draggable({
			helper: function () {
				var $helper = jQuery(this).clone();

				if ($helper.hasClass('spirebuilder-widget-template')) {
					$helper.find('.badge').remove();
				}

				return $helper
					.removeClass().not('.spirebuilder-widget-template')
					.addClass('spirebuilder-toolbar-item-helper');
			},
			revert: 'invalid',
			stack : '.spirebuilder-widget',
			zIndex: 450
		});

		// Widgets added in the content.
		jQuery('.spirebuilder-widget', '#spirebuilder-editzone').each(function () {
			var $this_widget = jQuery(this);

			$this_widget.draggable({
				delay : 50,
				handle: jQuery(this).children().children('.spirebuilder-box-header'),
				helper: 'clone',
				cursor: 'move',
				revert: 'invalid',
				start : function (event, ui) {
					// Change helper styles to improve usability.
					ui.helper.css({
						opacity: 0.7
					});

					// Fix the helper width when dragging. The fix is only for inner widgets not for the root widgets.
					if ($this_widget.parentsUntil('#spirebuilder-editzone', '.spirebuilder-widget').length) {
						ui.helper.css({
							width: 'inherit'
						});
					}

					// Change original element styles to improve usability.
					$this_widget.addClass('spirebuilder-widget-draggable-placeholder');

					if ($this_widget.prev().hasClass('spirebuilder-dropzone-little')) {
						// Widget is in a middle or bottom position.
						self.$widget_related_dropzones_little = $this_widget.prev();
					}
					else {
						// Widget is at top position.
						self.$widget_related_dropzones_little = $this_widget.prevAll('.spirebuilder-dropzone-little').eq(0);
					}
					self.$widget_related_dropzones_little = self.$widget_related_dropzones_little.add($this_widget.next());

					self.$widget_related_dropzones_little
						.css('visibility', 'hidden')
						.droppable('disable');
				},
				stop  : function () {
					$this_widget.removeClass('spirebuilder-widget-draggable-placeholder');

					self.$widget_related_dropzones_little
						.css('visibility', 'visible')
						.droppable('enable');
				},
				stack : '.spirebuilder-widget',
				zIndex: 500
			});
		});
	}

	/**
	 * Set all droppabbles behaviour here.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setDroppables():void {
		// Cache a reference to this class to use it in the inner functions.
		var self:SpireBuilder = this;

		/*
		 * Prevent adding the dropzones contained in the templates section.
		 * Cache references for performance reasons to be used later on.
		 */
		var $valid_dropzones = jQuery('.spirebuilder-dropzone, .spirebuilder-dropzone-little', '#spirebuilder-editzone');

		$valid_dropzones.droppable({
			accept     : '.spirebuilder-widgets-list li a, .spirebuilder-widget',

			// Droppables styling for user experience.
			activeClass: 'codefield-bg-yellow',
			hoverClass : 'codefield-bg-green',

			// Don't let outer parent droppables to capture the event also.
			greedy     : true,

			// Consider a dropover if the mouse pointer is entering the droppable.
			tolerance  : 'pointer',

			// On dropout event, to change mouse cursor with for the proper visual clues.
			over       : function () {
				var $this_dropzone = jQuery(this);

				jQuery('.ui-draggable-dragging').addClass('codefield-cursor-dragging-over');

				if ($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
					$this_dropzone.children().animate({
						'width': '100%'
					}, {
						duration: 200,
						queue   : false
					});
				}
			},

			// On dropout event, to change mouse cursor with for the proper visual clues.
			out        : function () {
				var $this_dropzone = jQuery(this);

				jQuery('.ui-draggable-dragging').removeClass('codefield-cursor-dragging-over');

				if ($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
					$this_dropzone.children().animate({
						'width': 21
					}, {
						duration: 200,
						queue   : false
					});
				}
			},

			// On drop event, to create the new widgets on the editzone.
			drop       : function (event, ui) {
				// Cache references for performance reasons to be used later on.
				var $this_dropzone = jQuery(this);

				if ($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
					$this_dropzone.children().animate({
						'width': 21
					}, {
						duration: 200,
						queue   : false
					});
				}

				// Cache the widget id so it can be used later on to access the widget DOM.
				var widget_type:string = jQuery(ui.helper).attr('data-widget-template');

				// Check whether is a new widget to be added into the DOM or an already created one.
				if (ui.draggable.hasClass('spirebuilder-widget')) {
					/*
					 * Is an already created one.
					 */
					var $cloned_HTML_section = ui.draggable.clone();

					/*
					 * Update top-level dropzone connector related variables according with the new position.
					 * Only the related dropzone id since the order will updated later.
					 */
					if ($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
						var related_dropzone_id = $this_dropzone.parent().children('.spirebuilder-dropzone').attr('id').split('spirebuilder-dropzone-')[1];
					}
					else {
						var related_dropzone_id = $this_dropzone.attr('id').split('spirebuilder-dropzone-')[1];
					}

					$cloned_HTML_section.attr('data-widget-related-dropzone-id', related_dropzone_id).hide();

					// Move the HTML section to its new place.
					SpireBuilder.moveWidgetSection(
						related_dropzone_id,
						($this_dropzone.hasClass('spirebuilder-dropzone-little')) ? $this_dropzone : false,
						$cloned_HTML_section);

					/*
					 * Remove the old widget HTML section.
					 */
					// Cache references for performance reasons to be used later on.
					var $widget = ui.draggable;
					var $prev_dropzone = $widget.prev('.spirebuilder-dropzone');

					/*
					 * If the previous dropzone not was the main one or was the main one and there is no widgets then show the main dropzone.
					 * <= 1: Since the widget is being removed after this check.
					 */
					if ($prev_dropzone.attr('data-widget-id') != '0' && $prev_dropzone.nextAll('.spirebuilder-widget').length <= 2
						|| $prev_dropzone.attr('data-widget-id') == '0' && jQuery('#spirebuilder-editzone').children('.spirebuilder-widget').length <= 2) {
						// Hide the little dropzone after the widget.
						$prev_dropzone.prev('.spirebuilder-dropzone-little').hide();

						// Enable dropzone occupied for this widget before it was added so it can be used again.
						$prev_dropzone.droppable('enable').show('blind');
					}

					/*
					 * And last but not least, remove the widget and from screen.
					 * Also remove the little dropzone after the widget.
					 */
					$widget.add($widget.next('.spirebuilder-dropzone-little')).hide('blind', function () {
						// In a callback due a jQuery bug that lets trash elements behind if the animation doesn't complete.
						jQuery(this).remove();
					});
				}
				else if (ui.draggable.hasClass('spirebuilder-widget-template')) {
					/*
					 * Is a template to be added.
					 */
					var data_widget_template_LOM = ui.draggable.attr('data-widget-template-lom');

					/*
					 * Regenerate template ids so they not clash with existent widgets.
					 */
					data_widget_template_LOM = data_widget_template_LOM.replace(/TEMPLATE/g, <Function>jQuery.now());

					data_widget_template_LOM = jQuery.parseJSON(data_widget_template_LOM);

					/*
					 * Top level widgets: set the correct related dropzone id to the one that this template was being dropped into.
					 * If the related dropzone is a little one, then set a special code and pass in a reference to the little one so can be added in its
					 * correct place.
					 */
					if ($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
						var related_dropzone_id = -1;
					}
					else {
						var related_dropzone_id = $this_dropzone.attr('id').split('spirebuilder-dropzone-')[1];
					}
					for (var widget in data_widget_template_LOM.children) {
						if (data_widget_template_LOM.children.hasOwnProperty(widget)) {
							data_widget_template_LOM.children[widget].related_dropzone_id = related_dropzone_id;
						}
					}

					/*
					 * The saved template LOM_section is the initial seed for the recursive function.
					 * Its needed to pass a reference to this class since the Z combinator rewrites the inner scope function.
					 */
					CodeField.Utils.Z(self.createHTMLFromLOM)(
						data_widget_template_LOM,
						self,
						($this_dropzone.hasClass('spirebuilder-dropzone-little')) ? $this_dropzone : false
					);
				}
				else {
					/*
					 * Is a new widget to be added.
					 */
					/*
					 * If the widget was dropped into a little dropzone, adjusts the needed references.
					 */
					var $dropzone;

					if ($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
						// Cache references for performance reasons to be used later on.
						$dropzone = $this_dropzone.parent().children('.spirebuilder-dropzone').first();
					}
					else {
						/*
						 * Was dropped into a normal droppable.
						 */
						// Cache references for performance reasons to be used later on.
						$dropzone = $this_dropzone;
					}

					/*
					 * Create the widget fragment to be appended to the DOM.
					 */
					var $inserted_widget = self.createWidgetFragment({
						type: widget_type,

						related_dropzone_id   : $dropzone.attr('id').split('spirebuilder-dropzone-')[1],
						related_dropzone_order: $dropzone.parent().children('.spirebuilder-widget').length,

						// Since this widget is a fresh one it will not have any options.
						options               : {}
					}, self, ($this_dropzone.hasClass('spirebuilder-dropzone-little')) ? $this_dropzone : false);

					/*
					 * Launch the Edit modal dialog on the new added widget.
					 * The timeout is for let the 'show' animation to be completed before show the modal dialog.
					 */
					if (jQuery('#spirebuilder-edit-modal').val() == 1 && jQuery.inArray(widget_type, jQuery('#spirebuilder-edit-modal-exclusions').val().split(',')) == -1) {
						setTimeout(function () {
							$inserted_widget.find('.spirebuilder-box .spirebuilder-control-edit').trigger('click');

						}, 150);
					}
				}

				// Update the widgets dropzone order.
				self.updateRelatedDropzoneOrder();

				// Call the global hook for post widget drop action.
				self.hookDropPostWidgetAction();
			}
		});
	}

	/**
	 * Hook: Execute global actions after widget specific ones on drop into dropzone.
	 * Mainly used for add behaviour to newly created elements.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	public hookDropPostWidgetAction():void {
		var self:SpireBuilder = this;

		var is_max_chars_enabled = jQuery('#spirebuilder-widget-content-max-chars').val() != 'all';

		if (is_max_chars_enabled) {
			var max_height = jQuery('#spirebuilder-widget-content-max-chars-restricted-to').val();
			var $text_content = jQuery('.spirebuilder-widget-field-text_content');

			$text_content.expander('destroy');

			$text_content.expander({
				collapseEffect: 'slideUp',
				collapseSpeed : 200,

				expandEffect: 'slideDown',
				expandSpeed : 200,
				expandText  : 'More',

				slicePoint      : max_height,
				userCollapseText: 'Less',
				widow           : 5
			});
		}

		// Refresh drag-n'-drop with all the new added ones.
		this.setDragAndDrop();

		// Refresh tooltips with all the new added ones.
		if (jQuery.fn.hasOwnProperty('tooltip')) {
			jQuery('.tip').tooltip();
		}

		// Due jQuery UI animation bug, it need to be finished when this run since its throws errors since its including the dragging widgets as well.
		setTimeout(function () {
			// Ensures it will not be triggered on first load causing a useless server request.
			if (self.flags.first_load) {
				self.flags.first_load = false;
			}
			else {
				// Autosave the changes.
				jQuery('.spire-builder-upload-lom').first().trigger('click');
			}
		}, 1000);
	}

	/**
	 * Wrap all widget actions behaviour here.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setWidgetActions():void {
		// General actions bar behaviour.
		this.setActionsToggleVisibility();

		// Set each action behaviour.
		this.setActionWidgetToggle();
		this.setActionWidgetRemove();

		this.setActionWidgetEdit();
		this.setActionWidgetEditSave();

		this.setActionWidgetClone();
		this.setActionWidgetSaveTemplate();

		this.setActionWidgetAdd();
	}

	/**
	 * Widget Action: Toggle minimize/maximize widget.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setActionsToggleVisibility():void {
		// Cache references for performance reasons to be used later on.
		var $body = jQuery('body');

		// Initial state.
		jQuery('.spirebuilder-actions').css('visibility', 'hidden');

		$body.on('mouseover', '#spirebuilder-editzone .spirebuilder-widget', function (event) {
			//  Ensures that the ancestor actions are hidden.
			jQuery('.spirebuilder-actions').css('visibility', 'hidden');

			// Only show this widget actions bar.
			jQuery(this).children().children('.spirebuilder-box-header').find('.spirebuilder-actions').css('visibility', 'visible');

			// Only allow event triggering on this element, not on the ancestors.
			event.stopPropagation();
		});

		$body.on('mouseleave', '#spirebuilder-editzone .spirebuilder-widget', function (event) {
			jQuery('.spirebuilder-actions').css('visibility', 'hidden');

			// Only allow event triggering on this element, not on the ancestors.
			event.stopPropagation();
		});
	}

	/**
	 * Widget Action: Toggle minimize/maximize widget.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setActionWidgetToggle():void {
		jQuery('body').on('click', '.spirebuilder-box .spirebuilder-control-drop', function () {
			var $this_button = jQuery(this);

			// Cache references for performance reasons to be used later on.
			var $box = $this_button.closest('.spirebuilder-box');
			var $box_content = $box.find('.spirebuilder-box-content');

			if ($this_button.hasClass('icon-chevron-up')) {
				$this_button.removeClass('icon-chevron-up').addClass('icon-chevron-down');

				$box.removeClass('spirebuilder-box-unfolded').addClass('spirebuilder-box-folded');
				$box_content.slideUp(200);
			}
			else {
				$this_button.removeClass('icon-chevron-down').addClass('icon-chevron-up');

				$box.removeClass('spirebuilder-box-folded').addClass('spirebuilder-box-unfolded');
				$box_content.slideDown(200);
			}
		});
	}

	/**
	 * Widget Action: Remove widget.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setActionWidgetRemove():void {
		var $filled_template;
		var $widget;
		var $prev_dropzone;
		var $body = jQuery('body');

		$body.on('click', '.spirebuilder-box .spirebuilder-control-remove', function () {
			// Cache references for performance reasons to be used later on.
			$widget = jQuery(this).closest('.spirebuilder-widget');
			$prev_dropzone = $widget.prev('.spirebuilder-dropzone');

			// Fill the widget template by passing the data to the knowledgeable specific function.
			$filled_template = jQuery(SpireBuilder.templates_container['modal-remove']({
				object: 'widget'
			}));

			// Finally, launch the modal with the filled template.
			$filled_template.modal().on('hidden', function () {
				jQuery(this).remove();
			});
		});

		$body.on('click', '.codefield-modal-button-remove-widget', function () {
			/*
			 * If the previous dropzone not was the main one or was the main one and there is no widgets then show the main dropzone.
			 * <= 1: Since the widget is being removed after this check.
			 */
			if ($prev_dropzone.attr('data-widget-id') != '0' && $prev_dropzone.nextAll('.spirebuilder-widget').length <= 1
				|| $prev_dropzone.attr('data-widget-id') == '0' && jQuery('#spirebuilder-editzone').children('.spirebuilder-widget').length <= 1) {
				// Hide the little dropzone after the widget.
				$prev_dropzone.prev('.spirebuilder-dropzone-little').hide();

				// Enable dropzone occupied for this widget before it was added so it can be used again.
				$prev_dropzone.droppable('enable').show('blind');
			}

			/*
			 * And last but not least, remove the widget and from screen.
			 * Also remove the little dropzone after the widget.
			 */
			$widget.add($widget.next('.spirebuilder-dropzone-little')).hide('blind', function () {
				// In a callback due a jQuery bug that lets trash elements behind if the animation doesn't complete.
				jQuery(this).remove();

				// Autosave the changes. Inside callback due racing condition.
				jQuery('.spire-builder-upload-lom').first().trigger('click');
			});
		});
	}

	/**
	 * Widget Action: Show edit widget options.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setActionWidgetEdit():void {
		jQuery('body').on('click', '.spirebuilder-box .spirebuilder-control-edit', function () {
			// Cache references for performance reasons to be used later on.
			var $this_button = jQuery(this);

			var widget_type:string = $this_button.closest('.spirebuilder-widget').attr('data-widget-type');

			// Used to merge the context between new and recreated widgets.
			var context = {
				id     : $this_button.closest('.spirebuilder-widget').attr('id').split('spirebuilder-widget-')[1],
				options: jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-options'))
			};

			/*
			 * A widget is being recreated so generate its options (with its default ones).
			 * Use for this the specific widget related function.
			 */
			context = SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].getShowOptionsContext(context);

			// Fill the widget template by passing the data to the knowledgeable specific function.
			var $filled_template = jQuery(SpireBuilder.templates_container['options-' + widget_type](context));

			// Build all advanced components used.
			$filled_template.find('.codefield-tabs, .codefield-tabs-second-lvl').tabs({
				collapsible: false,
				fx         : {
					duration: 150,
					opacity : 'toggle'
				},
				hide       : {
					// jQuery UI 1.10 compatibility.
					duration: 150
				},
				show       : {
					// jQuery UI 1.10 compatibility.
					duration: 150
				}
			});

			// Set widget specific Edit dialog behaviour, if any.
			if (SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('setShowOptionsBehaviour')) {
				SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].setShowOptionsBehaviour($filled_template);
			}

			// Finally, launch the modal with the filled template.
			jQuery($filled_template).modal({
				backdrop: 'static'
			}).on('hidden', function () {
					jQuery(this).remove();
				});
		});
	}

	/**
	 * Widget Action: Save edit widget options.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setActionWidgetEditSave():void {
		// Cache a reference to this class to use it in the inner functions. For performance reasons.
		var self:SpireBuilder = this;

		jQuery('body').on('click', '.codefield-modal .spirebuilder-modal-confirmation-button', function () {
			// Cache references for performance reasons to be used later on.
			var $codefield_modal = jQuery(this).closest('.codefield-modal');

			// Get some needed variables from the related widget so the processing can be made.
			var widget_id = $codefield_modal.find('input[name="id"]').val();

			// Cache references for performance reasons to be used later on.
			var $widget = jQuery('#spirebuilder-widget-' + widget_id);

			var widget_type:string = $widget.attr('data-widget-type');

			// Used to merge the context between new and recreated widgets.
			var serialized_options_array = $codefield_modal.closest('.codefield-modal').find('form').serializeArray();

			var options = {};

			// Transform the serialized options array into the context object format.
			jQuery.each(serialized_options_array, function (index, element) {
				// Search for nested fields.
				if (element.name.split('.').length > 1) {
					if (!options.hasOwnProperty(element.name.split('.')[0])) {
						options[element.name.split('.')[0]] = {};
					}

					options[element.name.split('.')[0]][element.name.split('.')[1]] = element.value;
				}
				else {
					options[element.name] = element.value;
				}
			});

			/*
			 * Check whether the modal has an inner iframe.
			 * If yes then add the inner fields to the context.
			 */
			if ($codefield_modal.find('iframe').length > 0) {
				$codefield_modal.find('iframe').each(function () {
					var serialized_options = jQuery('form', jQuery(this).contents()).serializeArray();

					// Transform the serialized options array into the context object format.
					jQuery.each(serialized_options, function (index, element) {
						// Search for nested fields.
						if (element.name.split('.').length > 1) {
							options[element.name.split('.')[0]] = {};
							options[element.name.split('.')[0]][element.name.split('.')[1]] = element.value;
						}
						else {
							options[element.name] = element.value;
						}
					});
				});
			}

			/*
			 * Compute the merged context to save it into the widget options.
			 * Use for this the specific widget related function.
			 */
			if (SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('getSaveOptionsContext')) {
				SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].getSaveOptionsContext(options);
			}

			var options_context = {};
			// Deep merge.
			jQuery.extend(
				true,
				options_context,
				JSON.parse($widget.attr('data-widget-options')),
				options);

			// Intermediate variable for the sake of clarity.
			var context = {
				id     : widget_id,
				options: options_context
			};

			// Save the widget context to the widget options.
			$widget.attr('data-widget-options', JSON.stringify(context.options));

			// Finally, updates any widget specific variables.
			if (SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('setUpdatedContent')) {
				SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].setUpdatedContent(context);
			}

			self.hookDropPostWidgetAction();
		});
	}

	/**
	 * Widget Action: Save edit widget options.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setActionWidgetClone():void {
		// Cache a reference to this class to use it in the inner functions.
		var self:SpireBuilder = this;

		jQuery('body').on('click', '.spirebuilder-box .spirebuilder-control-clone', function () {
			// Cache references for performance reasons to be used later on.
			var $this_button = jQuery(this);

			$this_button.tooltip('hide');

			// Clone the widget.
			self.cloneWidgetSection(
				$this_button.closest('.spirebuilder-widget') /* $source */,
				$this_button /* $destination*/,
				false /* is_section */);
		});
	}

	/**
	 * Clone a widget section with the provided params.
	 *
	 * @param   {*} $source
	 * @param   {*} $destination
	 * @param   {*} is_section
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private cloneWidgetSection($source:any, $destination:any, is_section:any):void {
		// Cache a reference to this class to use it in the inner functions.
		var self:SpireBuilder = this;

		// Clone the widget.
		var $cloned_HTML_section = $source.clone().hide();

		/*
		 * Only regenerate cloned root widget if any.
		 */
		if (!is_section) {
			// Update parent widget data.
			var new_widget_attributes = {
				// Regenerate the id to prevent clashing with progenitor.
				id                     : 'spirebuilder-widget-' + jQuery.now(),

				// The related_dropzone_id for the parent cloned widget remains the same.

				// To be updated later on.
				'data-widget-dropzones': {}
			};

			// Cache dropzones ids to check later whether the children belongs to a dropzone or not to update its correct related_dropzone_id.
			var old_dropzones_id = jQuery.parseJSON($cloned_HTML_section.attr('data-widget-dropzones'));

			// Append index to dropzone id to ensure uniqueness.
			var index = 0;
			for (var dropzone_id in old_dropzones_id) {
				if (old_dropzones_id.hasOwnProperty(dropzone_id)) {
					var new_dropzone_id = jQuery.now() + '-' + index;

					new_widget_attributes['data-widget-dropzones'][dropzone_id] = new_dropzone_id;

					// Update the dropzone with its new id.
					$cloned_HTML_section.find('#spirebuilder-dropzone-' + old_dropzones_id[dropzone_id]).attr('id', 'spirebuilder-dropzone-' + new_dropzone_id);

					index += 1;
				}
			}

			// Convert to JSON string to being able to save it to attribute.
			new_widget_attributes['data-widget-dropzones'] = JSON.stringify(new_widget_attributes['data-widget-dropzones']);

			// Finally, update its attributes.
			$cloned_HTML_section.attr(new_widget_attributes);

			var widget_type:string = $cloned_HTML_section.attr('data-widget-type');
			// Finally, updates any widget specific variables.
			if (SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('setUpdatedClone')) {
				SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].setUpdatedClone($cloned_HTML_section);
			}
		}

		/*
		 * There are widgets so process them.
		 * Flatten the tree layout to ease the parsing by iterating though the widgets instead the tree.
		 */
		$cloned_HTML_section.find('.spirebuilder-widget').each(function () {
			// Cache a reference to this button to use it in the inner functions.
			var $the_widget = jQuery(this);

			/*
			 * Update widget ancestor state on the LOM.
			 * Do this iterating though the widget ancestor chain.
			 */
			$the_widget.parentsUntil($cloned_HTML_section, '.spirebuilder-widget').reverse().each(function () {
				/*
				 * Cache a reference to this button to use it in the inner functions.
				 * It's ancestor instead of parent since we don't are sure that this is the actual parent.
				 */
				var $the_ancestor = jQuery(this);

				// Update parent widget data.
				var new_widget_attributes = {
					// Regenerate the id to prevent clashing with progenitor.
					id                               : 'spirebuilder-widget-' + jQuery.now(),

					// The related_dropzone_id for the parent cloned widget remains the same.
					'data-widget-related-dropzone-id': $the_ancestor.parent().children('.spirebuilder-dropzone').attr('id').split('spirebuilder-dropzone-')[1],

					// To be updated later on.
					'data-widget-dropzones'          : {}
				};

				// Cache dropzones ids to check later whether the children belongs to a dropzone or not to update its correct related_dropzone_id.
				var old_dropzones_id = jQuery.parseJSON($the_ancestor.attr('data-widget-dropzones'));

				// Append index to dropzone id to ensure uniqueness.
				var index = 0;
				for (var dropzone_id in old_dropzones_id) {
					if (old_dropzones_id.hasOwnProperty(dropzone_id)) {
						var new_dropzone_id = jQuery.now() + '-' + index;

						new_widget_attributes['data-widget-dropzones'][dropzone_id] = new_dropzone_id;

						// Update the dropzone with its new id.
						$the_ancestor.find('#spirebuilder-dropzone-' + old_dropzones_id[dropzone_id]).attr('id', 'spirebuilder-dropzone-' + new_dropzone_id);

						index += 1;
					}
				}

				// Convert to JSON string to being able to save it to attribute.
				new_widget_attributes['data-widget-dropzones'] = JSON.stringify(new_widget_attributes['data-widget-dropzones']);

				// Finally, update its attributes.
				$the_ancestor.attr(new_widget_attributes);

				var widget_type = $the_ancestor.attr('data-widget-type');
				// Finally, updates any widget specific variables.
				if (SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('setUpdatedClone')) {
					SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].setUpdatedClone($the_ancestor);
				}
			});

			// Update parent widget data.
			var new_widget_attributes = {
				// Regenerate the id to prevent clashing with progenitor.
				id                               : 'spirebuilder-widget-' + jQuery.now(),

				// The related_dropzone_id for the parent cloned widget remains the same.
				'data-widget-related-dropzone-id': $the_widget.parent().children('.spirebuilder-dropzone').attr('id').split('spirebuilder-dropzone-')[1],

				// To be updated later on.
				'data-widget-dropzones'          : {}
			};

			// Cache dropzones ids to check later whether the children belongs to a dropzone or not to update its correct related_dropzone_id.
			var old_dropzones_id = jQuery.parseJSON($the_widget.attr('data-widget-dropzones'));

			// Append index to dropzone id to ensure uniqueness.
			var index = 0;
			for (var dropzone_id in old_dropzones_id) {
				if (old_dropzones_id.hasOwnProperty(dropzone_id)) {
					var new_dropzone_id = jQuery.now() + '-' + index;

					new_widget_attributes['data-widget-dropzones'][dropzone_id] = new_dropzone_id;

					// Update the dropzone with its new id.
					$the_widget.find('#spirebuilder-dropzone-' + old_dropzones_id[dropzone_id]).attr('id', 'spirebuilder-dropzone-' + new_dropzone_id);

					index += 1;
				}
			}

			// Convert to JSON string to being able to save it to attribute.
			new_widget_attributes['data-widget-dropzones'] = JSON.stringify(new_widget_attributes['data-widget-dropzones']);

			// Finally, update its attributes.
			$the_widget.attr(new_widget_attributes);

			var widget_type = $the_widget.attr('data-widget-type');
			// Finally, updates any widget specific variables.
			if (SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('setUpdatedClone')) {
				SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].setUpdatedClone($the_widget);
			}
		});

		if (!is_section) {
			/*
			 * Is a standalone widget so insert it after the current widget.
			 */
			// Insert the already cloned element after the dropzone little related to the progenitor widget.
			$destination.closest('.spirebuilder-widget').next().after($cloned_HTML_section);

			var $inserted_widget = $destination.closest('.spirebuilder-widget').next().next();
		}
		else {
			/*
			 * Is a widget collection so append it to the destination.
			 */
			// Insert the already cloned element after the dropzone little related to the progenitor widget.
			$destination.append($cloned_HTML_section.unwrap());

			var $inserted_widget = $destination.children();
		}

		// Show the already inserted widget with a nice effect.
		$inserted_widget.show('fade');

		// Add the bottom little dropzone after the already added widget.
		$inserted_widget.after(SpireBuilder.templates_container['dropzone-little']());

		// Show the after widget little dropzone.
		$inserted_widget.next('.spirebuilder-dropzone-little').removeClass('codefield-display-none');

		// Update the widgets dropzone order.
		self.updateRelatedDropzoneOrder();

		// Call the global hook for post widget drop action.
		self.hookDropPostWidgetAction();
	}

	/**
	 * Widget Action: Show add widget dialog.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setActionWidgetAdd():void {
		// Cache a reference to this class to use it in the inner functions.
		var self:SpireBuilder = this;
		var $body = jQuery('body');

		// Cache a reference to be used on all the handlers.
		var $the_dropzone;
		var $filled_template;

		$body.on('click', '.spirebuilder-dropzone-little, .spirebuilder-dropzone', function () {
			// Update the reference to be used in the other handlers.
			$the_dropzone = jQuery(this);

			// Fill the widget template by passing the data to the knowledgeable specific function.
			$filled_template = jQuery(SpireBuilder.templates_container['dialog-widget-list']({}));

			// Finally, launch the modal with the filled template.
			$filled_template.find('.codefield-tabs, .codefield-tabs-second-lvl').tabs({
				collapsible: false,
				fx         : {
					duration: 150,
					opacity : 'toggle'
				},
				hide       : {
					// jQuery UI 1.10 compatibility.
					duration: 150
				},
				show       : {
					// jQuery UI 1.10 compatibility.
					duration: 150
				}
			});

			$filled_template.find('.spirebuilder-widgets-list').slimscroll({
				railVisible: true,
				height     : '400px'
			});

			$filled_template.modal().on('hidden', function () {
				jQuery(this).remove();
			});
		});

		$body.on('click', '#spirebuilder-dialog-widget-list .spirebuilder-widgets-list > li a', function () {
			// Cache references for performance reasons to be used later on.
			var $this_widget = jQuery(this);

			/*
			 * If the widget was dropped into a little dropzone, adjusts the needed references.
			 */
			if ($the_dropzone.hasClass('spirebuilder-dropzone-little')) {
				// Cache references for performance reasons to be used later on.
				var $dropzone = $the_dropzone.parent().children('.spirebuilder-dropzone').first();
			}
			else {
				/*
				 * Was dropped into a normal droppable.
				 */
				// Cache references for performance reasons to be used later on.
				var $dropzone = $the_dropzone;
			}

			// Cache the widget id so it can be used later on to access the widget DOM.
			var widget_type:string = $this_widget.attr('data-widget-template');

			if (widget_type == 'template') {
				/*
				 * Is a template to be added.
				 */
				var data_widget_template_LOM = $this_widget.attr('data-widget-template-lom');

				/*
				 * Regenerate template ids so they not clash with existent widgets.
				 */
				data_widget_template_LOM = data_widget_template_LOM.replace(/TEMPLATE/g, <Function>jQuery.now());

				data_widget_template_LOM = jQuery.parseJSON(data_widget_template_LOM);

				/*
				 * Top level widgets: set the correct related dropzone id to the one that this template was being dropped into.
				 * If the related dropzone is a little one, then set a special code and pass in a reference to the little one so can be added in its
				 * correct place.
				 */
				if ($the_dropzone.hasClass('spirebuilder-dropzone-little')) {
					var related_dropzone_id = -1;
				}
				else {
					var related_dropzone_id = $the_dropzone.attr('id').split('spirebuilder-dropzone-')[1];
				}

				for (var widget in data_widget_template_LOM.children) {
					if (data_widget_template_LOM.children.hasOwnProperty(widget)) {
						data_widget_template_LOM.children[widget].related_dropzone_id = related_dropzone_id;
					}
				}

				/*
				 * The saved template LOM_section is the initial seed for the recursive function.
				 * Its needed to pass a reference to this class since the Z combinator rewrites the inner scope function.
				 */
				CodeField.Utils.Z(self.createHTMLFromLOM)(
					data_widget_template_LOM,
					self,
					($the_dropzone.hasClass('spirebuilder-dropzone-little')) ? $the_dropzone : false
				);
			}
			else {
				/*
				 * Create the widget fragment to be appended to the DOM.
				 */
				var $inserted_widget = self.createWidgetFragment({
					type: widget_type,

					related_dropzone_id   : $dropzone.attr('id').split('spirebuilder-dropzone-')[1],
					related_dropzone_order: $dropzone.parent().children('.spirebuilder-widget').length,

					// Since this widget is a fresh one it will not have any options.
					options               : {}
				}, self, ($the_dropzone.hasClass('spirebuilder-dropzone-little')) ? $the_dropzone : false);

				/*
				 * Launch the Edit modal dialog on the new added widget.
				 * The timeout is for let the 'show' animation to be completed before show the modal dialog.
				 */
				if (jQuery('#spirebuilder-edit-modal').val() == 1 && jQuery.inArray(widget_type, jQuery('#spirebuilder-edit-modal-exclusions').val().split(',')) == -1) {
					setTimeout(function () {
						$inserted_widget.find('.spirebuilder-box .spirebuilder-control-edit').trigger('click');

					}, 150);
				}
			}

			// Update the widgets dropzone order.
			self.updateRelatedDropzoneOrder();

			// Call the global hook for post widget drop action.
			self.hookDropPostWidgetAction();

			// Close the Add widget dialog.
			$filled_template.modal('hide');

			// Disable the already consumed droppable.
			$dropzone.droppable('disable');
		});
	}

	/**
	 * Widget Action: Save the widget and its children as a template.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setActionWidgetSaveTemplate():void {
		// Cache a reference to this class to use it in the inner functions.
		var self:SpireBuilder = this;

		var $filled_template;
		var $body = jQuery('body');
		var LOM_template:LOM;
		var template_title:string;

		$body.on('click', '.spirebuilder-box .spirebuilder-control-save', function () {
			var $the_button = jQuery(this);

			/*
			 * Sync LOM before send it to server.
			 */
			LOM_template = self.updateHTML2LOM($the_button.closest('.spirebuilder-widget').clone().wrap('<div />').parent(), true /* is_template */);

			// Fill the widget template by passing the data to the knowledgeable specific function.
			$filled_template = jQuery(SpireBuilder.templates_container['dialog-widget-save']({}));

			// Finally, launch the modal with the filled template.
			$filled_template.modal()
				.on('hidden', function () {
					jQuery(this).remove();
				})
				.on('shown', function () {
					jQuery(this).find('input[type="text"]').focus();
				});
		});

		$body.on('click', '.spirebuilder-modal-button-save-widget', function () {
			var $the_button = jQuery(this);

			template_title = $the_button.closest('.modal').find('input[type="text"]').val();

			// Send query to server to save template.
			jQuery.ajax({
				data    : {
					action           : 'spirebuilder_update_builder_templates',
					post_id          : self.post_id,
					SpireBuilderNonce: SpireBuilderNonce.spirebuilder_update_builder_templates,

					LOM  : JSON.stringify(LOM_template),
					title: template_title
				},
				dataType: 'json',
				error   : function (jqXHR, textStatus:string, errorThrown:string) {
					// Don't show the message in case that the user cancel the query.
					if (errorThrown) {
						// Display message to user.
						CodeField.Utils.showMessage({
							type               : 'error',
							message            : textStatus + ': ' + errorThrown,
							templates_container: SpireBuilder.templates_container
						});
					}
				},
				success : function (response_from_server:ResponseFromServer) {
					if (response_from_server.type == 'success') {
						// Fill the widget template by passing the data to the knowledgeable specific function.
						var $filled_template = jQuery(SpireBuilder.templates_container['widget-template']({
							id   : response_from_server.data.id,
							title: template_title,
							LOM  : LOM_template
						}));

						jQuery('#spirebuilder-templates').find('.spirebuilder-widgets-list')
							.append($filled_template)
							.find('.spirebuilder-templates-empty-message ')
							.hide('blind');

						// Refresh drag-n'-drop with all the new added ones.
						self.setDragAndDrop();

						// Refresh tooltips with all the new added ones.
						if (jQuery.fn.hasOwnProperty('tooltip')) {
							jQuery('.tip').tooltip();
						}
					}

					// Display message to user.
					CodeField.Utils.showMessage({
						type               : response_from_server.type,
						message            : response_from_server.message,
						templates_container: SpireBuilder.templates_container
					});
				},
				type    : 'POST',
				url     : ajaxurl
			});
		});
	}

	/**
	 * Set all AJAX Forms behaviour here.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setAjaxForms():void {
		// Cache a reference to this class to use it in the inner functions.
		var self:SpireBuilder = this;

		jQuery('.codefield-submit-button', '.codefield-ajax-form').on('click', function () {
			var $the_form = jQuery('.codefield-ajax-form');
			var $the_button = jQuery(this);

			// Used to merge the context between new and recreated widgets.
			var serialized_options = jQuery('input[type="text"],input[type="hidden"],input[type="checkbox"],input[type="radio"],select,textarea', '.codefield-ajax-form')
				.serializeArray();

			// Transform the serialized options array into the context object format.
			var data = {
				action           : 'spirebuilder_update_builder_settings',
				post_id          : self.post_id,
				SpireBuilderNonce: SpireBuilderNonce.spirebuilder_update_builder_settings
			};
			data[$the_button.attr('name')] = '';

			jQuery.each(serialized_options, function (index, element) {
				data[element.name] = element.value;
			});

			jQuery.ajax({
				beforeSend: function () {
					// Used to disable the button and show the loader.
					$the_form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
					jQuery('.codefield-submit-tr button[type="submit"]', $the_form).button('disable');
				},
				data      : data,
				dataType  : 'json',
				error     : function (jqXHR, textStatus:string, errorThrown:string) {
					// Cache a reference to this button to use it in the inner functions. For performance reasons.
					var $message_dashboard = $the_form.find('.codefield-message-dashboard');

					// Don't show the message in case that the user cancel the query.
					if (errorThrown) {
						// Remove ajax loader and disable button.
						$the_form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
						jQuery('.codefield-submit-tr button[type="submit"]').button('enable');

						// Display message to user.
						CodeField.Utils.showMessage({
							type               : 'error',
							message            : textStatus + ': ' + errorThrown,
							$message_dashboard : $message_dashboard,
							templates_container: SpireBuilder.templates_container
						});
					}
				},
				success   : function (response_from_server:ResponseFromServer) {
					// Cache a reference to this button to use it in the inner functions. For performance reasons.
					var $message_dashboard = $the_form.find('.codefield-message-dashboard');

					// Remove ajax loader and show button.
					$the_form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
					jQuery('.codefield-submit-tr button[type="submit"]', $the_form).button('enable');

					// Display message to user.
					CodeField.Utils.showMessage({
						type               : response_from_server.type,
						message            : response_from_server.message,
						$message_dashboard : $message_dashboard,
						templates_container: SpireBuilder.templates_container
					});
				},
				type      : 'POST',
				url       : ajaxurl
			});

			// Prevent the submit since we are using AJAX.
			return false;
		});
	}

	/**
	 * Upload LOM to server.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setUploadLOM():void {
		// Cache a reference to this class to use it in the inner functions.
		var self:SpireBuilder = this;

		/*
		 * Handler: Send updated LOM to server to be saved.
		 */
		jQuery('.codefield-page').on('click', '.spire-builder-upload-lom', function () {
			/*
			 * Sync LOM before send it to server.
			 */
			self.LOM = self.updateHTML2LOM(jQuery('#spirebuilder-editzone'));

			// Update related hidden field to compatilize with WordPress Update button.
			jQuery('#spirebuilder-LOM').val(JSON.stringify(self.LOM));

			// Cache a reference to this button to use it in the inner functions.
			var $this_button = jQuery(this);

			// Cache a reference to this button to use it in the inner functions. For performance reasons.
			var $message_dashboard = $this_button.closest('.ui-tabs-panel').find('.codefield-message-dashboard');

			/*
			 * AJAX request to upload LOM to server.
			 */
			jQuery.ajax({
				beforeSend: function () {
					// Used to disable the button and show the loader.
					$this_button.find('i').toggleClass('icon-save icon-spinner icon-spin');
					$this_button.button('disable');
				},
				data      : {
					action           : 'spirebuilder_update_builder_lom',
					post_id          : self.post_id,
					SpireBuilderNonce: SpireBuilderNonce.spirebuilder_update_builder_lom,

					// Since the LOM content will be JSON, the bare minimum valid JSON is an empty object or array.
					LOM              : JSON.stringify(self.LOM)
				},
				dataType  : 'json',
				error     : function (jqXHR, textStatus:string, errorThrown:string) {
					// Don't show the message in case that the user cancel the query.
					if (errorThrown) {
						// Remove ajax loader and disable button.
						$this_button.find('i').toggleClass('icon-save icon-spinner icon-spin');
						$this_button.button('enable');

						// Display message to user.
						CodeField.Utils.showMessage({
							type               : 'error',
							message            : textStatus + ': ' + errorThrown,
							$message_dashboard : $message_dashboard,
							templates_container: SpireBuilder.templates_container
						});
					}
				},
				success   : function (response_from_server:ResponseFromServer) {
					// Remove ajax loader and show button.
					$this_button.find('i').toggleClass('icon-save icon-spinner icon-spin');
					$this_button.button('enable');

					// Clear message dashboard.
					$message_dashboard.html('');

					// Display message to user.
					CodeField.Utils.showMessage({
						type               : response_from_server.type,
						message            : response_from_server.message,
						$message_dashboard : $message_dashboard,
						templates_container: SpireBuilder.templates_container
					});
				},
				type      : 'POST',
				url       : ajaxurl
			});

			// Cancel the browser default link behaviour.
			return false;
		});
	}

	/**
	 * Sync HTML layout -> LOM.
	 *
	 * There are to ways to do this update:
	 *
	 * #1: That the specific action directly update the LOM whenever there is an operation on a widget.
	 * #2: That the specific action only update the local widget options and when the user updates the layout then a complete sync is made.
	 *
	 * The way already coded below is the #2 one but later on code the first one and compare what of them is efficient and effective.
	 *
	 * @param   {*} $LOM_section
	 * @param   {*} is_template
	 * @returns {LOM}
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private updateHTML2LOM($LOM_section, is_template ?:any):LOM {
		// Cache a reference to this class to use it in the inner functions. For performance reasons.
		var $widgets = jQuery('.spirebuilder-widget', $LOM_section);

		// Restore default LOM for the processing.
		var built_LOM:LOM = this.setDefaultLOM();

		/*
		 * There are widgets so process them.
		 * Flatten the tree layout to ease the parsing by iterating though the widgets instead the tree.
		 */
		$widgets.each(function () {
			// Cache a reference to this button to use it in the inner functions.
			var $the_widget = jQuery(this);

			/*
			 * Reference to the current element in the ancestor chain.
			 * Initially point it to the LOM top.
			 * This assignment is by reference since Objects are reference types not primitive ones.
			 */
			var ancestor_chain_reference:LOM = built_LOM;

			/*
			 * Update widget ancestor state on the LOM.
			 * Do this iterating though the widget ancestor chain.
			 */
			$the_widget.parentsUntil($LOM_section, '.spirebuilder-widget').reverse().each(function () {
				/*
				 * Cache a reference to this button to use it in the inner functions.
				 * It's ancestor instead of parent since we don't are sure that this is the actual parent.
				 */
				var $the_ancestor = jQuery(this);

				/*
				 * Create the children collection if is not there already.
				 * Isn't duplicated! Needed to prepare the ancestor for further children.
				 */
				if (!ancestor_chain_reference.hasOwnProperty('children')) {
					ancestor_chain_reference.children = <LOMChildrenMap> {};
				}

				// Cache the reference for clarity and performance reasons.
				var ancestor_widget_id = $the_ancestor.attr('id').split('spirebuilder-widget-')[1];

				// Check for ancestor existence in the LOM and add it if not.
				if (ancestor_chain_reference.children.hasOwnProperty(ancestor_widget_id)
					|| ancestor_chain_reference.children.hasOwnProperty(ancestor_widget_id + '-TEMPLATE')) {

					// The ancestor does exists so only update the ancestor chain reference.
					ancestor_chain_reference = ancestor_chain_reference.children[ancestor_widget_id + ((is_template) ? '-TEMPLATE' : '')];
				}
				else {
					// The ancestor doesn't exists so add it to the ancestor chain reference.

					// Build dropzones templates if is a template.
					var dropzones = jQuery.parseJSON($the_ancestor.attr('data-widget-dropzones'));
					if (is_template) {
						for (var dropzone in dropzones) {
							if (dropzones.hasOwnProperty(dropzone)) {
								dropzones[dropzone] = dropzones[dropzone] + '-TEMPLATE';
							}
						}
					}

					var widget_type:string = $the_ancestor.attr('data-widget-type');

					// Regenerate widget options if needed to.
					var options = jQuery.parseJSON($the_ancestor.attr('data-widget-options'));
					if (is_template) {
						if (SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('regenerateOptions')) {
							SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].regenerateOptions(options);
						}
					}

					ancestor_chain_reference.children[ancestor_widget_id + ((is_template) ? '-TEMPLATE' : '')] = {
						id  : $the_ancestor.attr('data-widget-id') + ((is_template) ? '-TEMPLATE' : ''),
						type: widget_type,

						related_dropzone_id   : $the_ancestor.attr('data-widget-related-dropzone-id') + ((is_template) ? '-TEMPLATE' : ''),
						related_dropzone_order: $the_ancestor.attr('data-widget-related-dropzone-order'),

						dropzones: dropzones,
						options  : options
					}
				}
			});

			/*
			 * Create the children collection if is not there already.
			 * Isn't duplicated! Needed for attaching the widget to its parent.
			 */
			if (!ancestor_chain_reference.hasOwnProperty('children')) {
				ancestor_chain_reference.children = <LOMChildrenMap> {};
			}

			// Cache the reference for clarity and performance reasons.
			var widget_id = $the_widget.attr('id').split('spirebuilder-widget-')[1];

			/*
			 * And last but not least, add the widget to the LOM.
			 * Use for this the last pointer to the closest widget ancestor: its parent.
			 */
			// Build dropzones templates if is a template.
			var dropzones = jQuery.parseJSON($the_widget.attr('data-widget-dropzones'));
			if (is_template) {
				for (var dropzone in dropzones) {
					if (dropzones.hasOwnProperty(dropzone)) {
						dropzones[dropzone] = dropzones[dropzone] + '-TEMPLATE';
					}
				}
			}

			var widget_type:string = $the_widget.attr('data-widget-type');

			// Regenerate widget options if needed to.
			var options = jQuery.parseJSON($the_widget.attr('data-widget-options'));
			if (is_template) {
				if (SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('regenerateOptions')) {
					SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].regenerateOptions(options);
				}
			}

			ancestor_chain_reference.children[widget_id + ((is_template) ? '-TEMPLATE' : '')] = {
				id  : widget_id + ((is_template) ? '-TEMPLATE' : ''),
				type: $the_widget.attr('data-widget-type'),

				related_dropzone_id   : $the_widget.attr('data-widget-related-dropzone-id') + ((is_template) ? '-TEMPLATE' : ''),
				related_dropzone_order: $the_widget.attr('data-widget-related-dropzone-order'),

				dropzones: dropzones,
				options  : options
			};
		});

		return built_LOM;
	}

	/**
	 * Sync LOM -> HTML layout.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private updateLOM2HTML():void {
		// Update LOM from hidden fields.
		var LOM_input_field:LOM = jQuery.parseJSON(jQuery('#spirebuilder-LOM').val());

		if (LOM_input_field) {
			this.LOM = LOM_input_field;
		}
		else {
			this.LOM = this.setDefaultLOM();
		}

		/*
		 * The LOM root is the initial seed for the recursive function.
		 * Its needed to pass a reference to this class since the Z combinator rewrites the inner scope function.
		 */
		CodeField.Utils.Z(this.createHTMLFromLOM)(
			this.LOM,
			this);

		// Update new added droppables.
		this.setDroppables();

		// Refresh the actions needing to.
		this.hookDropPostWidgetAction();

		// Disable all droppables that have at least one widget inside them.
		jQuery('.spirebuilder-dropzone', '#spirebuilder-editzone').each(function () {
			// Cache a reference to this to use it later. For performance reasons.
			var $this_dropzone = jQuery(this);

			if ($this_dropzone.next('.spirebuilder-widget').length != 0) {
				// There is at least one widget related to this dropzone so disable the droppable.
				$this_dropzone.droppable('disable');
			}
		});
	}

	/**
	 * A recursive function that will create jQuery Fragments from LOM fragments until it recreates the LOM.
	 * Further enhancement: execute it inside a worker to prevent browser unresponsiveness.
	 *
	 * @param {Function}        callee
	 * @param {LOM}             LOM_section
	 * @param {SpireBuilder}    self
	 * @param {*}               dropzone_little
	 *
	 * @since 0.1
	 * @version 0.1
	 *
	 */
	private createHTMLFromLOM(callee:Function, LOM_section:LOM, self:SpireBuilder, dropzone_little:any):void {
		/*
		 * To check whether is the LOM root to hover it since isn't needed to insert it into the editzone.
		 */
		if (LOM_section.type != 'root') {
			/*
			 * Create the widget fragment to be appended to the DOM.
			 */
			self.createWidgetFragment(LOM_section, self, (LOM_section.related_dropzone_id == -1) ? dropzone_little : false);
		}

		/*
		 * Continue the process to the widget children.
		 * First organize the children depending on its related order.
		 */
		// And then iterate through them.
		for (var widget in LOM_section.children) {
			// Ensures iteration over real widgets and not built-in properties.
			if (LOM_section.children.hasOwnProperty(widget)) {
				/*
				 * Iterate down the tree recursively to create the inner widgets.
				 */
				callee(
					LOM_section.children[widget],
					self,
					dropzone_little
				);
			}
		}
	}

	/**
	 * Create a jQuery fragment from a LOM object.
	 *
	 * @param   {LOM}             LOM_section
	 * @param   {SpireBuilder}    self
	 * @param   {*}               $dropzone_little
	 * @returns {*}
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private createWidgetFragment(LOM_section:any, self ?:SpireBuilder, $dropzone_little ?:any):any {
		/*
		 * Doesn't use the widget if is disabled in settings.
		 */
		if (jQuery.inArray(LOM_section.type, jQuery('#spirebuilder-available_widgets').val().split(',')) != -1) {
			/*
			 * Widget is enabled so process it.
			 */
			// To ensure compatibility with the different ways to call this method.
			var self:SpireBuilder = self || this;

			// Reinstate related_dropzone_id id of 0 if is a top level widget from a template (related_dropzone_id == -1)
			// Cache references for performance reasons to be used later on.
			var $dropzone = jQuery('#spirebuilder-dropzone-' + ((LOM_section.related_dropzone_id == -1) ? '0' : LOM_section.related_dropzone_id));

			/*
			 * Merge the context between new and recreated widgets.
			 * Use for this the specific widget related function.
			 * For the sake of clarity.
			 */
			var context = SpireBuilderWidgets[CodeField.Utils.toPascalCase(LOM_section.type)].getBuilderContext(LOM_section);

			// Fill the widget template by passing the data to the knowledgeable specific function.
			var $filled_template = jQuery(SpireBuilder.templates_container['builder-' + LOM_section.type](context));

			// Post process the template.
			$filled_template.hide();

			if ($dropzone_little) {
				// Prevent the widget adding before a main dropzone.
				if ($dropzone_little.next().hasClass('spirebuilder-dropzone')) {
					$dropzone_little = $dropzone_little.next();
				}

				// Insert widget template in its related dropzone.
				$dropzone_little.after($filled_template);

				// Cache references for performance reasons to be used later on.
				var $inserted_widget = $dropzone_little.next();
			}
			else {
				// Insert widget template in its related dropzone with the right order.
				if ($dropzone.nextAll('.spirebuilder-widget').length) {
					// There are more widgets so insert the new widget at its place.
					$dropzone.nextAll('.spirebuilder-widget').last().next('.spirebuilder-dropzone-little').after($filled_template);

					// Cache references for performance reasons to be used later on.
					var $inserted_widget = $dropzone.nextAll('.spirebuilder-widget').last();
				}
				else {
					// There are no widgets so insert the new widget right after the dropzone.
					$dropzone.after($filled_template);

					// Cache references for performance reasons to be used later on.
					var $inserted_widget = $dropzone.next();
				}
			}

			// Show the already inserted widget with a nice effect.
			$inserted_widget.show('fade');

			/*
			 * Show the mini dropzone before this dropzone.
			 */
			$dropzone.prev('.spirebuilder-dropzone-little').show('fade');

			// Add the bottom little dropzone after the already added widget.
			$inserted_widget.after(SpireBuilder.templates_container['dropzone-little']());

			// Show the after widget little dropzone.
			$inserted_widget.next('.spirebuilder-dropzone-little').removeClass('codefield-display-none');

			// Disable the already used dropzone so is not used again unless the widget is removed. Keep in the DOM for future reference.
			$dropzone.not('#spirebuilder-editzone').hide();

			// Set the specific widget behaviour, if any.
			if (SpireBuilderWidgets[CodeField.Utils.toPascalCase(LOM_section.type)].hasOwnProperty('setBuilderBehaviour')) {
				SpireBuilderWidgets[CodeField.Utils.toPascalCase(LOM_section.type)].setBuilderBehaviour($inserted_widget);
			}

			// Return the just inserted widget for further processing.
			return $inserted_widget;
		}
	}

	/**
	 * Move a jQuery tree fragment from dropzone.
	 *
	 * @param {number}  related_dropzone_id
	 * @param {*}       $dropzone_little
	 * @param {*}       $cloned_HTML_section
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	public static moveWidgetSection(related_dropzone_id:number, $dropzone_little:any, $cloned_HTML_section):void {
		// Cache references for performance reasons to be used later on.
		var $dropzone = jQuery('#spirebuilder-dropzone-' + related_dropzone_id);

		if ($dropzone_little) {
			// Prevent the widget adding before a main dropzone.
			if ($dropzone_little.next().hasClass('spirebuilder-dropzone')) {
				$dropzone_little = $dropzone_little.next();
			}

			// Insert widget template in its related dropzone.
			$dropzone_little.after($cloned_HTML_section);

			// Cache references for performance reasons to be used later on.
			var $inserted_widget = $dropzone_little.next();
		}
		else {
			// Insert widget template in its related dropzone with the right order.
			if ($dropzone.nextAll('.spirebuilder-widget').length) {
				// There are more widgets so insert the new widget at its place.
				$dropzone.nextAll('.spirebuilder-widget').last().next('.spirebuilder-dropzone-little').after($cloned_HTML_section);

				// Cache references for performance reasons to be used later on.
				var $inserted_widget = $dropzone.nextAll('.spirebuilder-widget').last();
			}
			else {
				// There are no widgets so insert the new widget right after the dropzone.
				$dropzone.after($cloned_HTML_section);

				// Cache references for performance reasons to be used later on.
				var $inserted_widget = $dropzone.next();
			}
		}

		// Refresh all widget needed behaviour along with children.
		var $widgets = $inserted_widget.add($inserted_widget.find('.spirebuilder-widget'));
		for (var i = 0; i < $widgets.length; i++) {
			var widget_type:string = $widgets.eq(i).attr('data-widget-type');

			// Finally, update any widget specific behaviour.
			if (SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('refreshBehaviour')) {
				SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].refreshBehaviour($widgets.eq(i));
			}
		}

		// Show the already inserted widget with a nice effect.
		$inserted_widget.removeClass('spirebuilder-widget-draggable-placeholder').show('fade');

		/*
		 * Show the mini dropzone before this dropzone.
		 */
		$dropzone.prev('.spirebuilder-dropzone-little').show('fade');

		// Add the bottom little dropzone after the already added widget.
		$inserted_widget.after(SpireBuilder.templates_container['dropzone-little']());

		// Show the after widget little dropzone.
		$inserted_widget.next('.spirebuilder-dropzone-little').removeClass('codefield-display-none');

		// Disable the already used dropzone so is not used again unless the widget is removed. Keep in the DOM for future reference.
		$dropzone.not('#spirebuilder-editzone').hide().droppable('disable');
	}

	/**
	 * Update the widgets dropzone order.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	public updateRelatedDropzoneOrder():void {
		// Cache a reference to this to use it in the inner functions. For performance reasons.
		var $widgets = jQuery('.spirebuilder-widget', '#spirebuilder-editzone');

		// If there is no widgets it means that the editzone is empty, so do nothing.
		if ($widgets.length != 0) {
			/*
			 * There are widgets so process them.
			 * Flatten the tree layout to ease the parsing by iterating though the widgets instead the tree.
			 */
			$widgets.each(function () {
				// Cache a reference to this widget to use it later ,for performance reasons.
				var $the_widget = jQuery(this);

				// Search its order within its brothers.
				var related_dropzone_order = $the_widget.prevAll('.spirebuilder-widget').length;

				// Update its related field.
				$the_widget.attr('data-widget-related-dropzone-order', related_dropzone_order);
			});
		}
	}

	/**
	 * Set the toolbar behaviour.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setToolbarBehaviour():void {
		this.setFixedToolbar();
		this.setRemoveTemplateBehaviour();
	}

	/**
	 * Set the fixed toolbar behaviour.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setFixedToolbar():void {
		// Get fixed toolbar setting and check if the toolbar is enabled or not.
		if (jQuery('#spirebuilder-toolbar-fixed').val() == 1) {
			// Cache a reference to this class to use it in the inner functions.
			var self:SpireBuilder = this;

			// Cache references for performance reasons to be used later on.
			var $toolbar = jQuery('#spirebuilder-builder-toolbar', '.codefield-page');

			jQuery(document).on('scroll', function () {
				self.updateToolbarPosition($toolbar);
			});

			jQuery(window).on('resize', function () {
				self.updateToolbarPosition($toolbar);
			});
		}
	}

	/**
	 * Updates toolbar position on conditions change.
	 *
	 * @param {*}    $toolbar
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private updateToolbarPosition($toolbar:any):void {
		var toolbar_offset:number = jQuery('#spirebuilder_builder').find('.inside').offset().top;

		if (jQuery(document).scrollTop() > toolbar_offset - 28) {
			// The toolbar becomes fixed.
			$toolbar.addClass('codefield-position-fixed')
				.css({
					left : $toolbar.parent('.codefield-page').offset().left,
					width: $toolbar.parent('.codefield-page').width()
				});

			// If Settings tabs if select then show the All tab.
			if ($toolbar.tabs('option', 'active') == 0) {
				$toolbar.tabs('option', 'active', 2);
			}
		}
		else {
			// The toolbar becomes static.
			$toolbar.removeClass('codefield-position-fixed')
				.css({
					left : 0,
					width: '100%'
				});
		}
	}

	/**
	 * Set the templates remove button behaviour.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
	private setRemoveTemplateBehaviour():void {
		var $filled_template;
		var $body = jQuery('body');
		var $template_item;

		/*
		 * Show/Hide the remove action button on mouse hovering.
		 */
		$body.on('mouseenter', '.spirebuilder-widget-template',function () {
			jQuery(this).find('.badge').show();
		}).on('mouseleave', '.spirebuilder-widget-template', function () {
				jQuery(this).find('.badge').hide();
			});

		/*
		 * Change color remove action button on mouse hovering.
		 */
		$body.on('mouseenter', '.spirebuilder-widget-template .spirebuilder-widget-template-remove',function () {
			jQuery(this).addClass('badge-important');
		}).on('mouseleave', '.spirebuilder-widget-template .badge', function () {
				jQuery(this).removeClass('badge-important');
			});

		/*
		 * The real click action on the remove button.
		 */
		$body.on('click', '.spirebuilder-widget-template .spirebuilder-widget-template-remove', function () {
			// For future use in other event handler if the user confirms deletion.
			$template_item = jQuery(this).closest('a');

			// Fill the widget template by passing the data to the knowledgeable specific function.
			$filled_template = jQuery(SpireBuilder.templates_container['modal-remove']({
				object: 'template'
			}));

			// Finally, launch the modal with the filled template.
			$filled_template.modal().on('hidden', function () {
				jQuery(this).remove();
			});
		});

		$body.on('click', '.codefield-modal-button-remove-template', function () {
			jQuery.ajax({
				data    : {
					action           : 'spirebuilder_update_builder_templates_delete',
					id               : $template_item.attr('data-widget-template-id'),
					SpireBuilderNonce: SpireBuilderNonce.spirebuilder_update_builder_templates_delete
				},
				dataType: 'json',
				error   : function (jqXHR, textStatus:string, errorThrown:string) {
					// Don't show the message in case that the user cancel the query.
					if (errorThrown) {
						// Display message to user.
						CodeField.Utils.showMessage({
							type               : 'error',
							message            : textStatus + ': ' + errorThrown,
							templates_container: SpireBuilder.templates_container
						});
					}
				},
				success : function (response_from_server:ResponseFromServer) {
					// Remove template from listing.
					$template_item.closest('li').hide('fold', function () {
						jQuery(this).remove();

						var $templates_list = jQuery('#spirebuilder-templates').find('.spirebuilder-widgets-list li');

						if ($templates_list.length == 2) {
							$templates_list.filter('.spirebuilder-templates-empty-message')
								.removeClass('codefield-display-none')
								.show('blind');
						}
						else {
							$templates_list.filter('.spirebuilder-templates-empty-message')
								.addClass('codefield-display-none')
								.hide('blind');
						}

						// Display message to user.
						CodeField.Utils.showMessage({
							type               : response_from_server.type,
							message            : response_from_server.message,
							templates_container: SpireBuilder.templates_container
						});
					});
				},
				type    : 'POST',
				url     : ajaxurl
			});
		});
	}
}

/*
 * Launch App code on DOM load.
 * Prevent name collisions wrapping the code in an anonymous function.
 */
jQuery(function () {
	var spire_builder = new SpireBuilder();
	spire_builder.init();

	SpireBuilder['builders'] = [];
	SpireBuilder['builders'].push(spire_builder);
});