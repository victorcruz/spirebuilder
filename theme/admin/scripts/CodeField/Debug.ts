/**
 * Debug functions.
 *
 * @fileOverview
 */

/**
 * CodeField. Container for global functionality.
 *
 * @namespace
 * @since 0.1
 * @version 0.1
 */
module CodeField {
	/**
	 * Debug. Will contain basic functions to be used in only for debugging purposes.
	 *
	 * @namespace
	 * @since 0.1
	 * @version 0.1
	 */
	export module Debug {
		/**
		 * Prints the structure with related values of a complex nested object in the browser console using recursivity.
		 *
		 * @param {*}      object    The object to be printed in the console.
		 * @param {string} indent    The string/character to be used as indentation.
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function printObject(object:any, indent?:string):void {
			for (var prop in object) {
				// Ensures iteration over custom properties and not default ones.
				if (object.hasOwnProperty(prop)) {
					if (typeof(object[prop]) == 'object') {
						console.log(((indent) ? indent : '') + prop + ':');

						// The object has another level of nesting.
						printObject(object[prop], (indent) ? indent + '\t' : '\t');
					}
					else {
						console.log(((indent) ? indent : '') + prop + ': "' + object[prop] + '"');
					}
				}
			}
		}
	}
}