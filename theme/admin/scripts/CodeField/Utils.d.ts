var jQuery;
var ajaxurl: string;
interface ResponseFromServer {
    type: string;
    message: string;
    data?: any;
}
interface MessageOptions extends ResponseFromServer {
    $message_dashboard?: any;
    templates_container?: any;
}
interface TemplatesContainer {
    [template: string]: Function;
}
interface AJAXFormOptions {
    $message_dashboard: any;
    templates_container: TemplatesContainer;
}
module CodeField.Utils {
    export function noConflict(): void;
    export function toPascalCase(identifier: string): string;
    export function showMessage(options: MessageOptions): void;
    export function Z(func: Function): Function;
    export function compileTemplates($templates_container: any): TemplatesContainer;
    export function registerHandlebarsHelpers(templates_container: TemplatesContainer): void;
    export function setWordPressAJAXForms(options: AJAXFormOptions): void;
    export function convertHex2RGB(hex: string): any;
    export function convertRGB2HSL(rgb: any): any;
    export function getIndexByAttribute(array: Array, attr: string, value: string): number;
}
