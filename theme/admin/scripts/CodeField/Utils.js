var CodeField;
(function (CodeField) {
    (function (Utils) {
        function noConflict() {
            jQuery.fn.btn = jQuery.fn.button.noConflict();
        }
        Utils.noConflict = noConflict;
        function toPascalCase(identifier) {
            return identifier.replace(/(?:_| |-|\b)(\w)/gi, function (all, letter) {
                return letter.toUpperCase();
            });
        }
        Utils.toPascalCase = toPascalCase;
        function showMessage(options) {
            switch(options.type) {
                case 'success': {
                    jQuery.growl.notice({
                        title: options.type + '!',
                        message: options.message
                    });
                    break;
                }

                case 'warning': {
                    jQuery.growl.warning({
                        title: options.type + '!',
                        message: options.message
                    });
                    break;
                }

                case 'info': {
                    jQuery.growl({
                        title: options.type + '!',
                        message: options.message
                    });
                    break;
                }

                default: {
                    jQuery.growl.error({
                        title: options.type + '!',
                        message: options.message
                    });
                    break;
                }

            }
        }
        Utils.showMessage = showMessage;
        function Z(func) {
            var f = function () {
                return func.apply(null, [
                    f
                ].concat([].slice.apply(arguments)));
            };
            return f;
        }
        Utils.Z = Z;
        function compileTemplates($templates_container) {
            var compiled_templates = {
            };
            $templates_container.find('[type="text/x-handlebars-template"]').each(function () {
                var $this_template = jQuery(this);
                var template_source = $this_template.html();
                compiled_templates[$this_template.data('codefieldTemplate') || $this_template.data('widgetTemplate')] = Handlebars.compile(template_source);
            });
            return compiled_templates;
        }
        Utils.compileTemplates = compileTemplates;
        function registerHandlebarsHelpers(templates_container) {
            Handlebars.registerHelper('JSON2string', function (object) {
                return JSON.stringify(object);
            });
            Handlebars.registerHelper('encodeURI', function (URI) {
                return encodeURIComponent(URI);
            });
            Handlebars.registerHelper('dropzone-little', function () {
                var dropzone_little = templates_container['dropzone-little']({
                });
                return new Handlebars.SafeString(dropzone_little);
            });
            Handlebars.registerHelper('equal', function (lvalue, rvalue, options) {
                if(arguments.length < 3) {
                    throw new Error('Handlebars Helper equal needs 2 parameters');
                }
                if(lvalue != rvalue) {
                    return options.inverse(this);
                } else {
                    return options.fn(this);
                }
            });
            Handlebars.registerHelper('select', function (variable, value) {
                if(variable == value) {
                    return 'selected=selected';
                } else {
                    return '';
                }
            });
            Handlebars.registerHelper('checkbox', function (variable) {
                if(variable == 1) {
                    return 'checked=checked';
                } else {
                    return '';
                }
            });
            Handlebars.registerHelper('radio', function (variable, value) {
                if(variable == value) {
                    return 'checked=checked';
                } else {
                    return '';
                }
            });
            Handlebars.registerHelper('sum', function () {
                var sum = 0;
                var v;
                for(var i = 0; i < arguments.length; i++) {
                    v = parseFloat(arguments[i]);
                    if(!isNaN(v)) {
                        sum += v;
                    }
                }
                return sum;
            });
            var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
            var Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
            Handlebars.registerHelper('content', function (context, options) {
                return context[Alphabet[options]];
            });
        }
        Utils.registerHandlebarsHelpers = registerHandlebarsHelpers;
        function setWordPressAJAXForms(options) {
            jQuery('.codefield-ajax-form').each(function () {
                var $this_form = jQuery(this);
                $this_form.ajaxForm({
                    beforeSubmit: function (form_data_arr, $form) {
                        $form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
                        jQuery('.codefield-submit-tr button[type="submit"]', $form).button('disable');
                    },
                    data: {
                        action: $this_form.data('codefieldAction'),
                        post_id: jQuery('#post_ID').val(),
                        CodeFieldNonce: $this_form.data('codefieldNonce')
                    },
                    dataType: 'json',
                    error: function (jqXHR, textStatus, errorThrown, $form) {
                        if(errorThrown) {
                            $form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
                            jQuery('.codefield-submit-tr button[type="submit"]').button('enable');
                            CodeField.Utils.showMessage({
                                type: 'error',
                                message: textStatus + ': ' + errorThrown,
                                $message_dashboard: options.$message_dashboard,
                                templates_container: options.templates_container
                            });
                        }
                    },
                    success: function (response_from_server, statusText, xhr, $form) {
                        $form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
                        jQuery('.codefield-submit-tr button[type="submit"]', $form).button('enable');
                        CodeField.Utils.showMessage({
                            type: response_from_server.type,
                            message: response_from_server.message,
                            $message_dashboard: options.$message_dashboard,
                            templates_container: options.templates_container
                        });
                    },
                    type: 'POST',
                    url: ajaxurl
                });
            });
        }
        Utils.setWordPressAJAXForms = setWordPressAJAXForms;
        function convertHex2RGB(hex) {
            var hex_value = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
            return [
                hex_value >> 16, 
                (hex_value & 65280) >> 8, 
                (hex_value & 255)
            ];
        }
        Utils.convertHex2RGB = convertHex2RGB;
        function convertRGB2HSL(rgb) {
            var min;
            var max;
            var delta;
            var h;
            var s;
            var l;

            var r = rgb[0];
            var g = rgb[1];
            var b = rgb[2];

            min = Math.min(r, Math.min(g, b));
            max = Math.max(r, Math.max(g, b));
            delta = max - min;
            l = (min + max) / 2;
            s = 0;
            if(l > 0 && l < 1) {
                s = delta / (l < 0.5 ? (2 * l) : (2 - 2 * l));
            }
            h = 0;
            if(delta > 0) {
                if(max == r && max != g) {
                    h += (g - b) / delta;
                }
                if(max == g && max != b) {
                    h += (2 + (b - r) / delta);
                }
                if(max == b && max != r) {
                    h += (4 + (r - g) / delta);
                }
                h /= 6;
            }
            return [
                h, 
                s, 
                l
            ];
        }
        Utils.convertRGB2HSL = convertRGB2HSL;
        function getIndexByAttribute(array, attr, value) {
            for(var i = 0; i < array.length; i++) {
                if(array[i].hasOwnProperty(attr) && array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        }
        Utils.getIndexByAttribute = getIndexByAttribute;
    })(CodeField.Utils || (CodeField.Utils = {}));
    var Utils = CodeField.Utils;

})(CodeField || (CodeField = {}));

//@ sourceMappingURL=Utils.js.map
