var CodeField;
(function (CodeField) {
    (function (Debug) {
        function printObject(object, indent) {
            for(var prop in object) {
                if(object.hasOwnProperty(prop)) {
                    if(typeof (object[prop]) == 'object') {
                        console.log(((indent) ? indent : '') + prop + ':');
                        printObject(object[prop], (indent) ? indent + '\t' : '\t');
                    } else {
                        console.log(((indent) ? indent : '') + prop + ': "' + object[prop] + '"');
                    }
                }
            }
        }
        Debug.printObject = printObject;
    })(CodeField.Debug || (CodeField.Debug = {}));
    var Debug = CodeField.Debug;

})(CodeField || (CodeField = {}));

//@ sourceMappingURL=Debug.js.map
