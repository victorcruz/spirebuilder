/**
 * Utils and helpers functions.
 *
 * @fileOverview
 */
/// <reference path="../tslib/handlebars.d.ts" />

/*
 * Declare all used external variables.
 */
// Declared and initialized by itself.
declare var jQuery;

// URL to http://site.com/wp-admin/admin-ajax.php. Declared and initialized through WordPress.
declare var ajaxurl:string;

/*
 * Declare all needed interfaces.
 */
/**
 * Interface: ResponseFromServer.
 *
 * @since 0.1
 * @version 0.1
 */
interface ResponseFromServer {
	type: string;
	message: string;
	data?: any;
}

/**
 * Interface: MessageOptions.
 *
 * @extends ResponseFromServer
 * @since 0.1
 * @version 0.1
 */
interface MessageOptions extends ResponseFromServer {
	$message_dashboard?: any;
	templates_container?: any;
}

/**
 * Interface: TemplatesContainer.
 *
 * @since 0.1
 * @version 0.1
 */
interface TemplatesContainer {
	[template: string]: Function;
}

/**
 * Interface: AJAXFormOptions.
 *
 * @since 0.1
 * @version 0.1
 */
interface AJAXFormOptions {
	$message_dashboard: any;
	templates_container: TemplatesContainer;
}

/**
 * CodeField. Container for global functionality.
 *
 * @namespace
 * @since 0.1
 * @version 0.1
 */
module CodeField {
	/**
	 * Utils. Will contain basic functions to be used in almost every site.
	 *
	 * @namespace
	 * @since 0.1
	 * @version 0.1
	 */
	export module Utils {
		/**
		 * Solve conflicts between UI libraries.
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function noConflict():void {
			/*
			 * Prevent jQuery UI Button and Twitter Bootstrap Button clash.
			 * Reverts $.fn.button to jQueryUI btn and assigns Twitter Bootstrap button functionality to $.fn.btn
			 */
			jQuery.fn.btn = jQuery.fn.button.noConflict();
		}

		/**
		 * Convert underscored string to Pascal case (uppercase camelcase variant).
		 *
		 * @param   {string} identifier     Identifier to be converted.
		 * @returns {string}                Returned converted identifier.
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function toPascalCase(identifier:string):string {
			return identifier.replace(/(?:_| |-|\b)(\w)/gi, function (all:any, letter:string) {
				return letter.toUpperCase();
			});
		}

		/**
		 * Displays a message to user.
		 *
		 * @param {MessageOptions} options      Message parameters to be shown.
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function showMessage(options:MessageOptions):void {
			switch (options.type) {
				case 'success':
				{
					jQuery.growl.notice({
						title  : options.type + '!',
						message: options.message
					});

					break;
				}

				case 'warning':
				{
					jQuery.growl.warning({
						title  : options.type + '!',
						message: options.message
					});

					break;
				}

				case 'info':
				{
					jQuery.growl({
						title  : options.type + '!',
						message: options.message
					});

					break;
				}

				default:
				{
					jQuery.growl.error({
						title  : options.type + '!',
						message: options.message
					});

					break;
				}
			}
		}

		/**
		 * The Z-combinator, a Y-combinator inspired anonymous recursion provider.
		 * See the links below for more reference:
		 * http://am.aurlien.net/post/2810658101/z-combinator
		 * http://stackoverflow.com/questions/17645356/anonymous-recursion-any-way-to-replace-javascript-arguments-callee-to-other
		 *
		 * @param   {Function} func       Function to be anonymized.
		 * @returns {Function}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function Z(func:Function):Function {
			var f:Function = function () {
				return func.apply(null, [f].concat([].slice.apply(arguments)));
			};
			return f;
		}

		/**
		 * Compile and return all Handlebars templates in the entered templates container.
		 *
		 * @param   {*} $templates_container       Container for all compiled templates.
		 * @returns {TemplatesContainer}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function compileTemplates($templates_container:any):TemplatesContainer {
			var compiled_templates:TemplatesContainer = {};

			$templates_container
				.find('[type="text/x-handlebars-template"]')
				.each(function () {
					// Cache a reference to this to use it in the inner functions. For performance reasons.
					var $this_template = jQuery(this);

					// Intermediate var for the sake of clarity.
					var template_source = $this_template.html();

					// Push the already compiled template to the returning object easy accessible through its template identifier.
					compiled_templates[$this_template.data('codefieldTemplate') || $this_template.data('widgetTemplate')] = Handlebars.compile(template_source);
				});

			// And last but not least, return the compiled templates to caller.
			return compiled_templates;
		}

		/**
		 * Register custom Handlebars helpers.
		 *
		 * @param {TemplatesContainer} templates_container      Container for all compiled templates in the page.
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function registerHandlebarsHelpers(templates_container:TemplatesContainer):void {
			/*
			 * Simple helper to stringify an object.
			 * Usage: {{ JSON2string object }}
			 */
			Handlebars.registerHelper('JSON2string', function (object:Object) {
				return JSON.stringify(object);
			});

			/*
			 * Simple helper to encode a string URL.
			 * Usage: {{ encodeURI URL_not_safe }}
			 */
			Handlebars.registerHelper('encodeURI', function (URI) {
				return encodeURIComponent(URI);
			});

			/*
			 * Helper that prints a little dropzone from built template.
			 * Usage: {{ dropzone-little }}
			 */
			Handlebars.registerHelper('dropzone-little', function () {
				var dropzone_little:string = templates_container['dropzone-little']({});

				// Tell Handlebars to output the raw HTML.
				return new Handlebars.SafeString(dropzone_little);
			});

			/*
			 * Need to check if a conditional statement is true or false.
			 * Usage: {{#equal some_var "some_value"}}content here will be printed{{/equal}}
			 */
			Handlebars.registerHelper('equal', function (lvalue, rvalue, options) {
				if (arguments.length < 3) {
					throw new Error('Handlebars Helper equal needs 2 parameters');
				}

				if (lvalue != rvalue) {
					return options.inverse(this);
				}
				else {
					return options.fn(this);
				}
			});

			/*
			 * Need to mark selected option inside a select.
			 * Usage: {{select options.style 'solid'}}
			 */
			Handlebars.registerHelper('select', function (variable, value) {
				if (variable == value) {
					return 'selected=selected';
				}
				else {
					return '';
				}
			});

			/*
			 * Need to check checkboxes upon value.
			 * Usage: {{checkbox options.use_title_separator}}
			 */
			Handlebars.registerHelper('checkbox', function (variable) {
				if (variable == 1) {
					return 'checked=checked';
				}
				else {
					return '';
				}
			});

			/*
			 * Need to check radios upon value.
			 * Usage: {{radio options.tab_alignment 'horizontal'}}
			 */
			Handlebars.registerHelper('radio', function (variable, value) {
				if (variable == value) {
					return 'checked=checked';
				}
				else {
					return '';
				}
			});

			/*
			 * Returns the sum of n items.
			 * Usage: {{sum @index 2}}
			 */
			Handlebars.registerHelper('sum', function () {
				var sum:number = 0;
				var v:number;

				for (var i = 0; i < arguments.length; i++) {
					v = parseFloat(arguments[i]);
					if (!isNaN(v)) {
						sum += v;
					}
				}
				return sum;
			});

			/*
			 * Return Alphabet[index].
			 * Usage: {{#content ../dropzones}}{{/content}}
			 */
			var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
			var Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

			Handlebars.registerHelper('content', function (context, options) {
				return context[Alphabet[options]];
			});
		}

		/**
		 * Set all AJAX forms behaviour.
		 *
		 * @param {AJAXFormOptions} options
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function setWordPressAJAXForms(options:AJAXFormOptions):void {
			jQuery('.codefield-ajax-form').each(function () {
				// Cache a reference to this button to use it in the inner functions.
				var $this_form = jQuery(this);

				$this_form.ajaxForm({
					beforeSubmit: function (form_data_arr, $form) {
						// Used to disable the button and show the loader.
						$form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
						jQuery('.codefield-submit-tr button[type="submit"]', $form).button('disable');
					},
					data        : {
						action        : $this_form.data('codefieldAction'),
						post_id       : jQuery('#post_ID').val(),
						CodeFieldNonce: $this_form.data('codefieldNonce')
					},
					dataType    : 'json',
					error       : function (jqXHR, textStatus:string, errorThrown:string, $form) {
						// Don't show the message in case that the user cancel the query.
						if (errorThrown) {
							// Remove ajax loader and disable button.
							$form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
							jQuery('.codefield-submit-tr button[type="submit"]').button('enable');

							// Display message to user.
							CodeField.Utils.showMessage({
								type               : 'error',
								message            : textStatus + ': ' + errorThrown,
								$message_dashboard : options.$message_dashboard,
								templates_container: options.templates_container
							});
						}
					},
					success     : function (response_from_server:ResponseFromServer, statusText:string, xhr, $form) {
						// Remove ajax loader and show button.
						$form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
						jQuery('.codefield-submit-tr button[type="submit"]', $form).button('enable');

						// Display message to user.
						CodeField.Utils.showMessage({
							type               : response_from_server.type,
							message            : response_from_server.message,
							$message_dashboard : options.$message_dashboard,
							templates_container: options.templates_container
						});
					},
					type        : 'POST',
					url         : ajaxurl
				});
			});
		}

		/**
		 * Convert Hex color value to RGB format.
		 *
		 * @param   {string} hex      Hex value to be converted to RGB.
		 * @returns {*}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function convertHex2RGB(hex:string):any {
			var hex_value:number = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
			return [hex_value >> 16, (hex_value & 0x00FF00) >> 8, (hex_value & 0x0000FF)];
		}

		/**
		 * Convert RGB color value to Hex format.
		 *
		 * @param   {*} rgb         RGB value to be converted to Hex.
		 * @returns {*}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function convertRGB2HSL(rgb:any):any {
			var min, max, delta, h, s, l;
			var r = rgb[0], g = rgb[1], b = rgb[2];
			min = Math.min(r, Math.min(g, b));
			max = Math.max(r, Math.max(g, b));
			delta = max - min;
			l = (min + max) / 2;
			s = 0;
			if (l > 0 && l < 1) {
				s = delta / (l < 0.5 ? (2 * l) : (2 - 2 * l));
			}
			h = 0;
			if (delta > 0) {
				if (max == r && max != g) h += (g - b) / delta;
				if (max == g && max != b) h += (2 + (b - r) / delta);
				if (max == b && max != r) h += (4 + (r - g) / delta);
				h /= 6;
			}
			return [h, s, l];
		}

		/**
		 * Gets objects index inside array.
		 *
		 * @param   {Array} array
		 * @param   {string} attr
		 * @param   {string} value
		 * @returns {number}
		 *
		 * @since 0.1
		 * @version 0.1
		 */
		export function getIndexByAttribute(array:Array, attr:string, value:string) {
			for (var i = 0; i < array.length; i++) {
				if (array[i].hasOwnProperty(attr) && array[i][attr] === value) {
					return i;
				}
			}
			return -1;
		}
	}
}