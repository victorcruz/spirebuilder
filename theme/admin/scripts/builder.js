var SpireBuilder = (function () {
    function SpireBuilder() {
        this.LOM = this.setDefaultLOM();
        this.post_id = jQuery('#post_ID').val();
        SpireBuilder.templates_container = {
        };
        this.flags = {
            first_load: true
        };
    }
    SpireBuilder.templates_container = null;
    SpireBuilder.prototype.init = function () {
        SpireBuilder.templates_container = CodeField.Utils.compileTemplates(jQuery('#codefield-templates'));
        CodeField.Utils.registerHandlebarsHelpers(SpireBuilder.templates_container);
        this.setAjaxForms();
        this.setUploadLOM();
        this.updateLOM2HTML();
        this.setWidgetActions();
        this.setToolbarBehaviour();
        this.setInitialBehaviour();
    };
    SpireBuilder.prototype.setInitialBehaviour = function () {
        jQuery('#spirebuilder-builder-toolbar', '.codefield-page').tabs('option', 'active', 2);
    };
    SpireBuilder.prototype.setDefaultLOM = function () {
        return {
            id: 0,
            type: 'root',
            related_dropzone_id: 0,
            related_dropzone_order: 0,
            options: {
            },
            children: {
            }
        };
    };
    SpireBuilder.prototype.setDragAndDrop = function () {
        this.setDraggables();
        this.setDroppables();
    };
    SpireBuilder.prototype.setDraggables = function () {
        var self = this;
        jQuery('.codefield-draggable', '.spirebuilder-widgets-list').draggable({
            helper: function () {
                var $helper = jQuery(this).clone();
                if($helper.hasClass('spirebuilder-widget-template')) {
                    $helper.find('.badge').remove();
                }
                return $helper.removeClass().not('.spirebuilder-widget-template').addClass('spirebuilder-toolbar-item-helper');
            },
            revert: 'invalid',
            stack: '.spirebuilder-widget',
            zIndex: 450
        });
        jQuery('.spirebuilder-widget', '#spirebuilder-editzone').each(function () {
            var $this_widget = jQuery(this);
            $this_widget.draggable({
                delay: 50,
                handle: jQuery(this).children().children('.spirebuilder-box-header'),
                helper: 'clone',
                cursor: 'move',
                revert: 'invalid',
                start: function (event, ui) {
                    ui.helper.css({
                        opacity: 0.7
                    });
                    if($this_widget.parentsUntil('#spirebuilder-editzone', '.spirebuilder-widget').length) {
                        ui.helper.css({
                            width: 'inherit'
                        });
                    }
                    $this_widget.addClass('spirebuilder-widget-draggable-placeholder');
                    if($this_widget.prev().hasClass('spirebuilder-dropzone-little')) {
                        self.$widget_related_dropzones_little = $this_widget.prev();
                    } else {
                        self.$widget_related_dropzones_little = $this_widget.prevAll('.spirebuilder-dropzone-little').eq(0);
                    }
                    self.$widget_related_dropzones_little = self.$widget_related_dropzones_little.add($this_widget.next());
                    self.$widget_related_dropzones_little.css('visibility', 'hidden').droppable('disable');
                },
                stop: function () {
                    $this_widget.removeClass('spirebuilder-widget-draggable-placeholder');
                    self.$widget_related_dropzones_little.css('visibility', 'visible').droppable('enable');
                },
                stack: '.spirebuilder-widget',
                zIndex: 500
            });
        });
    };
    SpireBuilder.prototype.setDroppables = function () {
        var self = this;
        var $valid_dropzones = jQuery('.spirebuilder-dropzone, .spirebuilder-dropzone-little', '#spirebuilder-editzone');
        $valid_dropzones.droppable({
            accept: '.spirebuilder-widgets-list li a, .spirebuilder-widget',
            activeClass: 'codefield-bg-yellow',
            hoverClass: 'codefield-bg-green',
            greedy: true,
            tolerance: 'pointer',
            over: function () {
                var $this_dropzone = jQuery(this);
                jQuery('.ui-draggable-dragging').addClass('codefield-cursor-dragging-over');
                if($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
                    $this_dropzone.children().animate({
                        'width': '100%'
                    }, {
                        duration: 200,
                        queue: false
                    });
                }
            },
            out: function () {
                var $this_dropzone = jQuery(this);
                jQuery('.ui-draggable-dragging').removeClass('codefield-cursor-dragging-over');
                if($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
                    $this_dropzone.children().animate({
                        'width': 21
                    }, {
                        duration: 200,
                        queue: false
                    });
                }
            },
            drop: function (event, ui) {
                var $this_dropzone = jQuery(this);
                if($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
                    $this_dropzone.children().animate({
                        'width': 21
                    }, {
                        duration: 200,
                        queue: false
                    });
                }
                var widget_type = jQuery(ui.helper).attr('data-widget-template');
                if(ui.draggable.hasClass('spirebuilder-widget')) {
                    var $cloned_HTML_section = ui.draggable.clone();
                    if($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
                        var related_dropzone_id = $this_dropzone.parent().children('.spirebuilder-dropzone').attr('id').split('spirebuilder-dropzone-')[1];
                    } else {
                        var related_dropzone_id = $this_dropzone.attr('id').split('spirebuilder-dropzone-')[1];
                    }
                    $cloned_HTML_section.attr('data-widget-related-dropzone-id', related_dropzone_id).hide();
                    SpireBuilder.moveWidgetSection(related_dropzone_id, ($this_dropzone.hasClass('spirebuilder-dropzone-little')) ? $this_dropzone : false, $cloned_HTML_section);
                    var $widget = ui.draggable;
                    var $prev_dropzone = $widget.prev('.spirebuilder-dropzone');
                    if($prev_dropzone.attr('data-widget-id') != '0' && $prev_dropzone.nextAll('.spirebuilder-widget').length <= 2 || $prev_dropzone.attr('data-widget-id') == '0' && jQuery('#spirebuilder-editzone').children('.spirebuilder-widget').length <= 2) {
                        $prev_dropzone.prev('.spirebuilder-dropzone-little').hide();
                        $prev_dropzone.droppable('enable').show('blind');
                    }
                    $widget.add($widget.next('.spirebuilder-dropzone-little')).hide('blind', function () {
                        jQuery(this).remove();
                    });
                } else {
                    if(ui.draggable.hasClass('spirebuilder-widget-template')) {
                        var data_widget_template_LOM = ui.draggable.attr('data-widget-template-lom');
                        data_widget_template_LOM = data_widget_template_LOM.replace(/TEMPLATE/g, jQuery.now());
                        data_widget_template_LOM = jQuery.parseJSON(data_widget_template_LOM);
                        if($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
                            var related_dropzone_id = -1;
                        } else {
                            var related_dropzone_id = $this_dropzone.attr('id').split('spirebuilder-dropzone-')[1];
                        }
                        for(var widget in data_widget_template_LOM.children) {
                            if(data_widget_template_LOM.children.hasOwnProperty(widget)) {
                                data_widget_template_LOM.children[widget].related_dropzone_id = related_dropzone_id;
                            }
                        }
                        CodeField.Utils.Z(self.createHTMLFromLOM)(data_widget_template_LOM, self, ($this_dropzone.hasClass('spirebuilder-dropzone-little')) ? $this_dropzone : false);
                    } else {
                        var $dropzone;
                        if($this_dropzone.hasClass('spirebuilder-dropzone-little')) {
                            $dropzone = $this_dropzone.parent().children('.spirebuilder-dropzone').first();
                        } else {
                            $dropzone = $this_dropzone;
                        }
                        var $inserted_widget = self.createWidgetFragment({
                            type: widget_type,
                            related_dropzone_id: $dropzone.attr('id').split('spirebuilder-dropzone-')[1],
                            related_dropzone_order: $dropzone.parent().children('.spirebuilder-widget').length,
                            options: {
                            }
                        }, self, ($this_dropzone.hasClass('spirebuilder-dropzone-little')) ? $this_dropzone : false);
                        if(jQuery('#spirebuilder-edit-modal').val() == 1 && jQuery.inArray(widget_type, jQuery('#spirebuilder-edit-modal-exclusions').val().split(',')) == -1) {
                            setTimeout(function () {
                                $inserted_widget.find('.spirebuilder-box .spirebuilder-control-edit').trigger('click');
                            }, 150);
                        }
                    }
                }
                self.updateRelatedDropzoneOrder();
                self.hookDropPostWidgetAction();
            }
        });
    };
    SpireBuilder.prototype.hookDropPostWidgetAction = function () {
        var self = this;
        var is_max_chars_enabled = jQuery('#spirebuilder-widget-content-max-chars').val() != 'all';
        if(is_max_chars_enabled) {
            var max_height = jQuery('#spirebuilder-widget-content-max-chars-restricted-to').val();
            var $text_content = jQuery('.spirebuilder-widget-field-text_content');
            $text_content.expander('destroy');
            $text_content.expander({
                collapseEffect: 'slideUp',
                collapseSpeed: 200,
                expandEffect: 'slideDown',
                expandSpeed: 200,
                expandText: 'More',
                slicePoint: max_height,
                userCollapseText: 'Less',
                widow: 5
            });
        }
        this.setDragAndDrop();
        if(jQuery.fn.hasOwnProperty('tooltip')) {
            jQuery('.tip').tooltip();
        }
        setTimeout(function () {
            if(self.flags.first_load) {
                self.flags.first_load = false;
            } else {
                jQuery('.spire-builder-upload-lom').first().trigger('click');
            }
        }, 1000);
    };
    SpireBuilder.prototype.setWidgetActions = function () {
        this.setActionsToggleVisibility();
        this.setActionWidgetToggle();
        this.setActionWidgetRemove();
        this.setActionWidgetEdit();
        this.setActionWidgetEditSave();
        this.setActionWidgetClone();
        this.setActionWidgetSaveTemplate();
        this.setActionWidgetAdd();
    };
    SpireBuilder.prototype.setActionsToggleVisibility = function () {
        var $body = jQuery('body');
        jQuery('.spirebuilder-actions').css('visibility', 'hidden');
        $body.on('mouseover', '#spirebuilder-editzone .spirebuilder-widget', function (event) {
            jQuery('.spirebuilder-actions').css('visibility', 'hidden');
            jQuery(this).children().children('.spirebuilder-box-header').find('.spirebuilder-actions').css('visibility', 'visible');
            event.stopPropagation();
        });
        $body.on('mouseleave', '#spirebuilder-editzone .spirebuilder-widget', function (event) {
            jQuery('.spirebuilder-actions').css('visibility', 'hidden');
            event.stopPropagation();
        });
    };
    SpireBuilder.prototype.setActionWidgetToggle = function () {
        jQuery('body').on('click', '.spirebuilder-box .spirebuilder-control-drop', function () {
            var $this_button = jQuery(this);
            var $box = $this_button.closest('.spirebuilder-box');
            var $box_content = $box.find('.spirebuilder-box-content');
            if($this_button.hasClass('icon-chevron-up')) {
                $this_button.removeClass('icon-chevron-up').addClass('icon-chevron-down');
                $box.removeClass('spirebuilder-box-unfolded').addClass('spirebuilder-box-folded');
                $box_content.slideUp(200);
            } else {
                $this_button.removeClass('icon-chevron-down').addClass('icon-chevron-up');
                $box.removeClass('spirebuilder-box-folded').addClass('spirebuilder-box-unfolded');
                $box_content.slideDown(200);
            }
        });
    };
    SpireBuilder.prototype.setActionWidgetRemove = function () {
        var $filled_template;
        var $widget;
        var $prev_dropzone;
        var $body = jQuery('body');
        $body.on('click', '.spirebuilder-box .spirebuilder-control-remove', function () {
            $widget = jQuery(this).closest('.spirebuilder-widget');
            $prev_dropzone = $widget.prev('.spirebuilder-dropzone');
            $filled_template = jQuery(SpireBuilder.templates_container['modal-remove']({
                object: 'widget'
            }));
            $filled_template.modal().on('hidden', function () {
                jQuery(this).remove();
            });
        });
        $body.on('click', '.codefield-modal-button-remove-widget', function () {
            if($prev_dropzone.attr('data-widget-id') != '0' && $prev_dropzone.nextAll('.spirebuilder-widget').length <= 1 || $prev_dropzone.attr('data-widget-id') == '0' && jQuery('#spirebuilder-editzone').children('.spirebuilder-widget').length <= 1) {
                $prev_dropzone.prev('.spirebuilder-dropzone-little').hide();
                $prev_dropzone.droppable('enable').show('blind');
            }
            $widget.add($widget.next('.spirebuilder-dropzone-little')).hide('blind', function () {
                jQuery(this).remove();
                jQuery('.spire-builder-upload-lom').first().trigger('click');
            });
        });
    };
    SpireBuilder.prototype.setActionWidgetEdit = function () {
        jQuery('body').on('click', '.spirebuilder-box .spirebuilder-control-edit', function () {
            var $this_button = jQuery(this);
            var widget_type = $this_button.closest('.spirebuilder-widget').attr('data-widget-type');
            var context = {
                id: $this_button.closest('.spirebuilder-widget').attr('id').split('spirebuilder-widget-')[1],
                options: jQuery.parseJSON($this_button.closest('.spirebuilder-widget').attr('data-widget-options'))
            };
            context = SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].getShowOptionsContext(context);
            var $filled_template = jQuery(SpireBuilder.templates_container['options-' + widget_type](context));
            $filled_template.find('.codefield-tabs, .codefield-tabs-second-lvl').tabs({
                collapsible: false,
                fx: {
                    duration: 150,
                    opacity: 'toggle'
                },
                hide: {
                    duration: 150
                },
                show: {
                    duration: 150
                }
            });
            if(SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('setShowOptionsBehaviour')) {
                SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].setShowOptionsBehaviour($filled_template);
            }
            jQuery($filled_template).modal({
                backdrop: 'static'
            }).on('hidden', function () {
                jQuery(this).remove();
            });
        });
    };
    SpireBuilder.prototype.setActionWidgetEditSave = function () {
        var self = this;
        jQuery('body').on('click', '.codefield-modal .spirebuilder-modal-confirmation-button', function () {
            var $codefield_modal = jQuery(this).closest('.codefield-modal');
            var widget_id = $codefield_modal.find('input[name="id"]').val();
            var $widget = jQuery('#spirebuilder-widget-' + widget_id);
            var widget_type = $widget.attr('data-widget-type');
            var serialized_options_array = $codefield_modal.closest('.codefield-modal').find('form').serializeArray();
            var options = {
            };
            jQuery.each(serialized_options_array, function (index, element) {
                if(element.name.split('.').length > 1) {
                    if(!options.hasOwnProperty(element.name.split('.')[0])) {
                        options[element.name.split('.')[0]] = {
                        };
                    }
                    options[element.name.split('.')[0]][element.name.split('.')[1]] = element.value;
                } else {
                    options[element.name] = element.value;
                }
            });
            if($codefield_modal.find('iframe').length > 0) {
                $codefield_modal.find('iframe').each(function () {
                    var serialized_options = jQuery('form', jQuery(this).contents()).serializeArray();
                    jQuery.each(serialized_options, function (index, element) {
                        if(element.name.split('.').length > 1) {
                            options[element.name.split('.')[0]] = {
                            };
                            options[element.name.split('.')[0]][element.name.split('.')[1]] = element.value;
                        } else {
                            options[element.name] = element.value;
                        }
                    });
                });
            }
            if(SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('getSaveOptionsContext')) {
                SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].getSaveOptionsContext(options);
            }
            var options_context = {
            };
            jQuery.extend(true, options_context, JSON.parse($widget.attr('data-widget-options')), options);
            var context = {
                id: widget_id,
                options: options_context
            };
            $widget.attr('data-widget-options', JSON.stringify(context.options));
            if(SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('setUpdatedContent')) {
                SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].setUpdatedContent(context);
            }
            self.hookDropPostWidgetAction();
        });
    };
    SpireBuilder.prototype.setActionWidgetClone = function () {
        var self = this;
        jQuery('body').on('click', '.spirebuilder-box .spirebuilder-control-clone', function () {
            var $this_button = jQuery(this);
            $this_button.tooltip('hide');
            self.cloneWidgetSection($this_button.closest('.spirebuilder-widget'), $this_button, false);
        });
    };
    SpireBuilder.prototype.cloneWidgetSection = function ($source, $destination, is_section) {
        var self = this;
        var $cloned_HTML_section = $source.clone().hide();
        if(!is_section) {
            var new_widget_attributes = {
                id: 'spirebuilder-widget-' + jQuery.now(),
                'data-widget-dropzones': {
                }
            };
            var old_dropzones_id = jQuery.parseJSON($cloned_HTML_section.attr('data-widget-dropzones'));
            var index = 0;
            for(var dropzone_id in old_dropzones_id) {
                if(old_dropzones_id.hasOwnProperty(dropzone_id)) {
                    var new_dropzone_id = jQuery.now() + '-' + index;
                    new_widget_attributes['data-widget-dropzones'][dropzone_id] = new_dropzone_id;
                    $cloned_HTML_section.find('#spirebuilder-dropzone-' + old_dropzones_id[dropzone_id]).attr('id', 'spirebuilder-dropzone-' + new_dropzone_id);
                    index += 1;
                }
            }
            new_widget_attributes['data-widget-dropzones'] = JSON.stringify(new_widget_attributes['data-widget-dropzones']);
            $cloned_HTML_section.attr(new_widget_attributes);
            var widget_type = $cloned_HTML_section.attr('data-widget-type');
            if(SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('setUpdatedClone')) {
                SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].setUpdatedClone($cloned_HTML_section);
            }
        }
        $cloned_HTML_section.find('.spirebuilder-widget').each(function () {
            var $the_widget = jQuery(this);
            $the_widget.parentsUntil($cloned_HTML_section, '.spirebuilder-widget').reverse().each(function () {
                var $the_ancestor = jQuery(this);
                var new_widget_attributes = {
                    id: 'spirebuilder-widget-' + jQuery.now(),
                    'data-widget-related-dropzone-id': $the_ancestor.parent().children('.spirebuilder-dropzone').attr('id').split('spirebuilder-dropzone-')[1],
                    'data-widget-dropzones': {
                    }
                };
                var old_dropzones_id = jQuery.parseJSON($the_ancestor.attr('data-widget-dropzones'));
                var index = 0;
                for(var dropzone_id in old_dropzones_id) {
                    if(old_dropzones_id.hasOwnProperty(dropzone_id)) {
                        var new_dropzone_id = jQuery.now() + '-' + index;
                        new_widget_attributes['data-widget-dropzones'][dropzone_id] = new_dropzone_id;
                        $the_ancestor.find('#spirebuilder-dropzone-' + old_dropzones_id[dropzone_id]).attr('id', 'spirebuilder-dropzone-' + new_dropzone_id);
                        index += 1;
                    }
                }
                new_widget_attributes['data-widget-dropzones'] = JSON.stringify(new_widget_attributes['data-widget-dropzones']);
                $the_ancestor.attr(new_widget_attributes);
                var widget_type = $the_ancestor.attr('data-widget-type');
                if(SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('setUpdatedClone')) {
                    SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].setUpdatedClone($the_ancestor);
                }
            });
            var new_widget_attributes = {
                id: 'spirebuilder-widget-' + jQuery.now(),
                'data-widget-related-dropzone-id': $the_widget.parent().children('.spirebuilder-dropzone').attr('id').split('spirebuilder-dropzone-')[1],
                'data-widget-dropzones': {
                }
            };
            var old_dropzones_id = jQuery.parseJSON($the_widget.attr('data-widget-dropzones'));
            var index = 0;
            for(var dropzone_id in old_dropzones_id) {
                if(old_dropzones_id.hasOwnProperty(dropzone_id)) {
                    var new_dropzone_id = jQuery.now() + '-' + index;
                    new_widget_attributes['data-widget-dropzones'][dropzone_id] = new_dropzone_id;
                    $the_widget.find('#spirebuilder-dropzone-' + old_dropzones_id[dropzone_id]).attr('id', 'spirebuilder-dropzone-' + new_dropzone_id);
                    index += 1;
                }
            }
            new_widget_attributes['data-widget-dropzones'] = JSON.stringify(new_widget_attributes['data-widget-dropzones']);
            $the_widget.attr(new_widget_attributes);
            var widget_type = $the_widget.attr('data-widget-type');
            if(SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('setUpdatedClone')) {
                SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].setUpdatedClone($the_widget);
            }
        });
        if(!is_section) {
            $destination.closest('.spirebuilder-widget').next().after($cloned_HTML_section);
            var $inserted_widget = $destination.closest('.spirebuilder-widget').next().next();
        } else {
            $destination.append($cloned_HTML_section.unwrap());
            var $inserted_widget = $destination.children();
        }
        $inserted_widget.show('fade');
        $inserted_widget.after(SpireBuilder.templates_container['dropzone-little']());
        $inserted_widget.next('.spirebuilder-dropzone-little').removeClass('codefield-display-none');
        self.updateRelatedDropzoneOrder();
        self.hookDropPostWidgetAction();
    };
    SpireBuilder.prototype.setActionWidgetAdd = function () {
        var self = this;
        var $body = jQuery('body');
        var $the_dropzone;
        var $filled_template;
        $body.on('click', '.spirebuilder-dropzone-little, .spirebuilder-dropzone', function () {
            $the_dropzone = jQuery(this);
            $filled_template = jQuery(SpireBuilder.templates_container['dialog-widget-list']({
            }));
            $filled_template.find('.codefield-tabs, .codefield-tabs-second-lvl').tabs({
                collapsible: false,
                fx: {
                    duration: 150,
                    opacity: 'toggle'
                },
                hide: {
                    duration: 150
                },
                show: {
                    duration: 150
                }
            });
            $filled_template.find('.spirebuilder-widgets-list').slimscroll({
                railVisible: true,
                height: '400px'
            });
            $filled_template.modal().on('hidden', function () {
                jQuery(this).remove();
            });
        });
        $body.on('click', '#spirebuilder-dialog-widget-list .spirebuilder-widgets-list > li a', function () {
            var $this_widget = jQuery(this);
            if($the_dropzone.hasClass('spirebuilder-dropzone-little')) {
                var $dropzone = $the_dropzone.parent().children('.spirebuilder-dropzone').first();
            } else {
                var $dropzone = $the_dropzone;
            }
            var widget_type = $this_widget.attr('data-widget-template');
            if(widget_type == 'template') {
                var data_widget_template_LOM = $this_widget.attr('data-widget-template-lom');
                data_widget_template_LOM = data_widget_template_LOM.replace(/TEMPLATE/g, jQuery.now());
                data_widget_template_LOM = jQuery.parseJSON(data_widget_template_LOM);
                if($the_dropzone.hasClass('spirebuilder-dropzone-little')) {
                    var related_dropzone_id = -1;
                } else {
                    var related_dropzone_id = $the_dropzone.attr('id').split('spirebuilder-dropzone-')[1];
                }
                for(var widget in data_widget_template_LOM.children) {
                    if(data_widget_template_LOM.children.hasOwnProperty(widget)) {
                        data_widget_template_LOM.children[widget].related_dropzone_id = related_dropzone_id;
                    }
                }
                CodeField.Utils.Z(self.createHTMLFromLOM)(data_widget_template_LOM, self, ($the_dropzone.hasClass('spirebuilder-dropzone-little')) ? $the_dropzone : false);
            } else {
                var $inserted_widget = self.createWidgetFragment({
                    type: widget_type,
                    related_dropzone_id: $dropzone.attr('id').split('spirebuilder-dropzone-')[1],
                    related_dropzone_order: $dropzone.parent().children('.spirebuilder-widget').length,
                    options: {
                    }
                }, self, ($the_dropzone.hasClass('spirebuilder-dropzone-little')) ? $the_dropzone : false);
                if(jQuery('#spirebuilder-edit-modal').val() == 1 && jQuery.inArray(widget_type, jQuery('#spirebuilder-edit-modal-exclusions').val().split(',')) == -1) {
                    setTimeout(function () {
                        $inserted_widget.find('.spirebuilder-box .spirebuilder-control-edit').trigger('click');
                    }, 150);
                }
            }
            self.updateRelatedDropzoneOrder();
            self.hookDropPostWidgetAction();
            $filled_template.modal('hide');
            $dropzone.droppable('disable');
        });
    };
    SpireBuilder.prototype.setActionWidgetSaveTemplate = function () {
        var self = this;
        var $filled_template;
        var $body = jQuery('body');
        var LOM_template;
        var template_title;
        $body.on('click', '.spirebuilder-box .spirebuilder-control-save', function () {
            var $the_button = jQuery(this);
            LOM_template = self.updateHTML2LOM($the_button.closest('.spirebuilder-widget').clone().wrap('<div />').parent(), true);
            $filled_template = jQuery(SpireBuilder.templates_container['dialog-widget-save']({
            }));
            $filled_template.modal().on('hidden', function () {
                jQuery(this).remove();
            }).on('shown', function () {
                jQuery(this).find('input[type="text"]').focus();
            });
        });
        $body.on('click', '.spirebuilder-modal-button-save-widget', function () {
            var $the_button = jQuery(this);
            template_title = $the_button.closest('.modal').find('input[type="text"]').val();
            jQuery.ajax({
                data: {
                    action: 'spirebuilder_update_builder_templates',
                    post_id: self.post_id,
                    SpireBuilderNonce: SpireBuilderNonce.spirebuilder_update_builder_templates,
                    LOM: JSON.stringify(LOM_template),
                    title: template_title
                },
                dataType: 'json',
                error: function (jqXHR, textStatus, errorThrown) {
                    if(errorThrown) {
                        CodeField.Utils.showMessage({
                            type: 'error',
                            message: textStatus + ': ' + errorThrown,
                            templates_container: SpireBuilder.templates_container
                        });
                    }
                },
                success: function (response_from_server) {
                    if(response_from_server.type == 'success') {
                        var $filled_template = jQuery(SpireBuilder.templates_container['widget-template']({
                            id: response_from_server.data.id,
                            title: template_title,
                            LOM: LOM_template
                        }));
                        jQuery('#spirebuilder-templates').find('.spirebuilder-widgets-list').append($filled_template).find('.spirebuilder-templates-empty-message ').hide('blind');
                        self.setDragAndDrop();
                        if(jQuery.fn.hasOwnProperty('tooltip')) {
                            jQuery('.tip').tooltip();
                        }
                    }
                    CodeField.Utils.showMessage({
                        type: response_from_server.type,
                        message: response_from_server.message,
                        templates_container: SpireBuilder.templates_container
                    });
                },
                type: 'POST',
                url: ajaxurl
            });
        });
    };
    SpireBuilder.prototype.setAjaxForms = function () {
        var self = this;
        jQuery('.codefield-submit-button', '.codefield-ajax-form').on('click', function () {
            var $the_form = jQuery('.codefield-ajax-form');
            var $the_button = jQuery(this);
            var serialized_options = jQuery('input[type="text"],input[type="hidden"],input[type="checkbox"],input[type="radio"],select,textarea', '.codefield-ajax-form').serializeArray();
            var data = {
                action: 'spirebuilder_update_builder_settings',
                post_id: self.post_id,
                SpireBuilderNonce: SpireBuilderNonce.spirebuilder_update_builder_settings
            };
            data[$the_button.attr('name')] = '';
            jQuery.each(serialized_options, function (index, element) {
                data[element.name] = element.value;
            });
            jQuery.ajax({
                beforeSend: function () {
                    $the_form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
                    jQuery('.codefield-submit-tr button[type="submit"]', $the_form).button('disable');
                },
                data: data,
                dataType: 'json',
                error: function (jqXHR, textStatus, errorThrown) {
                    var $message_dashboard = $the_form.find('.codefield-message-dashboard');
                    if(errorThrown) {
                        $the_form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
                        jQuery('.codefield-submit-tr button[type="submit"]').button('enable');
                        CodeField.Utils.showMessage({
                            type: 'error',
                            message: textStatus + ': ' + errorThrown,
                            $message_dashboard: $message_dashboard,
                            templates_container: SpireBuilder.templates_container
                        });
                    }
                },
                success: function (response_from_server) {
                    var $message_dashboard = $the_form.find('.codefield-message-dashboard');
                    $the_form.find('.codefield-submit-tr button[type="submit"] i').toggleClass('icon-save icon-spinner icon-spin');
                    jQuery('.codefield-submit-tr button[type="submit"]', $the_form).button('enable');
                    CodeField.Utils.showMessage({
                        type: response_from_server.type,
                        message: response_from_server.message,
                        $message_dashboard: $message_dashboard,
                        templates_container: SpireBuilder.templates_container
                    });
                },
                type: 'POST',
                url: ajaxurl
            });
            return false;
        });
    };
    SpireBuilder.prototype.setUploadLOM = function () {
        var self = this;
        jQuery('.codefield-page').on('click', '.spire-builder-upload-lom', function () {
            self.LOM = self.updateHTML2LOM(jQuery('#spirebuilder-editzone'));
            jQuery('#spirebuilder-LOM').val(JSON.stringify(self.LOM));
            var $this_button = jQuery(this);
            var $message_dashboard = $this_button.closest('.ui-tabs-panel').find('.codefield-message-dashboard');
            jQuery.ajax({
                beforeSend: function () {
                    $this_button.find('i').toggleClass('icon-save icon-spinner icon-spin');
                    $this_button.button('disable');
                },
                data: {
                    action: 'spirebuilder_update_builder_lom',
                    post_id: self.post_id,
                    SpireBuilderNonce: SpireBuilderNonce.spirebuilder_update_builder_lom,
                    LOM: JSON.stringify(self.LOM)
                },
                dataType: 'json',
                error: function (jqXHR, textStatus, errorThrown) {
                    if(errorThrown) {
                        $this_button.find('i').toggleClass('icon-save icon-spinner icon-spin');
                        $this_button.button('enable');
                        CodeField.Utils.showMessage({
                            type: 'error',
                            message: textStatus + ': ' + errorThrown,
                            $message_dashboard: $message_dashboard,
                            templates_container: SpireBuilder.templates_container
                        });
                    }
                },
                success: function (response_from_server) {
                    $this_button.find('i').toggleClass('icon-save icon-spinner icon-spin');
                    $this_button.button('enable');
                    $message_dashboard.html('');
                    CodeField.Utils.showMessage({
                        type: response_from_server.type,
                        message: response_from_server.message,
                        $message_dashboard: $message_dashboard,
                        templates_container: SpireBuilder.templates_container
                    });
                },
                type: 'POST',
                url: ajaxurl
            });
            return false;
        });
    };
    SpireBuilder.prototype.updateHTML2LOM = function ($LOM_section, is_template) {
        var $widgets = jQuery('.spirebuilder-widget', $LOM_section);
        var built_LOM = this.setDefaultLOM();
        $widgets.each(function () {
            var $the_widget = jQuery(this);
            var ancestor_chain_reference = built_LOM;
            $the_widget.parentsUntil($LOM_section, '.spirebuilder-widget').reverse().each(function () {
                var $the_ancestor = jQuery(this);
                if(!ancestor_chain_reference.hasOwnProperty('children')) {
                    ancestor_chain_reference.children = {
                    };
                }
                var ancestor_widget_id = $the_ancestor.attr('id').split('spirebuilder-widget-')[1];
                if(ancestor_chain_reference.children.hasOwnProperty(ancestor_widget_id) || ancestor_chain_reference.children.hasOwnProperty(ancestor_widget_id + '-TEMPLATE')) {
                    ancestor_chain_reference = ancestor_chain_reference.children[ancestor_widget_id + ((is_template) ? '-TEMPLATE' : '')];
                } else {
                    var dropzones = jQuery.parseJSON($the_ancestor.attr('data-widget-dropzones'));
                    if(is_template) {
                        for(var dropzone in dropzones) {
                            if(dropzones.hasOwnProperty(dropzone)) {
                                dropzones[dropzone] = dropzones[dropzone] + '-TEMPLATE';
                            }
                        }
                    }
                    var widget_type = $the_ancestor.attr('data-widget-type');
                    var options = jQuery.parseJSON($the_ancestor.attr('data-widget-options'));
                    if(is_template) {
                        if(SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('regenerateOptions')) {
                            SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].regenerateOptions(options);
                        }
                    }
                    ancestor_chain_reference.children[ancestor_widget_id + ((is_template) ? '-TEMPLATE' : '')] = {
                        id: $the_ancestor.attr('data-widget-id') + ((is_template) ? '-TEMPLATE' : ''),
                        type: widget_type,
                        related_dropzone_id: $the_ancestor.attr('data-widget-related-dropzone-id') + ((is_template) ? '-TEMPLATE' : ''),
                        related_dropzone_order: $the_ancestor.attr('data-widget-related-dropzone-order'),
                        dropzones: dropzones,
                        options: options
                    };
                }
            });
            if(!ancestor_chain_reference.hasOwnProperty('children')) {
                ancestor_chain_reference.children = {
                };
            }
            var widget_id = $the_widget.attr('id').split('spirebuilder-widget-')[1];
            var dropzones = jQuery.parseJSON($the_widget.attr('data-widget-dropzones'));
            if(is_template) {
                for(var dropzone in dropzones) {
                    if(dropzones.hasOwnProperty(dropzone)) {
                        dropzones[dropzone] = dropzones[dropzone] + '-TEMPLATE';
                    }
                }
            }
            var widget_type = $the_widget.attr('data-widget-type');
            var options = jQuery.parseJSON($the_widget.attr('data-widget-options'));
            if(is_template) {
                if(SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('regenerateOptions')) {
                    SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].regenerateOptions(options);
                }
            }
            ancestor_chain_reference.children[widget_id + ((is_template) ? '-TEMPLATE' : '')] = {
                id: widget_id + ((is_template) ? '-TEMPLATE' : ''),
                type: $the_widget.attr('data-widget-type'),
                related_dropzone_id: $the_widget.attr('data-widget-related-dropzone-id') + ((is_template) ? '-TEMPLATE' : ''),
                related_dropzone_order: $the_widget.attr('data-widget-related-dropzone-order'),
                dropzones: dropzones,
                options: options
            };
        });
        return built_LOM;
    };
    SpireBuilder.prototype.updateLOM2HTML = function () {
        var LOM_input_field = jQuery.parseJSON(jQuery('#spirebuilder-LOM').val());
        if(LOM_input_field) {
            this.LOM = LOM_input_field;
        } else {
            this.LOM = this.setDefaultLOM();
        }
        CodeField.Utils.Z(this.createHTMLFromLOM)(this.LOM, this);
        this.setDroppables();
        this.hookDropPostWidgetAction();
        jQuery('.spirebuilder-dropzone', '#spirebuilder-editzone').each(function () {
            var $this_dropzone = jQuery(this);
            if($this_dropzone.next('.spirebuilder-widget').length != 0) {
                $this_dropzone.droppable('disable');
            }
        });
    };
    SpireBuilder.prototype.createHTMLFromLOM = function (callee, LOM_section, self, dropzone_little) {
        if(LOM_section.type != 'root') {
            self.createWidgetFragment(LOM_section, self, (LOM_section.related_dropzone_id == -1) ? dropzone_little : false);
        }
        for(var widget in LOM_section.children) {
            if(LOM_section.children.hasOwnProperty(widget)) {
                callee(LOM_section.children[widget], self, dropzone_little);
            }
        }
    };
    SpireBuilder.prototype.createWidgetFragment = function (LOM_section, self, $dropzone_little) {
        if(jQuery.inArray(LOM_section.type, jQuery('#spirebuilder-available_widgets').val().split(',')) != -1) {
            var self = self || this;
            var $dropzone = jQuery('#spirebuilder-dropzone-' + ((LOM_section.related_dropzone_id == -1) ? '0' : LOM_section.related_dropzone_id));
            var context = SpireBuilderWidgets[CodeField.Utils.toPascalCase(LOM_section.type)].getBuilderContext(LOM_section);
            var $filled_template = jQuery(SpireBuilder.templates_container['builder-' + LOM_section.type](context));
            $filled_template.hide();
            if($dropzone_little) {
                if($dropzone_little.next().hasClass('spirebuilder-dropzone')) {
                    $dropzone_little = $dropzone_little.next();
                }
                $dropzone_little.after($filled_template);
                var $inserted_widget = $dropzone_little.next();
            } else {
                if($dropzone.nextAll('.spirebuilder-widget').length) {
                    $dropzone.nextAll('.spirebuilder-widget').last().next('.spirebuilder-dropzone-little').after($filled_template);
                    var $inserted_widget = $dropzone.nextAll('.spirebuilder-widget').last();
                } else {
                    $dropzone.after($filled_template);
                    var $inserted_widget = $dropzone.next();
                }
            }
            $inserted_widget.show('fade');
            $dropzone.prev('.spirebuilder-dropzone-little').show('fade');
            $inserted_widget.after(SpireBuilder.templates_container['dropzone-little']());
            $inserted_widget.next('.spirebuilder-dropzone-little').removeClass('codefield-display-none');
            $dropzone.not('#spirebuilder-editzone').hide();
            if(SpireBuilderWidgets[CodeField.Utils.toPascalCase(LOM_section.type)].hasOwnProperty('setBuilderBehaviour')) {
                SpireBuilderWidgets[CodeField.Utils.toPascalCase(LOM_section.type)].setBuilderBehaviour($inserted_widget);
            }
            return $inserted_widget;
        }
    };
    SpireBuilder.moveWidgetSection = function moveWidgetSection(related_dropzone_id, $dropzone_little, $cloned_HTML_section) {
        var $dropzone = jQuery('#spirebuilder-dropzone-' + related_dropzone_id);
        if($dropzone_little) {
            if($dropzone_little.next().hasClass('spirebuilder-dropzone')) {
                $dropzone_little = $dropzone_little.next();
            }
            $dropzone_little.after($cloned_HTML_section);
            var $inserted_widget = $dropzone_little.next();
        } else {
            if($dropzone.nextAll('.spirebuilder-widget').length) {
                $dropzone.nextAll('.spirebuilder-widget').last().next('.spirebuilder-dropzone-little').after($cloned_HTML_section);
                var $inserted_widget = $dropzone.nextAll('.spirebuilder-widget').last();
            } else {
                $dropzone.after($cloned_HTML_section);
                var $inserted_widget = $dropzone.next();
            }
        }
        var $widgets = $inserted_widget.add($inserted_widget.find('.spirebuilder-widget'));
        for(var i = 0; i < $widgets.length; i++) {
            var widget_type = $widgets.eq(i).attr('data-widget-type');
            if(SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].hasOwnProperty('refreshBehaviour')) {
                SpireBuilderWidgets[CodeField.Utils.toPascalCase(widget_type)].refreshBehaviour($widgets.eq(i));
            }
        }
        $inserted_widget.removeClass('spirebuilder-widget-draggable-placeholder').show('fade');
        $dropzone.prev('.spirebuilder-dropzone-little').show('fade');
        $inserted_widget.after(SpireBuilder.templates_container['dropzone-little']());
        $inserted_widget.next('.spirebuilder-dropzone-little').removeClass('codefield-display-none');
        $dropzone.not('#spirebuilder-editzone').hide().droppable('disable');
    }
    SpireBuilder.prototype.updateRelatedDropzoneOrder = function () {
        var $widgets = jQuery('.spirebuilder-widget', '#spirebuilder-editzone');
        if($widgets.length != 0) {
            $widgets.each(function () {
                var $the_widget = jQuery(this);
                var related_dropzone_order = $the_widget.prevAll('.spirebuilder-widget').length;
                $the_widget.attr('data-widget-related-dropzone-order', related_dropzone_order);
            });
        }
    };
    SpireBuilder.prototype.setToolbarBehaviour = function () {
        this.setFixedToolbar();
        this.setRemoveTemplateBehaviour();
    };
    SpireBuilder.prototype.setFixedToolbar = function () {
        if(jQuery('#spirebuilder-toolbar-fixed').val() == 1) {
            var self = this;
            var $toolbar = jQuery('#spirebuilder-builder-toolbar', '.codefield-page');
            jQuery(document).on('scroll', function () {
                self.updateToolbarPosition($toolbar);
            });
            jQuery(window).on('resize', function () {
                self.updateToolbarPosition($toolbar);
            });
        }
    };
    SpireBuilder.prototype.updateToolbarPosition = function ($toolbar) {
        var toolbar_offset = jQuery('#spirebuilder_builder').find('.inside').offset().top;
        if(jQuery(document).scrollTop() > toolbar_offset - 28) {
            $toolbar.addClass('codefield-position-fixed').css({
                left: $toolbar.parent('.codefield-page').offset().left,
                width: $toolbar.parent('.codefield-page').width()
            });
            if($toolbar.tabs('option', 'active') == 0) {
                $toolbar.tabs('option', 'active', 2);
            }
        } else {
            $toolbar.removeClass('codefield-position-fixed').css({
                left: 0,
                width: '100%'
            });
        }
    };
    SpireBuilder.prototype.setRemoveTemplateBehaviour = function () {
        var $filled_template;
        var $body = jQuery('body');
        var $template_item;
        $body.on('mouseenter', '.spirebuilder-widget-template', function () {
            jQuery(this).find('.badge').show();
        }).on('mouseleave', '.spirebuilder-widget-template', function () {
            jQuery(this).find('.badge').hide();
        });
        $body.on('mouseenter', '.spirebuilder-widget-template .spirebuilder-widget-template-remove', function () {
            jQuery(this).addClass('badge-important');
        }).on('mouseleave', '.spirebuilder-widget-template .badge', function () {
            jQuery(this).removeClass('badge-important');
        });
        $body.on('click', '.spirebuilder-widget-template .spirebuilder-widget-template-remove', function () {
            $template_item = jQuery(this).closest('a');
            $filled_template = jQuery(SpireBuilder.templates_container['modal-remove']({
                object: 'template'
            }));
            $filled_template.modal().on('hidden', function () {
                jQuery(this).remove();
            });
        });
        $body.on('click', '.codefield-modal-button-remove-template', function () {
            jQuery.ajax({
                data: {
                    action: 'spirebuilder_update_builder_templates_delete',
                    id: $template_item.attr('data-widget-template-id'),
                    SpireBuilderNonce: SpireBuilderNonce.spirebuilder_update_builder_templates_delete
                },
                dataType: 'json',
                error: function (jqXHR, textStatus, errorThrown) {
                    if(errorThrown) {
                        CodeField.Utils.showMessage({
                            type: 'error',
                            message: textStatus + ': ' + errorThrown,
                            templates_container: SpireBuilder.templates_container
                        });
                    }
                },
                success: function (response_from_server) {
                    $template_item.closest('li').hide('fold', function () {
                        jQuery(this).remove();
                        var $templates_list = jQuery('#spirebuilder-templates').find('.spirebuilder-widgets-list li');
                        if($templates_list.length == 2) {
                            $templates_list.filter('.spirebuilder-templates-empty-message').removeClass('codefield-display-none').show('blind');
                        } else {
                            $templates_list.filter('.spirebuilder-templates-empty-message').addClass('codefield-display-none').hide('blind');
                        }
                        CodeField.Utils.showMessage({
                            type: response_from_server.type,
                            message: response_from_server.message,
                            templates_container: SpireBuilder.templates_container
                        });
                    });
                },
                type: 'POST',
                url: ajaxurl
            });
        });
    };
    return SpireBuilder;
})();
jQuery(function () {
    var spire_builder = new SpireBuilder();
    spire_builder.init();
    SpireBuilder['builders'] = [];
    SpireBuilder['builders'].push(spire_builder);
});
//@ sourceMappingURL=builder.js.map
