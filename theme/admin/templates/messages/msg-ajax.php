{{!
	/**
	 * Template to show the messages via AJAX.
	 *
	 * @param    {string}  type           Message type. Can be 'error', 'success', 'warning', 'info'.
	 * @param    {string}  message        Message text to be shown to user.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
}}
<script data-codefield-template="messages" type="text/x-handlebars-template">
	<div class="alert alert-{{ type }}">
		<button data-dismiss="alert" class="close" type="button">×</button>
		<strong class="codefield-capitalize">{{ type }}<?php _e('!', SpireBuilder::$i18n_prefix); ?></strong> {{ message }}
	</div>
</script>



