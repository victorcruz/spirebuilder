{{!
	/**
	 * Template to show a little dropzone.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
}}
<script data-codefield-template="dropzone-little" type="text/x-handlebars-template">
	<div class="spirebuilder-dropzone-little codefield-display-none tip" title="<?php _e('Click to Add widget', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip">
		<div class="spirebuilder-dropzone-info">
			<i class="icon icon-plus icon-1x"></i>
		</div>
	</div>
</script>



