<?php
	/**
	 * Template to show the Add new widget dialog.
	 *
	 * @uses    array   $templates[]                                A collection of templates along with its data.
	 * @uses    string  $templates[]['id']                          The identifier for the templates. It have to be URL and id safe.
	 * @uses    string  $templates[]['title']                       The templates title to be displayed to users.
	 * @uses    string  $templates[]['LOM']                         The templates LOM section. The templates children widgets hierarchically ordered along with its data.
	 *
	 * @uses    array   $all_categories                                 A collection of widget categories along with its data.
	 * @uses    string  $all_categories[]['category_id']                The identifier for the category. It have to be URL and id safe.
	 * @uses    string  $all_categories[]['title']                      The category title to be displayed to users.
	 * @uses    string  $all_categories[]['icons']                      A collection of icons classes or paths to be used in the plugin.
	 * @uses    string  $all_categories[]['icons']['builder']           A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @uses    array   $all_categories[]['widgets']                    A collection of widgets along with its data.
	 * @uses    string  $all_categories[]['widgets']['id']              The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string  $all_categories[]['widgets']['title']           The widget title to be displayed to users.
	 * @uses    string  $all_categories[]['widgets']['description']     The widget description to be displayed to users.
	 *
	 * @uses    array   $all_categories[]['widgets']['icons']                A collection of icons classes or paths to be used in the plugin.
	 * @uses    string  $all_categories[]['widgets']['icons']['builder']     A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-codefield-template="dialog-widget-list" type="text/x-handlebars-template">
	<div id="spirebuilder-dialog-widget-list" class="codefield-modal modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4>
				<?php _e('Select one of the available widgets', SpireBuilder::$i18n_prefix); ?>
			</h4>
		</div>
		<div class="modal-body">
			<div class="codefield-tabs-second-lvl">
				<ul>
					<?php if (count($all_categories) >= 2): ?>
						<li>
							<a href="#spirebuilder-widget-list-all">
								<?php _e('All', SpireBuilder::$i18n_prefix); ?>
							</a>
						</li>
					<?php endif; ?>

					<?php foreach ($all_categories as $category): ?>
						<li>
							<a href="#spirebuilder-widget-list-<?php echo $category['category_id'] ?>">
								<i class="icon icon-large <?php echo $category['icons']['builder'] ?>"></i>

								<?php _e( $category['title'], SpireBuilder::$i18n_prefix); ?>
							</a>
						</li>
					<?php endforeach; ?>

					<li class="spirebuilder-widget-separator">
						<a>
							<div class="codefield-border-bottom-dashed"></div>
						</a>
					</li>

					<li class="spirebuilder-templates-add-widget">
						<a href="#spirebuilder-widget-list-templates">
							<i class="icon icon-large icon-file-text"></i>
							<?php _e('Templates', SpireBuilder::$i18n_prefix); ?>
						</a>
					</li>
				</ul>

				<?php foreach ($all_categories as $category): ?>
					<div id="spirebuilder-widget-list-<?php echo $category['category_id'] ?>" class="ui-corner-top">
						<ul class="spirebuilder-widgets-list">
							<?php foreach ($category['widgets'] as $widget): ?>
								<li>
									<a href="#" onclick="return false;" data-widget-template="<?php echo $widget['id']?>">
										<span class="pull-left">
											<?php if ( isset( $widget['icons']['builder'] ) ): ?>
												<i class="icon icon-2x <?php echo $widget['icons']['builder'] ?>"></i>
											<?php elseif ( ! isset( $widget['icons']['builder'] ) && file_exists(SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg') ): ?>
												<img width="32" src="<?php echo SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg'?>" />
											<?php elseif ( ! isset( $widget['icons']['builder'] ) && ! file_exists(SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg') ): ?>
												<i class="icon icon-2x icon-sign-blank"></i>
											<?php endif; ?>
										</span>

										<span class="pull-right">
											<span class="spirebuilder-header">
												<?php _e( $widget['title'], SpireBuilder::$i18n_prefix); ?>
											</span>

											<br />
											<span class="description">
												<?php _e( $widget['description'], SpireBuilder::$i18n_prefix); ?>
											</span>
										</span>

										<br class="clearfix" />
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					</div><!-- end #spirebuilder-widgets-[widget] -->
				<?php endforeach; ?>

				<?php if (count($all_categories) >= 2): ?>
					<div id="spirebuilder-widget-list-all" class="ui-corner-top">
						<ul class="spirebuilder-widgets-list">

							<?php foreach ($widgets as $widget): ?>
								<li>
									<a href="#" onclick="return false;" data-widget-template="<?php echo $widget['id']?>">
										<span class="pull-left">
											<?php if ( isset( $widget['icons']['builder'] ) ): ?>
												<i class="icon icon-2x <?php echo $widget['icons']['builder']; ?>"></i>
											<?php elseif ( ! isset( $widget['icons']['builder'] ) && file_exists(SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg') ): ?>
												<img width="32" src="<?php echo SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg'?>" />
											<?php elseif ( ! isset( $widget['icons']['builder'] ) && ! file_exists(SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg') ): ?>
												<i class="icon-2x icon-sign-blank <?php echo $settings[ 'toolbar_icon_size' ] .' ' .  $toolbar_layout;?>"></i>
											<?php endif; ?>
										</span>

										<span class="pull-right">
											<span class="spirebuilder-header">
												<?php _e( $widget['title'], SpireBuilder::$i18n_prefix); ?>
											</span>

											<br />
											<span class="description">
												<?php _e( $widget['description'], SpireBuilder::$i18n_prefix); ?>
											</span>
										</span>

										<br class="clearfix" />
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				<?php endif; ?><!-- end #spirebuilder-widgets-all -->

				<div id="spirebuilder-widget-list-templates" class="ui-corner-top">
					<ul class="spirebuilder-widgets-list">

						<?php foreach ($templates as $template): ?>
							<li>
								<a href="#" onclick="return false;" class="spirebuilder-widget-template" data-widget-template="template" data-widget-template-id="<?php echo $template['id'];?>" data-widget-template-LOM='<?php echo $template['LOM'];?>'>
									<i class="icon icon-2x icon-file-text"></i>

									<span class="spirebuilder-header">
										<?php _e( $template['title'], SpireBuilder::$i18n_prefix); ?>
									</span>
								</a>
							</li>
						<?php endforeach; ?>

						<li class="spirebuilder-templates-empty-message <?php echo (count($templates) == 0)?'codefield-display-none':'';?>">
							<div class="alert alert-info">
								<strong><?php _e('No user defined templates available.', SpireBuilder::$i18n_prefix); ?></strong>
							</div>
						</li>
					</ul>
				</div><!-- end #spirebuilder-widget-list-templates -->
			</div>
		</div>
		<div class="modal-footer">
			<button class="button button-large" data-dismiss="modal" onclick="return false;"><?php _e('Cancel', SpireBuilder::$i18n_prefix); ?></button>
		</div>
	</div>
</script>