<?php
	/**
	 * Template to show the Requirements page.
	 *
	 * @uses    array       $requirements                   A collection of plugin requirements.
	 * @uses    string      $requirements[]['type']         The type for the requirement. Can be one of 'error', 'success'.
	 * @uses    string      $requirements[]['message']      The information message being shown to the user.
	 *
	 * @package admin-panel
	 * @since   0.1
	 *
	 */
?>
<div id="codefield-requirements" class="wrap codefield-page">
	<div class="icon32" id="icon-link"><br /></div>
	<h2 class="codefield-page-header">
		<?php _e('Requirements', SpireBuilder::$i18n_prefix) ?>
		<br />
		<span class="description"><?php _e('This page shows you the plugin requirements.', SpireBuilder::$i18n_prefix) ?></span>
	</h2>

	<?php include "messages/msg-dashboard.php"; ?>

	<br />
	<table cellspacing="0">
		<tbody>
			<?php foreach ($requirements as $requirements_item): ?>
				<tr>
					<td class="codefield-icon-cell">
						<i class="icon icon-large icon-<?php echo ($requirements_item['type'] == 'error' )?'remove':'ok' ?>"></i>
					</td>
					<td class="codefield-msg-cell">
						<p>
							<?php echo $requirements_item['message'] ?>
						</p>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div><!-- end #spirebuilder-requirements.codefield-page -->

<div id="codefield-templates" class="codefield-display-none">
	<?php include "messages/msg-ajax.php"; ?>
</div>