<?php
	/**
	 * Template to show the Save As Template widget dialog.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-codefield-template="dialog-widget-save" type="text/x-handlebars-template">
	<div id="spirebuilder-dialog-widget-save" class="codefield-modal modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4>
				<?php _e('Save As Template', SpireBuilder::$i18n_prefix); ?>
			</h4>
		</div>
		<div class="modal-body">
			<label>
				<?php _e('Enter a title for the saved template', SpireBuilder::$i18n_prefix); ?>
				&nbsp;&nbsp;&nbsp;
				<input type="text" class="codefield-half-wide" name="template_title" value="" />
			</label>
		</div>
		<div class="modal-footer">
			<button class="spirebuilder-modal-button-save-widget button button-large button-primary" data-dismiss="modal" onclick="return false;"><?php _e('Save As Template', SpireBuilder::$i18n_prefix); ?></button>
			<button class="button button-large" data-dismiss="modal" onclick="return false;"><?php _e('Cancel', SpireBuilder::$i18n_prefix); ?></button>
		</div>
	</div>
</script>