<?php
	/**
	 * Template to show the Content Builder in the Post/Page screens.
	 *
	 * @uses    int         $post_id                                    Post or page identifier.
	 *
	 * Global Settings.
	 * @uses    Checkbox    $global_settings['fixed_toolbar']           1- The toolbar will become a floated bar when the scrolling hides it. 0- If not.
	 *
	 * @uses    Checkbox    $global_settings['edit_modal']              1- Launch the Edit dialog right after of widget addition. 0- If not.
	 * @uses    string      $global_settings['edit_modal_exclusions']   CSV collection of widget ids to be excluded from the Edit dialog launch behaviour. Default: 'container'.
	 *
	 * @uses    Radio       $global_settings['toolbar_layout']          Toolbar layout styles. Can be one of: text_below_icons, text_after_icons, icons_only, text_only. Default value: text_after_icons.
	 * @uses    Radio       $global_settings['toolbar_icon_size']       Toolbar icon styles. Can be one of: mini, normal, large. Default value: normal.
	 *
	 * @uses    string      $global_settings['widget_content_max_chars']                   Maximum characters shown in widget. Can be one of: all, restrict. Default value: 'all'.
	 * @uses    int         $global_settings['widget_content_max_chars_restricted_to']     Restrict the widget content to show only this amount of characters. Default value: 100.
	 *
	 * @uses    string      $available_widgets       CSV collection of widget types to be available in the builder.
	 *
	 * TAB: Settings.
	 * @uses    array       $settings                                   A collection of settings keys and values.
	 * @uses    Checkbox    $settings['allow_spire_builder_editor']     1- If user want to use generated pages for the content. 0- If not.
	 * @uses    Radio       $settings['content_placement']              SpireBuilder placement relative the WordPress content. Can be one of: replace, before, after. Default value: replace.
	 *
	 * @uses    string      $settings['LOM']                            The LOM (Layout Object Document), that is the compiled HTML layout in a JSON format.
	 *
	 * @uses    Submit      submit_update_builder_settings              Update Settings AJAX action.
	 *
	 * TAB: Templates
	 * @uses    array       $templates[]                                A collection of templates along with its data.
	 * @uses    string      $templates[]['id']                          The identifier for the templates. It have to be URL and id safe.
	 * @uses    string      $templates[]['title']                       The templates title to be displayed to users.
	 * @uses    string      $templates[]['LOM']                         The templates LOM section. The templates children widgets hierarchically ordered along with its data.
	 *
	 * TAB: Categories.
	 * @uses    array       $all_categories                                     A collection of widget categories along with its data.
	 * @uses    string      $all_categories[]['category_id']                    The identifier for the category. It have to be URL and id safe.
	 * @uses    string      $all_categories[]['title']                          The category title to be displayed to users.
	 * @uses    string      $all_categories[]['icons']                          A collection of icons classes or paths to be used in the plugin.
	 * @uses    string      $all_categories[]['icons']['builder']               A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @uses    array       $all_categories[]['widgets']                        A collection of widgets along with its data.
	 * @uses    string      $all_categories[]['widgets']['id']                  The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string      $all_categories[]['widgets']['title']               The widget title to be displayed to users.
	 * @uses    string      $all_categories[]['widgets']['description']         The widget description to be displayed to users.
	 *
	 * @uses    array       $all_categories[]['widgets']['icons']                A collection of icons classes or paths to be used in the plugin.
	 * @uses    string      $all_categories[]['widgets']['icons']['builder']     A CSS class related to the Fontawesome icon to be used. Empty if not set.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>

<?php // Don't change the simple quotes since the value is JSON. ?>
<input id="spirebuilder-LOM" type="hidden" name="spirebuilder_LOM" value='<?php echo $settings['LOM']; ?>'/>
<input id="spirebuilder-toolbar-fixed" type="hidden" name="fixed_toolbar" value="<?php echo $global_settings['fixed_toolbar']; ?>"/>

<input id="spirebuilder-edit-modal" type="hidden" name="edit_modal" value="<?php echo $global_settings['edit_modal']; ?>"/>
<input id="spirebuilder-edit-modal-exclusions" type="hidden" name="edit_modal_exclusions" value="<?php echo $global_settings['edit_modal_exclusions']; ?>"/>

<input id="spirebuilder-widget-content-max-chars" type="hidden" name="widget_content_max_chars" value="<?php echo $global_settings['widget_content_max_chars']; ?>"/>
<input id="spirebuilder-widget-content-max-chars-restricted-to" type="hidden" name="widget_content_max_chars_restricted_to" value="<?php echo $global_settings['widget_content_max_chars_restricted_to']; ?>"/>

<input id="spirebuilder-available_widgets" type="hidden" name="available_widgets" value="<?php echo $available_widgets; ?>"/>
<div class="codefield-page">
	<div id="spirebuilder-builder-toolbar" class="codefield-tabs">
		<ul>
			<li class="spirebuilder-settings"><a href="#spirebuilder-settings"><?php _e('Settings', SpireBuilder::$i18n_prefix); ?></a></li>
			<li class="spirebuilder-templates"><a href="#spirebuilder-templates"><?php _e('Templates', SpireBuilder::$i18n_prefix); ?></a></li>

			<?php if (count($all_categories) >= 2): ?>
				<li>
					<a href="#spirebuilder-widgets-all"><?php _e('All', SpireBuilder::$i18n_prefix); ?></a>
				</li>
			<?php endif; ?>

			<?php foreach ($all_categories as $category): ?>
				<li>
					<a href="#spirebuilder-widgets-<?php echo $category['category_id'] ?>">
						<?php _e( $category['title'], SpireBuilder::$i18n_prefix); ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>

		<div id="spirebuilder-settings" class="ui-corner-top codefield-position-relative">
			<div class="spirebuilder-tab-border codefield-position-absolute"></div>

			<div class="codefield-ajax-form">
				<table class="form-table">
					<tbody>
						<tr>
							<th scope="row"></th>
							<td>
								<span class="description">
									<strong><?php _e('These options supersedes the global settings options.', SpireBuilder::$i18n_prefix); ?></strong>
								</span>
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="spirebuilder-allow-spire-builder-editor">
									<?php _e('Use Spire Builder Editor', SpireBuilder::$i18n_prefix); ?>
								</label>
							</th>
							<td>
								<label>
									<input id="spirebuilder-allow-spire-builder-editor" type="checkbox" name="allow_spire_builder_editor" value="1" <?php echo ($settings['allow_spire_builder_editor'] == '1') ? 'checked="checked"' : ''; ?> />
									<span class="description"><?php _e('Check if you want to use the Spire Builder generated pages for the content.', SpireBuilder::$i18n_prefix); ?></span>
								</label>
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="spirebuilder-content-placement-replace-content">
									<?php _e('SpireBuilder Content Placement', SpireBuilder::$i18n_prefix); ?>
								</label>
							</th>
							<td>
								<label for="spirebuilder-content-placement-replace-content">
									<input id="spirebuilder-content-placement-replace-content" type="radio" name="content_placement" value="replace" <?php echo ($settings['content_placement'] == 'replace') ? 'checked="checked"' : ''; ?> />
									<strong><?php _e('Replace', SpireBuilder::$i18n_prefix); ?></strong><?php _e('  WordPress content with SpireBuilder content.', SpireBuilder::$i18n_prefix); ?>
								</label>

								<br />
								<label for="spirebuilder-content-placement-before-content">
									<input id="spirebuilder-content-placement-before-content" type="radio" name="content_placement" value="before" <?php echo ($settings['content_placement'] == 'before') ? 'checked="checked"' : ''; ?> />
									<?php _e('Place SpireBuilder content ', SpireBuilder::$i18n_prefix); ?><strong><?php _e('before', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' the WordPress content.', SpireBuilder::$i18n_prefix); ?>
								</label>

								<br />
								<label for="spirebuilder-content-placement-after-content">
									<input id="spirebuilder-content-placement-after-content" type="radio" name="content_placement" value="after" <?php echo ($settings['content_placement'] == 'after') ? 'checked="checked"' : ''; ?> />
									<?php _e('Place SpireBuilder content ', SpireBuilder::$i18n_prefix); ?><strong><?php _e('after', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' the WordPress content.', SpireBuilder::$i18n_prefix); ?>
								</label>
							</td>
						</tr>
						<tr class="codefield-submit-tr">
							<th></th>
							<td>
								<button type="submit" class="button button-primary button-large codefield-submit-button" name="submit_update_builder_settings">
									<i class="icon-save icon-1x"></i>
									<?php _e('Update Settings', SpireBuilder::$i18n_prefix); ?>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div><!-- end #spirebuilder-settings -->

		<div id="spirebuilder-templates" class="ui-corner-top  codefield-position-relative">
			<div class="spirebuilder-tab-border codefield-position-absolute"></div>

			<ul class="spirebuilder-widgets-list">
				<li class="<?php echo $toolbar_layout; ?> spirebuilder-item-has-button">
					<button type="button" class="codefield-button button button-primary spire-builder-upload-templates">
						<i class="icon icon-save <?php echo $toolbar_icon_size; ?>"></i>
						<span class="spirebuilder-text">
							<?php _e('Save', SpireBuilder::$i18n_prefix); ?>
						</span>
					</button>
				</li>
				<?php foreach ($templates as $template): ?>
					<li class="<?php echo $toolbar_layout; ?>">
						<a class="codefield-draggable tip spirebuilder-widget-template" href="#" onclick="return false;" data-widget-template="template" data-widget-template-id="<?php echo $template['id'];?>" data-widget-template-LOM='<?php echo $template['LOM'];?>' title="<?php _e('Drag to use this template.', SpireBuilder::$i18n_prefix); ?>" data-toggle="tooltip" data-placement="bottom">
							<i class="icon icon-file-text <?php echo $toolbar_icon_size; ?>"></i>

							<span class="spirebuilder-text">
								<?php _e( $template['title'], SpireBuilder::$i18n_prefix); ?>
							</span>

							<span class="badge spirebuilder-widget-template-remove">&times;</span>
						</a>
					</li>
				<?php endforeach; ?>

				<li class="spirebuilder-templates-empty-message <?php echo (count($templates) != 0)?'codefield-display-none':'';?>">
					<div class="alert alert-info">
						<strong><?php _e('No user defined templates available.', SpireBuilder::$i18n_prefix); ?></strong>
					</div>
				</li>
			</ul>
		</div><!-- end #spirebuilder-templates -->

		<?php foreach ($all_categories as $category): ?>
			<div id="spirebuilder-widgets-<?php echo $category['category_id']; ?>" class="ui-corner-top">

				<ul class="spirebuilder-widgets-list">
					<li class="<?php echo $toolbar_layout; ?> spirebuilder-item-has-button">
						<button type="button" class="codefield-button button button-primary spire-builder-upload-lom">
							<i class="icon icon-save <?php echo $toolbar_icon_size; ?>"></i>
							<span class="spirebuilder-text">
								<?php _e('Save', SpireBuilder::$i18n_prefix); ?>
							</span>
						</button>
					</li>
					<?php foreach ($category['widgets'] as $widget): ?>
						<li class="<?php echo $toolbar_layout; ?>">
							<a class="codefield-draggable tip" href="#" onclick="return false;" data-widget-template="<?php echo $widget['id']?>" title="<?php _e( $widget['description'], SpireBuilder::$i18n_prefix); ?>" data-toggle="tooltip" data-placement="bottom">
								<?php if ( isset( $widget['icons']['builder'] ) ): ?>
									<i class="icon <?php echo $widget['icons']['builder'] . ' ' . $toolbar_icon_size; ?>"></i>
								<?php elseif ( ! isset( $widget['icons']['builder'] ) && file_exists(SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg') ): ?>
									<img width="32" src="<?php echo SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg'?>" />
								<?php elseif ( ! isset( $widget['icons']['builder'] ) && ! file_exists(SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg') ): ?>
									<i class="icon icon-sign-blank <?php echo $toolbar_icon_size; ?>"></i>
								<?php endif; ?>
							<span class="spirebuilder-text">
								<?php _e( $widget['title'], SpireBuilder::$i18n_prefix); ?>
							</span>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div><!-- end #spirebuilder-widgets-[widget] -->
		<?php endforeach; ?>

		<?php if (count($all_categories) >= 2): ?>
			<div id="spirebuilder-widgets-all" class="ui-corner-top">
				<ul class="spirebuilder-widgets-list">

					<li class="<?php echo $toolbar_layout; ?> spirebuilder-item-has-button">
						<button type="button" class="codefield-button button button-primary spire-builder-upload-lom">
							<i class="icon icon-save <?php echo $toolbar_icon_size; ?>"></i>
							<span class="spirebuilder-text">
								<?php _e('Save', SpireBuilder::$i18n_prefix); ?>
							</span>
						</button>
					</li>
					<?php foreach ($widgets as $widget): ?>
						<li class="<?php echo $toolbar_layout ?>">
							<a class="codefield-draggable tip" href="#" onclick="return false;" data-widget-template="<?php echo $widget['id']?>" title="<?php _e( $widget['description'], SpireBuilder::$i18n_prefix); ?>" data-toggle="tooltip" data-placement="bottom">
								<?php if ( isset( $widget['icons']['builder'] ) ): ?>
									<i class="icon <?php echo $widget['icons']['builder'] . ' ' . $toolbar_icon_size;  ?>"></i>
								<?php elseif ( ! isset( $widget['icons']['builder'] ) && file_exists(SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg') ): ?>
									<img width="32" src="<?php echo SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg'?>" />
								<?php elseif ( ! isset( $widget['icons']['builder'] ) && ! file_exists(SpireBuilder::$widgets_url . $widget['id'] . '/images/builder.svg') ): ?>
									<i class="icon icon-sign-blank <?php echo $toolbar_icon_size;?>"></i>
								<?php endif; ?>
								<span class="spirebuilder-text">
									<?php _e( $widget['title'], SpireBuilder::$i18n_prefix); ?>
								</span>
							</a>
						</li>
					 <?php endforeach; ?>
				</ul>
			</div><!-- end #spirebuilder-widgets-all -->
		<?php endif; ?>
	</div>

	<div id="spirebuilder-editzone">
		<div class="spirebuilder-dropzone-little codefield-display-none tip" title="<?php _e('Click to Add widget', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip">
			<div class="spirebuilder-dropzone-info">
				<i class="icon icon-plus icon-1x"></i>
			</div>
		</div>

		<div id="spirebuilder-dropzone-0" class="spirebuilder-dropzone tip" title="<?php _e('Click to Add widget', SpireBuilder::$i18n_prefix) ?>" data-toggle="tooltip" data-widget-type="root" data-widget-id="0" data-widget-dropzone-id="0" data-widget-dropzone-order="0">
			<div class="spirebuilder-dropzone-info">
				<i class="icon icon-plus icon-2x"></i>
				<br />
				<?php _e('Dropzone', SpireBuilder::$i18n_prefix); ?>
			</div>
		</div>
	</div>

	<div id="codefield-templates" class="codefield-display-none">
		<?php include "messages/msg-ajax.php"; ?>
		<?php include "dropzones-little.php"; ?>

		<?php include "modal/add.php"; ?>
		<?php include "modal/remove.php"; ?>

		<?php include "add-widget.php"; ?>
		<?php include "save-widget.php"; ?>

		<?php include "widget-templates.php"; ?>

		<?php foreach ($all_categories as $category): ?>
			<?php foreach ($category['widgets'] as $widget): ?>
				<?php include (SpireBuilder::$widgets_dir . $widget['id'] . "/templates/builder/widget.php"); ?>
				<?php include (SpireBuilder::$widgets_dir . $widget['id'] . "/templates/builder/options.php"); ?>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</div>
</div>