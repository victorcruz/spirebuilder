<?php
	/**
	 * Template to show the widget templates.
	 *
	 * toolbar_layout
	 * $toolbar_icon_size
	 *
	 * @uses    string      $id         The identifier for the templates. It have to be URL and id safe.
	 * @uses    string      $title      The templates title to be displayed to users.
	 * @uses    string      $LOM        The templates LOM section. The templates children widgets hierarchically ordered along with its data.
	 *
	 * @package admin-panel
	 * @since   0.1
	 */
?>
<script data-codefield-template="widget-template" type="text/x-handlebars-template">
	<li class="<?php echo $toolbar_layout; ?>">
		<a class="codefield-draggable tip spirebuilder-widget-template" href="#" onclick="return false;" data-widget-template="template" data-widget-template-id="{{ id }}" data-widget-template-LOM="{{ JSON2string LOM }}" title="<?php _e('Drag to use this template.', SpireBuilder::$i18n_prefix); ?>" data-toggle="tooltip" data-placement="bottom">
			<i class="icon icon-file-text <?php echo $toolbar_icon_size; ?>"></i>

			<span class="spirebuilder-text">
				{{ title }}
			</span>

			<span class="badge spirebuilder-widget-template-remove">&times;</span>
		</a>
	</li>
</script>