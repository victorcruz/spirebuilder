{{!
	/**
	 * Template to show a modal Remove dialog.
	 *
	 * @param    {string}  object           Type of object which this modal will operate upon.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
}}
<script data-codefield-template="modal-remove" type="text/x-handlebars-template">
	<div class="codefield-modal modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4>
				<?php _e('Are you sure?', SpireBuilder::$i18n_prefix); ?>
			</h4>
		</div>
		<div class="modal-body">
			<?php _e('You are about to ', SpireBuilder::$i18n_prefix); ?>
			<strong><?php _e('REMOVE', SpireBuilder::$i18n_prefix); ?></strong>
			<?php _e(' this ', SpireBuilder::$i18n_prefix); ?>
			{{ object }}
			<?php _e('.', SpireBuilder::$i18n_prefix); ?>

			<br />
			<br />
			<strong><?php _e('Are you sure?', SpireBuilder::$i18n_prefix); ?></strong>
		</div>
		<div class="modal-footer">
			<button class="codefield-modal-button-remove-{{ object }} button button-large button-primary" data-dismiss="modal" onclick="return false;"><?php _e('Remove', SpireBuilder::$i18n_prefix); ?></button>
			<button class="button button-large" data-dismiss="modal" onclick="return false;"><?php _e('Cancel', SpireBuilder::$i18n_prefix); ?></button>
		</div>
	</div>
</script>