{{!
	/**
	 * Template to show a modal Add dialog.
	 *
	 * @param    {string}  object           Type of object which this modal will operate upon.
	 *
	 * @since 0.1
	 * @version 0.1
	 */
}}
<script data-codefield-template="modal-add" type="text/x-handlebars-template">
	<div class="codefield-modal modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4>
				<?php _e('Add New ', SpireBuilder::$i18n_prefix); ?>
				{{ object }}
			</h4>
		</div>
		<div class="modal-body">
			<label>
				<?php _e('Enter a title for the new ', SpireBuilder::$i18n_prefix); ?>
				{{ object }}
				&nbsp;&nbsp;&nbsp;
				<input type="text" class="codefield-half-wide" name="{{ object }}_title" value="" />
			</label>
		</div>
		<div class="modal-footer">
			<button class="codefield-modal-button-add-{{ object }} button button-large button-primary" data-dismiss="modal" onclick="return false;"><?php _e('Add', SpireBuilder::$i18n_prefix); ?></button>
			<button class="button button-large" data-dismiss="modal" onclick="return false;"><?php _e('Cancel', SpireBuilder::$i18n_prefix); ?></button>
		</div>
	</div>
</script>