<?php
	/**
	 * Template to show the Settings page.
	 *
	 * @uses    array       $settings                                       A collection of settings keys and values.
	 *
	 * TAB: General Settings.
	 * @uses    Checkbox    $settings['allow_spire_builder_editor']         1- If user want to use generated pages for the content. 0- If not.
	 * @uses    Radio       $settings['content_placement']                  SpireBuilder placement relative the WordPress content. Can be one of: replace, before, after. Default value: replace.
	 * @uses    Checkbox    $settings['available_widgets_all']
	 *
	 * @uses    array       $settings['builder_content_types'][]
	 * @uses    string      $settings['builder_content_types']['id']
	 * @uses    string      $settings['builder_content_types']['name']
	 * @uses    string      $settings['builder_content_types']['status']    Can be active or disable
	 *
	 * TAB: Builder Settings. Toolbar.
	 * @uses    Checkbox    $settings['fixed_toolbar']                      1- The toolbar will become a floated bar when the scrolling hides it. 0- If not.
	 * @uses    Checkbox    $settings['edit_modal']                         1- Launch the Edit dialog right after of widget addition. 0- If not.
	 * @uses    array       $settings['edit_modal_exclusions']              Collection of widget ids to be excluded from the Edit dialog launch behaviour. Default: 'container','columns'.
	 *
	 * @uses    Radio       $settings['toolbar_layout']                     Toolbar layout styles. Can be one of: text_below_icons, text_after_icons, icons_only, text_only. Default value: text_after_icons.
	 * @uses    Radio       $settings['toolbar_icon_size']                  Toolbar icon styles. Can be one of: mini, normal, large. Default value: normal.
	 *
	 * TAB: Builder Settings. Widgets.
	 * @uses    Radio       $settings['widget_content_max_chars']                   Maximum characters shown in widget. Can be one of: all, restrict. Default value: 'all'.
	 * @uses    int         $settings['widget_content_max_chars_restricted_to']     Restrict the widget content to show only this amount of characters. Default value: 100.
	 *
	 * @uses    array       $all_categories                                 A collection of widget categories along with its data.
	 * @uses    string      $all_categories[]['category_id']                The identifier for the category. It have to be URL and id safe.
	 * @uses    string      $all_categories[]['title']                      The category title to be displayed to users.
	 *
	 * @uses    array       $all_categories[]['widgets']                    A collection of widgets along with its data.
	 * @uses    string      $all_categories[]['widgets']['id']              The identifier for the widget. It have to be URL and id safe. Can be its folder name.
	 * @uses    string      $all_categories[]['widgets']['title']           The widget title to be displayed to users.
	 * @uses    string      $all_categories[]['widgets']['description']     The widget description to be displayed to users.
	 *
	 * @uses    int         $settings['available_widgets_all']              1- All widgets will be available in the builder. 0- If not.
	 * @uses    array       $settings['available_widgets']                  A collection of widgets to be available or not in the builder.
	 * @uses    array       $settings['available_widgets'][$id]             1- The widget will be available in the builder. 0- If not.
	 *
	 * @package admin-panel
	 * @since   0.1
	 *
	 */
?>
<div class="wrap codefield-page" id="spirebuilder-settings-page" xmlns="http://www.w3.org/1999/html">
	<div class="icon32" id="icon-options-general"><br /></div>
	<h2 class="codefield-page-header">
		<?php _e('Settings', SpireBuilder::$i18n_prefix) ?>
		<br />
		<span class="description"><?php _e('This page shows you the settings for the plugin.', SpireBuilder::$i18n_prefix) ?></span>
	</h2>

	<?php include "messages/msg-dashboard.php"; ?>

	<div class="codefield-tabs">
		<ul>
			<li class="spirebuilder-general-settings">
				<a href="#spirebuilder-general-settings"><?php _e('General Settings', SpireBuilder::$i18n_prefix); ?></a>
			</li>
			<li class="spirebuilder-builder-settings">
				<a href="#spirebuilder-builder-settings"><?php _e('Builder Settings', SpireBuilder::$i18n_prefix); ?></a>
			</li>
		</ul>

		<div id="spirebuilder-general-settings" class="ui-corner-top">
			<form class="codefield-ajax-form" action="" method="post" data-codefield-action="spirebuilder_update_general_settings" data-codefield-nonce="<?php echo wp_create_nonce('spirebuilder_update_general_settings'); ?>">
				<table class="form-table">
					<tbody>
						<tr>
							<th scope="row">
								<label for="spirebuilder-allow-spire-builder-editor">
									<?php _e('Use Spire Builder Editor', SpireBuilder::$i18n_prefix); ?>
								</label>
							</th>
							<td>
								<label>
									<input id="spirebuilder-allow-spire-builder-editor" type="checkbox" name="allow_spire_builder_editor" value="1" <?php echo ($settings['allow_spire_builder_editor'] == '1') ? 'checked="checked"' : ''; ?> />
									<span class="description"><?php _e('Check if you want to use the Spire Builder generated pages for the content.', SpireBuilder::$i18n_prefix); ?></span>
								</label>
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="spirebuilder-content-placement-replace-content">
									<?php _e('SpireBuilder Content Placement', SpireBuilder::$i18n_prefix); ?>
								</label>
							</th>
							<td>
								<label for="spirebuilder-content-placement-replace-content">
									<input id="spirebuilder-content-placement-replace-content" type="radio" name="content_placement" value="replace" <?php echo ($settings['content_placement'] == 'replace') ? 'checked="checked"' : ''; ?> />
									<strong><?php _e('Replace', SpireBuilder::$i18n_prefix); ?></strong><?php _e('  WordPress content with SpireBuilder content.', SpireBuilder::$i18n_prefix); ?>
								</label>

								<br />
								<label for="spirebuilder-content-placement-before-content">
									<input id="spirebuilder-content-placement-before-content" type="radio" name="content_placement" value="before" <?php echo ($settings['content_placement'] == 'before') ? 'checked="checked"' : ''; ?> />
									<?php _e('Place SpireBuilder content ', SpireBuilder::$i18n_prefix); ?><strong><?php _e('before', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' the WordPress content.', SpireBuilder::$i18n_prefix); ?>
								</label>

								<br />
								<label for="spirebuilder-content-placement-after-content">
									<input id="spirebuilder-content-placement-after-content" type="radio" name="content_placement" value="after" <?php echo ($settings['content_placement'] == 'after') ? 'checked="checked"' : ''; ?> />
									<?php _e('Place SpireBuilder content ', SpireBuilder::$i18n_prefix); ?><strong><?php _e('after', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' the WordPress content.', SpireBuilder::$i18n_prefix); ?>
								</label>
							</td>
						</tr>
						<tr>
							<th>
								<label for="spirebuilder-content-types-all">
									<?php _e('Content Types', SpireBuilder::$i18n_prefix); ?>
								</label>
							</th>
							<td>
								<span class="description">
									<?php _e('Select the content types where you want to show the builder.', SpireBuilder::$i18n_prefix); ?>
								</span>
								<?php foreach ( $settings['builder_content_types'] as $builder_content_type ): ?>
								<div class="codefield-margin-bottom"></div>
								<label>
									<input type="checkbox" name="builder_content_types[]" value="<?php echo $builder_content_type['id']; ?>" <?php echo ( $builder_content_type['status'] == 'active') ? 'checked="checked"' : ''; ?> />
									<span class="description">
										<?php _e( $builder_content_type['name'] , SpireBuilder::$i18n_prefix); ?>
									</span>
								</label>
								<?php endforeach; ?>
							</td>
						</tr>
						<tr class="codefield-submit-tr">
							<th></th>
							<td>
								<button type="submit" class="button button-primary button-large codefield-submit-button" name="submit_update_general_settings">
									<i class="icon-save icon-1x"></i>
									<?php _e('Update Settings', SpireBuilder::$i18n_prefix); ?>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div><!-- end #spirebuilder-general-settings -->

		<div id="spirebuilder-builder-settings" class="ui-corner-top">
			<form class="codefield-ajax-form" action="" method="post" data-codefield-action="spirebuilder_update_settings_builder_settings" data-codefield-nonce="<?php echo wp_create_nonce('spirebuilder_update_settings_builder_settings'); ?>">
				<div class="codefield-tabs-second-lvl ui-corner-all">
					<ul>
						<li class="codefield-tabs-second-lvl">
							<a href="#spirebuilder-builder-settings-toolbar-settings">
								<i class="icon icon-film icon-1x"></i>
								<?php _e('Toolbar', SpireBuilder::$i18n_prefix); ?>
							</a>
						</li>
						<li class="codefield-tabs-second-lvl">
							<a href="#spirebuilder-builder-settings-widgets">
								<i class="icon icon-cogs icon-1x"></i>
								<?php _e('Widgets', SpireBuilder::$i18n_prefix); ?>
							</a>
						</li>
					</ul>

					<div id="spirebuilder-builder-settings-toolbar-settings">
						<table class="form-table">
							<tbody>
								<tr>
									<th colspan="2">
										<h2><?php _e('Behaviour', SpireBuilder::$i18n_prefix); ?></h2>
									</th>
								</tr>
								<tr>
									<th>
										<label for="spirebuilder-fixed-toolbar">
											<?php _e('Enable fixed toolbar', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input id="spirebuilder-fixed-toolbar" type="checkbox" name="fixed_toolbar" value="1" <?php echo ($settings['fixed_toolbar'] == 1) ? 'checked="checked"' : ''; ?> />
											<span class="description">
												<?php _e('Leave it checked if you want that the builder toolbar convert to a floated bar when the scrolling hides it.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>
									</td>
								</tr>

								<tr>
									<th>
										<label for="spirebuilder-edit-modal">
											<?php _e('Launch Edit dialog behaviour', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input id="spirebuilder-edit-modal" type="checkbox" name="edit_modal" value="1" <?php echo ($settings['edit_modal'] == 1) ? 'checked="checked"' : ''; ?> />
											<span class="description">
												<?php _e('Check it to launch the ', SpireBuilder::$i18n_prefix); ?><strong><?php _e('Edit', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' options dialog right after of widget addition.', SpireBuilder::$i18n_prefix); ?>
											</span>
										</label>

										<br />
										<br />
										<span class="description">
											<strong><?php _e('Exclude below widgets from the launch Edit dialog behaviour.', SpireBuilder::$i18n_prefix); ?></strong>
										</span>
										<br />
										<br />

										<div class="select-container">
											<select class="chzn-select" name="edit_modal_exclusions[]" multiple="multiple" data-placeholder="<?php _e('Select widgets to be excluded.', SpireBuilder::$i18n_prefix); ?>">
												<option value=""></option>

												<?php foreach ( $all_categories as $category ): ?>

													<optgroup label="<?php _e($category['title'], SpireBuilder::$i18n_prefix); ?>">

													<?php foreach ( $category['widgets'] as $widget ): ?>
														<option value="<?php echo $widget['id']; ?>" <?php echo ( isset($settings['edit_modal_exclusions']) && $settings['edit_modal_exclusions'] != '' && in_array( $widget['id'], $settings['edit_modal_exclusions'] ) ) ? 'selected="selected"' : ''; ?>><?php _e($widget['title'], SpireBuilder::$i18n_prefix); ?></option>
													<?php endforeach; ?>
													</optgroup>

												<?php endforeach; ?>
											</select>
										</div>
									</td>
								</tr>

								<tr>
									<th colspan="2">
										<h2><?php _e('Styles', SpireBuilder::$i18n_prefix); ?></h2>
									</th>
								</tr>
								<tr class="spirebuilder-toolbar-settings-styles-row">
									<th>
										<label for="spirebuilder-toolbar-layout-text-below-icons">
											<?php _e('Layout', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label for="spirebuilder-toolbar-layout-text-below-icons">
											<input id="spirebuilder-toolbar-layout-text-below-icons" type="radio" name="toolbar_layout" value="text_below_icons" <?php echo ($settings['toolbar_layout'] == 'text_below_icons') ? 'checked="checked"' : ''; ?> />
											<?php _e('Text ', SpireBuilder::$i18n_prefix); ?><strong><?php _e('below', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' icons.', SpireBuilder::$i18n_prefix); ?>
										</label>

										<br />
										<label for="spirebuilder-toolbar-layout-text-after-icons">
											<input id="spirebuilder-toolbar-layout-text-after-icons" type="radio" name="toolbar_layout" value="text_after_icons" <?php echo ($settings['toolbar_layout'] == 'text_after_icons') ? 'checked="checked"' : ''; ?> />
											<?php _e('Text ', SpireBuilder::$i18n_prefix); ?><strong><?php _e('after', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' icons.', SpireBuilder::$i18n_prefix); ?>
										</label>

										<br />
										<label for="spirebuilder-toolbar-layout-icons-only">
											<input id="spirebuilder-toolbar-layout-icons-only" type="radio" name="toolbar_layout" value="icons_only" <?php echo ($settings['toolbar_layout'] == 'icons_only') ? 'checked="checked"' : ''; ?> />
											<?php _e('Icons ', SpireBuilder::$i18n_prefix); ?><strong><?php _e('only', SpireBuilder::$i18n_prefix); ?></strong><?php _e('.', SpireBuilder::$i18n_prefix); ?>
										</label>

										<br />
										<label for="spirebuilder-toolbar-layout-text-only">
											<input id="spirebuilder-toolbar-layout-text-only" type="radio" name="toolbar_layout" value="text_only" <?php echo ($settings['toolbar_layout'] == 'text_only') ? 'checked="checked"' : ''; ?> />
											<?php _e('Text ', SpireBuilder::$i18n_prefix); ?><strong><?php _e('only', SpireBuilder::$i18n_prefix); ?></strong><?php _e('.', SpireBuilder::$i18n_prefix); ?>
										</label>
									</td>
								</tr>

								<tr class="spirebuilder-toolbar-settings-styles-row">
									<th>
										<label for="spirebuilder-icon-size-mini">
											<?php _e('Icon size', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label for="spirebuilder-icon-size-mini">
											<input id="spirebuilder-icon-size-mini" type="radio" name="toolbar_icon_size" value="mini" <?php echo ($settings['toolbar_icon_size'] == 'mini') ? 'checked="checked"' : ''; ?> />
											<?php _e('Mini.', SpireBuilder::$i18n_prefix); ?>
										</label>

										<br />
										<label for="spirebuilder-icon-size-normal">
											<input id="spirebuilder-icon-size-normal" type="radio" name="toolbar_icon_size" value="normal" <?php echo ($settings['toolbar_icon_size'] == 'normal') ? 'checked="checked"' : ''; ?> />
											<?php _e('Normal.', SpireBuilder::$i18n_prefix); ?>
										</label>

										<br />
										<label for="spirebuilder-icon-size-large">
											<input id="spirebuilder-icon-size-large" type="radio" name="toolbar_icon_size" value="large" <?php echo ($settings['toolbar_icon_size'] == 'large') ? 'checked="checked"' : ''; ?> />
											<?php _e('Large.', SpireBuilder::$i18n_prefix); ?>
										</label>
									</td>
								</tr>

								<tr class="codefield-submit-tr">
									<th></th>
									<td>
										<button type="submit" class="button button-primary button-large codefield-submit-button" name="submit_update_settings_builder_toolbar">
											<i class="icon-save icon-1x"></i>
											<?php _e('Update Settings', SpireBuilder::$i18n_prefix); ?>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div><!-- end #spirebuilder-settings-toolbar -->

					<div id="spirebuilder-builder-settings-widgets">
						<table class="form-table">
							<tbody>
								<tr>
									<th colspan="2">
										<h2><?php _e('Content Settings', SpireBuilder::$i18n_prefix); ?></h2>
									</th>
								</tr>
								<tr>
									<th>
										<label for="spirebuilder-widget-content-max-chars-all">
											<?php _e('Maximum content shown in widget', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input id="spirebuilder-widget-content-max-chars-all" type="radio" name="widget_content_max_chars" value="all" <?php echo ($settings['widget_content_max_chars'] == 'all') ? 'checked="checked"' : ''; ?> />
											<?php _e('Show ', SpireBuilder::$i18n_prefix); ?><strong><?php _e('all', SpireBuilder::$i18n_prefix); ?></strong><?php _e(' available content in widgets.', SpireBuilder::$i18n_prefix); ?>
										</label>

										<br />
										<label>
											<input id="spirebuilder-widget-content-max-chars-restrict" type="radio" name="widget_content_max_chars" value="restrict" <?php echo ($settings['widget_content_max_chars'] == 'restrict') ? 'checked="checked"' : ''; ?> />
											<?php _e('Show only the first ', SpireBuilder::$i18n_prefix); ?>
										</label>
										<label>
											<input type="text" id="spirebuilder-widget-content-max-chars-restricted-to" class="codefield-text-align-right" name="widget_content_max_chars_restricted_to" value="<?php echo $settings['widget_content_max_chars_restricted_to']; ?>" />
											<?php _e(' characters.', SpireBuilder::$i18n_prefix); ?>
										</label>
									</td>
								</tr>

								<tr>
									<th colspan="2">
										<h2><?php _e('Available Widgets List', SpireBuilder::$i18n_prefix); ?></h2>
									</th>
								</tr>
								<tr>
									<th>
										<label for="spirebuilder-toolbar-layout-text-below-icons">
											<?php _e('Select the available widgets', SpireBuilder::$i18n_prefix); ?>
										</label>
									</th>
									<td>
										<label>
											<input id="spirebuilder-available-widgets-all" type="checkbox" name="available_widgets_all" value="1" <?php echo ($settings['available_widgets_all'] == 1)?'checked="checked"':''; ?> />
											<span class="description"><?php _e('All widgets will be available to be used in the builder.', SpireBuilder::$i18n_prefix); ?></span>
										</label>

										<div id="spirebuilder-widgets-list-container">
											<span class="description"><?php _e('Or check which widgets will be available in the builder in the section below.', SpireBuilder::$i18n_prefix); ?></span>

											<table class="codefield-wide">
												<tbody>
													<?php foreach ( $all_categories as $category ): ?>
														<tr>
															<th colspan="2">
																<h2><?php _e($category['title'], SpireBuilder::$i18n_prefix); ?></h2>
															</th>
														</tr>
														<?php foreach ( $category['widgets'] as $widget ): ?>
															<tr>
																<th class="spirebuilder-field-cell">
																	<input id="spirebuilder-widget-<?php echo $widget['id']; ?>" type="checkbox" name="available_widgets[]" value="<?php echo  $widget['id']; ?>" <?php echo ( $widget['status'] == 'active')?'checked="checked"':''; ?> />
																</th>
																<td>

																	<label for="spirebuilder-widget-<?php echo $widget['id']; ?>">
																		<i class="icon icon-large <?php echo $widget['icons']['builder']; ?>"></i>
																		<span class="spirebuilder-header"><?php _e($widget['title'], SpireBuilder::$i18n_prefix); ?></span>
																		<br />
																		<span class="description"><?php _e($widget['description'], SpireBuilder::$i18n_prefix); ?></span>
																	</label>
																</td>
															</tr>
														<?php endforeach; ?>
												<?php endforeach; ?>
												</tbody>
											</table>
										</div><!-- end #spirebuilder-widgets-list-container -->
									</td>
								</tr>

								<tr class="codefield-submit-tr">
									<th></th>
									<td>
										<button type="submit" class="button button-primary button-large codefield-submit-button" name="submit_update_settings_builder_widgets">
											<i class="icon-save icon-1x"></i>
											<?php _e('Update Settings', SpireBuilder::$i18n_prefix); ?>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div><!-- end #spirebuilder-settings-widgets -->
				</div>
			</form>
		</div><!-- end #spirebuilder-builder-settings -->
	</div><!-- end .codefield-tabs -->
</div><!-- end .codefield-page -->

<div id="codefield-templates" class="codefield-display-none">
	<?php include "messages/msg-ajax.php"; ?>
</div>