<?php
/**
 * Page to handle all Ajax requests for update Settings
 *
 */

// Message for all wrong request
$message_to_return = __('Wrong request.', SpireBuilder::$i18n_prefix);
$type_to_return    = 'error';
$data_to_return    = null;


/**
 * General Settings
 */

// General
if (isset($_POST['action']) && 'spirebuilder_update_general_settings' == $_POST['action'] && isset($_POST['submit_update_general_settings'])) {
	if (wp_verify_nonce($_POST['CodeFieldNonce'], 'spirebuilder_update_general_settings')) {

		$settings = SpireBuilder_Settings::get_options();

		$settings['settings']['allow_spire_builder_editor'] = 0;
		if (isset ($_POST['allow_spire_builder_editor']) && SpireBuilder_Validator::parse_int($_POST['allow_spire_builder_editor'])) {
			$settings['settings']['allow_spire_builder_editor'] = 1;
		}
		$settings['settings']['content_placement'] = $_POST['content_placement'];
		SpireBuilder_Settings::update_options($settings);

		if (isset($_POST['builder_content_types'])) {
			SpireBuilder_Settings::update_builder_content_types($_POST['builder_content_types']);
		} else {
			SpireBuilder_Settings::update_builder_content_types(__return_empty_array());
		}

		$message_to_return = __('The settings were updated successfully.', SpireBuilder::$i18n_prefix);
		$type_to_return    = 'success';
	}
}

// Toolbar
if (isset($_POST['action']) && 'spirebuilder_update_settings_builder_settings' == $_POST['action'] && isset($_POST['submit_update_settings_builder_toolbar'])) {
	if (wp_verify_nonce($_POST['CodeFieldNonce'], 'spirebuilder_update_settings_builder_settings')) {

		$settings = SpireBuilder_Settings::get_options();

		$settings['settings']['fixed_toolbar'] = 0;
		if (isset ($_POST['fixed_toolbar']) && SpireBuilder_Validator::parse_int($_POST['fixed_toolbar'])) {
			$settings['settings']['fixed_toolbar'] = 1;
		}
		$settings['settings']['edit_modal'] = 0;
		if (isset ($_POST['edit_modal']) && SpireBuilder_Validator::parse_int($_POST['edit_modal'])) {
			$settings['settings']['edit_modal'] = 1;
		}

		$settings['settings']['toolbar_layout']    = $_POST['toolbar_layout'];
		$settings['settings']['toolbar_icon_size'] = $_POST['toolbar_icon_size'];

		if (isset($_POST['edit_modal_exclusions'])) {
			$settings['settings']['edit_modal_exclusions'] = $_POST['edit_modal_exclusions'];
		} else {
			$settings['settings']['edit_modal_exclusions'] = array();
		}

		SpireBuilder_Settings::update_options($settings);
		$message_to_return = __('The settings were updated successfully.', SpireBuilder::$i18n_prefix);
		$type_to_return    = 'success';
	}
}

// Widgets
if (isset($_POST['action']) && 'spirebuilder_update_settings_builder_settings' == $_POST['action'] && isset($_POST['submit_update_settings_builder_widgets'])) {
	if (wp_verify_nonce($_POST['CodeFieldNonce'], 'spirebuilder_update_settings_builder_settings')) {

		$settings = SpireBuilder_Settings::get_options();

		$settings['settings']['available_widgets_all'] = 0;
		if (isset ($_POST['available_widgets_all']) && SpireBuilder_Validator::parse_int($_POST['available_widgets_all'])) {
			$settings['settings']['available_widgets_all'] = 1;
		}

		$settings['settings']['widget_content_max_chars_restricted_to'] = 100;
		if (isset ($_POST['widget_content_max_chars_restricted_to']) && SpireBuilder_Validator::parse_int($_POST['widget_content_max_chars_restricted_to'])) {
			$settings['settings']['widget_content_max_chars_restricted_to'] = $_POST['widget_content_max_chars_restricted_to'];
		}

		$settings['settings']['widget_content_max_chars'] = 'restrict';
		if (isset ($_POST['widget_content_max_chars']) && SpireBuilder_Validator::parse_string($_POST['widget_content_max_chars'])) {
			$settings['settings']['widget_content_max_chars'] = $_POST['widget_content_max_chars'];
		}

		SpireBuilder_Settings::update_options($settings);

		if (isset($_POST['available_widgets'])) {
			SpireBuilder_Settings::update_spirebuilder_widgets_status($_POST['available_widgets']);
		} else {
			// In this case available_widgets is all disable set array in null.
			SpireBuilder_Settings::update_spirebuilder_widgets_status(__return_empty_array());
		}

		$message_to_return = __('The settings were updated successfully.', SpireBuilder::$i18n_prefix);
		$type_to_return    = 'success';
	}
}

/**
 * Page-Post
 *
 */

// Update/Save Settings
if (isset($_POST['action']) && 'spirebuilder_update_builder_settings' == $_POST['action'] && isset($_POST['submit_update_builder_settings'])) {
	if (isset($_POST['post_id']) && '' != SpireBuilder_Validator::parse_int($_POST['post_id']) && wp_verify_nonce($_POST['SpireBuilderNonce'], 'spirebuilder_update_builder_settings')) {

		$settings['allow_spire_builder_editor'] = 0;
		if (isset ($_POST['allow_spire_builder_editor']) && SpireBuilder_Validator::parse_int($_POST['allow_spire_builder_editor']))
			$settings['allow_spire_builder_editor'] = 1;

		$settings['content_placement'] = $_POST['content_placement'];

		SpireBuilder_WPPosts::update_spire_builder_settings_post($_POST['post_id'], $settings);
		$message_to_return = __('The settings were updated successfully.', SpireBuilder::$i18n_prefix);
		$type_to_return    = 'success';
	}
}

/*
 * Components
 */

// Update/Save Components
if (isset($_POST['action']) && 'spirebuilder_update_builder_lom' == $_POST['action']) {
	if (isset($_POST['post_id']) && '' != SpireBuilder_Validator::parse_int($_POST['post_id']) && wp_verify_nonce($_POST['SpireBuilderNonce'], 'spirebuilder_update_builder_lom')) {

		$sanitized_LOM = stripslashes($_POST['LOM']);

		// Its saved the original string and not the sanitized one since it will be re-parsed in front-end, and must be the original.
		SpireBuilder_WPPosts::update_spirebuilder_LOM($_POST['post_id'], $_POST['LOM']);

		$LOM_to_object = json_decode($sanitized_LOM);

		$content_html = SpireBuilder_Central::get_LOMSection2HTML($LOM_to_object);

		SpireBuilder_WPPosts::update_spirebuilder_content_LOM($_POST['post_id'], $content_html);

		$message_to_return = __('The layout was updated successfully.', SpireBuilder::$i18n_prefix);
		$type_to_return    = 'success';
	} else {
		$message_to_return = __('Error, post no found.', SpireBuilder::$i18n_prefix);
		$type_to_return    = 'error';
	}
}

// Create/Update Templates
if (isset($_POST['action']) && $_POST['action'] == 'spirebuilder_update_builder_templates') {
	if (isset($_POST['post_id']) && '' != SpireBuilder_Validator::parse_int($_POST['post_id']) && wp_verify_nonce($_POST['SpireBuilderNonce'], 'spirebuilder_update_builder_templates')) {
		// Get form db options
		$options = SpireBuilder_Settings::get_options();

		// Get Seconds since the Unix Epoch and merge with a random number, this way minimise duplicate case.
		$data_to_return['id'] = date('U') . rand(1, 999999999);
		$options['templates'] = array_merge($options['templates'], array(array('title' => $_POST['title'], 'id' => $data_to_return['id'], 'LOM' => stripslashes($_POST['LOM']))));

		// Update options
		SpireBuilder_Settings::update_options($options);

		$message_to_return = __('The template was saved successfully.', SpireBuilder::$i18n_prefix);
		$type_to_return    = 'success';
	} else {
		$message_to_return = __('Error, post no found.', SpireBuilder::$i18n_prefix);
		$type_to_return    = 'error';
	}
}

// Delete Templates
if (isset($_POST['action']) && $_POST['action'] == 'spirebuilder_update_builder_templates_delete') {
	if (isset($_POST['id']) && '' != SpireBuilder_Validator::parse_int($_POST['id']) && wp_verify_nonce($_POST['SpireBuilderNonce'], 'spirebuilder_update_builder_templates_delete')) {
		// Get form db options
		$options = SpireBuilder_Settings::get_options();

		$templates = $options['templates'];

		// Eliminate
		SpireBuilder_Miscellaneous::remove_element_with_value($templates, 'id', $_POST['id']);
		$options['templates'] = $templates;

		// Update options
		SpireBuilder_Settings::update_options($options);

		$message_to_return = __('The template was deleted successfully.', SpireBuilder::$i18n_prefix);
		$type_to_return    = 'success';
	} else {
		$message_to_return = __('Error, template no found.', SpireBuilder::$i18n_prefix);
		$type_to_return    = 'error';
	}
}

// Show message
$data            = array();
$data['message'] = $message_to_return;
$data['type']    = $type_to_return;

if (isset($data_to_return)) {
	$data['data'] = $data_to_return;
}

$json = json_encode($data);
echo $json;
exit();