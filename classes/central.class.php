<?php
if (!class_exists('SpireBuilder_Central')) {
	/**
	 * Class SpireBuilder_Central
	 */
	class SpireBuilder_Central {

		/**
		 * The filename of the file package.json
		 *
		 * @static
		 * @var string
		 */
		static $filename_package = 'package.json';

		/**
		 * Constructor
		 */
		function __construct() { }

		/**
		 * LOMSection2HTML
		 *
		 * @param  ObjectArray $LOM_section
		 * @param              $handlebars_instance Instance of the handlebars_Engine Class
		 * @param string       $string              Result with HTML
		 *
		 */
		static public function LOMSection2HTML($LOM_section, $handlebars_instance, &$string) {
			if (isset ($LOM_section->type) && $LOM_section->type != 'root') {
				// Exist dropzones?
				if (isset($LOM_section->dropzones) && count(get_object_vars($LOM_section->dropzones)) > 0) {
					foreach ($LOM_section->dropzones as &$dropzone) {
						$dropzone_id = $dropzone;
						$dropzone    = '';
						if (isset ($LOM_section->children)) {
							// Sort children
							foreach ($LOM_section->children as $children) {
								if ($dropzone_id == $children->related_dropzone_id) {
									$dropzone .= self::LOMSection2HTML($children, $handlebars_instance, $string);
								}
							}
						}
					}
					$front_end = '';
					if (is_file(SpireBuilder::$widgets_dir . $LOM_section->type . '/templates/front-end/widget.php')) {
						$front_end = self::read_data_file(SpireBuilder::$widgets_dir . $LOM_section->type . '/templates/front-end/widget.php', array());
					}

					return $handlebars_instance->render($front_end, $LOM_section);
				} else {
					$front_end = '';
					if (is_file(SpireBuilder::$widgets_dir . $LOM_section->type . '/templates/front-end/widget.php')) {
						$front_end = self::read_data_file(SpireBuilder::$widgets_dir . $LOM_section->type . '/templates/front-end/widget.php', array());
					}

					return $handlebars_instance->render($front_end, $LOM_section);
				}
			} else {
				if (isset ($LOM_section->children)) {
					foreach ($LOM_section->children as $children) {
						$string .= self::LOMSection2HTML($children, $handlebars_instance, $string);
					}
				}
			}
		}

		/**
		 * @param $LOM
		 *
		 * @return string
		 */
		static function get_LOMSection2HTML($LOM) {
			Handlebars_Autoloader::register();
			$handlebars_instance = new Handlebars_Engine;

			/*
			 * Register needed custom helpers.
			 */
			$handlebars_instance->addHelper('equal', array('SpireBuilder_handlebars_helpers', '_helper_equal'));
			$handlebars_instance->addHelper('content', array('SpireBuilder_handlebars_helpers', '_helper_content'));
			$handlebars_instance->addHelper('column-each',array('SpireBuilder_handlebars_helpers','_helper_column_each'));

			$string = '';
			self::LOMSection2HTML($LOM, $handlebars_instance, $string);

			return $string;
		}

		/**
		 * Get all categories form widgets
		 *
		 * @since 0.1
		 *
		 * @return array $categories
		 */
		static public function get_categories() {
			$categories  = array();
			$all_widgets = SpireBuilder_Settings::get_spirebuilder_widgets();

			foreach ($all_widgets as $widget_value) {
				$category = array_merge(array('category_id' => str_replace(" ", "", strtolower($widget_value['category']['title']))), $widget_value['category']);
				// Check duplicates categories
				if (SpireBuilder_Validator::parse_string($widget_value['category']['title']) != '' && !in_array($category, $categories)) {
					$categories[] = $category;
				}
			}

			return $categories;
		}

		/**
		 *
		 * Get all content types
		 *
		 * http://codex.wordpress.org/Function_Reference/get_post_types
		 *
		 */
		static function get_content_types() {
			$settings = SpireBuilder_Settings::get_settings();

			if (isset($settings['builder_content_types'])) {
				$content_types_on_db = $settings['builder_content_types']; // get_post_types( '', 'names' ) ;
			} else {
				$content_types_on_db = __return_empty_array();
			}

			if (isset($settings['builder_content_types_not_shown'])) {
				$content_types_on_db_not_shown = $settings['builder_content_types_not_shown']; // get_post_types( '', 'names' ) ;
			} else {
				$content_types_on_db_not_shown = __return_empty_array();
			}

			// Search all types the post on cms
			$output     = 'objects'; // names or objects
			$post_types = get_post_types('', $output);

			$content_types_on_wp = array();
			// Create array with content types
			foreach ($post_types as $post_type) {
				$content_types_on_wp[$post_type->name]['id']     = $post_type->name;
				$content_types_on_wp[$post_type->name]['status'] = 'disable';
				$content_types_on_wp[$post_type->name]['name']   = $post_type->labels->name;
			}

			// Get only content type the not present in db
			$content_types_to_add_diff_wp_and_not_show = array_diff_key($content_types_on_wp, $content_types_on_db_not_shown);
			$content_types_to_add                      = array_diff_key($content_types_to_add_diff_wp_and_not_show, $content_types_on_db);

			return array_merge($content_types_on_db, $content_types_to_add);
		}

		/**
		 * All files of widgets available in the plugins
		 *
		 * @since v0.1
		 *
		 * @param string $widgets_dir
		 *
		 * @return array list with all subdirectories widgets
		 */
		static public function get_list_all_dir_widgets($widgets_dir = '') {
			if ($widgets_dir == '') {
				$widgets_dir = SpireBuilder::$widgets_dir;
			}
			if (isset($widgets_dir) && is_dir($widgets_dir)) {
				return self::list_files($widgets_dir);
			} else {
				return __return_empty_array();
			}
		}

		/**
		 * All widgets categories available in the plugins
		 *
		 * @since v0.1
		 *
		 * @return array
		 */
		static public function get_widgets_categories_from_dir() {
			$list_all_dir_widgets = self::get_list_all_dir_widgets();

			$list_widgets = array();

			foreach ($list_all_dir_widgets as $full_dir_widgets) {
				if (is_file($full_dir_widgets)) {
					// Find "package.json" and save path intro var
					$dir_widgets = pathinfo($full_dir_widgets);
					if (isset($dir_widgets['basename']) && self::$filename_package == $dir_widgets['basename']) {
						// Get name widget: /path/to/widgets/name-widget-to-get/package.json
						$result_explode = array_reverse(explode('/', $full_dir_widgets));
						$folder_name    = $result_explode[1];
						if ($folder_name != '_skel') {
							$list_widgets[$folder_name] = array_merge(array('id' => $folder_name), json_decode(self::read_data_file($full_dir_widgets), true));
						}
					}
				}
			}

			return $list_widgets;
		}

		/**
		 * Read data from a file
		 *
		 * @since v0.1
		 *
		 * @param string $file
		 *
		 * @return string $file_data
		 *
		 */
		static protected function read_data_file($file) {
			$file_open = fopen($file, 'r');
			$file_data = fread($file_open, filesize($file));
			fclose($file_open);

			return $file_data;
		}

		/**
		 * Compare and get the difference between the db widgets and disk widgets
		 *
		 * This function get widgets directories and compare to widgets intro db.
		 *
		 * @since v0.1
		 *
		 * @return array
		 */
		static function get_diff_widgets_db_and_disk() {
			$widgets_form_db   = SpireBuilder_Settings::get_spirebuilder_widgets();
			$widgets_form_disk = SpireBuilder_Central::get_widgets_categories_from_dir();
			// Initialization array
			$widgets_to_save_is_in_db                 = array();
			$widgets_to_save_is_in_db_and_not_in_disk = array();
			$widgets_to_save_is_in_disk_and_not_in_db = array();

			// Search all widgets is in db and disk.
			foreach ($widgets_form_db as $widgets_db) {
				// Search if widget in db is found on disk
				if (!SpireBuilder_Miscellaneous::search($widgets_form_disk, 'id', $widgets_db['id'])) {
					$widgets_db['status']                       = 'deleted';
					$widgets_to_save_is_in_db_and_not_in_disk[] = $widgets_db;
				}
			}
			// Search in disk vs the found in db
			foreach ($widgets_form_disk as $widgets_disk) {
				// Search if widget of disk is found on db or not found on db
				if ($widget = SpireBuilder_Miscellaneous::search($widgets_form_db, 'id', $widgets_disk['id'])) {
					// In this case keep status of the DB
					if (isset ($widget[0]['status']) && $widget[0]['status'] == 'deleted') {
						$widgets_disk['status'] = 'active'; // If the widgets was deleted and now set status in active again.
					} else {
						$widgets_disk['status'] = $widget[0]['status'];
					}
					$widgets_to_save_is_in_db[] = $widgets_disk;
				} else {
					// In this case the widgets is new: set status to active.
					$widgets_disk['status']                     = 'active';
					$widgets_to_save_is_in_disk_and_not_in_db[] = $widgets_disk;
				}
			}

			return array_merge($widgets_to_save_is_in_db, $widgets_to_save_is_in_disk_and_not_in_db, $widgets_to_save_is_in_db_and_not_in_disk);
		}

		/**
		 * Returns a listing of all files in the specified folder and all subdirectories up to 100 levels deep.
		 * The depth of the recursiveness can be controlled by the $levels param.
		 *
		 * @since 0.1
		 *
		 * @param string $folder Full path to folder
		 * @param int    $levels (optional) Levels of folders to follow, Default: 100 (PHP Loop limit).
		 *
		 * @return bool|array False on failure, Else array of files
		 */
		static protected function list_files($folder = '', $levels = 100) {
			if (empty($folder)) {
				return false;
			}

			if (!$levels) {
				return false;
			}

			$files = array();
			if ($dir = @opendir($folder)) {
				while (($file = readdir($dir)) !== false) {
					if (in_array($file, array('.', '..'))) {
						continue;
					}
					if (is_dir($folder . '/' . $file)) {
						$files2 = self::list_files($folder . '/' . $file, $levels - 1);
						if ($files2) {
							$files = array_merge($files, $files2);
						} else {
							$files[] = $folder . '/' . $file . '/';
						}
					} else {
						$files[] = $folder . '/' . $file;
					}
				}
			}
			@closedir($dir);

			return $files;
		}

		static function cmp_related_dropzone_order($children_a, $children_b) {
			if ((int) $children_a->related_dropzone_order < (int) $children_b->related_dropzone_order)
				return -1;
			if ((int) $children_a->related_dropzone_order > (int) $children_b->related_dropzone_order)
				return 1;

			return 0;
		}

		/**
		 *
		 * Get all widgets and convert to $categories[]['widgets']
		 *
		 * @since v0.1
		 *
		 * @param string $status (optional) Can be 'all', 'active', 'disable', 'deleted' or 'active_and_disable'. By default 'all'.
		 *
		 * @return array
		 */
		static function get_all_category_with_widgets($status = 'all') {
			$all_categories = SpireBuilder_Central::get_categories();
			$all_widgets    = SpireBuilder_Settings::get_spirebuilder_widgets(); // Get all widgets from options variable.

			$categories_result = array();

			foreach ($all_categories as $category_key => $category_value) {
				foreach ($all_widgets as $widget_value) {
					// Create one widget
					$widget = array_merge($widget_value['widget'], array('id' => $widget_value['id'], 'status' => $widget_value['status']));

					// In this case save active and disable.
					if ($status == 'active_and_disable' && ($widget_value['status'] == 'active' || $widget_value['status'] == 'disable')) {
						if ($category_value['title'] == $widget_value['category']['title']) {
							if (!isset($categories_result[$category_key])) {
								$categories_result[$category_key] = $all_categories[$category_key];
							}
							$categories_result[$category_key]['widgets'][] = $widget; // Save widget
						}
					} elseif ($status == 'active' && $widget_value['status'] == 'active') {
						if ($category_value['title'] == $widget_value['category']['title']) {
							if (!isset($categories_result[$category_key])) {
								$categories_result[$category_key] = $all_categories[$category_key];
							}
							$categories_result[$category_key]['widgets'][] = $widget; // Save widget
						}
					} elseif ($status == 'disable' && $widget_value['status'] == 'disable') {
						if ($category_value['title'] == $widget_value['category']['title']) {
							if (!isset($categories_result[$category_key])) {
								$categories_result[$category_key] = $all_categories[$category_key];
							}
							$categories_result[$category_key]['widgets'][] = $widget; // Save widget
						}
					} elseif ($status == 'deleted' && $widget_value['status'] == 'deleted') {
						if ($category_value['title'] == $widget_value['category']['title']) {
							if (!isset($categories_result[$category_key])) {
								$categories_result[$category_key] = $all_categories[$category_key];
							}
							$categories_result[$category_key]['widgets'][] = $widget; // Save widget
						}
					} elseif ($status == 'all') {
						if ($category_value['title'] == $widget_value['category']['title']) {
							if (!isset($categories_result[$category_key])) {
								$categories_result[$category_key] = $all_categories[$category_key];
							}
							$categories_result[$category_key]['widgets'][] = $widget; // Save widget
						}
					}
				}
			}

			if (isset($categories_result)) {
				return $categories_result;
			} else {
				return __return_empty_array();
			}
		}
	}
}