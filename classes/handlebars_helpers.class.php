<?php
if (!class_exists('SpireBuilder_handlebars_helpers')) {
	/**
	 * Class SpireBuilder_handlebars_helpers
	 */
	class SpireBuilder_handlebars_helpers {

		/**
		 * Handlebars Helper Equal
		 *
		 * @param $template
		 * @param $context
		 * @param $args
		 * @param $source
		 *
		 * @return Handlebars_String
		 * @throws InvalidArgumentException
		 */
		static function _helper_equal($template, $context, $args, $source) {
			$tmp    = explode(' ', $args);
			$buffer = '';

			if (count($tmp) < 2) {
				throw new InvalidArgumentException('Handlebars Helper equal needs 2 parameters');
			}
			if ($context->get($tmp[0]) == $tmp[1]) {
				$buffer = $template->render($context);
			}

			return new Handlebars_String($buffer);
		}

		/**
		 * Handlebars Helper Content. Return object->Alphabet[index].
		 *
		 * @param $template
		 * @param $context
		 * @param $args
		 * @param $source
		 *
		 * @return Handlebars_String
		 * @throws InvalidArgumentException
		 */
		public static function _helper_content($template, $context, $args, $source) {
			$Alphabet = range('A', 'Z');

			$tmp    = explode(' ', $args);
			$buffer = '';

			$object = $context->get($tmp[0]);

			if (count($tmp) < 2 || $tmp[1] == '@index') {
				$last_index = $context->lastIndex();
				$letter     = $Alphabet[$last_index];
			} else {
				$index  = $context->get($tmp[1]);
				$letter = $Alphabet[$index];
			}

			$buffer .= $object->$letter;

			return new Handlebars_String($buffer);
		}

		/**
		 * Handlebars Helper Column Each. Iterate n-column times depending on the column type.
		 *
		 * @param $template
		 * @param $context
		 * @param $args
		 * @param $source
		 *
		 * @return mixed
		 */
		public static function _helper_column_each($template, $context, $args, $source) {
			$tmp = $context->get($args);

			$arg_split = explode('+', $tmp);

			$buffer = '';
			if (is_array($arg_split) || $arg_split instanceof Traversable) {
				$islist = (array_keys($arg_split) == range(0, count($arg_split) - 1));

				foreach ($arg_split as $key => $var) {
					$fraction     = explode('-', $arg_split[$key]);
					$column_width = $fraction[0] * 12 / $fraction[1];

					if ($islist) {
						$context->pushIndex($key);
					} else {
						$context->pushKey($column_width);
					}
					$context->push($column_width);
					$buffer .= $template->render($context);
					$context->pop();
					if ($islist) {
						$context->popIndex();
					} else {
						$context->popKey();
					}
				}
			}

			return $buffer;
		}
	}
}