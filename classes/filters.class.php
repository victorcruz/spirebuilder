<?php
if (!class_exists('SpireBuilder_Filters')) {
	/**
	 * Class SpireBuilder_Filters
	 */
	class SpireBuilder_Filters {

		/**
		 * Filter Post Slug
		 *
		 * When editing a post a slug will be generated and modified by this
		 * removing common words like 'a', 'the', 'in' from post slugs to improve SEO.
		 *
		 * If the Post is edited, this won't change it.
		 *
		 * @param    string $slug
		 *
		 * @return    string
		 */
		static function filter_post_slug($slug) {
			// If settings is enable, filter the Slug
			$settings = SpireBuilder_Settings::get_options();
			if ($settings['enable_convertion_post_slug'] == '1') {
				// We don't want to change an existing slug
				if ($slug) {
					return $slug;
				}

				global $wpdb;
				if (SpireBuilder_Settings::support_multibyte()) {
					$seo_slug = mb_strtolower(stripslashes($_POST['post_title']), 'UTF-8');
				} else {
					$seo_slug = strtolower(stripslashes($_POST['post_title']));
				}

				$seo_slug = preg_replace('/&.+?;/', '', $seo_slug); // kill HTML entities
				// kill anything that is not a letter, digit, space or apostrophe
				$seo_slug = preg_replace("/[^a-zA-Z0-9 \']/", "", $seo_slug);

				// Turn it to an array and strip common words by comparing against c.w. array
				$seo_slug_array = array_diff(explode(" ", $seo_slug), SpireBuilder_Settings::get_slugs_stop_words());

				// Turn the sanitized array into a string
				$seo_slug = implode("-", $seo_slug_array);

				return $seo_slug;
			}

			return $slug;
		}

		/**
		 *
		 * Filter the POST title
		 *
		 * @param    string $title
		 * @param    array  $keyword_arr
		 * @param    string $settings
		 *
		 * @return    string
		 */
		static function filter_post_title($title, $keyword_arr, $settings) {
			if ($keyword_arr[0] == '' || trim($title) == '') {
				return $title;
			}

			// If setting allow plugin to add keyword, add if isn't already
			if (($settings['allow_add_keyword_in_titles']) && !SpireBuilder_Keywords::keyword_in_content($keyword_arr, $title)) {
				// Add keyword behind the TITLE, like this: title | keyword
				$new_title = $title . ' | ' . $keyword_arr[0];

				return $new_title;
			} else {
				return $title;
			}
		}

		/**
		 * Apply bold, italic and underline to a content
		 *
		 * Just once for every typeface is enough. If there is less than 3 occurrences,
		 * then, priority to bold, then italize, then underline.
		 *
		 * @param    string $content
		 * @param    array  $keyword
		 *
		 * @return    string
		 * @access    public
		 */
		static function apply_biu_to_content($content, $keyword_arr) {
			$settings = SpireBuilder_Central::get_md5_settings(true);

			$new_content = $content;

			/*
			 * Make the same for each keyword
			 */

			foreach ($keyword_arr as $keyword_arr_item) {

				/*
				 * Check for this $already_apply_... because:
				 * if the first keyword is italized, second is underlined,
				 * then, we will find the third to bold. If there is no third keyword, then no bold face
				 *
				 */
				if ($settings['allow_bold_style_to_apply'] === 1 || $settings['allow_bold_style_to_apply'] === '1') {
					$already_apply_bold = false;
				} else {
					$already_apply_bold = true;
				}
				if ($settings['allow_italic_style_to_apply'] === 1 || $settings['allow_italic_style_to_apply'] === '1') {
					$already_apply_italic = false;
				} else {
					$already_apply_italic = true;
				}
				if ($settings['allow_underline_style_to_apply'] === 1 || $settings['allow_underline_style_to_apply'] === '1') {
					$already_apply_underline = false;
				} else {
					$already_apply_underline = true;
				}

				// Pass through all keyword until ends or until are applied all designs
				$how_many_keys = SpireBuilder_Keywords::how_many_keywords(array($keyword_arr_item), $new_content);


				// To avoid make the request for each keyword: Get pieces by keyword for determine if some has the design applied
				$pieces_by_keyword         = SpireBuilder_Keywords::get_pieces_by_keyword(array($keyword_arr_item), $new_content, true);
				$pieces_by_keyword_matches = $pieces_by_keyword[1];
				$pieces_by_keyword         = $pieces_by_keyword[0];

				// First, only check for designs already applied
				for ($i = 1; $i <= $how_many_keys; $i++) {

					// Stop if are already all the design applied
					if ($already_apply_bold && $already_apply_italic && $already_apply_underline) {
						break;
					}

					// Getting the position
					$key_pos = SpireBuilder_Keywords::strpos_offset($keyword_arr_item, $new_content, $i, $pieces_by_keyword, $pieces_by_keyword_matches);

					if ($key_pos !== false) {
						$already_style = SpireBuilder_HtmlStyles::if_some_style_or_in_tag_attribute($new_content, array($keyword_arr_item), $i);

						if ($already_style) {
							if ($already_style[1] == 'bold') {
								$already_apply_bold = true;
							} elseif ($already_style[1] == 'italic') {
								$already_apply_italic = true;
							} elseif ($already_style[1] == 'underline') {
								$already_apply_underline = true;
							}
						}
					}
				}

				// Apply designs pendings to apply
				for ($i = 1; $i <= $how_many_keys; $i++) {

					// Stop if are already all the design applied
					if ($already_apply_bold && $already_apply_italic && $already_apply_underline) {
						break;
					}

					// Getting the position. Here can't be calculate one time ($pieces_by_keyword) and rehuse it because the content changes
					$key_pos                        = SpireBuilder_Keywords::strpos_offset($keyword_arr_item, $new_content, $i);
					$pieces_by_keyword_matches_item = $pieces_by_keyword_matches[$i - 1];

					/*
					// Determine which is the keyword that continues
					// Getting this text of keyword, allow us to be aware if a Keyword has any upper case
					if (SpireBuilder_Settings::support_multibyte()) {
						$text_replace = mb_substr($new_content,$key_pos,mb_strlen($pieces_by_keyword_matches_item,'UTF-8'),'UTF-8');
					}
					else {
						//unused //$piece_with_keyword = substr($new_content,$key_pos);
						$text_replace = substr($new_content,$key_pos,strlen($pieces_by_keyword_matches_item));
					}
					*/

					if ($key_pos !== false) {
						$already_style = SpireBuilder_HtmlStyles::if_some_style_or_in_tag_attribute($new_content, array($keyword_arr_item), $i);

						if ($already_style) {
							if ($already_style[1] == 'bold') {
								$already_apply_bold = true;
							} elseif ($already_style[1] == 'italic') {
								$already_apply_italic = true;
							} elseif ($already_style[1] == 'underline') {
								$already_apply_underline = true;
							}
						} else {
							if (!$already_apply_bold) {
								$keyword_with_style = SpireBuilder_HtmlStyles::apply_bold_styles($pieces_by_keyword_matches_item);
								$already_apply_bold = true;
							} elseif (!$already_apply_italic) {
								$keyword_with_style   = SpireBuilder_HtmlStyles::apply_italic_styles($pieces_by_keyword_matches_item);
								$already_apply_italic = true;
							} elseif (!$already_apply_underline) {
								$keyword_with_style      = SpireBuilder_HtmlStyles::apply_underline_styles($pieces_by_keyword_matches_item);
								$already_apply_underline = true;
							}

							if (SpireBuilder_Settings::support_multibyte()) {
								$new_content = self::mb_substr_replace($new_content, $keyword_with_style, $key_pos, ($key_pos + mb_strlen($pieces_by_keyword_matches_item, 'UTF-8') - 1));
							} else {
								$new_content = substr_replace($new_content, $keyword_with_style, $key_pos, strlen($pieces_by_keyword_matches_item));
							}

							// Calculate how many keyword, because in case the keyword is, for example "b" this value will change
							$how_many_keys = SpireBuilder_Keywords::how_many_keywords(array($keyword_arr_item), $new_content);
						}
					}
				}
			}

			return $new_content;
		}

		static function mb_substr_replace($output, $replace, $posOpen, $posClose) {
			return mb_substr($output, 0, $posOpen) . $replace . mb_substr($output, $posClose + 1);
		}

		/**
		 * Apply Automatic Internal Links
		 *
		 * @param string $content
		 */
		static private function apply_automatic_internal_links($content) {
			$all_keywords_links = SpireBuilder_AutomaticInternalLinks::get_all();

			foreach ($all_keywords_links as $all_keywords_links_item) {
				$how_many = $all_keywords_links_item->how_many;
				if ($how_many == '' || $how_many < 0) {
					$how_many = 0;
				}

				// For cases of internal links with 0 as the number of ocurrences
				if ($how_many == 0) {
					break;
				}

				if (trim($all_keywords_links_item->keywords) != '') {

					$tmp_keywords_arr = explode(',', $all_keywords_links_item->keywords);
					$tmp_post_id      = $all_keywords_links_item->post_id;

					if (count($tmp_keywords_arr) > 0) {
						// Get permalink to this Post
						$tmp_post_link = get_permalink($tmp_post_id);

						foreach ($tmp_keywords_arr as $tmp_keywords_arr_item) {

							$tmp_keyword_item = trim($tmp_keywords_arr_item);
							if ($tmp_keyword_item != '') {

								// To avoid make the request for each keyword: Get pieces by keyword for determine if some has the design applied
								$pieces_by_keyword         = SpireBuilder_Keywords::get_pieces_by_keyword(array($tmp_keyword_item), $content, true);
								$pieces_by_keyword_matches = $pieces_by_keyword[1];
								$pieces_by_keyword         = $pieces_by_keyword[0];

								// First, only check for designs already applied
								for ($i = 1; $i <= count($pieces_by_keyword); $i++) {
									// Getting the position
									$key_pos                        = SpireBuilder_Keywords::strpos_offset($tmp_keyword_item, $content, $i, $pieces_by_keyword, $pieces_by_keyword_matches);
									$pieces_by_keyword_matches_item = $pieces_by_keyword_matches[$i - 1];

									if ($key_pos !== false) {
										$ready_to_apply_link = SpireBuilder_HtmlStyles::if_ready_to_apply_internal_link($content, array($tmp_keyword_item), $i);

										if ($ready_to_apply_link) {
											// Setting text of keyword, allow us to be aware if a Keyword has any upper case, has characters to omit, etc
											$text_replace = '<a href="' . $tmp_post_link . '">' . $pieces_by_keyword_matches_item . '</a>';

											if (SpireBuilder_Settings::support_multibyte()) {
												$content = self::mb_substr_replace($content, $text_replace, $key_pos, ($key_pos + mb_strlen($pieces_by_keyword_matches_item, 'UTF-8') - 1));
											} else {
												$content = substr_replace($content, $text_replace, $key_pos, strlen($tmp_keyword_item));
											}

											$how_many--;
											if ($how_many == 0) {
												break 2; // Go out the per content-keywork foreach, and the keywords-to-link foreach
											}

											// If a change was made in the content, take the pieces by keywords again, because the possitions changes
											$pieces_by_keyword         = SpireBuilder_Keywords::get_pieces_by_keyword(array($tmp_keyword_item), $content, true);
											$pieces_by_keyword_matches = $pieces_by_keyword[1];
											$pieces_by_keyword         = $pieces_by_keyword[0];
										}
									}
								}
							}
						}
					}
				}
			}

			return $content;
		}

		/**
		 * Apply External Cloaked Links
		 *
		 * Example of Cloacked link:
		 * <a href="http://theirsite.com/recommends/wikipedia"
		 * onclick="javascript:this.href='http://wikipedia.com/';"
		 * rel="nofollow">keyword</a>
		 *
		 * @param string $content
		 */
		static private function apply_external_cloaked_links($content) {
			// Get all keywords-links
			$all_keywords_links = SpireBuilder_ExternalCloackedLinks::get_all();

			// Get current site url
			$wp_url = get_bloginfo('wpurl');

			foreach ($all_keywords_links as $all_keywords_links_item) {
				$how_many = $all_keywords_links_item->how_many;
				if ($how_many == '' || $how_many < 0) {
					$how_many = 0;
				}

				// For cases of internal links with 0 as the number of ocurrences
				if ($how_many == 0) {
					break;
				}

				if (trim($all_keywords_links_item->keywords) != '') {
					$tmp_keywords_arr = explode(',', $all_keywords_links_item->keywords);

					if (count($tmp_keywords_arr) > 0) {
						// Get data to build the link
						$tmp_cloaking_folder = $all_keywords_links_item->cloaking_folder;
						$tmp_external_url    = $all_keywords_links_item->external_url;
						/*
						 * Get main domain from $tmp_external_url
						 * Example:
						 * 			from: http://www.wikipedia.org/some/page.php?var1=value1
						 * 			we get:  wikipedia
						 */
						$tmp_external_url_domain = str_replace('http://', '', $tmp_external_url);
						$tmp_external_url_domain = str_replace('https://', '', $tmp_external_url_domain);

						// Add http:// if isn't in external Url
						if (substr_count($tmp_external_url, 'http://') == 0 && substr_count($tmp_external_url, 'https://') == 0) {
							$tmp_external_url = 'http://' . $tmp_external_url;
						}

						if (strpos($tmp_external_url_domain, '/')) {
							$tmp_external_url_domain = substr($tmp_external_url_domain, 0, strpos($tmp_external_url_domain, '/'));
						}
						if (strrpos($tmp_external_url_domain, '.')) {
							$tmp_external_url_domain = substr($tmp_external_url_domain, 0, strrpos($tmp_external_url_domain, '.'));
						}
						if (strrpos($tmp_external_url_domain, '.')) {
							$tmp_external_url_domain = substr($tmp_external_url_domain, strrpos($tmp_external_url_domain, '.'));
						}
						$tmp_external_url_domain = trim($tmp_external_url_domain, '.');

						// For each keyword make the replaces
						foreach ($tmp_keywords_arr as $tmp_keywords_arr_item) {
							$tmp_keyword_item = trim($tmp_keywords_arr_item);
							if ($tmp_keyword_item != '') {

								// To avoid make the request for each keyword: Get pieces by keyword for determine if some has the design applied
								$pieces_by_keyword         = SpireBuilder_Keywords::get_pieces_by_keyword(array($tmp_keyword_item), $content, true);
								$pieces_by_keyword_matches = $pieces_by_keyword[1];
								$pieces_by_keyword         = $pieces_by_keyword[0];

								// First, only check for designs already applied
								for ($i = 1; $i <= count($pieces_by_keyword); $i++) {
									// Getting the position
									$key_pos                        = SpireBuilder_Keywords::strpos_offset($tmp_keyword_item, $content, $i, $pieces_by_keyword, $pieces_by_keyword_matches);
									$pieces_by_keyword_matches_item = $pieces_by_keyword_matches[$i - 1];

									if ($key_pos !== false) {
										$ready_to_apply_link = SpireBuilder_HtmlStyles::if_ready_to_apply_internal_link($content, array($tmp_keyword_item), $i);

										if ($ready_to_apply_link) {
											// Prepare cloacked link
											$cloacked_link = '<a href="' . $wp_url . '/' . $tmp_cloaking_folder . '/' . $tmp_external_url_domain . '" onclick="javascript:this.href=' . "'$tmp_external_url'" . ';" rel="nofollow">' . $pieces_by_keyword_matches_item . '</a>';

											if (SpireBuilder_Settings::support_multibyte()) {
												$content = self::mb_substr_replace($content, $cloacked_link, $key_pos, ($key_pos + mb_strlen($pieces_by_keyword_matches_item, 'UTF-8') - 1));
											} else {
												$content = substr_replace($content, $cloacked_link, $key_pos, strlen($pieces_by_keyword_matches_item));
											}

											$how_many--;
											if ($how_many == 0) {
												break 2; // Go out the per content-keywork foreach, and the keywords-to-link foreach
											}

											// If a change was made in the content, take the pieces by keywords again, because the possitions changes
											$pieces_by_keyword         = SpireBuilder_Keywords::get_pieces_by_keyword(array($tmp_keyword_item), $content, true);
											$pieces_by_keyword_matches = $pieces_by_keyword[1];
											$pieces_by_keyword         = $pieces_by_keyword[0];
										}
									}
								}
							}
						}
					}
				}
			}

			return $content;
		}

		/**
		 * apply_code_snippets
		 *
		 * @param    string $content
		 * @param    int    $post_id
		 *
		 * @return    string
		 * @access    public
		 */
		public static function apply_code_snippets($content, $post_id) {

			if ((is_single() || is_page()) && !is_feed()) {
				$post = get_post($post_id);

				// Add Review, microformat
				$rating = get_post_meta($post_id, 'google_rich_snippet_rating', true);
				if ($rating != '') {
					$title       = $post->post_title;
					$dateTime    = date_create($post->post_date);
					$date        = $dateTime->format("Y-m-d");
					$date_only   = $dateTime->format("M j");
					$author      = get_post_meta($post_id, 'google_rich_snippet_author', true);
					$author      = ('' == $author) ? ucfirst(get_the_author_meta('display_name', $post->post_author)) : $author;
					$summary     = get_post_meta($post_id, 'google_rich_snippet_summary', true);
					$description = get_post_meta($post_id, 'google_rich_snippet_description', true);

					$output = "<div class=\"hreview\" style=\"display:none\">
        					<span class=\"item\"><span class=\"fn entry-title\">$title</span></span>
        					Reviewed by <span class=\"reviewer\">$author</span> on <span class=\"dtreviewed\">
        					$date_only<span class=\"value-title\" title=\"$date\"></span></span>
        					Rating: <span class=\"rating\">$rating</span>
        					<span class=\"summary\">$summary</span>
        					<span class=\"description\">$description</span></div>";

					$content = $output . $content;
				}

				// Add Event, microformat
				$seop_grs_event_name = get_post_meta($post_id, 'seop_grs_event_name', true);
				if ($seop_grs_event_name != '') {
					$seop_grs_event_url                              = get_post_meta($post_id, 'seop_grs_event_url', true);
					$seop_grs_event_startdate                        = get_post_meta($post_id, 'seop_grs_event_startdate', true);
					$seop_grs_event_location_name                    = get_post_meta($post_id, 'seop_grs_event_location_name', true);
					$seop_grs_event_location_address_streetaddress   = get_post_meta($post_id, 'seop_grs_event_location_address_streetaddress', true);
					$seop_grs_event_location_address_addresslocality = get_post_meta($post_id, 'seop_grs_event_location_address_addresslocality', true);
					$seop_grs_event_location_address_addressregion   = get_post_meta($post_id, 'seop_grs_event_location_address_addressregion', true);

					$output = '<div class="vevent" style="display:none">
							<a href="' . $seop_grs_event_url . '" class="url summary">' . $seop_grs_event_name . '</a>
						   <span class="dtstart">
						      ' . $seop_grs_event_startdate . '<span class="value-title" title="' . $seop_grs_event_startdate . '"></span>
						   </span>
							<div class="location vcard">
						      <span class="fn org">' . $seop_grs_event_location_name . '</span>,
						      <span class="adr">
						         <span class="street-address">' . $seop_grs_event_location_address_streetaddress . '</span>,
						         <span class="locality">' . $seop_grs_event_location_address_addresslocality . '</span>,
						         <span class="region">' . $seop_grs_event_location_address_addressregion . '</span>
						      </span>
						   </div>
         			</div>';

					$content = $output . $content;
				}

				// Add People, microformat
				$seop_grs_people_name_given_name = get_post_meta($post_id, 'seop_grs_people_name_given_name', true);
				if ($seop_grs_people_name_given_name != '') {
					$seop_grs_people_name_family_name = get_post_meta($post_id, 'seop_grs_people_name_family_name', true);
					$seop_grs_people_home_url         = get_post_meta($post_id, 'seop_grs_people_home_url', true);
					$seop_grs_people_locality         = get_post_meta($post_id, 'seop_grs_people_locality', true);
					$seop_grs_people_region           = get_post_meta($post_id, 'seop_grs_people_region', true);
					$seop_grs_people_title            = get_post_meta($post_id, 'seop_grs_people_title', true);
					$seop_grs_people_photo            = get_post_meta($post_id, 'seop_grs_people_photo', true);

					$output = '<div class="vcard" style="display:none">
							<span class="fn">' . $seop_grs_people_name_given_name . ' ' . $seop_grs_people_name_family_name . '</span>
						   <a href="' . $seop_grs_people_home_url . '" class="url">' . $seop_grs_people_home_url . '</a>.
						   <span class="adr">
						      <span class="locality">' . $seop_grs_people_locality . '</span>,
						      <span class="region">' . $seop_grs_people_region . '</span>
						   </span>
						   and work as an
						   <span class="title">' . $seop_grs_people_title . '</span>
						   <span class="photo">' . $seop_grs_people_photo . '</span>
         			</div>';

					$content = $output . $content;
				}

				// Add Product, NO microformat because doesn't allow multiple Offers
				$seop_grs_product_name = get_post_meta($post_id, 'seop_grs_product_name', true);
				if ($seop_grs_product_name != '') {
					$seop_grs_product_image       = get_post_meta($post_id, 'seop_grs_product_image', true);
					$seop_grs_product_description = get_post_meta($post_id, 'seop_grs_product_description', true);
					$seop_grs_product_offers      = get_post_meta($post_id, 'seop_grs_product_offers', true);

					$output = '<div itemscope="itemscope" itemtype="http://schema.org/Product" style="display:none">
							<img src="' . $seop_grs_product_image . '" title="' . $seop_grs_product_name . '" alt="' . $seop_grs_product_name . '" itemprop="image">
							
							<div itemprop="description">' . $seop_grs_product_description . '</div>';
					// Add item for each Offer
					$seop_grs_product_offers_arr = explode("\n", $seop_grs_product_offers);
					foreach ($seop_grs_product_offers_arr as $seop_grs_product_offers_item) {
						$seop_grs_product_offers_item       = trim($seop_grs_product_offers_item);
						$seop_grs_product_offers_item_arr   = explode(' ', $seop_grs_product_offers_item);
						$seop_grs_product_offers_item_price = $seop_grs_product_offers_item_arr[0];
						if (count($seop_grs_product_offers_item_arr) > 1) {
							$seop_grs_product_offers_item_currency = $seop_grs_product_offers_item_arr[1];
						} else {
							// No currency specified
							$seop_grs_product_offers_item_currency = '';
						}
						if ($seop_grs_product_offers_item_price != '') {
							$output .= '<span itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
	        					<span itemprop="price">' . $seop_grs_product_offers_item_price . '</span>
	        					<span itemprop="priceCurrency" content="' . $seop_grs_product_offers_item_currency . '"></span>
	        					</span>';
						}
					}
					$output .= '</div>';

					$content = $output . $content;
				}

				// Add Receipt, microformat
				$seop_grs_recipe_name = get_post_meta($post_id, 'seop_grs_recipe_name', true);
				if ($seop_grs_recipe_name != '') {
					$seop_grs_recipe_yield                 = get_post_meta($post_id, 'seop_grs_recipe_yield', true);
					$seop_grs_recipe_author                = get_post_meta($post_id, 'seop_grs_recipe_author', true);
					$seop_grs_recipe_photo                 = get_post_meta($post_id, 'seop_grs_recipe_photo', true);
					$seop_grs_recipe_nutrition_calories    = get_post_meta($post_id, 'seop_grs_recipe_nutrition_calories', true);
					$seop_grs_recipe_nutrition_sodium      = get_post_meta($post_id, 'seop_grs_recipe_nutrition_sodium', true);
					$seop_grs_recipe_nutrition_fat         = get_post_meta($post_id, 'seop_grs_recipe_nutrition_fat', true);
					$seop_grs_recipe_nutrition_protein     = get_post_meta($post_id, 'seop_grs_recipe_nutrition_protein', true);
					$seop_grs_recipe_nutrition_cholesterol = get_post_meta($post_id, 'seop_grs_recipe_nutrition_cholesterol', true);
					$seop_grs_recipe_total_time_minutes    = get_post_meta($post_id, 'seop_grs_recipe_total_time_minutes', true);
					$seop_grs_recipe_cook_time_minutes     = get_post_meta($post_id, 'seop_grs_recipe_cook_time_minutes', true);
					$seop_grs_recipe_prep_time_minutes     = get_post_meta($post_id, 'seop_grs_recipe_prep_time_minutes', true);
					$seop_grs_recipe_ingredient            = get_post_meta($post_id, 'seop_grs_recipe_ingredient', true);

					$output = '<div class="hrecipe" style="display:none">
						   <span class="fn">' . $seop_grs_recipe_name . '</span>
						   <img src="' . $seop_grs_recipe_photo . '" class="photo" />
						   By <span class="author">' . $seop_grs_recipe_author . '</span>
						   Prep time: <span class="preptime">' . $seop_grs_recipe_prep_time_minutes . '</span>
						   Cook time: <span class="cooktime">' . $seop_grs_recipe_cook_time_minutes . '</span>   
						   Total time: <span class="duration">' . $seop_grs_recipe_total_time_minutes . '</span>
						   Yield: <span class="yield">' . $seop_grs_recipe_yield . '</span>
						   <span class="nutrition">
						      Calories per serving: <span class="calories">' . $seop_grs_recipe_nutrition_calories . '</span>
						      Fat per serving: <span class="fat">' . $seop_grs_recipe_nutrition_fat . '</span>
						      Fat per serving: <span class="protein">' . $seop_grs_recipe_nutrition_protein . '</span>
						      Fat per serving: <span class="sodium">' . $seop_grs_recipe_nutrition_sodium . '</span>
						      Fat per serving: <span class="cholesterol">' . $seop_grs_recipe_nutrition_cholesterol . '</span>
						   </span>';
					// Add item for each Ingredient
					$seop_grs_recipe_ingredient_arr = explode("\n", $seop_grs_recipe_ingredient);
					foreach ($seop_grs_recipe_ingredient_arr as $seop_grs_recipe_ingredient_item) {
						if (trim($seop_grs_recipe_ingredient_item) != '') {
							$output .= '<span class="ingredient">' . $seop_grs_recipe_ingredient_item . '</span>';
						}
					}
					$output .= '</div>';

					$content = $output . $content;
				}
			}

			return $content;
		}

		/**
		 *
		 * Filter the POST content
		 *
		 * @param    array  $keyword_arr
		 * @param    string $content Take in care should be the Post Content to Edit
		 * @param    string $settings
		 * @param    int    $post_id
		 *
		 * @return    string
		 * @access    public
		 */
		static function filter_post_content($keyword_arr, $content, $settings, $post_id, $current_md5 = '') {

			$new_content = $content;

			// Apply Automatic Internal Links
			$new_content = self::apply_automatic_internal_links($new_content);

			// Apply External Cloaked Links
			$new_content = self::apply_external_cloaked_links($new_content);

			// Apply settings related to keyword, if keyword is specified
			if ($keyword_arr[0] != '') {
				$new_content = self::apply_biu_to_content($new_content, $keyword_arr);
			}

			// Decorates all Images Alt and Title attributes
			$new_content = SpireBuilder_HtmlStyles::decorates_images($new_content, $keyword_arr, $post_id, $settings);

			// Add of rel="nofollow" to external links
			$new_content = SpireBuilder_HtmlStyles::add_rel_nofollow_external_links($new_content, $settings);

			// Add of rel="nofollow" to Image links
			$new_content = SpireBuilder_HtmlStyles::add_rel_nofollow_image_links($new_content, $settings);

			// Update the Post Content with the filtered Data
			global $wpdb;
			$query = $wpdb->prepare("update $wpdb->posts set post_content=%s where ID=%d", $new_content, $post_id);
			$wpdb->query($query); // Query instead of wp_update_post to avoid call the hook that cause recursive loop

			// Update filtered content Md5, since if some of this changes, could change the filtered content too
			if ($current_md5 == '') {
				SpireBuilder_Central::get_content_cache_current_md5($post_id, array(), $keyword_arr, $content);
			}
			update_post_meta($post_id, SpireBuilder_Central::$cache_md5_for_filter_content, $current_md5);
			update_post_meta($post_id, SpireBuilder_Central::$cache_md5_filter_content_last_mod_time, time());

			return $new_content;
		}
	}
}