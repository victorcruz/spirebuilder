<?php
if (!class_exists('SpireBuilder_WPPosts')) {
	/**
	 * Class SpireBuilder_WPPosts
	 */
	class SpireBuilder_WPPosts {

		/**
		 * MetaKey setting: use_for_meta_keyword
		 * Values can be: seopressor_keywords, tags, categories
		 *
		 * @static
		 * @var string
		 */
		static $spirebuilder_LOM_metadata = '_spirebuilder_LOM';

		/**
		 * MetaDescription setting: allow_meta_description
		 *
		 * @static
		 * @var string
		 */
		static $spirebuilder_LOM_content_metadata = '_spirebuilder_LOM_content';

		/**
		 * MetaKey setting: use_for_meta_keyword
		 * Values can be: seopressor_keywords, tags, categories
		 *
		 * @static
		 * @var string
		 */
		static $spire_builder_settings_post_metadata = '_spire_builder_settings_post';

		/**
		 * Update MetaKey setting: allow_keyword_overriding_in_sentences
		 *
		 * @param    int    $post_id
		 * @param    string $value Can be '1' or '0'
		 *
		 * @return    bool    False on failure, true if success.
		 */
		static function update_allow_spire_builder_editor($post_id, $value) {
			return update_post_meta($post_id, self::$allow_spire_builder_editor_metadata, $value);
		}

		/**
		 * Update MetaKey setting: use_for_meta_keyword
		 *
		 * @param    int    $post_id
		 * @param    string $value values can be: seopressor_keywords, tags, categories
		 *
		 * @return    bool    False on failure, true if success.
		 */
		static function update_spirebuilder_LOM($post_id, $value) {
			return update_post_meta($post_id, self::$spirebuilder_LOM_metadata, $value);
		}

		/**
		 * Update MetaKey setting: use_for_meta_keyword
		 *
		 * @param    int    $post_id
		 * @param    string $value values can be: seopressor_keywords, tags, categories
		 *
		 * @return    bool    False on failure, true if success.
		 */
		static function update_spirebuilder_content_LOM($post_id, $value) {
			return update_post_meta($post_id, self::$spirebuilder_LOM_content_metadata, $value);
		}

		/**
		 * Get MetaKey setting: use_for_meta_keyword
		 *
		 * @param    int $post_id
		 *
		 * @return    string    values can be: seopressor_keywords, tags, categories
		 */
		static function get_spirebuilder_LOM($post_id) {
			$metavalue = get_post_meta($post_id, self::$spirebuilder_LOM_metadata, true);

			return $metavalue;
		}

		/**
		 * Get MetaKey setting: use_for_meta_keyword
		 *
		 * @param    int $post_id
		 *
		 * @return    string    values can be: seopressor_keywords, tags, categories
		 */
		static function get_spirebuilder_LOM_content($post_id) {
			$metavalue = get_post_meta($post_id, self::$spirebuilder_LOM_content_metadata, true);

			return $metavalue;
		}

		/**
		 * Get allow_spire_builder_editor setting: $allow_spire_builder_editor_metadata
		 *
		 * @param    int $post_id
		 *
		 * @return    string
		 */
		static function get_allow_spire_builder_editor($post_id) {
			$settings = self::get_spire_builder_settings_post($post_id);

			return $settings['allow_spire_builder_editor'];
		}

		/**
		 * Get allow_spire_builder_editor setting: $allow_spire_builder_editor_metadata
		 *
		 * @param    int $post_id
		 *
		 * @return    string
		 */
		static function get_content_placement($post_id) {
			$settings = self::get_spire_builder_settings_post($post_id);

			if (isset($settings['content_placement'])) {
				return $settings['content_placement'];
			} else {
				return '';
			}
		}

		static function get_widgets_types_inside_LOM($post_id) {
			$LOM = SpireBuilder_WPPosts::get_spirebuilder_LOM($post_id);

			$explode_LOM_by_type = explode(',"type":"', $LOM);

			$result = array();
			foreach ($explode_LOM_by_type as $explode_value) {
				$tmp = explode('"', $explode_value);
				if (isset($tmp[0]) && $tmp[0] != '' && $tmp[0] != '{' && $tmp[0] != 'root') {
					$result[$tmp[0]] = $tmp[0];
				}
			}

			return $result;
		}

		/**
		 * Get allow_spire_builder_editor setting: $allow_spire_builder_editor_metadata
		 *
		 * @param    int $post_id
		 *
		 * @return    string
		 */
		static function get_toolbar_layout($post_id) {
			$settings = get_spire_builder_settings_post($post_id);

			return $settings['toolbar_layout'];
		}

		/**
		 * Get content_placement setting: $content_placement_metadata
		 *
		 * @param    int $post_id
		 *
		 * @return    string
		 */
		static function get_spire_builder_settings_post($post_id) {
			$metavalue = get_post_meta($post_id, self::$spire_builder_settings_post_metadata, true);

			return $metavalue;
		}

		/**
		 * Update MetaKey setting: use_for_meta_keyword
		 *
		 * @param    int    $post_id
		 * @param    string $value values can be: seopressor_keywords, tags, categories
		 *
		 * @return    bool    False on failure, true if success.
		 */
		static function update_spire_builder_settings_post($post_id, $value) {
			return update_post_meta($post_id, self::$spire_builder_settings_post_metadata, $value);
		}
	}
}