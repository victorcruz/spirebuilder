<?php
if (!class_exists('SpireBuilder_Settings')) {
	/**
	 * Class SpireBuilder_Settings
	 *
	 * All Settings the Spire Builder
	 *
	 */
	class SpireBuilder_Settings {
		/**
		 * The name for plugin options in the DB
		 *
		 * @var string
		 */
		static $db_option = 'SpireBuilder_Options';

		/**
		 * Get widgets
		 *
		 * Use for bulk processing of posts with invalid cache
		 *
		 * @static
		 * @return array
		 * @access public
		 */
		static public function get_spirebuilder_widgets() {
			$options = self::get_options();

			return $options['spirebuilder_widgets'];
		}

		/**
		 * Get setting value: global allow_spire_builder_editor
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_allow_spire_builder_editor() {
			$options = self::get_options();

			return $options['settings']['allow_spire_builder_editor'];
		}

		/**
		 * Get setting value: global allow_spire_builder_editor
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_builder_content_types() {
			$options = self::get_options();

			return $options['settings']['builder_content_types'];
		}

		/**
		 * Get setting values
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_settings() {
			$options = self::get_options();

			return $options['settings'];
		}

		/**
		 * Get setting values
		 *
		 * @static
		 * @return int 1 set spire builder editor by default, 0 not
		 * @access public
		 */
		static public function get_requirements() {
			$options = self::get_options();

			return $options['requirements'];
		}

		/**
		 * Get setting value: locale
		 *
		 * @static
		 * @return bool
		 * @access public
		 */
		static public function get_locale() {
			$options = self::get_options();

			return $options['locale'];
		}

		/**
		 * Update widgets status into db
		 *
		 * @param array $list_of_widgets_modified_status
		 *
		 * @return array
		 */
		static function update_spirebuilder_widgets_status($list_of_widgets_modified_status) {
			// Get options saved in db.
			$options = self::get_options();
			// Get list the widgets the db.
			$widgets_active_and_disable = $options['spirebuilder_widgets'];

			foreach ($widgets_active_and_disable as &$widget) {
				if (in_array($widget['id'], $list_of_widgets_modified_status) && ($widget['status'] == 'active' || $widget['status'] == 'disable')) {
					$widget['status'] = 'active';
				} elseif ($widget['status'] == 'active' || $widget['status'] == 'disable') {
					$widget['status'] = 'disable';
				}
			}
			$options['spirebuilder_widgets'] = $widgets_active_and_disable;

			return self::update_options($options);
		}

		/**
		 * Update widgets status into db
		 *
		 * @param array builder_content_types
		 *
		 * @return array
		 */
		static function update_builder_content_types($lista) {
			// Get options saved in db.
			$options = self::get_options();
			// Get list the widgets the db.
			$settings_builder_content_types = $options['settings']['builder_content_types'];

			foreach ($settings_builder_content_types as &$builder_content_type) {
				if (in_array($builder_content_type['id'], $lista)) {
					$builder_content_type['status'] = 'active';
				} else {
					$builder_content_type['status'] = 'disable';
				}
			}
			$options['settings']['builder_content_types'] = $settings_builder_content_types;

			return self::update_options($options);
		}

		/**
		 * Updates the General Settings of Plugin
		 *
		 * @param array $options
		 *
		 * @return array
		 * @access public
		 */
		static public function update_options($options) {
			// Save Class variable
			SpireBuilder::$options = $options;

			return update_option(self::$db_option, $options);
		}

		/**
		 * Return the General Settings of Plugin, and set them to default values if they are empty
		 *
		 * @return array general options of plugin
		 * @access public
		 */
		static function get_options() {
			// If isn't empty, return class variable
			if (SpireBuilder::$options) {
				return SpireBuilder::$options;
			}

			// default values
			$options = array( // Global Settings
				'settings' => array('allow_spire_builder_editor' => 1, // set spire builder editor by default
									'content_placement' => 'replace', // SpireBuilder placement relative the WordPress content. Can be one of: replace, before, after. Default value: replace.
									'fixed_toolbar' => 1, // 1- The toolbar will become a floated bar when the scrolling hides it. 0- If not.
									'edit_modal' => 1, // 1- Launch the Edit dialog right after of widget addition. 0- If not.
									'available_widgets_all' => 1, //
									'toolbar_layout' => 'text_after_icons', // Toolbar layout styles. Can be one of: text_below_icons, text_after_icons, icons_only, text_only. Default value: text_after_icons.
									'toolbar_icon_size' => 'normal', // Toolbar icon styles. Can be one of: mini, normal, large. Default value: normal.
									'builder_content_types' => array('post' => array('id' => 'post', 'name' => 'Post', 'status' => 'active'), 'page' => array('id' => 'page', 'name' => 'Page', 'status' => 'active')), //
									'builder_content_types_not_shown' => array('nav_menu_item' => 'nav_menu_item', 'revision' => 'revision', 'attachment' => 'attachment'), // Content types not shown
									'edit_modal_exclusions' => array('accordion', 'columns', 'container', 'tabs', 'wp-content'), // Content types not shown
									'widget_content_max_chars' => 'all', // Maximum characters in widget content. Can be one of: all, restrict. Default value: 'all'.
									'widget_content_max_chars_restricted_to' => 100, // Restrict the widget content to show only this amount of characters. Default value: 100.
				), // end settings
				// Requirements
				'requirements' => array('wp_version' => '3.5.2', 'php_version_min' => '5.2.9', 'php_version_tested' => '5.4.1'), // end requirements
				// Widgets
				'spirebuilder_widgets' => array(), //
				'locale' => '', // '' is default => English
				'templates' => array(), //
			);

			// get saved options
			$saved = get_option(self::$db_option);

			// assign them
			if (!empty($saved)) {
				foreach ($saved as $key => $option) {
					$options[$key] = $option;
				}
			}

			// update the options if necessary
			if ($saved != $options) {
				update_option(self::$db_option, $options);
			}

			// Save class variable
			SpireBuilder::$options = $options;

			//return the options
			return $options;
		}
	}
}
