<?php
/**
 * Create/Update database structure
 *
 * Called in activation hook
 */

// Define plugin tables prefix
global $wpdb;
$prefix = $wpdb->prefix . SpireBuilder::$db_prefix;