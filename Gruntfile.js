module.exports = function (grunt) {
	/*
	 * Project configuration.
	 */
	// TODO: D: que se cree /<plugin-name>-build un nivel superior para que el phpstorm no joda.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		mkdir: {
			build: {
				options: {
					create: ['build/<%= pkg.name %>'],
					mode  : '0775'
				}
			}
		},

		copy: {
			release: {
				files: [
					{
						src : [
							'**',
							'!node_modules/**',
							'!Gruntfile*',
							'!.*',
							'!package.json',

							'!**/theme/**/css/*.css',
							'**/theme/**/css/*.min.css',

							'!**/theme/**/scripts/**/*.{js,ts,d.ts,js.map}',
							'**/theme/**/scripts/{j,t}slib/**',
							'**/theme/**/scripts/**/*.min.js'
						],
						dest: '<%= mkdir.build.options.create[0] %>/'
					}
				]
			},
			dev    : {
				files: [
					{
						src : [
							'**',
							'!node_modules/**',
							'!.*'
						],
						dest: '<%= mkdir.build.options.create[0] %>/'
					}
				]
			}
		},

		compress: {
			build: {
				options: {
					archive: '<%= pkg.name %>.zip',
					level  : 6
				},
				expand : true,
				cwd    : '<%= mkdir.build.options.create[0] %>/',
				src    : ['**/*'],
				dest   : ''
			}
		},

		clean: {
			build: ['build/']
		}
	});

	/*
	 * Load plugins.
	 */
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-mkdir');

	/*
	 * Custom tasks.
	 */
	grunt.registerTask('release', [
		'mkdir:build',
		'copy:release',
		'compress:build',
		'clean:build']);

	grunt.registerTask('dev', [
		'mkdir:build',
		'copy:dev',
		'compress:build',
		'clean:build']);

	/*
	 * Default tasks.
	 */
	grunt.registerTask('default', ['release']);
};